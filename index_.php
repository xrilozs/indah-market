<?php
error_reporting(0);
if($_GET['status']=="berhasil"){
    echo '<script>alert("Data Berhasil Disimpan")</script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Latihan lap prod CRUD</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">

    <link rel="stylesheet" href="cssbaru/stylebaru.css"/>
    <script src="js/jquery-1.3.2.js"></script>
    <script>
        $(function(){
            $('#navigation a').stop().animate({'marginLeft':'-85px'},1000);
            $('#navigation li').hover(
                function(){
                    $('a', $(this)).stop().animate({'marginLeft':'0px'},200);
                },
                function(){
                    $('a',$(this)).stop().animate({'marginLeft': '-85px'},200);
                }
            );
        });
    </script>
</head>
<body>

<div class="container">

    <ul id="navigation">

        <li class="home"><a href="index.php?get=home" title="Home"></a></li>
        <li class="about"><a href="index.php?get=input_data" title="Input data"></a></li>
        <li class="search"><a href="index.php?get=statistik" title="Statistik"></a></li>
        <li class="search"><a href="index.php?get=ret_input_data" title="Input Retur"></a></li>
        <li class="rss"><a href="#" title="Rss Feed"></a></li>
        <li class="contact"><a href="#" title="Contact"></a></li>
    </ul>

    <div class="navbar navbar-inner">
        <div class="brand">Lap. Produksi dan Returan</div>
    </div>
    <?php
        $get=$_GET['get'];
        if($get == 'home'){
            include 'home.php';
        }
        elseif ($get == 'input_data'){
              include 'input_data.php';
              }
        elseif ($get == 'statistik'){
            include 'proses/diagram.php';
        }
        elseif ($get == 'ret_input_data'){
            include 'ret_input_data.php';
        }

    ?>

</div>

</body>
</html>

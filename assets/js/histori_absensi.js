let user_login = get_login(),
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'id',
    selected_date = format_to_db_date(new Date())

$('.selectpicker').selectpicker();

$(document).ready(function(){
    $("#filter_tanggal").val(selected_date)
    if(user_login.type != 'super_admin' && user_login.type != 'hut_put'){
        $('#histori_absensi_update_section').hide()
    }
    $(".monthly-datepicker").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    //data table
    let histori_absensi_table = $('#histori_absensi_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_histori_absensi',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let date = selected_date

            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            table_dir = d.order[0].dir
            switch (column) {
                case 2:
                    newObj.order_by = 'jumlah_akumulasi_hadir_biasa'
                    break
                case 3:
                    newObj.order_by = 'jumlah_akumulasi_hari_libur'
                    break
                case 4:
                    newObj.order_by = 'jumlah_akumulasi_jam_lembur'
                    break
                case 5:
                    newObj.order_by = 'jumlah_akumulasi_telat'
                    break
                case 6:
                    newObj.order_by = 'jumlah_akumulasi_absen'
                    break
                case 7:
                    newObj.order_by = 'tanggal_periode'
                    break
                default:
                    newObj.order_by = 'id'
                    break
            }
            table_order_by = newObj.order_by
            rerender_print_link()

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "pegawai_nip",
                orderable: true
            },
            { 
                data: "pegawai_name",
                orderable: true
            },
            { 
                data: "jumlah_akumulasi_hadir_biasa",
                orderable: true
            },
            { 
                data: "jumlah_akumulasi_hari_libur",
                orderable: true
            },
            { 
                data: "jumlah_akumulasi_jam_lembur",
                orderable: true
            },
            { 
                data: "jumlah_akumulasi_telat",
                orderable: true
            },
            { 
                data: "jumlah_akumulasi_absen",
                orderable: true
            },
            {
                data: "tanggal_periode",
                orderable: true,
                render: function (data, type, row, meta) {
                    return format_from_db_date(data)
                }
            }
        ]
    });

    $('#filter_tanggal').change(function(){
        selected_date = $(this).val()
        histori_absensi_table.draw()
    })
})

$('#update-absensi-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-absensi-button').html(loading_button)
    $('#update-absensi-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_histori_absensi_process`,
        type: 'POST',
        data: JSON.stringify({search: table_search, date: selected_date}),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-absensi-button').html('IYA')
            $('#update-absensi-button').prop('disabled', false)
            $('#update-absensi-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Update Akumulasi selesai!</a>.</div>`
            $('#update-absensi-button').html('IYA')
            $('#update-absensi-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function format_to_db_date(date){
    let mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}`
    return new_date
}

function format_from_db_date(date){
    let months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    let date_arr = date.split('-')
    let new_date = `${months[parseInt(date_arr[1])-1]} ${date_arr[0]}`
    return new_date
}

function rerender_print_link(){
    $('#histori_absensi_print').prop('href', `backend/get_histori_absensi_and_print?sort=${table_dir}&order_by=${table_order_by}&search=${table_search}&date=${selected_date}`)
}
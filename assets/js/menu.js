$(document).ready(function(){
    console.log("PAGE: ", CURRENT_PAGE)
    const user_login = get_login()
    const proret_pages = ['dashboard', 'produksi/produksi_harian', 'produksi/returan', 'produksi/report_produksi_bulanan', 'produksi/report_produksi_harian', 'produksi/report_returan_bulanan', 'produksi/report_returan_harian', 'piutang/customer', 'spl/list', 'spl/monthly_report', 'delivery/data_barang', 'myprofile']
    const hut_put_pages = ['dashboard', 'absensi/', 'myprofile']
    const delivery_pages = ['dashboard', 'delivery/', 'myprofile']
    const adm_prod_pages = ['dashboard', 'delivery/setor_barang_jadi', 'delivery/data_barang', 'myprofile']
    
    if(user_login.type == 'proret'){
        $('#proret-menu').css("display", "block")
        $('#spl-menu').css("display", "block")
        $('#delivery-menu').css("display", "block")
        $('.proret-item').css("display", "block")

        const index = proret_pages.findIndex(page => {
            return CURRENT_PAGE.includes(page)
        })
        if(index < 0){
            window.location.href = `${get_web_url()}/dashboard`
        }
    }else if(user_login.type == 'hut_put'){
        $('#absensi-menu').css("display", "block")

        const index = hut_put_pages.findIndex(page => {
            return CURRENT_PAGE.includes(page)
        })
        if(index < 0){
            window.location.href = `${get_web_url()}/dashboard`
        }
    }else if(user_login.type == 'delivery'){
        $('#delivery-menu').css("display", "block")
        $('.delivery-menu-item').css("display", "block")

        const index = delivery_pages.findIndex(page => {
            return CURRENT_PAGE.includes(page)
        })
        if(index < 0){
            window.location.href = `${get_web_url()}/dashboard`
        }
    }else if(user_login.type == 'adm_prod'){
        $('#delivery-menu').css("display", "block")
        $('.admprod-item').css("display", "block")

        const index = adm_prod_pages.findIndex(page => {
            return CURRENT_PAGE.includes(page)
        })
        if(index < 0){
            window.location.href = `${get_web_url()}/dashboard`
        }
    }else if(user_login.type == 'super_admin'){
        $('.hidden-menu').css("display", "block")
        $('.proret-menu-item').css("display", "block")
        $('.delivery-menu-item').css("display", "block")
        $('.spl-menu-item').css("display", "block")
    }
})

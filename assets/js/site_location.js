let SITE_LOCATION

$(document).ready(function(){
    getSiteLocation()

    $('#site-location-form').submit(function(e){
        e.preventDefault()

        let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`
        $('#site-location-button').html(loading_button)
        $('#site-location-button').prop('disabled', true);

        let $form = $(this),
            request = {
                lat: $form.find("input[name='lat']").val(),
                long: $form.find("input[name='long']").val(),
                address: $form.find("textarea[name='address']").val(),
            }

        let method = 'POST'
        if(SITE_LOCATION){
            request.id = SITE_LOCATION.id
            method = "PUT"
        }

        $.ajax({
            async: true,
            url: `site-location`,
            type: method,
            data: JSON.stringify(request),
            error: function(res) {
                console.log("ERROR: ", res)
                let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Update Site Location gagal dilakukan!</a>.</div>`

                $('#site-location-button').html('Submit')
                $('#site-location-button').prop('disabled', false)
                $('#site-location-warning').html(message)
            },
            success: function(res) {
                console.log("RESPONSE: ", res)
                let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Site Location berhasil diupdate!</a>.</div>`
                $('#site-location-button').prop('disabled', false)
                $('#site-location-button').html('Submit')
                $('#site-location-warning').html(message)
            }
        });
    })
})

function getSiteLocation(){
    $.ajax({
        async: true,
        url: `site-location`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
        },
        success: function(res) {
            if(res.data){
                SITE_LOCATION = res.data
                renderFormSiteLocation(res.data)
            }
        }
    });
}

function renderFormSiteLocation(data){
    let $form = $('#site-location-form')
    $form.find("input[name='lat']").val(data.lat)
    $form.find("input[name='long']").val(data.long)
    $form.find("textarea[name='address']").val(data.address)
}
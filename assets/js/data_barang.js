let user_login = get_login(),
    selected_date = format_to_db_date(new Date()),
    delivery_id,
    SPL,
    DELIVERY

$(document).ready(function(){
    $("#filter_tanggal").val(selected_date)
    $(".monthly-datepicker").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });

    //data table
    let data_barang_table = $('#databarang_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_delivery_montlhy',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let date = $('#filter_tanggal').val()

            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            switch (column) {
                case 2:
                    newObj.order_by = 'kodeCust'
                    break
                case 3:
                    newObj.order_by = 'tglSetorBarang1'
                    break
                case 4:
                    newObj.order_by = 'jlhYgDisetor1'
                    break
                case 5:
                    newObj.order_by = 'tglSetorBarang2'
                    break
                case 6:
                    newObj.order_by = 'jlhYgDisetor2'
                    break
                case 7:
                    newObj.order_by = 'received'
                    break
                case 8:
                    newObj.order_by = 'delivered'
                    break
                case 9:
                    newObj.order_by = 'returned'
                    break
                case 10:
                    newObj.order_by = 'ret_desc'
                    break
                default:
                    newObj.order_by = 'datecreated'
                    break
            }

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "spl_no",
                orderable: false,
                render: function (data, type, row, meta) {
                    let button = `<button class="btn btn-link detail-spl-toggle" data-id="${row.spl_id}" data-toggle="modal" data-target="#detail-spl-modal">
                        ${data}
                    </button>`

                    return button;
                }
            },
            { 
                data: "ukuran",
                orderable: false
            },
            { 
                data: "kodeCust",
                orderable: true
            },
            { 
                data: "tglSetorBarang1",
                orderable: true
            },
            { 
                data: "jlhYgDisetor1",
                orderable: true
            },
            { 
                data: "tglSetorBarang2",
                orderable: true
            },
            { 
                data: "jlhYgDisetor2",
                orderable: true
            },
            { 
                data: "received",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = `<span class="label label-default">NO</span>`
                    if(parseInt(data)){
                        label = `<span class="label label-success">YES</span>`
                    }

                    return label;
                }
            },
            {
                data: "delivered",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = `<span class="label label-default">NO</span>`
                    if(parseInt(data)){
                        label = `<span class="label label-success">YES</span>`
                    }

                    return label;
                }
            },
            {
                data: "returned",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = `<span class="label label-default">NO</span>`
                    if(parseInt(data)){
                        label = `<span class="label label-success">YES</span>`
                    }

                    return label;
                }
            },
            {
                data: "ret_desc",
                orderable: true,
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    return data ? data : "-"
                }
            },
            { 
                data: "datecreated",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    let button = '-'
                    if(user_login.type == 'super_admin'){
                        button = `<button class="btn btn-danger delete-delivery-toggle" data-id="${data}" data-toggle="modal" data-target="#delete-delivery-modal">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button> <button class="btn btn-success update-delivery-toggle" data-id="${data}" data-toggle="modal" data-target="#update-delivery-modal">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </button>`
                    }
                    
                    return button
                },
                orderable: false
            }
        ]
    });

    $("body").delegate(".detail-spl-toggle", "click", function(e) {
        let spl_id = $(this).data("id")

        $.ajax({
            async: true,
            url: `backend/get_spl_detail?id=${spl_id}`,
            type: 'GET',
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                $('#detail-spl-modal').toggle("modal")
            },
            success: function(res) {
                const response = JSON.parse(res)
                console.log("RESPONSE SUCCESS: ", response)
                // SPL = response.spl
                // DELIVERY = response.delivery
                if(response.info == 'success'){
                    fill_spl_form(response.data)
                }else{
                    $('#detail-spl-modal').toggle("modal")
                }
                // $('#search-delivery-button').prop('disabled', false)
                // $('#search-delivery-button').html('Cari')
            }
        });
    })

    $("body").delegate(".delete-delivery-toggle", "click", function(e) {
        delivery_id = $(this).data("id")
    })

    $("body").delegate(".update-delivery-toggle", "click", function(e) {
        let id = $(this).data("id")
        $.ajax({
            async: true,
            url: `backend/get_delivery_by_spl_id?id=${id}`,
            type: 'GET',
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                $('#update-delivery-modal').toggle("modal")
            },
            success: function(res) {
                console.log("RESPONSE SUCCESS: ", res)
                let response = res.data
                fill_delivery_form(response)
            }
        });
    })

    $('#filter_tanggal').change(function(){
        data_barang_table.draw()
    })

    $('#delete-delivery-button').click(function(){
        $.ajax({
            async: true,
            url: `backend/delete_delivery?id=${delivery_id}`,
            type: 'DELETE',
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal</strong> menghapus data barang!</a>.</div>`
                $('#delete-delivery-warning').html(message)
            },
            success: function(res) {
                let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses</strong> menghapus data barang!</a>.</div>`
                $('#delete-delivery-button').html('IYA')
                $('#delete-delivery-warning').html(message)
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        });
    })

    $('#update-delivery-form').submit(function(e){
        e.preventDefault()

        let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

        $('#update-delivery-button').html(loading_button)
        $('#update-delivery-button').prop('disabled', true);

        let $form = $(this),
            request = DELIVERY

        request.jlhYgDisetor1 = $form.find("input[name='jumlah_setor1']").val()
        request.jlhYgDisetor2 = $form.find("input[name='jumlah_setor2']").val()
        request.ret_desc = $form.find("input[name='ret_desc']").val()
        if(user_login.type == 'super_admin'){
            request.received = $form.find("input[name='received']:checked").val()
            request.delivered = $form.find("input[name='delivered']:checked").val()
            request.returned = $form.find("input[name='returned']:checked").val()
        }
        $.ajax({
            async: true,
            url: `backend/create_update_delivery`,
            type: 'POST',
            data: JSON.stringify(request),
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal</strong> mengubah data barang!</a>.</div>`
                $('#update-delivery-warning').html(message)
                $('#update-delivery-button').html('Simpan')
            },
            success: function(res) {
                let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses</strong> mengubah data barang!</a>.</div>`
                $('#update-delivery-button').html('Simpan')
                $('#update-delivery-warning').html(message)
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        });
    })
})

function format_to_db_date(date){
    let mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}`
    return new_date
}

function fill_spl_form(data){
    let form = $(`#detail-spl-form`)
    form.find( "input[name='customer_name']" ).val(data.customer_name)
    form.find( "input[name='lebar_bahan']" ).val(data.lebar_bahan)
    form.find( "input[name='tebal_bahan']" ).val(data.tebal_bahan)
    form.find( "input[name='ukuran_potong']" ).val(data.ukuran_potong)
    form.find( "input[name='satuan']" ).val(data.satuan)
    form.find( "textarea[name='keterangan']" ).val(data.keterangan)
    form.find( "input[name='jumlah_order']" ).val(data.jumlah_order)
    form.find( "input[name='harga_satuan']" ).val(data.harga_satuan)
    form.find( "input[name='komposisi']" ).val(data.komposisi)
    form.find( "input[name='jumlah_roll']" ).val(data.jumlah_roll)
    form.find( "input[name='hasil_rumus']" ).val(data.hasil_rumus)
}

function fill_delivery_form(data){
    SPL = data.spl
    DELIVERY = data.delivery

    let form = $(`#update-delivery-form`)
    form.find( "input[name='no_spl']" ).val(SPL.no_spl)
    form.find( "input[name='customer']" ).val(SPL.customer_name)
    form.find( "input[name='ukuran']" ).val(SPL.ukuran_potong)
    form.find( "input[name='hasil_rumus']" ).val(SPL.hasil_rumus)
    form.find( "input[name='tanggal_setor_barang1']" ).val(DELIVERY.tglSetorBarang1)
    form.find( "input[name='tanggal_setor_barang2']" ).val(DELIVERY.tglSetorBarang2)
    form.find( "input[name='jumlah_setor1']" ).val(DELIVERY.jlhYgDisetor1)
    form.find( "input[name='jumlah_setor2']" ).val(DELIVERY.jlhYgDisetor2)
    form.find( "input[name='ret_desc']" ).val(DELIVERY.ret_desc)

    if(user_login.type=='super_admin'){
        $(".delivery-status").css("display", "block")

        if(parseInt(DELIVERY.received)){
            $('#received-yes').prop("checked", true)
            $('#received-no').prop("checked", false)
        }else{
            $('#received-yes').prop("checked", false)
            $('#received-no').prop("checked", true)
        }

        if(parseInt(DELIVERY.delivered)){
            $('#delivered-yes').prop("checked", true)
            $('#delivered-no').prop("checked", false)
        }else{
            $('#delivered-yes').prop("checked", false)
            $('#delivered-no').prop("checked", true)
        }

        if(parseInt(DELIVERY.returned)){
            $('#returned-yes').prop("checked", true)
            $('#returned-no').prop("checked", false)
        }else{
            $('#returned-yes').prop("checked", false)
            $('#returned-no').prop("checked", true)
        }
    }
}
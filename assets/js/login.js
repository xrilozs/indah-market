$(".submit-btn").click(function(e){
  e.preventDefault();
  var form_id           = $(this).attr("form-id");
  var form              = $("#"+form_id);
  var submit_url        = form.attr('action');
  var refresh_url       = form.attr('refresh');
  var datastring        = form.serialize();
  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.info=="success"){
        $("#warning_message").html(result.message);
        localStorage.setItem("user-token", result.token)
        localStorage.setItem("user-refresh-token", result.refresh_token)
        window.location.href = 'dashboard'
      }
      else if(result.info=="failed"){
        $("#warning_message").html(result.message);
        // form[0].reset();
        // location.reload();
      }
    },
    dataType: "json"
  });
});
let office_hour_id = null,
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'id',
    URL = 'office-hour'

$(document).ready(function(){
    //Timepicker
    $('.timepicker').timepicker({
        showMeridian: false
    })

    //data table
    let office_hour_table = $('#office_hour_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: `${URL}/web`,
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.sort = d.order[0].dir
            table_dir = d.order[0].dir
            switch (column) {
                case 0:
                    newObj.order_by = 'shift'
                    break
                case 1:
                    newObj.order_by = 'start_hour'
                    break
                case 2:
                    newObj.order_by = 'end_hour'
                    break
                case 3:
                    newObj.order_by = 'start_hour_saturday'
                    break
                case 4:
                    newObj.order_by = 'end_hour_saturday'
                    break
                case 5:
                    newObj.order_by = 'is_active'
                    break
                default:
                    newObj.order_by = 'shift'
                    break
            }
            table_order_by = newObj.order_by

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "shift",
                orderable: true
            },
            { 
                data: "start_hour",
                orderable: true
            },
            { 
                data: "end_hour",
                orderable: true
            },
            { 
                data: "start_hour_saturday",
                orderable: true
            },
            { 
                data: "end_hour_saturday",
                orderable: true
            },
            { 
                data: "is_active",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(parseInt(data)){
                        label = `<span class="label label-success">Aktif</span>`
                    }else{
                        label = `<span class="label label-danger">Non-aktif</span>`
                    }
                    return label
                }
            },
            {
                data: "id",
                className: "dt-body-center",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = `<button class="btn btn-success update-office-hour-toggle" data-id="${data}" data-toggle="modal" data-target="#update-office-hour-modal">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Ubah
                        </button>
                        &nbsp;&nbsp;`
                    if(parseInt(row.is_active)){
                        button += `<button class="btn btn-danger inactive-office-hour-toggle" data-id="${data}" data-toggle="modal" data-target="#inactive-office-hour-modal">
                            <i class="fa fa-ban" aria-hidden="true"></i> Non-aktif
                        </button>`
                    }else{
                        button += `<button class="btn btn-primary active-office-hour-toggle" data-id="${data}" data-toggle="modal" data-target="#active-office-hour-modal">
                            <i class="fa fa-check" aria-hidden="true"></i> Aktif
                        </button>`
                    }
                    
                    return button
                },
                orderable: false
            }
        ]
    });
})

$("body").delegate(".update-office-hour-toggle", "click", function(e) {
    clear_office_hour_form()
    office_hour_id = $(this).data('id')
    console.log("ID:", office_hour_id)
    $('#update-office-hour-toggle').prop('disabled', true)
    $.ajax({
        async: true,
        url: `${URL}/web-detail/${office_hour_id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
            $('#update-office-hour-toggle').prop('disabled', false)
        },
        success: function(res) {
            console.log("RESPONSE SUCCESS: ", res)
            fill_office_hour_form(res.data)
            $('#update-office-hour-toggle').prop('disabled', false)
        }
    });
})

$("body").delegate(".active-office-hour-toggle", "click", function(e) {
    office_hour_id = $(this).data('id')
})

$("body").delegate(".inactive-office-hour-toggle", "click", function(e) {
    office_hour_id = $(this).data('id')
})

$('#create-office-hour-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-office-hour-button').html(loading_button)
    $('#create-office-hour-button').prop('disabled', true);

    let $form = $('#create-office-hour-form'),
        request = {
            shift: $('#shift').find(":selected").val(),
            start_hour: $form.find("input[name='start_hour']").val(),
            end_hour: $form.find("input[name='end_hour']").val(),
            start_hour_saturday: $form.find("input[name='start_hour_saturday']").val(),
            end_hour_saturday: $form.find("input[name='end_hour_saturday']").val(),
        }


    $.ajax({
        async: true,
        url: `${URL}`,
        type: 'POST',
        data: JSON.stringify(request),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Office Hour gagal ditambah!</a>.</div>`
            $('#create-office-hour-button').html('Create Office Hour')
            $('#create-office-hour-button').prop('disabled', false)
            $('#create-office-hour-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Office Hour telah ditambah!</a>.</div>`
            $('#create-office-hour-button').html('Create Office Hour')
            $('#create-office-hour-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#update-office-hour-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-office-hour-button').html(loading_button)
    $('#update-office-hour-button').prop('disabled', true);

    let $form = $('#update-office-hour-form'),
        request = {
            id: office_hour_id,
            shift: $('#shift').find(":selected").val(),
            start_hour: $form.find("input[name='start_hour']").val(),
            end_hour: $form.find("input[name='end_hour']").val(),
            start_hour_saturday: $form.find("input[name='start_hour_saturday']").val(),
            end_hour_saturday: $form.find("input[name='end_hour_saturday']").val(),
        }

    $.ajax({
        async: true,
        url: `${URL}`,
        type: 'PUT',
        data: JSON.stringify(request),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-office-hour-button').html('Update')
            $('#update-office-hour-button').prop('disabled', false)
            $('#update-office-hour-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Office Hour berhasil diubah!</a>.</div>`
            $('#update-office-hour-button').html('Update')
            $('#update-office-hour-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});


$('#active-office-hour-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#active-office-hour-button').html(loading_button)
    $('#active-office-hour-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `${URL}/enable/${office_hour_id}`,
        type: 'PUT',
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#active-office-hour-button').html('IYA')
            $('#active-office-hour-button').prop('disabled', false)
            $('#active-office-hour-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Office Hour berhasil diaktifkan!</a>.</div>`
            $('#active-office-hour-button').html('IYA')
            $('#active-office-hour-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#inactive-office-hour-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#inactive-office-hour-button').html(loading_button)
    $('#inactive-office-hour-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `${URL}/disable/${office_hour_id}`,
        type: 'PUT',
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#inactive-office-hour-button').html('IYA')
            $('#inactive-office-hour-button').prop('disabled', false)
            $('#inactive-office-hour-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Office Hour berhasil dinonaktifkan!</a>.</div>`
            $('#inactive-office-hour-button').html('IYA')
            $('#inactive-office-hour-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function fill_office_hour_form(office_hour){
    let form = $(`#update-office-hour-form`)
    $('#shift').val(office_hour.shift).change()
    $('#start_hour').timepicker('setTime', office_hour.start_hour)
    $('#end_hour').timepicker('setTime', office_hour.end_hour)
    $('#start_hour_saturday').timepicker('setTime', office_hour.start_hour_saturday)
    $('#end_hour_saturday').timepicker('setTime', office_hour.end_hour_saturday)
}
    
function clear_office_hour_form(){
    let form = $(`#update-office-hour-form`)
    form.find( "input[name='start_hour']" ).val("")
    form.find( "input[name='end_hour']" ).val("")
    form.find( "input[name='start_hour_saturday']" ).val("")
    form.find( "input[name='end_hour_saturday']" ).val("")
}

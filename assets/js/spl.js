let user_login = get_login(),
    spl_id = null,
    selected_date = format_to_db_date(new Date()),
    selected_rating = null

$('.selectpicker').selectpicker();
$(".monthly-datepicker").datepicker( {
    format: "yyyy-mm",
    viewMode: "months", 
    minViewMode: "months",
    orientation: 'bottom'
});

$(document).ready(function(){
    //data table
    let spl_manager_table = $('#spl_manager_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_spl',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            // console.log("D itu:", d)
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let status = $('#status_spl').val()
            let date = $('#filter_tanggal').val()
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            if(status != 'ALL') newObj.status = status 
            switch (column) {
                case 0:
                    newObj.order_by = 'no_spl'
                    break
                case 1:
                    newObj.order_by = 'customer_id'
                    break
                case 3:
                    newObj.order_by = 'user_id'
                    break
                case 4:
                    newObj.order_by = 'hasil_rumus'
                    break
                case 5:
                    newObj.order_by = 'jumlah_order'
                    break
                case 6:
                    newObj.order_by = 'harga_satuan'
                    break
                case 7:
                    newObj.order_by = 'lebar_bahan'
                    break
                case 8:
                    newObj.order_by = 'ukuran_potong'
                    break
                case 9:
                    newObj.order_by = 'komposisi'
                    break
                case 10:
                    newObj.order_by = 'jumlah_roll'
                    break
                case 11:
                    newObj.order_by = 'keterangan'
                    break
                case 12:
                    newObj.order_by = 'approval_status'
                    break
                case 13:
                    newObj.order_by = 'spl_status'
                    break
                case 14:
                    newObj.order_by = 'tanggal_spl'
                    break
                default:
                    newObj.order_by = 'no_spl'
                    break
            }

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "no_spl",
                orderable: true
            },
            { 
                data: "customer_name",
                orderable: true
            },
            { 
                data: "customer_rating",
                orderable: false,
                render: function (data, type, row, meta) {
                    return data ? data : 'N/A'
                }
            },
            { 
                data: "marketing_name",
                orderable: true
            },
            { 
                data: "hasil_rumus",
                orderable: true
            },
            { 
                data: "jumlah_order",
                orderable: true
            },
            { 
                data: "harga_satuan",
                orderable: true
            },
            {
                data: "lebar_bahan",
                orderable: true,
                render: function (data, type, row, meta) {
                    let bahan = `${row.lebar_bahan}/${row.tebal_bahan}`
                    return bahan
                }
            },
            {
                data: "ukuran_potong",
                orderable: true
            },
            {
                data: "komposisi",
                orderable: true
            },
            {
                data: "jumlah_roll",
                orderable: true
            },
            {
                data: "keterangan",
                orderable: true
            },
            {
                data: "approval_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'REQUESTED'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'APPROVED'){
                        label = `<span class="label label-success">${data}</span>`
                    }else if(data == 'TIDAK_APPROVED'){
                        label = `<span class="label label-danger">REJECTED</span>`
                    }
                    return label
                }
            },
            {
                data: "spl_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'SELESAI'){
                        label = `<span class="label label-primary">${data}</span>`
                    }else if(data == 'BELUM'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'DITITIPKAN'){
                        label = `<span class="label label-warning">${data}</span>`
                    }else if(data == 'HANGUS'){
                        label = `<span class="label label-danger">${data}</span>`
                    }
                    return label
                }
            },
            {
                data: "tanggal_spl",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = ``
                    //spl hangus button
                    if(row.spl_status != 'HANGUS'){
                        if(row.approval_status == 'REQUESTED'){
                            button += `<button class="btn btn-success edit-spl-button" data-id="${data}" data-toggle="modal" data-target="#update-spl-modal">
                                <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                            </button>
                            &nbsp;&nbsp;`

                            if(user_login.type == 'super_admin'){
                                button += `<button type="button" class="btn btn-info approve-spl-toggle" data-id="${data}" data-toggle="modal" data-target="#approve-spl-modal">
                                    <i class="fa fa-check" aria-hidden="true"></i> Approve
                                </button>
                                &nbsp;&nbsp;
                                <button type="button" class="btn btn-danger noapprove-spl-toggle" data-id="${data}" data-toggle="modal" data-target="#noapprove-spl-modal">
                                    <i class="fa fa-close" aria-hidden="true"></i> Reject
                                </button>
                                &nbsp;&nbsp;`
                            }
                        }else if(row.approval_status == 'TIDAK_APPROVED'){
                            button += `<button class="btn btn-success recreate-spl-button" data-id="${data}" data-toggle="modal" data-target="#recreate-spl-modal">
                                <i class="fa fa-pencil" aria-hidden="true"></i> Recreate
                            </button>
                            &nbsp;&nbsp;`
                            if(user_login.type == 'super_admin'){
                                button += `<button class="btn btn-danger delete-spl-toggle" data-id="${data}" data-toggle="modal" data-target="#delete-spl-modal">
                                    <i class="fa fa-trash" aria-hidden="true"></i> Delete
                                </button>
                                &nbsp;&nbsp;`
                            }
                        }else{
                            if(row.spl_status == 'BELUM'){
                                if(user_login.type == 'super_admin'){
                                    button += `<button class="btn btn-success edit-spl-button" data-id="${data}" data-toggle="modal" data-target="#update-spl-modal">
                                        <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                                    </button>
                                    &nbsp;&nbsp;
                                    <button class="btn btn-primary done-spl-toggle" data-id="${data}" data-toggle="modal" data-target="#done-spl-modal">
                                        <i class="fa fa-check" aria-hidden="true"></i> Done
                                    </button>
                                    &nbsp;&nbsp;`
                                }

                                button += `<button class="btn btn-warning delivery-spl-button" data-id="${data}" data-toggle="modal" data-target="#delivery-spl-modal">
                                    <i class="fa fa-truck" aria-hidden="true"></i> Delivery
                                </button>`
								if(calculateDateDifference(row.tanggal_spl) < 5){
									button += `<a href="backend/print_spl?id=${data}" class="btn btn-default print-spl-button" target="_blank">
										<i class="fa fa-save" aria-hidden="true"></i> Print
									</a>
									&nbsp;&nbsp;`
								}
                            }else{
                                if(calculateDateDifference(row.tanggal_spl) < 5){
									button += `<a href="backend/print_spl?id=${data}" class="btn btn-default print-spl-button" target="_blank">
										<i class="fa fa-save" aria-hidden="true"></i> Print
									</a>
									&nbsp;&nbsp;`
								}
                            }
                        }

                        //spl hangus button
                        if(row.spl_status != 'DITITIPKAN'){
                            button += `<button class="btn burn-spl-toggle" data-id="${data}" data-toggle="modal" data-target="#burn-spl-modal" style="background-color:black !important; color:white;">
                                <i class="fa fa-fire" aria-hidden="true"></i> Hangus
                            </button>`
                        }
                    }
                    
                    return button
                },
                orderable: false
            }
        ]
    });

    //get requested spl
    if(user_login.type == 'super_admin'){
        get_requested_spl()
    }else{
        $('#multiple_approve_section').hide()
        $('#autodone_section').hide()
    }

    $('#status_spl').change(function(){
        spl_manager_table.draw()
        // spl_manager_table.ajax.reload()
    })

    $('#filter_tanggal').change(function(){
        selected_date = $(this).val()
        spl_manager_table.draw()
        get_requested_spl()
        // spl_manager_table.ajax.reload()
    })

    $('#spl-customer-create').change(function(){
        selected_rating = $(this).find(':selected').data('rating')
        console.log("customer: ", $(this).val())
        console.log("customer rating: ", selected_rating)
        renderRating(selected_rating, 'create')
    })
})

$("body").delegate(".edit-spl-button", "click", function(e) {
    clear_spl_form('update')
    id = $(this).data('id')
    console.log("ID:", id)
    $('#update-spl-button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_spl_detail?id=${id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
        },
        success: function(res) {
            const response = JSON.parse(res)
            console.log("RESPONSE SUCCESS: ", response)
            
            //fill form
            fill_spl_form(response.data, 'update')

            $('#update-spl-button').prop('disabled', false)
        }
    });
})

$("body").delegate(".delivery-spl-button", "click", function(e) {
    clear_spl_form('delivery')
    id = $(this).data('id')
    console.log("ID:", id)
    $('#delivery-spl-button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_spl_detail?id=${id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
        },
        success: function(res) {
            const response = JSON.parse(res)
            console.log("RESPONSE SUCCESS: ", response)
            
            //fill form
            fill_spl_form(response.data, 'delivery')

            $('#delivery-spl-button').prop('disabled', false)
        }
    });
})

$("body").delegate(".recreate-spl-button", "click", function(e) {
    clear_spl_form('recreate')
    id = $(this).data('id')
    console.log("ID:", id)
    $('#recreate-spl-button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_spl_detail?id=${id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
        },
        success: function(res) {
            const response = JSON.parse(res)
            console.log("RESPONSE SUCCESS: ", response)
            
            //fill form
            fill_spl_form(response.data, 'recreate')

            $('#recreate-spl-button').prop('disabled', false)
        }
    });
})

$("body").delegate(".approve-spl-toggle", "click", function(e) {
    spl_id = $(this).data('id')
})

$("body").delegate(".noapprove-spl-toggle", "click", function(e) {
    spl_id = $(this).data('id')
})

$("body").delegate(".done-spl-toggle", "click", function(e) {
    spl_id = $(this).data('id')
})

$("body").delegate(".delete-spl-toggle", "click", function(e) {
    spl_id = $(this).data('id')
})

$("body").delegate(".burn-spl-toggle", "click", function(e) {
    spl_id = $(this).data('id')
})

$('#create-spl-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-spl-button').html(loading_button)
    $('#create-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/create_spl_process`,
        type: 'POST',
        data: new FormData($('#create-spl-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#create-spl-button').html('Create')
            $('#create-spl-button').prop('disabled', false)
            $('#create-spl-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#create-spl-button').html('Create')
            $('#create-spl-warning').html(response.message)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
});

$('#update-spl-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-spl-button').html(loading_button)
    $('#update-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_spl_process`,
        type: 'POST',
        data: new FormData($('#update-spl-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#update-spl-button').html('Update')
            $('#update-spl-button').prop('disabled', false)
            $('#update-spl-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#update-spl-button').html('Update')
            $('#update-spl-warning').html(response.message)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
});

$('#delivery-spl-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delivery-spl-button').html(loading_button)
    $('#delivery-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_spl_process`,
        type: 'POST',
        data: new FormData($('#delivery-spl-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#delivery-spl-button').html('Delivery')
            $('#delivery-spl-button').prop('disabled', false)
            $('#delivery-spl-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#delivery-spl-button').html('Delivery')
            $('#delivery-spl-warning').html(response.message)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
});

$('#recreate-spl-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#recreate-spl-button').html(loading_button)
    $('#recreate-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_spl_process`,
        type: 'POST',
        data: new FormData($('#recreate-spl-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#recreate-spl-button').html('Recreate')
            $('#recreate-spl-button').prop('disabled', false)
            $('#recreate-spl-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#recreate-spl-button').html('Recreate')
            $('#recreate-spl-warning').html(response.message)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
});

$('#autodone-spl-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#autodone-spl-button').html(loading_button)
    $('#autodone-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/autodone_spl_process`,
        type: 'POST',
        data: new FormData($('#autodone-spl-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`

            $('#autodone-spl-button').html('Submit')
            $('#autodone-spl-button').prop('disabled', false)
            $('#autodone-spl-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Auto Done berhasil dilakukan!</a>.</div>`

            $('#autodone-spl-button').html('Submit')
            $('#autodone-spl-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#multiple_approve').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`,
        list_approve = $( "#list_approve" ).val()
        
    console.log("MULTIPLE: ", list_approve)
    $('#multiple_approve_button').html(loading_button)
    $('#multiple_approve_button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/multiple_approve_spl_process`,
        type: 'POST',
        data: JSON.stringify({ids: list_approve}),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${response.message}</a>.</div>`

            $('#multiple_approve_button').html('Approve')
            $('#multiple_approve_button').prop('disabled', false)
            $('#multiple_approve_warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL terpilih telah diapprove!</a>.</div>`

            $('#multiple_approve_button').html('Approve')
            $('#multiple_approve_warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 2000);
        }
    });
});

$('#approve-spl-button').click(function(e){
    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#approve-spl-button').html(loading_button)
    $('#approve-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/approve_spl_process`,
        type: 'POST',
        data: JSON.stringify({id: spl_id}),
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${response.message}!</a>.</div>`
            console.log("RESPONSE ERROR: ", response)
            $('#approve-spl-button').html('IYA')
            $('#approve-spl-button').prop('disabled', false)
            $('#approve-spl-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL telah disetujui!</a>.</div>`
            $('#approve-spl-button').html('IYA')
            $('#approve-spl-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#noapprove-spl-button').click(function(e){
    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#noapprove-spl-button').html(loading_button)
    $('#noapprove-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/noapprove_spl_process`,
        type: 'POST',
        data: JSON.stringify({id: spl_id}),
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${response.message}!</a>.</div>`
            console.log("RESPONSE ERROR: ", response)
            $('#noapprove-spl-button').html('IYA')
            $('#noapprove-spl-button').prop('disabled', false)
            $('#noapprove-spl-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL telah ditolak!</a>.</div>`
            $('#noapprove-spl-button').html('IYA')
            $('#noapprove-spl-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 2000);
        }
    });
});

$('#done-spl-button').click(function(e){
    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#done-spl-button').html(loading_button)
    $('#done-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/done_spl_process`,
        type: 'POST',
        data: JSON.stringify({id: spl_id}),
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${response.message}!</a>.</div>`
            console.log("RESPONSE ERROR: ", response)
            $('#done-spl-button').html('IYA')
            $('#done-spl-button').prop('disabled', false)
            $('#done-spl-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL telah selesai!</a>.</div>`
            $('#done-spl-button').html('IYA')
            $('#done-spl-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#delete-spl-button').click(function(e){
    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delete-spl-button').html(loading_button)
    $('#delete-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/delete_spl_process`,
        type: 'POST',
        data: JSON.stringify({id: spl_id}),
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${response.message}!</a>.</div>`
            console.log("RESPONSE ERROR: ", response)
            $('#delete-spl-button').html('IYA')
            $('#delete-spl-button').prop('disabled', false)
            $('#delete-spl-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL telah dihapus!</a>.</div>`
            $('#delete-spl-button').html('IYA')
            $('#delete-spl-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#burn-spl-button').click(function(e){
    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#burn-spl-button').html(loading_button)
    $('#burn-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/burn_spl_satuan_process`,
        type: 'POST',
        data: JSON.stringify({id: spl_id}),
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${response.message}!</a>.</div>`
            console.log("RESPONSE ERROR: ", response)
            $('#burn-spl-button').html('IYA')
            $('#burn-spl-button').prop('disabled', false)
            $('#burn-spl-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL telah dihanguskan!</a>.</div>`
            $('#burn-spl-button').html('IYA')
            $('#burn-spl-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function get_requested_spl(){
    $('#multiple_approve_button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_spl?page_number=0&page_size=100&date=${selected_date}&approval_status=REQUESTED&order_by=no_spl&sort=DESC`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
            $('#multiple_approve_button').prop('disabled', false)
        },
        success: function(res) {
            generate_spl_option(res.data)
            $('#multiple_approve_button').prop('disabled', false)
        }
    });
}

function generate_spl_option(list){
    let spl_option_html = ``
    for(let item of list){
        let option_html = `<option value="${item.id}">(${item.no_spl}) ${item.customer_name}</option>`
        spl_option_html += option_html
    }
    $('#list_approve').html(spl_option_html)
    $('.selectpicker').selectpicker('refresh')
}

function format_to_db_date(date){
    let dd = String(date.getDate()).padStart(2, '0'),
        mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}-${dd}`
    return new_date
}

function fill_spl_form(spl, action){
    let form = $(`#${action}-spl-form`)
    $( `#customer_id_spl_${action}` ).val(spl.customer_id).change()
    form.find( "input[name='id']" ).val(spl.id)
    form.find( "input[name='lebar_bahan']" ).val(spl.lebar_bahan)
    form.find( "input[name='tebal_bahan']" ).val(spl.tebal_bahan)
    form.find( "input[name='ukuran_potong']" ).val(spl.ukuran_potong)
    $( `#satuan_spl_${action}` ).val(spl.satuan).change()
    form.find( "textarea[name='keterangan']" ).val(spl.keterangan)
    form.find( "input[name='jumlah_order']" ).val(spl.jumlah_order)
    form.find( "input[name='harga_satuan']" ).val(spl.harga_satuan)
    form.find( "input[name='komposisi']" ).val(spl.komposisi)
    form.find( "input[name='jumlah_roll']" ).val(spl.jumlah_roll)
    form.find( "input[name='hasil_rumus']" ).val(spl.hasil_rumus)
    if(action == 'delivery'){
        form.find( "input[name='tanggal_kirim1']" ).val(spl.tanggal_kirim1)
        form.find( "input[name='jumlah_kirim1']" ).val(spl.jumlah_kirim1)
        form.find( "input[name='tanggal_kirim2']" ).val(spl.tanggal_kirim2)
        form.find( "input[name='jumlah_kirim2']" ).val(spl.jumlah_kirim2)
        form.find( "input[name='tanggal_kirim3']" ).val(spl.tanggal_kirim3)
        form.find( "input[name='jumlah_kirim3']" ).val(spl.jumlah_kirim3)
        form.find( "input[name='sjkirim1']" ).val(spl.sjkirim1)
        form.find( "input[name='sjkirim2']" ).val(spl.sjkirim2)
        form.find( "input[name='sjkirim3']" ).val(spl.sjkirim3)
    }
}
    
function clear_spl_form(action){
    let form = $(`#${action}-spl-form`)
    form.find( "input[name='lebar_bahan']" ).val("")
    form.find( "input[name='tebal_bahan']" ).val("")
    form.find( "input[name='ukuran_potong']" ).val("")
    form.find( "textarea[name='keterangan']" ).val("")
    form.find( "input[name='jumlah_order']" ).val("")
    form.find( "input[name='harga_satuan']" ).val("")
    form.find( "input[name='komposisi']" ).val("")
    form.find( "input[name='jumlah_roll']" ).val("")
    form.find( "input[name='hasil_rumus']" ).val("")
}

function renderRating(rating, type){
    let rating_html = ''
    for(let i=0; i<rating; i++){
        rating_html += `<i class="fa fa-star" aria-hidden="true"></i>`
    }
    for(let i=0; i<(5-rating); i++){
        rating_html += `<i class="fa fa-star-o" aria-hidden="true"></i>`
    }
    $(`#customer-rating-${type}`).html(rating_html)
    if(rating == 0){
        $(`#${type}-spl-warning`).html(`<div class="alert alert-danger alert-dismissible">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        <strong>PERINGATAN!</strong> Kustomer masuk ke dalam <b><i>BLACK LIST</i></b> atau belum memiliki <b><i>RATING</i></b>!<br>Tidak dapat melanjutkan pembuatan SPL.</div>`)
        $(`#${type}-spl-button`).hide()
    }else if(rating > 0 && rating < 4){
        $(`#${type}-spl-warning`).html(`<div class="alert alert-warning alert-dismissible">
        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        <strong>PERHATIAN!</strong> Cek Hutangnya/Bayar DP Dulu!</div>`)
        $(`#${type}-spl-button`).show()
    }else{
        $(`#${type}-spl-warning`).html('')
        $(`#${type}-spl-button`).show()
    }
}

function calculateDateDifference(dateString) {
    // Parse the date string to create a Date object (assuming format YYYY-MM-DD)
    const targetDate = new Date(dateString);
    
    // Get the current date and set time to 00:00:00 to ignore time differences
    const currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);

    // Set time of targetDate to 00:00:00 to ignore time differences
    targetDate.setHours(0, 0, 0, 0);
    
    // Calculate the difference in milliseconds
    const differenceInMillis = currentDate - targetDate;
    
    // Convert the difference from milliseconds to days
    const differenceInDays = Math.round(differenceInMillis / (1000 * 60 * 60 * 24));
	console.log("DIFF", differenceInDays)
    
    return differenceInDays;
}

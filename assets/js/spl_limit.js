let LIMIT_ID

$(document).ready(function(){
    getSplLimit()

    $('#spl-limit-form').submit(function(e){
        e.preventDefault()

        let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`
        $('#spl-limit-button').html(loading_button)
        $('#spl-limit-button').prop('disabled', true);

        let $form = $(this),
            request = {
                nilai: $form.find("input[name='nilai']").val()
            }

        if(LIMIT_ID) request.id = LIMIT_ID

        $.ajax({
            async: true,
            url: `backend/update_spl_limit`,
            type: 'POST',
            data: JSON.stringify(request),
            error: function(res) {
                console.log("ERROR: ", res)
                let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Update SPL limit gagal dilakukan!</a>.</div>`

                $('#spl-limit-button').html('Submit')
                $('#spl-limit-button').prop('disabled', false)
                $('#spl-limit-warning').html(message)
            },
            success: function(res) {
                console.log("RESPONSE: ", res)
                let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL limit berhasil diupdate!</a>.</div>`
                $('#spl-limit-button').prop('disabled', false)
                $('#spl-limit-button').html('Submit')
                $('#spl-limit-warning').html(message)
            }
        });
    })
})

function getSplLimit(){
    $.ajax({
        async: true,
        url: `backend/get_spl_limit`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
        },
        success: function(res) {
            if(res.data){
                LIMIT_ID = res.data.id
                renderFormSplLimit(res.data)
            }
        }
    });
}

function renderFormSplLimit(data){
    let $form = $('#spl-limit-form')
    $form.find("input[name='nilai']").val(data.nilai)
}
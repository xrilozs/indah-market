let user_login = get_login(),
    selected_date = format_to_db_date(new Date()),
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'no_spl',
    table_status = 'ALL'

$(document).ready(function(){
    $("#filter_tanggal").val(selected_date)
    $(".monthly-datepicker").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    //data table
    let spl_manager_table = $('#spl_manager_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_spl_monthly',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            // console.log("D itu:", d)
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let status = $('#status_spl').val()
            let date = $('#filter_tanggal').val()
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            table_dir= d.order[0].dir
            if(status != 'ALL'){
                newObj.status = status 
                table_status = status 
            }
            switch (column) {
                case 0:
                    newObj.order_by = 'no_spl'
                    break
                case 1:
                    newObj.order_by = 'customer_id'
                    break
                case 2:
                    newObj.order_by = 'user_id'
                    break
                case 3:
                    newObj.order_by = 'hasil_rumus'
                    break
                case 4:
                    newObj.order_by = 'jumlah_order'
                    break
                case 5:
                    newObj.order_by = 'harga_satuan'
                    break
                case 6:
                    newObj.order_by = 'lebar_bahan'
                    break
                case 7:
                    newObj.order_by = 'ukuran_potong'
                    break
                case 8:
                    newObj.order_by = 'komposisi'
                    break
                case 9:
                    newObj.order_by = 'jumlah_roll'
                    break
                case 10:
                    newObj.order_by = 'keterangan'
                    break
                case 11:
                    newObj.order_by = 'approval_status'
                    break
                case 12:
                    newObj.order_by = 'spl_status'
                    break
                case 13:
                    newObj.order_by = 'tanggal_spl'
                    break
                default:
                    newObj.order_by = 'no_spl'
                    break
            }
            table_order_by = newObj.order_by

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "no_spl",
                orderable: true
            },
            { 
                data: "customer_name",
                orderable: true
            },
            { 
                data: "marketing_name",
                orderable: true
            },
            { 
                data: "hasil_rumus",
                orderable: true
            },
            { 
                data: "jumlah_order",
                orderable: true
            },
            { 
                data: "harga_satuan",
                orderable: true
            },
            {
                data: "lebar_bahan",
                orderable: true,
                render: function (data, type, row, meta) {
                    let bahan = `${row.lebar_bahan}/${row.tebal_bahan}`
                    return bahan
                }
            },
            {
                data: "ukuran_potong",
                orderable: true
            },
            {
                data: "komposisi",
                orderable: true
            },
            {
                data: "jumlah_roll",
                orderable: true
            },
            {
                data: "keterangan",
                orderable: true
            },
            {
                data: "approval_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'REQUESTED'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'APPROVED'){
                        label = `<span class="label label-success">${data}</span>`
                    }else if(data == 'TIDAK_APPROVED'){
                        label = `<span class="label label-danger">REJECTED</span>`
                    }
                    return label
                }
            },
            {
                data: "spl_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'SELESAI'){
                        label = `<span class="label label-primary">${data}</span>`
                    }else if(data == 'BELUM'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'DITITIPKAN'){
                        label = `<span class="label label-warning">${data}</span>`
                    }else if(data == 'HANGUS'){
                        label = `<span class="label label-danger">${data}</span>`
                    }
                    return label
                }
            },
            {
                data: "tanggal_spl",
                orderable: true
            }
        ]
    });

    $('#status_spl').change(function(){
        spl_manager_table.draw()
    })

    $('#filter_tanggal').change(function(){
        spl_manager_table.draw()
    })
})

$('#delete-spl-button').click(function(){
    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delete-spl-button').html(loading_button)
    $('#delete-spl-button').prop('disabled', true);

    let request = {
        date: selected_date,
        search: table_search,
        status: table_status
    }

    $.ajax({
        async: true,
        url: `backend/delete_spl_batch`,
        type: 'POST', 
        data: JSON.stringify(request),
        error: function(res) {
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> SPL yang dipilih gagal dihapus!</a>.</div>`
            $('#delete-spl-button').html('Hapus SPL')
            $('#delete-spl-button').prop('disabled', false)
            $('#delete-spl-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL yang dipilih berhasil dihapus!</a>.</div>`
            $('#delete-spl-button').html('Hapus SPL')
            $('#delete-spl-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
})

function format_to_db_date(date){
    let mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}`
    return new_date
}

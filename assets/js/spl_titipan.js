let user_login = get_login(),
    spl_id = null,
    selected_date = format_to_db_date(new Date())

$('.selectpicker').selectpicker();
$("#filter_tanggal").val(selected_date)
$(".monthly-datepicker").datepicker( {
    format: "yyyy-mm",
    viewMode: "months", 
    minViewMode: "months",
    orientation: 'bottom'
});


$(document).ready(function(){
    //data table
    let spltitipan_manager_table = $('#spltitipan_manager_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_spl_titipan',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            // console.log("D itu:", d)
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let date = $('#filter_tanggal').val()
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            switch (column) {
                case 0:
                    newObj.order_by = 'no_spl_titipan'
                    break
                case 2:
                    newObj.order_by = 'site_titipan'
                    break
                case 3:
                    newObj.order_by = 'site_awal'
                    break
                case 4:
                    newObj.order_by = 'marketing_penerima'
                    break
                case 6:
                    newObj.order_by = 'ket_kirim1'
                    break
                case 7:
                    newObj.order_by = 'ket_kirim2'
                    break
                case 8:
                    newObj.order_by = 'ket_retur'
                    break
                case 9:
                    newObj.order_by = 'status'
                    break
                case 10:
                    newObj.order_by = 'tanggal_spl_titipan'
                    break
                case 11:
                    newObj.order_by = 'tanggal_spl_asli'
                    break
                default:
                    newObj.order_by = 'no_spl_titipan'
                    break
            }

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "no_spl_titipan",
                orderable: true
            },
            { 
                data: "no_spl",
                orderable: false
            },
            { 
                data: "site_titipan",
                orderable: true
            },
            { 
                data: "site_awal",
                orderable: true
            },
            { 
                data: "marketing_penerima",
                orderable: true
            },
            { 
                data: "customer_name",
                orderable: false
            },
            { 
                data: "ket_kirim1",
                orderable: true
            },
            { 
                data: "ket_kirim2",
                orderable: true
            },
            { 
                data: "ket_retur",
                orderable: true
            },
            { 
                data: "status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'SELESAI'){
                        label = `<span class="label label-primary">${data}</span>`
                    }else if(data == 'BELUM SELESAI'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'DITITIPKAN'){
                        label = `<span class="label label-warning">${data}</span>`
                    }else if(data == 'HANGUS'){
                        label = `<span class="label label-danger">${data}</span>`
                    }
                    return label
                }
            },
            {
                data: "tanggal_spl_titipan",
                orderable: true
            },
            {
                data: "tanggal_spl_asli",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = ``
                    if(row.status == 'BELUM SELESAI'){
                        button += `<button class="btn btn-success edit-spltitipan-button" data-id="${data}" data-toggle="modal" data-target="#update-spltitipan-modal">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                        </button>`
                    }else{
                        if(user_login.type == 'super_admin'){
                            button += `<button class="btn btn-success edit-spltitipan-button" data-id="${data}" data-toggle="modal" data-target="#update-spltitipan-modal">
                                <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                            </button>`
                        }
                    }
					if(calculateDateDifference(row.tanggal_spl_asli) < 5){
						button += `<a href="backend/print_spl_titipan?id=${data}" class="btn btn-default print-spl-button" target="_blank">
							<i class="fa fa-save" aria-hidden="true"></i> Print
						</a>`
					}
                    
                    return button
                },
                orderable: false
            }
        ]
    });

    $('#filter_tanggal').change(function(){
        selected_date = $(this).val()
        spltitipan_manager_table.draw()
    })

    let select_spl = $('#select_spl').select2({
		dropdownParent: $('#create-spltitipan-modal'),
        width: '100%',
        placeholder: "Ketik Nomor SPL..",
        ajax: {
          url: 'backend/get_spl_by_spl_no',
          data: function (params) {
            var query = {
              q: params.term,
              approval_status: "APPROVED"
            }
      
            return query;
          },
          processResults: function (data) {
            // Transforms the top-level key of the response object from 'items' to 'results'
            return {
              results: JSON.parse(data)
            };
          }
        }
    });
    select_spl.data('select2').$selection.css('height', '34px');

    $('#select_spl').on('select2:select', function (e) {
        var data = e.params.data;
        console.log(data);
        fill_spltitipan_form(data, "create")
    });
})

$("body").delegate(".edit-spltitipan-button", "click", function(e) {
    clear_spltitipan_form('update')
    id = $(this).data('id')
    console.log("ID:", id)
    $('#update-spltitipan-button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_spltitipan_detail?id=${id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
        },
        success: function(res) {
            const response = JSON.parse(res)
            console.log("RESPONSE SUCCESS: ", response)
            
            //fill form
            fill_spltitipan_form(response.data, 'update')

            $('#update-spltitipan-button').prop('disabled', false)
        }
    });
})

$('#create-spltitipan-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-spltitipan-button').html(loading_button)
    $('#create-spltitipan-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/create_spltitipan_process`,
        type: 'POST',
        data: new FormData($('#create-spltitipan-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#create-spltitipan-button').html('Create')
            $('#create-spltitipan-button').prop('disabled', false)
            $('#create-spltitipan-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#create-spltitipan-button').html('Create')
            $('#create-spltitipan-warning').html(response.message)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
});

$('#update-spltitipan-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-spltitipan-button').html(loading_button)
    $('#update-spltitipan-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_spltitipan_process`,
        type: 'POST',
        data: new FormData($('#update-spltitipan-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#update-spltitipan-button').html('Update')
            $('#update-spltitipan-button').prop('disabled', false)
            $('#update-spltitipan-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#update-spltitipan-button').html('Update')
            $('#update-spltitipan-warning').html(response.message)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
});

function format_to_db_date(date){
    let dd = String(date.getDate()).padStart(2, '0'),
        mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}`
    return new_date
}

function fill_spltitipan_form(spltitipan, action){
    let form = $(`#${action}-spltitipan-form`)
    const site_awal = spltitipan.no_spl.charAt(0)
    form.find( "input[name='site_awal']" ).val(site_awal)
    form.find( "input[name='customer']" ).val(spltitipan.customer_name)
    form.find( "input[name='tebal_bahan']" ).val(spltitipan.tebal_bahan)
    form.find( "input[name='lebar_bahan']" ).val(spltitipan.lebar_bahan)
    form.find( "input[name='ukuran_potong']" ).val(spltitipan.ukuran_potong)
    form.find( "textarea[name='keterangan']" ).val(spltitipan.keterangan)
    form.find( "input[name='komposisi']" ).val(spltitipan.komposisi)
    form.find( "input[name='harga_satuan']" ).val(spltitipan.harga_satuan)
    form.find( "input[name='jumlah_roll']" ).val(spltitipan.jumlah_roll)
    form.find( "input[name='hasil_rumus']" ).val(spltitipan.hasil_rumus)
    if(action == 'update'){
        form.find( "input[name='id']" ).val(spltitipan.id)
        form.find( "input[name='no_spl_titipan']" ).val(spltitipan.no_spl_titipan)
        form.find( "input[name='no_spl']" ).val(spltitipan.no_spl)
        form.find( "input[name='marketing_penerima']" ).val(spltitipan.marketing_penerima)
        form.find( "input[name='site_titipan']" ).val(spltitipan.site_titipan)
        form.find( "textarea[name='ket_kirim1']" ).val(spltitipan.ket_kirim1)
        form.find( "textarea[name='ket_kirim2']" ).val(spltitipan.ket_kirim2)
        form.find( "textarea[name='ket_kirim3']" ).val(spltitipan.ket_kirim3)
        form.find( "textarea[name='ket_kirim4']" ).val(spltitipan.ket_kirim4)
        form.find( "textarea[name='ket_kirim5']" ).val(spltitipan.ket_kirim5)
        form.find( "textarea[name='ket_retur']" ).val(spltitipan.ket_retur)
        $('#status').val(spltitipan.status).change()
        if(user_login.type == 'super_admin'){
            $('#status-section').show()
        }else{
            $('#status-section').hide()
        }
    }
}
    
function clear_spltitipan_form(action){
    let form = $(`#${action}-spltitipan-form`)
    form.find( "input[name='site_awal']" ).val("")
    form.find( "input[name='customer']" ).val("")
    form.find( "input[name='tebal_bahan']" ).val("")
    form.find( "input[name='lebar_bahan']" ).val("")
    form.find( "input[name='ukuran_potong']" ).val("")
    form.find( "textarea[name='keterangan']" ).val("")
    form.find( "input[name='komposisi']" ).val("")
    form.find( "input[name='harga_satuan']" ).val("")
    form.find( "input[name='jumlah_roll']" ).val("")
    if(action == 'update'){
        form.find( "input[name='id']" ).val("")
        form.find( "input[name='no_spl_titipan']" ).val("")
        form.find( "input[name='no_spl_awal']" ).val("")
        form.find( "input[name='marketing_penerima']" ).val("")
        form.find( "input[name='site_titipan']" ).val("")
        form.find( "textarea[name='ket_kirim1']" ).val("")
        form.find( "textarea[name='ket_kirim2']" ).val("")
        form.find( "textarea[name='ket_kirim3']" ).val("")
        form.find( "textarea[name='ket_kirim4']" ).val("")
        form.find( "textarea[name='ket_kirim5']" ).val("")
        form.find( "textarea[name='ket_retur']" ).val("")
        $('#status').val("BELUM SELESAI").change()
        if(user_login.type == 'super_admin'){
            $('#status-section').show()
        }else{
            $('#status-section').hide()
        }
    }
}

function calculateDateDifference(dateString) {
    // Parse the date string to create a Date object (assuming format YYYY-MM-DD)
    const targetDate = new Date(dateString);
    
    // Get the current date and set time to 00:00:00 to ignore time differences
    const currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);

    // Set time of targetDate to 00:00:00 to ignore time differences
    targetDate.setHours(0, 0, 0, 0);
    
    // Calculate the difference in milliseconds
    const differenceInMillis = currentDate - targetDate;
    
    // Convert the difference from milliseconds to days
    const differenceInDays = Math.round(differenceInMillis / (1000 * 60 * 60 * 24));
	console.log("DIFF", differenceInDays)
    
    return differenceInDays;
}

let user_login = get_login(),
    absensi_id = null,
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'id',
    selected_date = format_to_db_date(new Date()),
    selected_jabatan = ''

$('.selectpicker').selectpicker();

$(document).ready(function(){
    //data table
    let absensi_table = $('#absensi_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_absensi',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let date = selected_date
            let jabatan = selected_jabatan

            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.jabatan = jabatan
            newObj.sort = d.order[0].dir
            switch (column) {
                case 2:
                    newObj.order_by = 'jumlah_hadir_biasa'
                    break
                case 3:
                    newObj.order_by = 'jumlah_hari_libur'
                    break
                case 4:
                    newObj.order_by = 'jumlah_jam_lembur'
                    break
                case 5:
                    newObj.order_by = 'jumlah_telat'
                    break
                case 6:
                    newObj.order_by = 'jumlah_telat_hari'
                    break
                case 7:
                    newObj.order_by = 'jumlah_absen'
                    break
                case 8:
                    newObj.order_by = 'potongan_kasbon'
                    break
                case 9:
                    newObj.order_by = 'uang_tunjangan'
                    break
                case 10:
                    newObj.order_by = 'tanggal_periode_absensi'
                    break
                default:
                    newObj.order_by = 'id'
                    break
            }
            rerender_print_link()
            rerender_print_link2()

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "pegawai_nip",
                orderable: false
            },
            { 
                data: "pegawai_name",
                orderable: false
            },
            { 
                data: "jumlah_hadir_biasa",
                orderable: true
            },
            { 
                data: "jumlah_hari_libur",
                orderable: true
            },
            { 
                data: "jumlah_jam_lembur",
                orderable: true
            },
            { 
                data: "jumlah_telat",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            { 
                data: "jumlah_telat_hari",
                orderable: true
            },
            { 
                data: "jumlah_absen",
                orderable: true
            },
            {
                data: "potongan_kasbon",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            {
                data: "uang_tunjangan",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            {
                data: "tanggal_periode_absensi",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = `<button class="btn btn-success edit-absensi-toggle" data-id="${data}" data-toggle="modal" data-target="#update-absensi-modal">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-danger delete-absensi-toggle" data-id="${data}" data-toggle="modal" data-target="#delete-absensi-modal">
                            <i class="fa fa-trash" aria-hidden="true"></i> Delete
                        </button>`
                    
                    return button
                },
                orderable: false
            }
        ]
    });

    let select_create = $('#pegawai_id_create').select2({
        width: '100%',
        placeholder: "Ketik nama atau NIP..",
        ajax: {
          url: 'backend/get_pegawai_all',
          data: function (params) {
            var query = {
              q: params.term
            }
      
            // Query parameters will be ?search=[term]&type=public
            return query;
          },
          processResults: function (data) {
            // Transforms the top-level key of the response object from 'items' to 'results'
            return {
              results: JSON.parse(data)
            };
          }
        }
    });
    select_create.data('select2').$selection.css('height', '34px');

    select_create.on("select2:selecting", function(e) { 
        const selected_data = e.params.args.data
        console.log(selected_data)
        $('#potongan_kasbon_create').val(selected_data.potongan_kasbon)
    });

    $('#filter_tanggal').change(function(){
        selected_date = $(this).val()
        absensi_table.draw()
    })

    $('#filter_jabatan').change(function(){
        selected_jabatan = $(this).val()
        absensi_table.draw()
    })

    $('.input_hadir').change(function(){
        const jumlah_hadir = $(this).val()
        let jumlah_absen = 6 - parseFloat(jumlah_hadir)
        jumlah_absen = jumlah_absen >= 0 ? jumlah_absen : 0;
        $('.input_absen').val(jumlah_absen)
    })
})

$("body").delegate(".edit-absensi-toggle", "click", function(e) {
    clear_absensi_form()
    absensi_id = $(this).data('id')
    console.log("ID:", absensi_id)
    $('#update-absensi-toggle').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_absensi_detail?id=${absensi_id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
            $('#update-absensi-toggle').prop('disabled', false)
        },
        success: function(res) {
            console.log("RESPONSE SUCCESS: ", res)
            fill_absensi_form(res.data)
            $('#update-absensi-toggle').prop('disabled', false)
        }
    });
})

$("body").delegate(".delete-absensi-toggle", "click", function(e) {
    console.log("DELETE")
    absensi_id = $(this).data('id')
})

$('#create-absensi-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-absensi-button').html(loading_button)
    $('#create-absensi-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/create_absensi_process`,
        type: 'POST',
        data: new FormData($('#create-absensi-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> absensi gagal ditambah!</a>.</div>`
            $('#create-absensi-button').html('Tambah')
            $('#create-absensi-button').prop('disabled', false)
            $('#create-absensi-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> absensi telah ditambah!</a>.</div>`
            $('#create-absensi-button').html('Tambah')
            $('#create-absensi-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#update-absensi-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-absensi-button').html(loading_button)
    $('#update-absensi-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_absensi_process`,
        type: 'POST',
        data: new FormData($('#update-absensi-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-absensi-button').html('Update')
            $('#update-absensi-button').prop('disabled', false)
            $('#update-absensi-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> absensi berhasil diubah!</a>.</div>`
            $('#update-absensi-button').html('Update')
            $('#update-absensi-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#delete-absensi-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delete-absensi-button').html(loading_button)
    $('#delete-absensi-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/delete_absensi_process`,
        type: 'POST',
        data: JSON.stringify({id: absensi_id, keyword: $('#keyword_delete').val()}),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#delete-absensi-button').html('IYA')
            $('#delete-absensi-button').prop('disabled', false)
            $('#delete-absensi-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> absensi berhasil dihapus!</a>.</div>`
            $('#delete-absensi-button').html('IYA')
            $('#delete-absensi-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function get_requested_absensi(){
    $('#multiple_approve_button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_absensi?page_number=0&page_size=100&date=${selected_date}&approval_status=REQUESTED&order_by=no_absensi&sort=DESC`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
            $('#multiple_approve_button').prop('disabled', false)
        },
        success: function(res) {
            generate_absensi_option(res.data)
            $('#multiple_approve_button').prop('disabled', false)
        }
    });
}

function generate_absensi_option(list){
    let absensi_option_html = ``
    for(let item of list){
        let option_html = `<option value="${item.id}">(${item.no_absensi}) ${item.customer_name}</option>`
        absensi_option_html += option_html
    }
    $('#list_approve').html(absensi_option_html)
    $('.selectpicker').selectpicker('refresh')
}

function format_to_db_date(date){
    let dd = String(date.getDate()).padStart(2, '0'),
        mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}-${dd}`
    return new_date
}

function fill_absensi_form(absensi){
    console.log("FORM: ", absensi)
    let form = $(`#update-absensi-form`)
    form.find( "input[name='id']" ).val(absensi.id)
    form.find( "input[name='jumlah_hadir_biasa']" ).val(absensi.jumlah_hadir_biasa)
    form.find( "input[name='jumlah_hari_libur']" ).val(absensi.jumlah_hari_libur)
    form.find( "input[name='jumlah_jam_lembur']" ).val(absensi.jumlah_jam_lembur)
    form.find( "input[name='jumlah_telat']" ).val(formatRupiah(absensi.jumlah_telat))
    form.find( "input[name='jumlah_telat_hari']" ).val(absensi.jumlah_telat_hari)
    form.find( "input[name='jumlah_absen']" ).val(absensi.jumlah_absen)
    form.find( "input[name='potongan_kasbon']" ).val(formatRupiah(absensi.potongan_kasbon))
    form.find( "input[name='uang_tunjangan']" ).val(formatRupiah(absensi.uang_tunjangan))
    form.find( "input[name='tanggal_periode_absensi']" ).val(absensi.tanggal_periode_absensi)
    $('#pegawai_id_update').html(`<option value="${absensi.pegawai_id}" selected>(${absensi.pegawai_nip}) ${absensi.pegawai_name}</option>`)

    let select_update = $('#pegawai_id_update').select2({
        width: '100%',
        placeholder: "Ketik nama atau NIP..",
        ajax: {
          url: 'backend/get_pegawai_all',
          data: function (params) {
            var query = {
              q: params.term
            }
      
            // Query parameters will be ?search=[term]&type=public
            return query;
          },
          processResults: function (data) {
            // Transforms the top-level key of the response object from 'items' to 'results'
            return {
              results: JSON.parse(data)
            };
          }
        }
    });
    select_update.data('select2').$selection.css('height', '34px');

    select_update.on("select2:selecting", function(e) { 
        const selected_data = e.params.args.data
        console.log(selected_data)
        $('#potongan_kasbon_update').val(selected_data.potongan_kasbon)
    });
}
    
function clear_absensi_form(){
    let form = $(`#update-absensi-form`)
    form.find( "input[name='id']" ).val('')
    form.find( "input[name='jumlah_hadir_biasa']" ).val('')
    form.find( "input[name='jumlah_hari_libur']" ).val('')
    form.find( "input[name='jumlah_jam_lembur']" ).val('')
    form.find( "input[name='jumlah_telat']" ).val('')
    form.find( "input[name='jumlah_telat_hari']" ).val('')
    form.find( "input[name='jumlah_absen']" ).val('')
    form.find( "input[name='potongan_kasbon']" ).val('')
    form.find( "input[name='uang_tunjangan']" ).val('')
}

function rerender_print_link(){
    $('#absensi_print').prop('href', `backend/get_absensi_and_print?sort=${table_dir}&order_by=${table_order_by}&search=${table_search}&date=${selected_date}&jabatan=${selected_jabatan}`)
}

function rerender_print_link2(){
    $('#rekap_absensi_print').prop('href', `backend/get_absensi_and_print_rekap?sort=${table_dir}&order_by=${table_order_by}&search=${table_search}&date=${selected_date}&jabatan=${selected_jabatan}`)
}
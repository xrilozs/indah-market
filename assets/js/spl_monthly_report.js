let user_login = get_login(),
    selected_date = format_to_db_date(new Date()),
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'no_spl',
    table_status = 'ALL'

$(document).ready(function(){
    $("#filter_tanggal").val(selected_date)
    $(".monthly-datepicker").datepicker( {
        format: "yyyy-mm",
        viewMode: "months", 
        minViewMode: "months"
    });
    //data table
    let spl_manager_table = $('#spl_manager_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_spl_monthly',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            // console.log("D itu:", d)
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let status = $('#status_spl').val()
            let date = $('#filter_tanggal').val()
            selected_date = $('#filter_tanggal').val()
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            table_dir= d.order[0].dir
            if(status != 'ALL'){
                newObj.status = status 
                table_status = status 
            }
            switch (column) {
                case 0:
                    newObj.order_by = 'no_spl'
                    break
                case 1:
                    newObj.order_by = 'customer_id'
                    break
                case 3:
                    newObj.order_by = 'user_id'
                    break
                case 4:
                    newObj.order_by = 'hasil_rumus'
                    break
                case 5:
                    newObj.order_by = 'jumlah_order'
                    break
                case 6:
                    newObj.order_by = 'harga_satuan'
                    break
                case 7:
                    newObj.order_by = 'lebar_bahan'
                    break
                case 8:
                    newObj.order_by = 'ukuran_potong'
                    break
                case 9:
                    newObj.order_by = 'komposisi'
                    break
                case 10:
                    newObj.order_by = 'jumlah_roll'
                    break
                case 11:
                    newObj.order_by = 'keterangan'
                    break
                case 12:
                    newObj.order_by = 'approval_status'
                    break
                case 13:
                    newObj.order_by = 'spl_status'
                    break
                case 14:
                    newObj.order_by = 'tanggal_spl'
                    break
                default:
                    newObj.order_by = 'no_spl'
                    break
            }
            table_order_by = newObj.order_by
            rerender_print_link()
            rerender_export_link()

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "no_spl",
                orderable: true
            },
            { 
                data: "customer_name",
                orderable: true
            },
            { 
                data: "customer_rating",
                orderable: false,
                render: function (data, type, row, meta) {
                    return data ? data : 'N/A'
                }
            },
            { 
                data: "marketing_name",
                orderable: true
            },
            { 
                data: "hasil_rumus",
                orderable: true
            },
            { 
                data: "jumlah_order",
                orderable: true
            },
            { 
                data: "harga_satuan",
                orderable: true
            },
            {
                data: "lebar_bahan",
                orderable: true,
                render: function (data, type, row, meta) {
                    let bahan = `${row.lebar_bahan}/${row.tebal_bahan}`
                    return bahan
                }
            },
            {
                data: "ukuran_potong",
                orderable: true
            },
            {
                data: "komposisi",
                orderable: true
            },
            {
                data: "jumlah_roll",
                orderable: true
            },
            {
                data: "keterangan",
                orderable: true
            },
            {
                data: "approval_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'REQUESTED'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'APPROVED'){
                        label = `<span class="label label-success">${data}</span>`
                    }else if(data == 'TIDAK_APPROVED'){
                        label = `<span class="label label-danger">REJECTED</span>`
                    }
                    return label
                }
            },
            {
                data: "spl_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'SELESAI'){
                        label = `<span class="label label-primary">${data}</span>`
                    }else if(data == 'BELUM'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'DITITIPKAN'){
                        label = `<span class="label label-warning">${data}</span>`
                    }else if(data == 'HANGUS'){
                        label = `<span class="label label-danger">${data}</span>`
                    }
                    return label
                }
            },
            {
                data: "tanggal_spl",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    return `<button class="btn btn-warning delivery-spl-button" data-id="${data}" data-toggle="modal" data-target="#delivery-spl-modal">
                                <i class="fa fa-truck" aria-hidden="true"></i> Delivery
                            </button>`
                }
            }
        ]
    });

    $('#status_spl').change(function(){
        spl_manager_table.draw()
    })

    $('#filter_tanggal').change(function(){
        spl_manager_table.draw()
    })
})

$("body").delegate(".delivery-spl-button", "click", function(e) {
    clear_spl_form('delivery')
    id = $(this).data('id')
    console.log("ID:", id)
    $('#delivery-spl-button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_spl_detail?id=${id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
        },
        success: function(res) {
            const response = JSON.parse(res)
            console.log("RESPONSE SUCCESS: ", response)
            
            //fill form
            fill_spl_form(response.data, 'delivery')

            $('#delivery-spl-button').prop('disabled', false)
        }
    });
})

$('#delivery-spl-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delivery-spl-button').html(loading_button)
    $('#delivery-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_spl_process`,
        type: 'POST',
        data: new FormData($('#delivery-spl-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#delivery-spl-button').html('Delivery')
            $('#delivery-spl-button').prop('disabled', false)
            $('#delivery-spl-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#delivery-spl-button').html('Delivery')
            $('#delivery-spl-warning').html(response.message)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
});

function fill_spl_form(spl, action){
    let form = $(`#${action}-spl-form`)
    $( `#customer_id_spl_${action}` ).val(spl.customer_id).change()
    form.find( "input[name='id']" ).val(spl.id)
    form.find( "input[name='lebar_bahan']" ).val(spl.lebar_bahan)
    form.find( "input[name='tebal_bahan']" ).val(spl.tebal_bahan)
    form.find( "input[name='ukuran_potong']" ).val(spl.ukuran_potong)
    $( `#satuan_spl_${action}` ).val(spl.satuan).change()
    form.find( "textarea[name='keterangan']" ).val(spl.keterangan)
    form.find( "input[name='jumlah_order']" ).val(spl.jumlah_order)
    form.find( "input[name='harga_satuan']" ).val(spl.harga_satuan)
    form.find( "input[name='komposisi']" ).val(spl.komposisi)
    form.find( "input[name='jumlah_roll']" ).val(spl.jumlah_roll)
    form.find( "input[name='hasil_rumus']" ).val(spl.hasil_rumus)
    if(action == 'delivery'){
        form.find( "input[name='tanggal_kirim1']" ).val(spl.tanggal_kirim1)
        form.find( "input[name='jumlah_kirim1']" ).val(spl.jumlah_kirim1)
        form.find( "input[name='tanggal_kirim2']" ).val(spl.tanggal_kirim2)
        form.find( "input[name='jumlah_kirim2']" ).val(spl.jumlah_kirim2)
        form.find( "input[name='tanggal_kirim3']" ).val(spl.tanggal_kirim3)
        form.find( "input[name='jumlah_kirim3']" ).val(spl.jumlah_kirim3)
        form.find( "input[name='sjkirim1']" ).val(spl.sjkirim1)
        form.find( "input[name='sjkirim2']" ).val(spl.sjkirim2)
        form.find( "input[name='sjkirim3']" ).val(spl.sjkirim3)
    }
}
    
function clear_spl_form(action){
    let form = $(`#${action}-spl-form`)
    form.find( "input[name='lebar_bahan']" ).val("")
    form.find( "input[name='tebal_bahan']" ).val("")
    form.find( "input[name='ukuran_potong']" ).val("")
    form.find( "textarea[name='keterangan']" ).val("")
    form.find( "input[name='jumlah_order']" ).val("")
    form.find( "input[name='harga_satuan']" ).val("")
    form.find( "input[name='komposisi']" ).val("")
    form.find( "input[name='jumlah_roll']" ).val("")
    form.find( "input[name='hasil_rumus']" ).val("")
}

function rerender_print_link(){
    $('#print-monthly-spl-button').prop('href', `backend/get_spl_monthly_and_print?date=${selected_date}&status=${table_status}&sort=${table_dir}&order_by=${table_order_by}&search=${table_search}`)
}

function rerender_export_link(){
    $('#export-monthly-spl-button').prop('href', `backend/get_spl_monthly_and_export?date=${selected_date}&status=${table_status}&sort=${table_dir}&order_by=${table_order_by}&search=${table_search}`)
}

function format_to_db_date(date){
    let mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}`
    return new_date
}

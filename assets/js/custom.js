jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});

$('#logout-button').click(function(){
  const SESSION = localStorage.getItem("user-token");
  $.ajax({
    type: "POST",
    url: "logout_process",
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    success: function(response) {
      console.log(response)
      localStorage.removeItem("user-token");
      localStorage.removeItem("user-refresh-token");
      window.location.href = "logout"
    }
  });
})

$(document).on('keydown', '.input_decimal', function(e){
  var input = $(this);
  var oldVal = input.val();
  var regex = new RegExp(input.attr('pattern'), 'g');

  setTimeout(function(){
    var newVal = input.val();
    if(!regex.test(newVal)){
      input.val(oldVal); 
    }
  }, 1);
});

$(document).on('change', '.input_currency', function(e){
  var oldVal = $(this).val();
  $(this).val(formatRupiah(oldVal))
});

function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function get_web_url(){
  let base_url = window.location.origin;
  return base_url.includes("localhost") ? BASE_URL : ""
}

function get_login(){
  let data = null
  $.ajax({
    async: false,
    url: `${get_web_url()}/backend/get_login`,
    type: 'GET',
    error: function(res) {
        console.log("ERROR: ", res)
    },
    success: function(res) {
      data = res.data
    }
  });

  return data
}

$(".submit-ajax").click(function(e){
  e.preventDefault();
  var form_id           = $(this).attr("form-id");
  var form              = $("#"+form_id);
  var alert_name        = $(this).attr("warning_message");
  var alert_view        = $("#"+alert_name);
  var submit_url        = form.attr('action');
//   var refresh_url       = form.attr('refresh');
  var datastring        = form.serialize();
  form.validate();
  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.info=="success"){
        alert_view.html(result.message);
        form[0].reset();
//         alert_view.fadeOut();
      }
      else if(result.info=="failed"){
        alert_view.html(result.message);
        form[0].reset();
//         alert_view.fadeOut();
      }
    },
    dataType: "json"
  });
});

$('#submit-form').submit(function(e){
  e.preventDefault()
  e.stopImmediatePropagation();
  let $form = $(this)
  let url = $form.data('url')

  // let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

  // $('#create-spl-button').html(loading_button)
  // $('#create-spl-button').prop('disabled', true);

  $.ajax({
      async: true,
      url: url,
      type: 'POST',
      data: new FormData($($form)[0]),
      processData: false, 
      contentType: false, 
      error: function(res) {
          console.log("ERROR: ", res)
      },
      success: function(res) {
          let response = JSON.parse(res);
          const status = response.info
          $('.create_modal_form_warning').html(response.message);
          if(status == 'success'){
              setTimeout(() => { 
                  location.reload(); 
              }, 1000);
          }
      }
  });
});
$('#submit-form-edit').submit(function(e){
  e.preventDefault()
  e.stopImmediatePropagation();
  let $form = $(this)
  let url = $form.data('url')

  // let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

  // $('#create-spl-button').html(loading_button)
  // $('#create-spl-button').prop('disabled', true);

  $.ajax({
      async: true,
      url: url,
      type: 'POST',
      data: new FormData($($form)[0]),
      processData: false, 
      contentType: false, 
      error: function(res) {
          console.log("ERROR: ", res)
      },
      success: function(res) {
          let response = JSON.parse(res);
          const status = response.info
          $('.edit_modal_form_warning').html(response.message);
          if(status == 'success'){
              setTimeout(() => { 
                  location.reload(); 
              }, 1000);
          }
      }
  });
});
$(".special-submit-ajax").click(function(){
 var type               = $(this).attr("submit-type");
  var form_id           = $(this).attr("form-id");
  var form              = $("#"+form_id);
  var alert_name        = $(this).attr("warning_message");
  var alert_view        = $("#"+alert_name);
  var submit_url_save   = form.attr('action-simpan');
  var submit_url_print  = form.attr('action-cetak');
//   var refresh_url       = form.attr('refresh');
  var datastring        = form.serialize();
  form.validate();
  var url               = "";
 if(type=='simpan')
  form.attr("action", submit_url_save);
 else form.attr("action", submit_url_print);
  form.submit();
});
$(".edit-ajax").click(function(e){
  e.preventDefault();
  var form_id           = $(this).attr("form-id");
  var form              = $("#"+form_id);
  var alert_name        = $(this).attr("warning_message");
  var alert_view        = $("#"+alert_name);
  var submit_url        = form.attr('action');
//   var refresh_url       = form.attr('refresh');
  var datastring        = form.serialize();
  form.validate();
  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.info=="success"){
        alert_view.html(result.message);
//         form[0].reset();
//         alert_view.fadeOut();
      }
      else if(result.info=="failed"){
        alert_view.html(result.message);
//         form[0].reset();
//         alert_view.fadeOut();
      }
    },
    dataType: "json"
  });
});
$(".edit-user").click(function(e){
  e.preventDefault
  var url = $(this).attr("url");
  $.getJSON(url, function(response){
    var result =jQuery.parseJSON( JSON.stringify(response ));
    $("#edit_name").val(result.name);
    $("#edit_username").val(result.username);
    $("#edit_id").val(result.id);
    $("#edit_phone").val(result.phone);
//     $("#edit_password").val(result.password);
    $("#edit_type").val(result.type).change();
    $("#edit_site_code").val(result.site_code).change();
    });
});
$(".edit-customer").click(function(e){
  e.preventDefault
  var url = $(this).attr("url");
  $.getJSON(url, function(response){
    var result =jQuery.parseJSON( JSON.stringify(response ));
    $("#edit_id").val(result.id);
    $("#edit_name").val(result.name);
    $("#edit_work_phone").val(result.work_phone);
    $("#edit_contact_person").val(result.contact_person);
    $("#edit_address_1").val(result.address_1);
    $("#edit_address_2").val(result.address_2);
    $("#edit_tax_non_tax").val(result.tax_non_tax).change();
    $("#edit_limit_plafon").val(result.limit_plafon);
    $("#edit_business_type").val(result.business_type).change();
    $("#nik_ktp").val(result.nik_ktp);
    $("#no_npwp").val(result.no_npwp);
    $("#no_hp").val(result.no_hp);
    $("#sumber_info").val(result.sumber_info);
    $("#tipe_lampiran").val(result.tipe_lampiran);
    $("#cara_bayar").val(result.cara_bayar);
    $("#mkt").val(result.mkt);
    $("#pembayaran_ekspedisi").val(result.pembayaran_ekspedisi);
    $("#acc_oleh").val(result.acc_oleh);
    $("#rating").val(result.rating);

    });
});
$(".edit-returan").click(function(e){
  e.preventDefault
  var url = $(this).attr("url");
  $.getJSON(url, function(response){
    var result =jQuery.parseJSON( JSON.stringify(response ));
    $("#edit_site_code").val(result.site_code).change();
    $("#id").val(result.id);
    $("#edit_tanggal_diterima").val(result.tanggal_diterima);
    $("#edit_customer_id").val(result.customer_id).change();
    $("#edit_ukuran").val(result.ukuran);
    $("#edit_jml_prod_kg").val(result.jumlah_retur_kg);
    $("#edit_jml_prod_lbr").val(result.jumlah_retur_lembar);
    $("#edit_tanda_terima").val(result.tanda_terima);
    $("#edit_alasan").val(result.alasan_retur);
    $("#edit_tindak_lanjut").val(result.tindak_lanjut);
    });
});
$(document).on('click', '.edit-returan', function(e) {
  e.preventDefault
  var url = $(this).attr("url");
  $.getJSON(url, function(response){
    var result =jQuery.parseJSON( JSON.stringify(response ));
    $("#edit_site_code").val(result.site_code).change();
    $("#id").val(result.id);
    $("#edit_tanggal_diterima").val(result.tanggal_diterima);
    $("#edit_customer_id").val(result.customer_id).change();
    $("#edit_ukuran").val(result.ukuran);
    $("#edit_jml_prod_kg").val(result.jumlah_retur_kg);
    $("#edit_jml_prod_lbr").val(result.jumlah_retur_lembar);
    $("#edit_tanda_terima").val(result.tanda_terima);
    $("#edit_alasan").val(result.alasan_retur);
    $("#edit_tindak_lanjut").val(result.tindak_lanjut);
    });
});
$(".edit-fakturin").click(function(e){
  e.preventDefault
  var url = $(this).attr("url");
  $.getJSON(url, function(response){
    var result =jQuery.parseJSON( JSON.stringify(response ));
    $("#edit_status").val(result.status).change();
    $("#edit_id").val(result.id);
    $("#edit_jumlah_bayar").val(result.jumlah_bayar);
    $("#edit_tanggal_bayar").val(result.tanggal_bayar);
    $("#edit_keterangan_pembayaran").val(result.keterangan_pembayaran);
    $("#edit_keterangan_pembayaran").html(result.keterangan_pembayaran);
    });
});
$(".edit-faktur").click(function(e){
  e.preventDefault
  var url = $(this).attr("url");
  $.getJSON(url, function(response){
    var result =jQuery.parseJSON( JSON.stringify(response ));
    $("#edit_status_lunas").val(result.status_lunas).change();
    $("#edit_id").val(result.id);
    $("#edit_jumlah_bayar").val(result.jumlah_bayar);
    $("#edit_tanggal_bayar").val(result.tanggal_bayar);
    $("#edit_keterangan_pembayaran").val(result.keterangan_pembayaran).change();
    });
});
$(".submit-btn").click(function(e){
  e.preventDefault();
  var form_id           = $(this).attr("form-id");
  var form              = $("#"+form_id);
  var submit_url        = form.attr('action');
  var refresh_url       = form.attr('refresh');
  var datastring        = form.serialize();
  $.ajax({
    type: "POST",
    url: submit_url,
    data: datastring,
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.info=="success"){
        $("#warning_message").html(result.message);
        // location.reload();
        if(refresh_url !== "") window.location = refresh_url;
        // alert(refresh_url);
      }
      else if(result.info=="failed"){
        $("#warning_message").html(result.message);
        // form[0].reset();
        // location.reload();
      }
    },
    dataType: "json"
  });
});
$(".delete-ajax").click(function(e){
  e.preventDefault();
  var url = $(this).attr("url");
  $("#delete_footer").attr("href", url);
});
$(document).on('click', '.delete-ajax', function(e) {
  e.preventDefault();
  var url = $(this).attr("url");
  $("#delete_footer").attr("href", url);
});
$(document).ready(function(){
  var date = new Date();
  var d = date.getDay();
  var  m = date.getMonth()+1;
  var y = date.getFullYear();
  $("#year_select").val(y);
  $("#month_select").val(m);
  $(".table").DataTable();
  $("form .warning_message").hide();
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    orientation: 'bottom'
});
  if($('.datepicker').val()=="")
  $('.datepicker').datepicker("setDate", new Date());
});

$("#month_select").change(function(){
  var url = $(this).attr("url");
  var year = $("#year_select").val();
  var month = $("#month_select").val();
  var flag = $("#flag_select").val();
  var customer_id = $("#customer_select").val();
  var no_mesin = $("#no_mesin").val();
  $(".load_table").load(url+"?year="+year+"&month="+month+"&flag="+flag+"&customer_id="+customer_id+"&no_mesin="+no_mesin);
});

$("#year_select").change(function(){
  var url = $(this).attr("url");
  var year = $("#year_select").val();
  var month = $("#month_select").val();
  var flag = $("#flag_select").val();
  var customer_id = $("#customer_select").val();
  var no_mesin = $("#no_mesin").val();
  $(".load_table").load(url+"?year="+year+"&month="+month+"&flag="+flag+"&customer_id="+customer_id+"&no_mesin="+no_mesin);
});
$("#no_mesin_select").change(function(){
  var url = $(this).attr("url");
  var date = $("#date_select").val();
  var year = $("#year_select").val();
  var month = $("#month_select").val();
  var flag = $("#flag_select").val();
  var customer_id = $("#customer_select").val();
  var no_mesin = $("#no_mesin_select").val();
  $(".load_table").load(url+"?date="+date+"&year="+year+"&month="+month+"&flag="+flag+"&customer_id="+customer_id+"&no_mesin="+no_mesin);
});
$("#flag_select").change(function(){
  var url = $(this).attr("url");
  var date = $("#date_select").val();
  var year = $("#year_select").val();
  var month = $("#month_select").val();
  var flag = $("#flag_select").val();
  var customer_id = $("#customer_select").val();
  $(".load_table").load(url+"?date="+date+"&year="+year+"&month="+month+"&flag="+flag+"&customer_id="+customer_id);
});
$("#date_select").change(function(){
  var url = $(this).attr("url");
  var date = $(this).val();
  var no_mesin = $("#no_mesin_select").val();
  $(".load_table").load(url+"?date="+date+"&no_mesin="+no_mesin);
});
$("#customer_select").change(function(){
  var url = $(this).attr("url");
  var date = $("#date_select").val()
  var customer_id = $("#customer_select").val();
  var year = $("#year_select").val();
  var month = $("#month_select").val();
  $(".load_table").load(url+"?date="+date+"&year="+year+"&month="+month+"&customer_id="+customer_id);
});

$("#banyaknya_barang").change(function(){
  var banyaknya_barang = $(this).val();
  var harga_barang = $("#harga_barang").val();
  $("#jumlah_tagihan").val(harga_barang*banyaknya_barang);
  var besar_persen = $("#ppn").attr("besar_persen")/100;
  var harga_total = $("#jumlah_tagihan").val();
  $("#ppn").val(harga_total*besar_persen);
});
$("#harga_barang").change(function(){
  var banyaknya_barang = $("#banyaknya_barang").val();
  var harga_barang = $(this).val();
  $("#jumlah_tagihan").val(harga_barang*banyaknya_barang);
  var besar_persen = $("#ppn").attr("besar_persen")/100;
  var harga_total = $("#jumlah_tagihan").val();
  $("#ppn").val(harga_total*besar_persen);
});
$(".delete_produk").click(function(e){
  e.preventDefault();
  var row_id = $(this).attr("id_row");
  $("#"+row_id).remove();
});
$(".produk_add").click(function(){
  var jumlah_produk = $("#jumlah_produk").val();
  var new_jumlah_produk = parseInt(jumlah_produk)+1;
  var table = $("#produk_add_table");
  $("#jumlah_produk").val(new_jumlah_produk);
  var new_row = '<tr id="row_'+new_jumlah_produk+'"><td><input type="text" class="form-control" name="banyaknya_barang_'+new_jumlah_produk+'"></td><td><input type="text" class="form-control" name="no_faktur_'+new_jumlah_produk+'"></td><td><select name="tipe_berat_barang_'+new_jumlah_produk+'" class="form-control"><option value="">Choose..</option><option value="kg">Kg</option><option value="lb">Lb</option><option value="rl">Rl</option></select></td><td><input type="text" class="form-control" name="nama_barang_'+new_jumlah_produk+'"></td><td><input type="text" class="form-control" name="harga_barang_'+new_jumlah_produk+'"></td><td><input type="button" class="delete_produk btn-danger btn" value="Hapus" onclick="$(\'#row_'+new_jumlah_produk+'\').remove();$(\'#jumlah_produk\').val('+jumlah_produk+')" id_row="row_'+new_jumlah_produk+'"></td></tr>';
  table.append(new_row);
});
$(".produk_add_piutang").click(function(){
  var jumlah_produk = $("#jumlah_produk").val();
  var new_jumlah_produk = parseInt(jumlah_produk)+1;
  var table = $("#produk_add_table");
  $("#jumlah_produk").val(new_jumlah_produk);
  var new_row = '<tr id="row_'+new_jumlah_produk+'"><td><input type="text" class="form-control" name="banyaknya_barang_'+new_jumlah_produk+'"></td><td><select name="tipe_berat_barang_'+new_jumlah_produk+'" class="form-control"><option value="">Choose..</option><option value="kg">Kg</option><option value="lb">Lb</option><option value="rl">Rl</option></select></td><td><input type="text" class="form-control" name="nama_barang_'+new_jumlah_produk+'"></td><td><input type="text" class="form-control" name="no_spl_barang_'+new_jumlah_produk+'"></td><td><input type="text" " class="form-control" name="harga_barang_'+new_jumlah_produk+'"></td><td></td><td><input type="button" class="delete_produk btn-danger btn" value="Hapus" onclick="$(\'#row_'+new_jumlah_produk+'\').remove();$(\'#jumlah_produk\').val('+jumlah_produk+')" id_row="row_'+new_jumlah_produk+'"></td></tr>';
  table.append(new_row);
});
$(".sj_add").click(function(){
  var jumlah_sj = $("#jumlah_sj").val();
  var new_jumlah_sj = parseInt(jumlah_sj)+1;
  var table = $("#sj_add_table");
  $("#jumlah_sj").val(new_jumlah_sj);
  var new_row = '<tr id="row_sj_'+new_jumlah_sj+'"><td><input type="number" class="form-control" name="no_sj_'+new_jumlah_sj+'"></td><td><input type="button" class="delete_produk btn-danger btn" value="Hapus" onclick="$(\'#row_sj_'+new_jumlah_sj+'\').remove();$(\'#jumlah_sj\').val('+jumlah_sj+')" id_row="row_sj_'+new_jumlah_sj+'"></td></tr>';
  table.append(new_row);
});
$(".tambah_faktur").click(function(e){
  e.preventDefault();
  var faktur = $("#faktur_id");
  var val    = faktur.val();
  var id     = $(this).attr("id");
  if(val=="") faktur.val(id);
  else faktur.val(val+','+id);
});
$("#sj_no_search_btn").click(function(){
  var submit_url = $("#sj_no_search").attr("data-url");
  var jumlah_sj = parseInt($("#jumlah_no_sj").val());
  var sj_no = $("#sj_no_search").val();
   $.ajax({
    type: "POST",
    url: submit_url,
    data: {jumlah_sj:jumlah_sj, no_sj:sj_no},
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.info=="success"){
        $("#info_sj").show();
        $("#info_sj").html(result.success_message);
        $("#sj_list").append(result.message);
        $("#jumlah_no_sj").val(jumlah_sj+1);
        var sj_prev = $("#sj").val();
        if(sj_prev!="")
        $("#sj").val(sj_prev+","+sj_no);
        else $("#sj").val(sj_no);
        $("#sj_no_search").val('');
        $("#info_sj").delay(5000).fadeOut(400);
      }
      else if(result.info=="failed"){
        $("#info_sj").show();
        $("#info_sj").html(result.message);
        $("#info_sj").delay(5000).fadeOut(400);
      }
    },
    dataType: "json"
  });
});
$("#faktur_no_search_btn").click(function(){
  var submit_url = $("#faktur_no_search").attr("data-url");
  var jumlah_faktur = parseInt($("#jumlah_no_faktur").val());
  var faktur_no = $("#faktur_no_search").val();
   $.ajax({
    type: "POST",
    url: submit_url,
    data: {faktur_no_search:faktur_no},
    success: function(response) {
      var result =jQuery.parseJSON( JSON.stringify(response ));
      if(result.info=="success"){
        $("#info_faktur").show();
        $("#info_faktur").html(result.success_message);
        $("#faktur_list").append(result.message);
        $("#jumlah_no_faktur").val(jumlah_faktur+1);
        var faktur_prev = $("#faktur").val();
        if(faktur_prev!="")
        $("#faktur").val(faktur_prev+","+faktur_no);
        else $("#faktur").val(faktur_no);
        $("#faktur_no_search").val('');
        $("#info_faktur").delay(5000).fadeOut(400);
      }
      else if(result.info=="failed"){
        $("#info_faktur").show();
        $("#info_faktur").html(result.message);
        $("#info_faktur").delay(5000).fadeOut(400);
      }
    },
    dataType: "json"
  });
});
$(document).on('click', '.delete-sj', function(e) {
  e.preventDefault();
  var sj_no = $(this).attr('sj_no');
  var sj_prev = $("#sj").val().replace(sj_no,'');
  var sj_id = $(this).attr("sj_id");
  $("#sj").val(sj_prev);
  $("#"+sj_id).remove();
  $("#"+sj_id).fadeOut();
});

$(document).on('click', '.delete-faktur', function(e) {
  e.preventDefault();
  var faktur_no = $(this).attr('faktur_no');
  var faktur_prev = $("#faktur").val().replace(faktur_no,'');
  var faktur_id = $(this).attr("faktur_id");
  $("#faktur").val(faktur_prev);
  $("#"+faktur_id).remove();
  $("#"+faktur_id).fadeOut();
});
$(".email_laporan").click(function(e){
    e.preventDefault();
    var date = $("#date_select").val();
    var year = $("#year_select").val();
    var month = $("#month_select").val();
    var no_mesin = $("#no_mesin_select").val();
    var flag = $("#flag").val();
    var customer_id = $("#customer_id_select").val();
    $("#year").val(year);
    $("#month").val(month);
    $("#no_mesin").val(no_mesin);
    $("#flag").val(flag);
    $("#customer_id").val(customer_id);
    $("#date").val(date);
})







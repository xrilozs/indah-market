let office_hour_id = null,
    URL = 'hari-libur',
    selected_date = format_to_db_date(new Date())

$(document).ready(function(){
    $("#filter_tanggal").val(selected_date)
    $(".yearly-datepicker").datepicker( {
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });

    //data table
    let hari_libur_table = $('#hari_libur_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: `${URL}/list`,
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let year = selected_date
            
            newObj.year = year
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.sort = d.order[0].dir
            switch (column) {
                case 0:
                    newObj.order_by = 'date'
                    break
                case 1:
                    newObj.order_by = 'description'
                    break
                default:
                    newObj.order_by = 'date'
                    break
            }

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "date",
                orderable: true
            },
            { 
                data: "description",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-center",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = `<button class="btn btn-success update-hari-libur-toggle" data-id="${data}" data-toggle="modal" data-target="#update-hari-libur-modal">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Update
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-danger delete-hari-libur-toggle" data-id="${data}" data-toggle="modal" data-target="#delete-hari-libur-modal">
                            <i class="fa fa-trash" aria-hidden="true"></i> Delete
                        </button>`
                    
                    return button
                },
                orderable: false
            }
        ]
    });

    $('#filter_tanggal').change(function(){
        selected_date = $(this).val()
        hari_libur_table.draw()
    })
})

$("body").delegate(".update-hari-libur-toggle", "click", function(e) {
    clear_office_hour_form()
    office_hour_id = $(this).data('id')
    console.log("ID:", office_hour_id)
    $('#update-hari-libur-toggle').prop('disabled', true)
    $.ajax({
        async: true,
        url: `${URL}/detail/${office_hour_id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
            $('#update-hari-libur-toggle').prop('disabled', false)
        },
        success: function(res) {
            console.log("RESPONSE SUCCESS: ", res)
            fill_office_hour_form(res.data)
            $('#update-hari-libur-toggle').prop('disabled', false)
        }
    });
})

$("body").delegate(".delete-hari-libur-toggle", "click", function(e) {
    office_hour_id = $(this).data('id')
})

$('#create-hari-libur-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-hari-libur-button').html(loading_button)
    $('#create-hari-libur-button').prop('disabled', true);

    let $form = $('#create-hari-libur-form'),
        request = {
            date: $form.find("input[name='date']").val(),
            description: $form.find("input[name='description']").val()
        }

    $.ajax({
        async: true,
        url: `${URL}`,
        type: 'POST',
        data: JSON.stringify(request),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Hari Libur gagal ditambah!</a>.</div>`
            $('#create-hari-libur-button').html('Create Hari Libur')
            $('#create-hari-libur-button').prop('disabled', false)
            $('#create-hari-libur-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Hari Libur telah ditambah!</a>.</div>`
            $('#create-hari-libur-button').html('Create Hari Libur')
            $('#create-hari-libur-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#update-hari-libur-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-hari-libur-button').html(loading_button)
    $('#update-hari-libur-button').prop('disabled', true);

    let $form = $('#update-hari-libur-form'),
        request = {
            id: office_hour_id,
            date: $form.find("input[name='date']").val(),
            description: $form.find("input[name='description']").val()
        }

    $.ajax({
        async: true,
        url: `${URL}`,
        type: 'PUT',
        data: JSON.stringify(request),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-hari-libur-button').html('Update')
            $('#update-hari-libur-button').prop('disabled', false)
            $('#update-hari-libur-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Hari Libur berhasil diubah!</a>.</div>`
            $('#update-hari-libur-button').html('Update')
            $('#update-hari-libur-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#delete-hari-libur-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delete-hari-libur-button').html(loading_button)
    $('#delete-hari-libur-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `${URL}/delete/${office_hour_id}`,
        type: 'DELETE',
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#delete-hari-libur-button').html('IYA')
            $('#delete-hari-libur-button').prop('disabled', false)
            $('#delete-hari-libur-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Hari Libur berhasil dihapus!</a>.</div>`
            $('#delete-hari-libur-button').html('IYA')
            $('#delete-hari-libur-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function fill_office_hour_form(office_hour){
    let form = $(`#update-hari-libur-form`)
    // $('.datepicker').data({date: office_hour.date});
    // $('.datepicker').datepicker('update');
    $('.datepicker').datepicker('update', office_hour.date);
    form.find( "input[name='date']" ).val(office_hour.date)
    form.find( "input[name='description']" ).val(office_hour.description)
}
    
function clear_office_hour_form(){
    let form = $(`#update-hari-libur-form`)
    form.find( "input[name='date']" ).val("")
    form.find( "input[name='description']" ).val("")
}

function format_to_db_date(date){
    let year = date.getFullYear()
    return year
}

let keyword_id = null,
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'id'

$(document).ready(function(){
    //data table
    let keyword_table = $('#keyword_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_keyword',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.sort = d.order[0].dir
            table_dir = d.order[0].dir
            switch (column) {
                case 0:
                    newObj.order_by = 'module'
                    break
                case 1:
                    newObj.order_by = 'description'
                    break
                case 2:
                    newObj.order_by = 'status'
                    break
                case 3:
                    newObj.order_by = 'datecreated'
                    break
                default:
                    newObj.order_by = 'module'
                    break
            }
            table_order_by = newObj.order_by

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "module",
                orderable: true
            },
            { 
                data: "description",
                orderable: true
            },
            { 
                data: "status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'ACTIVE'){
                        label = `<span class="label label-success">${data}</span>`
                    }else if(data == 'INACTIVE'){
                        label = `<span class="label label-danger">${data}</span>`
                    }
                    return label
                }
            },
            { 
                data: "datecreated",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-center",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = `<button class="btn btn-success update-keyword-toggle" data-id="${data}" data-toggle="modal" data-target="#update-keyword-modal">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Update
                        </button>
                        &nbsp;&nbsp;`
                    if(row.status == 'ACTIVE'){
                        button += `<button class="btn btn-danger inactive-keyword-toggle" data-id="${data}" data-toggle="modal" data-target="#inactive-keyword-modal">
                            <i class="fa fa-ban" aria-hidden="true"></i> Inactive
                        </button>`
                    }else if(row.status == 'INACTIVE'){
                        button += `<button class="btn btn-primary active-keyword-toggle" data-id="${data}" data-toggle="modal" data-target="#active-keyword-modal">
                            <i class="fa fa-check" aria-hidden="true"></i> Active
                        </button>`
                    }
                    
                    return button
                },
                orderable: false
            }
        ]
    });
})

$("body").delegate(".update-keyword-toggle", "click", function(e) {
    clear_keyword_form()
    keyword_id = $(this).data('id')
    console.log("ID:", keyword_id)
    $('#update-keyword-toggle').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_keyword_detail?id=${keyword_id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
            $('#update-keyword-toggle').prop('disabled', false)
        },
        success: function(res) {
            console.log("RESPONSE SUCCESS: ", res)
            fill_keyword_form(res.data)
            $('#update-keyword-toggle').prop('disabled', false)
        }
    });
})

$("body").delegate(".active-keyword-toggle", "click", function(e) {
    keyword_id = $(this).data('id')
})

$("body").delegate(".inactive-keyword-toggle", "click", function(e) {
    keyword_id = $(this).data('id')
})

$('#create-keyword-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-keyword-button').html(loading_button)
    $('#create-keyword-button').prop('disabled', true);

    let $form = $('#create-keyword-form'),
        request = {
            module: $('#module').find(":selected").val(),
            description: $form.find("textarea[name='description']").val(),
            keyword: $form.find("input[name='keyword']").val()
        }


    $.ajax({
        async: true,
        url: `backend/create_keyword_process`,
        type: 'POST',
        data: JSON.stringify(request),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Keyword gagal ditambah!</a>.</div>`
            $('#create-keyword-button').html('Create Keyword')
            $('#create-keyword-button').prop('disabled', false)
            $('#create-keyword-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Keyword telah ditambah!</a>.</div>`
            $('#create-keyword-button').html('Create Keyword')
            $('#create-keyword-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#update-keyword-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-keyword-button').html(loading_button)
    $('#update-keyword-button').prop('disabled', true);

    let $form = $('#update-keyword-form'),
        request = {
            id: keyword_id,
            description: $form.find("textarea[name='description']").val(),
            keyword: $form.find("input[name='keyword']").val()
        }

    $.ajax({
        async: true,
        url: `backend/update_keyword_process`,
        type: 'POST',
        data: JSON.stringify(request),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-keyword-button').html('Update')
            $('#update-keyword-button').prop('disabled', false)
            $('#update-keyword-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Keyword berhasil diubah!</a>.</div>`
            $('#update-keyword-button').html('Update')
            $('#update-keyword-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});


$('#active-keyword-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#active-keyword-button').html(loading_button)
    $('#active-keyword-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/active_keyword_process`,
        type: 'POST',
        data: JSON.stringify({id: keyword_id}),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#active-keyword-button').html('IYA')
            $('#active-keyword-button').prop('disabled', false)
            $('#active-keyword-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Keyword berhasil diaktifkan!</a>.</div>`
            $('#active-keyword-button').html('IYA')
            $('#active-keyword-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#inactive-keyword-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#inactive-keyword-button').html(loading_button)
    $('#inactive-keyword-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/inactive_keyword_process`,
        type: 'POST',
        data: JSON.stringify({id: keyword_id}),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#inactive-keyword-button').html('IYA')
            $('#inactive-keyword-button').prop('disabled', false)
            $('#inactive-keyword-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Keyword berhasil dinonaktifkan!</a>.</div>`
            $('#inactive-keyword-button').html('IYA')
            $('#inactive-keyword-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function fill_keyword_form(keyword){
    let form = $(`#update-keyword-form`)
    form.find( "input[name='module']" ).val(keyword.module)
    form.find( "input[name='keyword']" ).val("")
    form.find( "textarea[name='description']" ).val(keyword.description)
}
    
function clear_keyword_form(){
    let form = $(`#update-keyword-form`)
    form.find( "input[name='keyword']" ).val("")
    form.find( "textarea[name='description']" ).val("")
}

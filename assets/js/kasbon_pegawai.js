let user_login = get_login(),
    kasbon_id = null,
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'id',
    selected_date_awal = format_to_db_date(new Date()),
    selected_date_akhir = format_to_db_date(new Date()),
    is_keyword_mandatory = true

$('.selectpicker').selectpicker();

$(document).ready(function(){
    //get keyword setting
    get_keyword()
    //data table
    let kasbon_table = $('#kasbon_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_kasbon_pegawai',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let start_date = selected_date_awal
            let end_date = selected_date_akhir
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.start_date = start_date
            newObj.end_date = end_date
            newObj.sort = d.order[0].dir
            table_dir = d.order[0].dir
            switch (column) {
                case 2:
                    newObj.order_by = 'jumlah_kasbon'
                    break
                case 3:
                    newObj.order_by = 'alasan_kasbon'
                    break
                case 4:
                    newObj.order_by = 'potong_mingguan'
                    break
                case 5:
                    newObj.order_by = 'potong_bulanan'
                    break
                case 6:
                    newObj.order_by = 'tanggal_kasbon'
                    break
                default:
                    newObj.order_by = 'id'
                    break
            }
            table_order_by = newObj.order_by
            rerender_print_link()

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "pegawai_nip",
                orderable: true
            },
            { 
                data: "pegawai_name",
                orderable: true
            },
            { 
                data: "jumlah_kasbon",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            { 
                data: "alasan_kasbon",
                orderable: true
            },
            { 
                data: "potong_mingguan",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            { 
                data: "potong_bulanan",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            { 
                data: "tanggal_kasbon",
                orderable: true
            },
            {
                data: "id",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = `<button class="btn btn-success edit-kasbon-toggle" data-id="${data}" data-toggle="modal" data-target="#update-kasbon-modal">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-danger delete-kasbon-toggle" data-id="${data}" data-toggle="modal" data-target="#delete-kasbon-modal">
                            <i class="fa fa-trash" aria-hidden="true"></i> Delete
                        </button>`
                    
                    return button
                },
                orderable: false
            }
        ]
    });

    let select_create = $('#pegawai_id_create').select2({
        width: '100%',
        placeholder: "Ketik nama atau NIP..",
        ajax: {
          url: 'backend/get_pegawai_all',
          data: function (params) {
            var query = {
              q: params.term
            }
      
            // Query parameters will be ?search=[term]&type=public
            return query;
          },
          processResults: function (data) {
            // Transforms the top-level key of the response object from 'items' to 'results'
            return {
              results: JSON.parse(data)
            };
          }
        }
    });
    select_create.data('select2').$selection.css('height', '34px');

    $('#filter_tanggal_awal').change(function(){
        selected_date_awal = $(this).val()
        kasbon_table.draw()
    })
    $('#filter_tanggal_akhir').change(function(){
        selected_date_akhir = $(this).val()
        kasbon_table.draw()
    })
})

$("body").delegate(".edit-kasbon-toggle", "click", function(e) {
    clear_kasbon_form()
    kasbon_id = $(this).data('id')
    console.log("ID:", kasbon_id)
    $('#update-kasbon-toggle').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_kasbon_pegawai_detail?id=${kasbon_id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
            $('#update-kasbon-toggle').prop('disabled', false)
        },
        success: function(res) {
            console.log("RESPONSE SUCCESS: ", res)
            fill_kasbon_form(res.data)
            $('#update-kasbon-toggle').prop('disabled', false)
        }
    });
})

$("body").delegate(".delete-kasbon-toggle", "click", function(e) {
    console.log("DELETE")
    kasbon_id = $(this).data('id')
})

$('#create-kasbon-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-kasbon-button').html(loading_button)
    $('#create-kasbon-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/create_kasbon_pegawai_process`,
        type: 'POST',
        data: new FormData($('#create-kasbon-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> kasbon gagal ditambah!</a>.</div>`
            $('#create-kasbon-button').html('Tambah Kasbon')
            $('#create-kasbon-button').prop('disabled', false)
            $('#create-kasbon-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> kasbon telah ditambah!</a>.</div>`
            $('#create-kasbon-button').html('Tambah Kasbon')
            $('#create-kasbon-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#update-kasbon-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-kasbon-button').html(loading_button)
    $('#update-kasbon-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_kasbon_pegawai_process`,
        type: 'POST',
        data: new FormData($('#update-kasbon-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-kasbon-button').html('Update Kasbon')
            $('#update-kasbon-button').prop('disabled', false)
            $('#update-kasbon-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> kasbon berhasil diubah!</a>.</div>`
            $('#update-kasbon-button').html('Update Kasbon')
            $('#update-kasbon-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#delete-kasbon-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delete-kasbon-button').html(loading_button)
    $('#delete-kasbon-button').prop('disabled', true);

    let data = {
        id: kasbon_id
    }
    if(is_keyword_mandatory){
        data.keyword = $('#keyword-delete').val()
    }

    $.ajax({
        async: true,
        url: `backend/delete_kasbon_pegawai_process`,
        type: 'POST',
        data: JSON.stringify(data),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#delete-kasbon-button').html('IYA')
            $('#delete-kasbon-button').prop('disabled', false)
            $('#delete-kasbon-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> kasbon berhasil dihapus!</a>.</div>`
            $('#delete-kasbon-button').html('IYA')
            $('#delete-kasbon-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function format_to_db_date(date){
    let dd = String(date.getDate()).padStart(2, '0'),
        mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}-${dd}`
    return new_date
}

function fill_kasbon_form(kasbon){
    console.log("FORM: ", kasbon)
    let form = $(`#update-kasbon-form`)
    form.find( "input[name='id']" ).val(kasbon.id)
    // form.find( "input[name='pegawai_id']" ).val(kasbon.pegawai_id)
    form.find( "input[name='jumlah_kasbon']" ).val(formatRupiah(kasbon.jumlah_kasbon))
    form.find( "textarea[name='alasan_kasbon']" ).val(kasbon.alasan_kasbon)
    form.find( "input[name='potong_mingguan']" ).val(formatRupiah(kasbon.potong_mingguan))
    form.find( "input[name='potong_bulanan']" ).val(formatRupiah(kasbon.potong_bulanan))
    form.find( "input[name='tanggal_kasbon']" ).val(kasbon.tanggal_kasbon)
    $('#pegawai_id_update').html(`<option value="${kasbon.pegawai_id}" selected>(${kasbon.pegawai_nip}) ${kasbon.pegawai_name}</option>`)

    let select_update = $('#pegawai_id_update').select2({
        width: '100%',
        placeholder: "Ketik nama atau NIP..",
        ajax: {
          url: 'backend/get_pegawai_all',
          data: function (params) {
            var query = {
              q: params.term
            }
      
            // Query parameters will be ?search=[term]&type=public
            return query;
          },
          processResults: function (data) {
            // Transforms the top-level key of the response object from 'items' to 'results'
            return {
              results: JSON.parse(data)
            };
          }
        }
    });
    select_update.data('select2').$selection.css('height', '34px');
}
    
function clear_kasbon_form(){
    let form = $(`#update-kasbon-form`)
    form.find( "input[name='pegawai_id']" ).val("")
    form.find( "input[name='jumlah_kasbon']" ).val("")
    form.find( "textarea[name='alasan_kasbon']" ).val("")
    form.find( "input[name='potong_mingguan']" ).val(formatRupiah(""))
    form.find( "input[name='potong_bulanan']" ).val("")
    form.find( "input[name='tanggal_kasbon']" ).val("")
}

function rerender_print_link(){
    $('#kasbon_print').prop('href', `backend/get_kasbon_pegawai_and_print?sort=${table_dir}&order_by=${table_order_by}&search=${table_search}&start_date=${selected_date_awal}&end_date=${selected_date_akhir}`)
}

function get_keyword(){
    $.ajax({
        async: true,
        url: `backend/get_keyword_by_module?module=KASBON`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
            $('#keyword-create-field').hide()
            $('#keyword-update-field').hide()
            $('#keyword-delete-field').hide()
            $('#keyword-create').prop("required", false)
            $('#keyword-update').prop("required", false)
            $('#keyword-delete').prop("required", false)
            is_keyword_mandatory = false
        },
        success: function(res) {
            console.log("SUCCESS: ", res)
            is_keyword_mandatory = true
        }
    });
}
let user_login = get_login(),
    pegawai_id = null,
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'id',
    is_keyword_mandatory = true

$('.selectpicker').selectpicker();

$(document).ready(function(){
    //get keyword setting
    get_keyword()
    //data table
    let pegawai_table = $('#pegawai_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_pegawai',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.sort = d.order[0].dir
            table_dir = d.order[0].dir
            switch (column) {
                case 0:
                    newObj.order_by = 'nip'
                    break
                case 1:
                    newObj.order_by = 'nama'
                    break
                case 2:
                    newObj.order_by = 'jabatan'
                    break
                case 3:
                    newObj.order_by = 'role'
                    break
                case 4:
                    newObj.order_by = 'tanggal_masuk'
                    break
                case 5:
                    newObj.order_by = 'jumlah_kasbon'
                    break
                case 6:
                    newObj.order_by = 'upah_harian'
                    break
                case 7:
                    newObj.order_by = 'upah_bulanan'
                    break
                case 8:
                    newObj.order_by = 'upah_lembur'
                    break
                case 9:
                    newObj.order_by = 'upah_libur'
                    break
                default:
                    newObj.order_by = 'nip'
                    break
            }
            table_order_by = newObj.order_by
            rerender_print_link()

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "nip",
                orderable: true
            },
            { 
                data: "nama",
                orderable: true
            },
            { 
                data: "jabatan",
                orderable: true
            },
            { 
                data: "role",
                orderable: true
            },
            { 
                data: "tanggal_masuk",
                orderable: true
            },
            { 
                data: "jumlah_kasbon",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            { 
                data: "upah_harian",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            {
                data: "upah_bulanan",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            {
                data: "upah_lembur",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            {
                data: "upah_libur",
                orderable: true,
                render: function (data, type, row, meta) {
                    return formatRupiah(data)
                }
            },
            {
                data: "id",
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    console.log("ROW: ", row)
                    let button = `<button class="btn btn-success edit-pegawai-toggle" data-id="${data}" data-toggle="modal" data-target="#update-pegawai-modal">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                        </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-danger delete-pegawai-toggle" data-id="${data}" data-toggle="modal" data-target="#delete-pegawai-modal">
                            <i class="fa fa-trash" aria-hidden="true"></i> Delete
                        </button>`
                    
                    return button
                },
                orderable: false
            }
        ]
    });
})

$('#create-pegawai-toggle').click(function (){
    if(user_login.type == 'super_admin'){
        $('#password-create-section').css("display", "block");
    }else{
        $('#password-create').prop("required", false);
    }
})

$("body").delegate(".edit-pegawai-toggle", "click", function(e) {
    clear_pegawai_form()
    pegawai_id = $(this).data('id')
    console.log("ID:", pegawai_id)
    $('#update-pegawai-toggle').prop('disabled', true)

    if(user_login.type == 'super_admin'){
        $('#password-update-section').css("display", "block");
    }else{
        $('#password-update').prop("required", false);
    }
    
    $.ajax({
        async: true,
        url: `backend/get_pegawai_detail?id=${pegawai_id}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
            $('#update-pegawai-toggle').prop('disabled', false)
        },
        success: function(res) {
            console.log("RESPONSE SUCCESS: ", res)
            fill_pegawai_form(res.data)
            $('#update-pegawai-toggle').prop('disabled', false)
        }
    });
})

$("body").delegate(".delete-pegawai-toggle", "click", function(e) {
    console.log("DELETE")
    pegawai_id = $(this).data('id')
})

$('#create-pegawai-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-pegawai-button').html(loading_button)
    $('#create-pegawai-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/create_pegawai_process`,
        type: 'POST',
        data: new FormData($('#create-pegawai-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Pegawai gagal ditambah!</a>.</div>`
            $('#create-pegawai-button').html('Tambah')
            $('#create-pegawai-button').prop('disabled', false)
            $('#create-pegawai-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Pegawai telah ditambah!</a>.</div>`
            $('#create-pegawai-button').html('Tambah')
            $('#create-pegawai-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#update-pegawai-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-pegawai-button').html(loading_button)
    $('#update-pegawai-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_pegawai_process`,
        type: 'POST',
        data: new FormData($('#update-pegawai-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-pegawai-button').html('Update')
            $('#update-pegawai-button').prop('disabled', false)
            $('#update-pegawai-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Pegawai berhasil diubah!</a>.</div>`
            $('#update-pegawai-button').html('Update')
            $('#update-pegawai-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#delete-pegawai-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delete-pegawai-button').html(loading_button)
    $('#delete-pegawai-button').prop('disabled', true);

    let data = {
        id: pegawai_id
    }
    if(is_keyword_mandatory){
        data.keyword = $('#keyword-delete').val()
    }

    $.ajax({
        async: true,
        url: `backend/delete_pegawai_process`,
        type: 'POST',
        data: JSON.stringify(data),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#delete-pegawai-button').html('IYA')
            $('#delete-pegawai-button').prop('disabled', false)
            $('#delete-pegawai-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Pegawai berhasil dihapus!</a>.</div>`
            $('#delete-pegawai-button').html('IYA')
            $('#delete-pegawai-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function get_requested_pegawai(){
    $('#multiple_approve_button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_pegawai?page_number=0&page_size=100&date=${selected_date}&approval_status=REQUESTED&order_by=no_pegawai&sort=DESC`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
            $('#multiple_approve_button').prop('disabled', false)
        },
        success: function(res) {
            generate_pegawai_option(res.data)
            $('#multiple_approve_button').prop('disabled', false)
        }
    });
}

function get_keyword(){
    $.ajax({
        async: true,
        url: `backend/get_keyword_by_module?module=PEGAWAI`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
            $('#keyword-update-field').hide()
            $('#keyword-delete-field').hide()
            $('#keyword-update').prop("required", false)
            $('#keyword-delete').prop("required", false)
            is_keyword_mandatory = false
        },
        success: function(res) {
            console.log("SUCCESS: ", res)
            is_keyword_mandatory = true
        }
    });
}

function generate_pegawai_option(list){
    let pegawai_option_html = ``
    for(let item of list){
        let option_html = `<option value="${item.id}">(${item.no_pegawai}) ${item.customer_name}</option>`
        pegawai_option_html += option_html
    }
    $('#list_approve').html(pegawai_option_html)
    $('.selectpicker').selectpicker('refresh')
}

function format_to_db_date(date){
    let dd = String(date.getDate()).padStart(2, '0'),
        mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}-${dd}`
    return new_date
}

function fill_pegawai_form(pegawai){
    let form = $(`#update-pegawai-form`)
    form.find( "input[name='id']" ).val(pegawai.id)
    form.find( "input[name='nama']" ).val(pegawai.nama)
    form.find( "textarea[name='alamat']" ).val(pegawai.alamat)
    form.find( "input[name='telepon']" ).val(pegawai.telepon)
    // form.find( "input[name='jabatan']" ).val(pegawai.jabatan)
    $('#jabatan-update').val(pegawai.jabatan).change()
    $('#role-update').val(pegawai.role).change()
    form.find( "input[name='tanggal_masuk']" ).val(pegawai.tanggal_masuk)
    form.find( "input[name='jumlah_kasbon']" ).val(formatRupiah(pegawai.jumlah_kasbon))
    form.find( "input[name='upah_harian']" ).val(formatRupiah(pegawai.upah_harian))
    form.find( "input[name='upah_bulanan']" ).val(formatRupiah(pegawai.upah_bulanan))
    form.find( "input[name='upah_lembur']" ).val(formatRupiah(pegawai.upah_lembur))
    form.find( "input[name='upah_libur']" ).val(formatRupiah(pegawai.upah_libur))
    if(user_login.type == 'super_admin'){
        form.find( "input[name='password']" ).val(pegawai.password)
    }
}
    
function clear_pegawai_form(){
    let form = $(`#update-pegawai-form`)
    form.find( "input[name='id']" ).val("")
    form.find( "input[name='nama']" ).val("")
    form.find( "textarea[name='alamat']" ).val("")
    form.find( "input[name='telepon']" ).val("")
    // form.find( "input[name='jabatan']" ).val("")
    $('#jabatan-create').val("Ka.Bag").change()
    $('#role-create').val("PEGAWAI").change()
    form.find( "input[name='tanggal_masuk']" ).val("")
    form.find( "input[name='jumlah_kasbon']" ).val("")
    form.find( "input[name='upah_harian']" ).val("")
    form.find( "input[name='upah_bulanan']" ).val("")
    form.find( "input[name='upah_lembur']" ).val("")
    form.find( "input[name='upah_libur']" ).val("")
    if(user_login.type == 'super_admin'){
        form.find( "input[name='password']" ).val("")
    }
}

function rerender_print_link(){
    $('#pegawait_print').prop('href', `backend/get_pegawai_and_print?sort=${table_dir}&order_by=${table_order_by}&search=${table_search}`)
}
let user_login = get_login(),
    selected_date = format_to_db_date(new Date())

$('.selectpicker').selectpicker();
$("#filter_tanggal").val(selected_date)
$(".monthly-datepicker").datepicker( {
    format: "yyyy-mm",
    viewMode: "months", 
    minViewMode: "months",
    orientation: 'bottom'
});


$(document).ready(function(){
    //data table
    let burn_spl_table = $('#burn-spl-table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_spl_monthly',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let date = $('#filter_tanggal').val()
            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            newObj.status = "HANGUS"
            switch (column) {
                case 0:
                    newObj.order_by = 'no_spl'
                    break
                case 1:
                    newObj.order_by = 'customer_id'
                    break
                case 3:
                    newObj.order_by = 'user_id'
                    break
                case 4:
                    newObj.order_by = 'hasil_rumus'
                    break
                case 5:
                    newObj.order_by = 'jumlah_order'
                    break
                case 6:
                    newObj.order_by = 'harga_satuan'
                    break
                case 7:
                    newObj.order_by = 'lebar_bahan'
                    break
                case 8:
                    newObj.order_by = 'ukuran_potong'
                    break
                case 9:
                    newObj.order_by = 'komposisi'
                    break
                case 10:
                    newObj.order_by = 'jumlah_roll'
                    break
                case 11:
                    newObj.order_by = 'keterangan'
                    break
                case 12:
                    newObj.order_by = 'approval_status'
                    break
                case 13:
                    newObj.order_by = 'spl_status'
                    break
                case 14:
                    newObj.order_by = 'tanggal_spl'
                    break
                default:
                    newObj.order_by = 'no_spl'
                    break
            }

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "no_spl",
                orderable: true
            },
            { 
                data: "customer_name",
                orderable: true
            },
            { 
                data: "customer_rating",
                orderable: false,
                render: function (data, type, row, meta) {
                    return data ? data : 'N/A'
                }
            },
            { 
                data: "marketing_name",
                orderable: true
            },
            { 
                data: "hasil_rumus",
                orderable: true
            },
            { 
                data: "jumlah_order",
                orderable: true
            },
            { 
                data: "harga_satuan",
                orderable: true
            },
            {
                data: "lebar_bahan",
                orderable: true,
                render: function (data, type, row, meta) {
                    let bahan = `${row.lebar_bahan}/${row.tebal_bahan}`
                    return bahan
                }
            },
            {
                data: "ukuran_potong",
                orderable: true
            },
            {
                data: "komposisi",
                orderable: true
            },
            {
                data: "jumlah_roll",
                orderable: true
            },
            {
                data: "keterangan",
                orderable: true
            },
            {
                data: "approval_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'REQUESTED'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'APPROVED'){
                        label = `<span class="label label-success">${data}</span>`
                    }else if(data == 'TIDAK_APPROVED'){
                        label = `<span class="label label-danger">REJECTED</span>`
                    }
                    return label
                }
            },
            {
                data: "spl_status",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = ''
                    if(data == 'SELESAI'){
                        label = `<span class="label label-primary">${data}</span>`
                    }else if(data == 'BELUM'){
                        label = `<span class="label label-default">${data}</span>`
                    }else if(data == 'DITITIPKAN'){
                        label = `<span class="label label-warning">${data}</span>`
                    }else if(data == 'HANGUS'){
                        label = `<span class="label label-danger">${data}</span>`
                    }
                    return label
                }
            },
            {
                data: "tanggal_spl",
                orderable: true
            }
        ]
    });

    $('#filter_tanggal').change(function(){
        selected_date = $(this).val()
        burn_spl_table.draw()
    })

    $('#burn-spl-toggle').click(function(){
        const date_arr = selected_date.split("-"),
              year = date_arr[0],
              month = date_arr[1]
        
        const month_str = convert_month_to_string(month)
        $('#tanggal_text').val(`${month_str} ${year}`)
        $('#tanggal').val(selected_date)
    })

    let select2 = $('.phone-number-option').select2({
        width: '100%',
        placeholder: "Ketik Nomor Telepon..",
        tags: true,
        ajax: {
          url: 'backend/get_phone_number',
          data: function (params) {
            var query = {
              q: params.term
            }
      
            return query;
          },
          processResults: function (data) {
            return {
              results: JSON.parse(data)
            };
          }
        }
    });
})

$('#burn-spl-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#burn-spl-button').html(loading_button)
    $('#burn-spl-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/burn_spl_process`,
        type: 'POST',
        data: new FormData($('#burn-spl-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            $('#burn-spl-button').html('Burn')
            $('#burn-spl-button').prop('disabled', false)
            $('#burn-spl-warning').html(res.message)
        },
        success: function(res) {
            let response = JSON.parse(res);
            const status = response.info
            console.log("RES: ", res)
            console.log("RESPONSE: ", response)
            $('#burn-spl-button').html('Burn')
            $('#burn-spl-warning').html(response.message)
            
            let filename = `spl-burn-${$('#tanggal_text').val()}.pdf`
            download(response.url, filename)
            if(status == 'success'){
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        }
    });
})

function download(url, filename) {
    fetch(url)
      .then(response => response.blob())
      .then(blob => {
        const link = document.createElement("a");
        link.href = URL.createObjectURL(blob);
        link.download = filename;
        link.click();
    })
    .catch(console.error);
  }

function format_to_db_date(date){
    let dd = String(date.getDate()).padStart(2, '0'),
        mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}`
    return new_date
}

function convert_month_to_string(month){
    const month_int = parseInt(month)
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    return monthNames[month_int-1]
}
let user_login = get_login(),
    table_search = '',
    table_dir = 'DESC',
    table_order_by = 'id',
    start_date = format_to_db_date(new Date()),
    end_date = format_to_db_date(new Date()),
    SESSION = localStorage.getItem("user-token"),
    JURNAL_ID

$('.selectpicker').selectpicker();

$(document).ready(function(){
    $("#filter_tanggal_awal").val(start_date)
    $("#filter_tanggal_akhir").val(end_date)
    
    $(".datepicker").datepicker( {
        format: "yyyy-mm-dd",
    });

    //Timepicker
    $('.timepicker').timepicker({
        showMeridian: false
    })

    //data table
    let jurnal_absensi_table = $('#jurnal_absensi_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_absensi_harian',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column

            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            table_search = d.search.value
            newObj.draw = d.draw
            newObj.start_date = start_date
            newObj.end_date = end_date
            newObj.sort = d.order[0].dir
            table_dir = d.order[0].dir
            switch (column) {
                case 1:
                    newObj.order_by = 'shift'
                    break
                case 2:
                    newObj.order_by = 'start_hour'
                    break
                case 3:
                    newObj.order_by = 'end_hour'
                    break
                case 4:
                    newObj.order_by = 'absensi_date'
                    break
                case 5:
                    newObj.order_by = 'rec_id'
                    break
                case 7:
                    newObj.order_by = 'is_sync'
                    break
                case 8:
                    newObj.order_by = 'is_overtime'
                    break
                case 9:
                    newObj.order_by = 'is_late'
                    break
                case 10:
                    newObj.order_by = 'is_weekend'
                    break
                case 11:
                    newObj.order_by = 'late_hour'
                    break
                case 12:
                    newObj.order_by = 'overtime_hour'
                    break
                case 13:
                    newObj.order_by = 'datecreated'
                    break
                default:
                    newObj.order_by = 'id'
                    break
            }
            table_order_by = newObj.order_by
            rerender_print_link()

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
            // localStorage.removeItem("user-token");
            // localStorage.removeItem("user-refresh-token");
            // window.location.href = "logout"
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            { 
                data: "pegawai_name",
                orderable: true
            },
            { 
                data: "shift",
                orderable: true
            },
            { 
                data: "start_hour",
                orderable: true
            },
            { 
                data: "end_hour",
                orderable: true
            },
            { 
                data: "absensi_date",
                orderable: true
            },
            { 
                data: "rec_id",
                orderable: true
            },
            { 
                data: "supervisor_name",
                orderable: true
            },
            { 
                data: "is_sync",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label_color = data && parseInt(data) ? 'primary' : 'default'; 
                    let label_text = data && parseInt(data) ? 'SUDAH DIREKAP' : 'BELUM DIREKAP';
                    return `<span class="label label-${label_color}">${label_text}</span>`
                }
            },
            { 
                data: "is_overtime",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label_color = data && parseInt(data) ? 'primary' : 'default'; 
                    let label_text = data && parseInt(data) ? 'LEMBUR' : 'TIDAK LEMBUR';
                    return `<span class="label label-${label_color}">${label_text}</span>`
                }
            },
            { 
                data: "is_late",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label_color = data && parseInt(data) ? 'primary' : 'default'; 
                    let label_text = data && parseInt(data) ? 'TERLAMBAT' : 'TEPAT WAKTU';
                    return `<span class="label label-${label_color}">${label_text}</span>`
                }
            },
            { 
                data: "is_weekend",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label_color = data && parseInt(data) ? 'primary' : 'default'; 
                    let label_text = data && parseInt(data) ? 'WEEKEND' : 'HARI BIASA';
                    return `<span class="label label-${label_color}">${label_text}</span>`
                }
            },
            { 
                data: "late_hour",
                orderable: true,
                render: function (data, type, row, meta) {
                    let text = data && parseInt(data) ? `${data} jam` : '-'
                    return text
                }
            },
            { 
                data: "overtime_hour",
                orderable: true,
                render: function (data, type, row, meta) {
                    let text = data && parseInt(data) ? `${data} jam` : '-'
                    return text
                }
            },
            {
                data: "datecreated",
                orderable: true
            },
            {
                data: "id",
                render: function (data, type, row, meta) {
                    if(!parseInt(row.is_sync)){
                        return `<button class="btn btn-primary jurnal-edit-toggle" data-id="${data}" data-toggle="modal" data-target="#jurnal-edit-modal">Edit</button>`
                    }else{
                        return "-"
                    }
                }
            }
        ]
    });

    $('#filter_tanggal_awal').change(function(){
        start_date = $(this).val()
        jurnal_absensi_table.draw()
    })

    $('#filter_tanggal_akhir').change(function(){
        end_date = $(this).val()
        jurnal_absensi_table.draw()
    })

    $('#sync-jurnal-absensi-button').click(function(){
        let data = {
            start_date: start_date,
            end_date: end_date
        }

        $.ajax({
            async: true,
            url: `backend/sync_absensi_harian`,
            type: 'POST',
            data: JSON.stringify(data),
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                let alert = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
                $('#sync-jurnal-absensi-warning').html(alert)
            },
            success: function(res) {
                const response = res.data
                const message = res.message
                let alert = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> ${message}!</a>.</div>`
                console.log("RESPONSE SUCCESS: ", response)
                $('#sync-jurnal-absensi-warning').html(alert)
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        });
    })

    $("body").delegate(".jurnal-edit-toggle", "click", function(e) {
        JURNAL_ID = $(this).data('id')
        get_jurnal_detail()
    })

    $('#jurnal-edit-form').submit(function(e){
        e.preventDefault()

        let form = $(this),
            request = {
                id: JURNAL_ID,
                start_hour: form.find("input[name='start_hour']").val(),
                end_hour: form.find("input[name='end_hour']").val(),
            }

        $.ajax({
            async: true,
            url: `backend/update_absensi_harian`,
            type: 'POST',
            data: JSON.stringify(request),
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                let alert = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
                $('#jurnal-edit-warning').html(alert)
            },
            success: function(res) {
                console.log("RESPONSE SUCCESS: ", res)
                const response = res.data
                const message = res.message
                let alert = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> ${message}!</a>.</div>`
                console.log("RESPONSE SUCCESS: ", response)
                $('#jurnal-edit-warning').html(alert)
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        });
    })
})

function format_to_db_date(date){
    const date_iso = date.toISOString().split('T')[0]
    return date_iso
}

function rerender_print_link(){
    $('#print-jurnal-absensi').prop('href', `backend/print_absensi_harian?start_date=${start_date}&end_date=${end_date}`)
}

function get_jurnal_detail(){
    $.ajax({
        async: true,
        url: `backend/get_absensi_harian_by_id?id=${JURNAL_ID}`,
        type: 'GET',
        error: function(res) {
            console.log("RESPONSE ERROR: ", res)
        },
        success: function(res) {
            console.log("RESPONSE SUCCESS: ", res)
            fill_jurnal_form(res.data)
        }
    });
}

function fill_jurnal_form(data){
    let form = $(`#jurnal-edit-form`)
    form.find( "input[name='pegawai_name']" ).val(data.pegawai_name)
    form.find( "input[name='supervisor_name']" ).val(data.supervisor_name)
    form.find( "input[name='shift']" ).val(data.shift)
    // form.find( "input[name='start_hour']" ).val(data.start_hour)
    // form.find( "input[name='end_hour']" ).val(data.end_hour)
    $('#start_hour').timepicker('setTime', data.start_hour)
    $('#end_hour').timepicker('setTime', data.end_hour)
    form.find( "input[name='absensi_date']" ).val(data.absensi_date)

}
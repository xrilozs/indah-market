let user_login = get_login(),
    SPL,
    DELIVERY, 
    $datepicker1 = $('#datepicker-barang1'),
    $datepicker2 = $('#datepicker-barang2')
    

$(document).ready(function(){
    $datepicker1.datepicker({format: 'yyyy-mm-dd'})
    $datepicker2.datepicker({format: 'yyyy-mm-dd'})

    $('#search-delivery-form').submit(function(e){
        e.preventDefault()

        let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`
        $('#search-delivery-button').html(loading_button)
        $('#search-delivery-button').prop('disabled', true);

        let $form = $(this),
            spl_no = $form.find("input[name='no_spl']").val()

        $.ajax({
            async: true,
            url: `backend/get_delivery_by_spl_no?spl_no=${spl_no}`,
            type: 'GET',
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                $('#search-delivery-button').prop('disabled', false)
                $('#search-delivery-button').html('Cari')
                let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>SPL tidak ditemukan!</a>.</div>`
                $('#search-delivery-warning').html(message)

            },
            success: function(res) {
                console.log("RESPONSE SUCCESS: ", res)
                let response = res.data
                SPL = response.spl
                DELIVERY = response.delivery
                fill_delivery_form(res.data)
                $('#search-delivery-button').prop('disabled', false)
                $('#search-delivery-button').html('Cari')
            }
        });
    })

    $('#delivery-form').submit(function(e){
        e.preventDefault()

        let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`
        $('#delivery-button').html(loading_button)
        $('#delivery-button').prop('disabled', true);

        let $form = $(this),
            customer = $form.find("input[name='customer']").val()
            request = {
                spl_id: SPL.id,
                kodeCust: customer.replace(" ", "").slice(0,4).toUpperCase(),
                tglSetorBarang1: $form.find("input[name='tanggal_setor_barang1']").val(),
                jlhYgDisetor1: $form.find("input[name='jumlah_setor1']").val(),
            },
            tglSetorBarang2 = $form.find("input[name='tanggal_setor_barang2']").val(),
            jlhYgDisetor2 = $form.find("input[name='jumlah_setor2']").val()

        if(DELIVERY) request.id = DELIVERY.id
        
        if(tglSetorBarang2 && tglSetorBarang2){
            request.tglSetorBarang2 = tglSetorBarang2
            request.jlhYgDisetor2 = jlhYgDisetor2
        }

        $.ajax({
            async: true,
            url: `backend/create_update_delivery`,
            type: 'POST',
            data: JSON.stringify(request),
            error: function(res) {
                console.log("RESPONSE ERROR: ", res)
                $('#delivery-button').prop('disabled', false)
                $('#delivery-button').html('Simpan')
                let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data delivery gagal disimpan!</a>.</div>`
                $('#delivery-warning').html(message)
                $('#delivery-button').prop('disabled', false)
                $('#delivery-button').html('Simpan')
            },
            success: function(res) {
                console.log("RESPONSE SUCCESS: ", res)
                let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data delivery berhasil disimpan!</a>.</div>`
                $('#delivery-warning').html(message)
                $('#delivery-button').prop('disabled', false)
                $('#delivery-button').html('Simpan')
            }
        });
    })
})


$('#create-absensi-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#create-absensi-button').html(loading_button)
    $('#create-absensi-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/create_absensi_process`,
        type: 'POST',
        data: new FormData($('#create-absensi-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> absensi gagal ditambah!</a>.</div>`
            $('#create-absensi-button').html('Tambah')
            $('#create-absensi-button').prop('disabled', false)
            $('#create-absensi-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> absensi telah ditambah!</a>.</div>`
            $('#create-absensi-button').html('Tambah')
            $('#create-absensi-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#update-absensi-form').submit(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#update-absensi-button').html(loading_button)
    $('#update-absensi-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/update_absensi_process`,
        type: 'POST',
        data: new FormData($('#update-absensi-form')[0]),
        processData: false, 
        contentType: false, 
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#update-absensi-button').html('Update')
            $('#update-absensi-button').prop('disabled', false)
            $('#update-absensi-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> absensi berhasil diubah!</a>.</div>`
            $('#update-absensi-button').html('Update')
            $('#update-absensi-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

$('#delete-absensi-button').click(function(e){
    e.preventDefault()

    let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

    $('#delete-absensi-button').html(loading_button)
    $('#delete-absensi-button').prop('disabled', true);

    $.ajax({
        async: true,
        url: `backend/delete_absensi_process`,
        type: 'POST',
        data: JSON.stringify({id: absensi_id, keyword: $('#keyword_delete').val()}),
        error: function(res) {
            console.log("ERROR: ", res)
            let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ${res.responseJSON.message}!</a>.</div>`
            $('#delete-absensi-button').html('IYA')
            $('#delete-absensi-button').prop('disabled', false)
            $('#delete-absensi-warning').html(message)
        },
        success: function(res) {
            let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> absensi berhasil dihapus!</a>.</div>`
            $('#delete-absensi-button').html('IYA')
            $('#delete-absensi-warning').html(message)
            setTimeout(() => { 
                location.reload(); 
            }, 1000);
        }
    });
});

function get_requested_absensi(){
    $('#multiple_approve_button').prop('disabled', true)
    $.ajax({
        async: true,
        url: `backend/get_absensi?page_number=0&page_size=100&date=${selected_date}&approval_status=REQUESTED&order_by=no_absensi&sort=DESC`,
        type: 'GET',
        error: function(res) {
            console.log("ERROR: ", res)
            $('#multiple_approve_button').prop('disabled', false)
        },
        success: function(res) {
            generate_absensi_option(res.data)
            $('#multiple_approve_button').prop('disabled', false)
        }
    });
}

function fill_delivery_form(){
    console.log("USER: ", user_login)
    $('#delivery-section').css("display", "block")
    let form = $(`#delivery-form`)
    form.find( "input[name='no_spl']" ).val(SPL.no_spl)
    form.find( "input[name='customer']" ).val(SPL.customer_name)
    form.find( "input[name='ukuran']" ).val(SPL.ukuran_potong)
    form.find( "input[name='hasil_rumus']" ).val(SPL.hasil_rumus)
    if(DELIVERY){
        if(DELIVERY.tglSetorBarang1) $datepicker1.datepicker('setDate', new Date(DELIVERY.tglSetorBarang1));
        else $datepicker1.datepicker('setDate', new Date());

        if(DELIVERY.tglSetorBarang2) $datepicker2.datepicker('setDate', DELIVERY.tglSetorBarang2);
        else $datepicker2.datepicker('setDate', new Date());

        form.find( "input[name='jumlah_setor1']" ).val(DELIVERY.jlhYgDisetor1)
        form.find( "input[name='jumlah_setor2']" ).val(DELIVERY.jlhYgDisetor2)

        if(DELIVERY.tglSetorBarang2){
            $("#delivery-form input").prop("disabled", true);
            $('#delivery-button').css("display", "none")
        }
    }else{
        $datepicker1.datepicker('setDate', new Date());
        $('.update-section').css("display", "none")
        if(user_login.type == 'delivery'){
            $('#delivery-button').css("display", "none")
        }
    }
}
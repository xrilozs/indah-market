let user_login = get_login(),
    RECEIVED_LIST = [],
    DELIVERED_LIST = [],
    RETURNED_LIST = []

$(document).ready(function(){
    //data table
    let data_barang_table = $('#databarang_table').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: 'backend/get_delivery',
          type: "GET",
          dataType: "json",
          data: function ( d ) {
            let newObj = {}
            let start = d.start
            let size = d.length
            let column = d.order[0].column
            let date = $('#filter_tanggal').val()

            
            newObj.page_number = d.start > 0 ? (start/size) : 0;
            newObj.page_size = size
            newObj.search = d.search.value
            newObj.draw = d.draw
            newObj.date = date
            newObj.sort = d.order[0].dir
            switch (column) {
                case 2:
                    newObj.order_by = 'kodeCust'
                    break
                case 3:
                    newObj.order_by = 'tglSetorBarang1'
                    break
                case 4:
                    newObj.order_by = 'jlhYgDisetor1'
                    break
                case 5:
                    newObj.order_by = 'tglSetorBarang2'
                    break
                case 6:
                    newObj.order_by = 'jlhYgDisetor2'
                    break
                case 7:
                    newObj.order_by = 'received'
                    break
                case 8:
                    newObj.order_by = 'delivered'
                    break
                case 9:
                    newObj.order_by = 'returned'
                    break
                case 10:
                    newObj.order_by = 'ret_desc'
                    break
                default:
                    newObj.order_by = 'datecreated'
                    break
            }

            d = newObj
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            console.log("RESPONSE ERROR: ", response)
          }
        },
        order: [[ 0, "desc" ]],
        columns: [
            {
                data: "spl_no",
                orderable: false,
                render: function (data, type, row, meta) {
                    let button = `<button class="btn btn-link detail-spl-toggle" data-id="${row.spl_id}" data-toggle="modal" data-target="#detail-spl-modal">
                        ${data}
                    </button>`

                    return button;
                }
            },
            { 
                data: "ukuran",
                orderable: false
            },
            { 
                data: "kodeCust",
                orderable: true
            },
            { 
                data: "tglSetorBarang1",
                orderable: true
            },
            { 
                data: "jlhYgDisetor1",
                orderable: true
            },
            { 
                data: "tglSetorBarang2",
                orderable: true
            },
            { 
                data: "jlhYgDisetor2",
                orderable: true
            },
            { 
                data: "received",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = `<div class="material-switch">
                        <input id="receivedswitch-${row.id}" class="received-switch" data-id="${row.id}" type="checkbox"/>
                        <label for="receivedswitch-${row.id}" class="label-success"></label>
                    </div>`
                    if(parseInt(data)){
                        label = `<span class="label label-success">YES</span>`
                    }

                    return label;
                }
            },
            {
                data: "delivered",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = `<div class="material-switch">
                        <input id="deliveredswitch-${row.id}" class="delivered-switch" data-id="${row.id}" type="checkbox"/>
                        <label for="deliveredswitch-${row.id}" class="label-success"></label>
                    </div>`
                    if(parseInt(data)){
                        label = `<span class="label label-success">YES</span>`
                    }

                    return label;
                }
            },
            {
                data: "returned",
                orderable: true,
                render: function (data, type, row, meta) {
                    let label = `<div class="material-switch">
                        <input id="returnedswitch-${row.id}" class="returned-switch" data-id="${row.id}" type="checkbox"/>
                        <label for="returnedswitch-${row.id}" class="label-success"></label>
                    </div>`
                    if(parseInt(data)){
                        label = `<span class="label label-success">YES</span>`
                    }

                    return label;
                }
            },
            {
                data: "ret_desc",
                orderable: true,
                className: "dt-body-left",
                render: function (data, type, row, meta) {
                    let ret = data ? data : ""
                    return `<textarea id="returneddesc-${row.id}" class="return-description" data-id="${row.id}" style="resize: none;" maxlength="30" disabled>${ret}</textarea>`
                }
            },
            { 
                data: "datecreated",
                orderable: true
            },
        ]
    });

    $('#filter_tanggal').change(function(){
        data_barang_table.draw()
    })

    $("body").delegate(".received-switch", "change", function() {
        let id = $(this).data("id")
        if($(this).prop("checked")){
            console.log("CHECK RECEIVED")
            RECEIVED_LIST.push(id)
        }else{
            console.log("UNCHECK RECEIVED")
            let index = RECEIVED_LIST.indexOf(id)
            console.log("index: ", index)
            if(index >= 0){
                console.log("before: ", RECEIVED_LIST)
                RECEIVED_LIST.splice(index, 1)
                console.log("after: ", RECEIVED_LIST)
            }
        }
    })
    $("body").delegate(".delivered-switch", "change", function() {
        console.log("CHANGE DELIVERED")
        let id = $(this).data("id")
        if($(this).prop("checked")){
            DELIVERED_LIST.push(id)
        }else{
            let index = DELIVERED_LIST.findIndex(r => r==id)
            if(index){
                DELIVERED_LIST.pop(index, 1)
            }
        }
    })
    $("body").delegate(".returned-switch", "change", function() {
        console.log("CHANGE RECEIVED")
        let id = $(this).data("id")
        if($(this).prop("checked")){
            RETURNED_LIST.push({id:id})
            $(`#returneddesc-${id}`).prop("disabled", false)
        }else{
            let index = RETURNED_LIST.findIndex(r => r.id==id)
            if(index){
                RETURNED_LIST.pop(index, 1)
            }
            $(`#returneddesc-${id}`).val("")
            $(`#returneddesc-${id}`).prop("disabled", true)
        }
    })

    $('#save-delivery-button').click(function(){
        let loading_button = `<span class="fa fa-spinner fa-spin" role="status" aria-hidden="true"></span>`

        $('#save-delivery-button').html(loading_button)
        $('#save-delivery-button').prop('disabled', true);

        RETURNED_LIST = RETURNED_LIST.map(r => {
            let ret_desc = $(`#returneddesc-${r.id}`).val()
            return {id:r.id, text: ret_desc}
        })

        let request = {
            received: RECEIVED_LIST,
            delivered: DELIVERED_LIST,
            returned: RETURNED_LIST
        }

        console.log("REQUEST: ", request)

        $.ajax({
            async: true,
            url: `backend/update_deliveries`,
            type: 'POST',
            data: JSON.stringify(request),
            error: function(res) {
                console.log("ERROR: ", res)
                let message = `<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> simpan perubahan delivery gagal!.</div>`
                $('#save-delivery-button').html('<i class="fa fa-save"></i> Simpan')
                $('#save-delivery-button').prop('disabled', false)
                $('#save-delivery-warning').html(message)
            },
            success: function(res) {
                let message = `<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> simpan perubahan delivery berhasil!.</div>`
                $('#save-delivery-button').html('<i class="fa fa-save"></i> Simpan')
                $('#save-delivery-warning').html(message)
                setTimeout(() => { 
                    location.reload(); 
                }, 1000);
            }
        });
    })
})

function format_to_db_date(date){
    let mm = String(date.getMonth() + 1).padStart(2, '0'),
        yyyy = date.getFullYear()

    let new_date = `${yyyy}-${mm}`
    return new_date
}

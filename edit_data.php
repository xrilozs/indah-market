<?php
include "koneksi.php";

$id = $_GET['id'];

$query	= "select * from produksi WHERE prod_id = '$id'";
$sql	= mysql_query($query) ;
$r = mysql_fetch_array($sql);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Latihan CRUD</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="favicon.png"/>
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
</head>
<body>

<div class="container">
    <div class="navbar navbar-inner">
        <div class="brand">Latihan Lap. Prod.</div>
    </div>

    <h3><a href="index.php" class="btn btn-warning"><i class="icon-arrow-left"></i></a> Edit Data</h3>

    <form action="proses/proses_edit.php" method="post">

        <input type="hidden" name="id" value="<?php echo $r['prod_id']?>">
        <table class="table table-hover" width="100%">

            <tr>
                <td>TGL</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <input type="text" name="tgl" placeholder="YYYY-MM-DD" class="input-xlarge" value="<?php echo $r['prod_tgl'] ?>"/>
                    </div>
                </td>
            </tr>

            <tr>
                <td>NoMesin</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <input type="text" name="nom" placeholder="NO MESIN" class="input-xlarge" value="<?php echo $r['prod_nom'] ?>"/>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Ukuran</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <textarea name="uk" placeholder="UKURAN" rows="5" class="input-xlarge" <?php echo $r['prod_ukuran']?>></textarea>
                    </div>
                </td>
            </tr>

            <tr>
                <td>KG</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="kg" placeholder="KG" class="input-xlarge" value="<?php echo $r['prod_kg'] ?>">
                    </div>
                </td>
            </tr>
            <tr>
                <td>Lembar</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="lb" placeholder="LEMBAR" class="input-xlarge" value="<?php echo $r['prod_lb'] ?>">
                    </div>
                </td>
            </tr>

            <tr>
                <td>Keterangan</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="ket" placeholder="KET" class="input-xlarge" value="<?php echo $r['prod_ket'] ?>">
                    </div>
                </td>
            </tr>

            <tr>
                <td></td>
                <td>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" name="submit" class="btn btn-danger">Reset</button>
                </td>
            </tr>

        </table>
    </form>
</div>
</body>
</html>
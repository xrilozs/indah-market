<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                = 'home';
$route['404_override']                      = '';
$route['translate_uri_dashes']              = FALSE;
$route['login']                             = 'home/login';
$route['logout']                            = 'backend/logout';
$route['logout_process']                    = 'backend/logout_process';
$route['dashboard']                         = 'home';
$route['user_manager']                      = 'home/user_manager';
$route['myprofile']                         = 'home/myprofile';
$route['produksi_harian']                   = 'home/produksi_harian';
$route['returan']                           = 'home/returan';
$route['report/produksi_bulanan']           = 'home/report_produksi_bulanan';
$route['report/produksi_harian']            = 'home/report_produksi_harian';
$route['report/returan_bulanan']            = 'home/report_returan_bulanan';
$route['report/returan_harian']             = 'home/report_returan_harian';
$route['report/pembelian_hutang_bulanan']   = 'home/report_pembelian_bulanan';
$route['report/pembelian_hutang_harian']    = 'home/report_pembelian_harian';
$route['report/nota_penjualan']             = 'home/report_nota_penjualan';
$route['report/faktur_harian']              = 'home/report_faktur_harian';
$route['update_returan']                    = 'home/update_returan';
$route['hutang']                            = 'home/hutang';
$route['pelunasan_hutang']                  = 'home/pelunasan_hutang';
$route['customer']                          = 'home/customer';
$route['faktur']                            = 'home/faktur';
$route['nota_penjualan']                    = 'home/nota_penjualan';
$route['kwitansi']                          = 'home/kwitansi';
$route['pelunasan_hutang']                  = 'home/pelunasan_hutang';
$route['update_pelunasan_faktur']           = 'home/pelunasan_faktur';
$route['report/penjualan_piutang_bulanan']  = 'home/penjualan_piutang_bulanan';
$route['report/faktur_pajak_bulanan']       = 'home/faktur_pajak_bulanan';
$route['surat_jalan']                       = 'home/surat_jalan';
$route['spl']                               = 'home/spl';
$route['spl-titipan']                       = 'home/spl_titipan';
$route['spl-monthly-report']                = 'home/spl_monthly_report';
$route['spl-delete']                        = 'home/spl_delete';
$route['spl-limit']                         = 'home/spl_limit';
$route['spl-burn']                          = 'home/spl_burn';
$route['pegawai']                           = 'home/pegawai';
$route['absensi']                           = 'home/absensi';
$route['jurnal-absensi']                    = 'home/jurnal_absensi';
$route['delete-jurnal-absensi']             = 'home/delete_jurnal_absensi';
$route['histori-absensi']                   = 'home/histori_absensi';
$route['kasbon-pegawai']                    = 'home/kasbon_pegawai';
$route['setor-barang-jadi']                 = 'home/setor_barang_jadi';
$route['terima-barang-jadi']                = 'home/terima_barang_jadi';
$route['data-barang']                       = 'home/data_barang';
$route['keyword']                           = 'home/keyword';
$route['office_hour']                       = 'home/office_hour';
$route['site_location']                     = 'home/site_location';
$route['hari-libur']['GET']                 = 'home/hari_libur';
$route['laporan/download_produksi_bulanan'] = 'backend/download_produksi_bulanan';
$route['laporan/download_returan_bulanan']  = 'backend/download_returan_bulanan';
$route['laporan/download_pembelian_bulanan']= 'backend/download_pembelian_bulanan';
$route['laporan/download_produksi_harian']  = 'backend/download_produksi_harian';
$route['laporan/download_returan_harian']   = 'backend/download_returan_harian';
$route['laporan/download_pembelian_harian'] = 'backend/download_pembelian_harian';
$route['laporan/download_penjualan_bulanan']= 'backend/download_penjualan_bulanan';
$route['delete-token']['GET']               = 'backend/delete_token';
#office hour
$route['office-hour/web']['GET']            = 'office_hour/get_office_hours_web';
$route['office-hour/web-detail/(:any)']['GET']= 'office_hour/get_office_hour_by_id_web/$1';
$route['office-hour']['POST']               = 'office_hour/create_office_hour';
$route['office-hour']['PUT']                = 'office_hour/update_office_hour';
$route['office-hour/disable/(:any)']['PUT'] = 'office_hour/disable_office_hour/$1';
$route['office-hour/enable/(:any)']['PUT']  = 'office_hour/enable_office_hour/$1';
#site location
$route['site-location/apps']['GET']         = 'site_location/get_site_location_apps';
$route['site-location']['GET']              = 'site_location/get_site_location';
$route['site-location/detail/(:any)']['GET']= 'site_location/get_site_location_detail/$1';
$route['site-location']['POST']             = 'site_location/create_site_location';
$route['site-location']['PUT']              = 'site_location/update_site_location';
$route['site-location/(:any)']['DELETE']    = 'site_location/delete_site_location/$1';
#Apps Endpoint
$route['auth/create']['POST']                   = 'auth/get_auth_pegawai';
$route['auth/profile']['GET']                   = 'auth/get_profile';
$route['office-hour/apps']['GET']               = 'office_hour/get_office_hours_apps';
$route['office-hour/apps-detail/(:any)']['GET'] = 'office_hour/get_office_hour_by_id_apps/$1';
#pegawai
$route['pegawai/start-working']['POST']         = 'absensi/start_working';
$route['pegawai/end-working']['POST']           = 'absensi/end_working';
$route['pegawai/absensi-detail-self']['GET']    = 'absensi/get_absensi_detail_self';
$route['pegawai/absensi-history']['GET']        = 'absensi/get_absensi_history';
#supervisor
$route['spv/auth']['POST']                      = 'auth/get_auth_supervisor';
$route['spv/pegawai-list']['GET']               = 'absensi/get_pegawai_list';
$route['spv/verify-absensi']['POST']            = 'absensi/verify_absensi_pegawai';
$route['spv/verify-absensi-bulk']['POST']       = 'absensi/verify_absensi_pegawai_bulk';
$route['spv/verify-absensi-self']['POST']       = 'absensi/verify_absensi_spv';
$route['spv/absensi-list']['GET']               = 'absensi/get_spv_absensi_list';
$route['spv/absensi-detail/(:any)']['GET']      = 'absensi/get_spv_absensi_detail/$1';
$route['spv/absensi-detail-self']['GET']        = 'absensi/get_absensi_detail_self';
$route['spv/absensi-by-code/(:any)']['GET']     = 'absensi/get_absensi_by_code/$1';
$route['spv/start-working']['POST']             = 'absensi/spv_start_working';
$route['spv/end-working']['POST']               = 'absensi/spv_end_working';
$route['spv/help-start-working']['POST']        = 'absensi/spv_help_start_working';
$route['spv/help-end-working']['POST']          = 'absensi/spv_help_end_working';
$route['spv/absensi-history']['GET']            = 'absensi/get_absensi_history';
#Hari Libur
$route['hari-libur/list']['GET']                = 'hari_libur/get_hari_libur';
$route['hari-libur/detail/(:any)']['GET']       = 'hari_libur/get_hari_libur_by_id/$1';
$route['hari-libur']['POST']                    = 'hari_libur/create_hari_libur';
$route['hari-libur']['PUT']                     = 'hari_libur/update_hari_libur';
$route['hari-libur/delete/(:any)']['DELETE']    = 'hari_libur/delete_hari_libur/$1';
#CRON
$route['absensi/overtime-reminder']['GET']      = 'absensi/overtime_reminder';

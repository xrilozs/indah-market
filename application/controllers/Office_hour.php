<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Office_hour extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

/*---------Start of Web---------*/
    function get_office_hours_web(){
        $respObj = new ResponseApi();
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $site_code = $user->site_code;

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $office_hours = $this->office_hour_model->get_office_hour($search, $site_code, $order, $limit);
        $total = $this->office_hour_model->get_office_hour_total($search, $site_code);

        #response
        $respObj->setResponseDatatable(200, $office_hours, $draw, $total, $total);
        $resp = $respObj->getResponseDatatable();
        setOutput($resp);
        return;
    }

    function get_office_hour_by_id_web($id){
        $respObj = new ResponseApi();
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $office_hour = $this->office_hour_model->get_office_hour_by_id($id);
        $respObj->setResponse(200, "success", "Get office hour by id success", $office_hour);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function create_office_hour(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if(!checkParameterByKeys($request, array('shift', 'start_hour', 'end_hour', 'start_hour_saturday', 'end_hour_saturday'))){
            $respObj->setResponse(400, "failed", "Missing mandatory request");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['site_code'] = $user->site_code;
        $flag = $this->office_hour_model->create_office_hour($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", "Internal server error! Insert office hour failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Create office hour success", $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function update_office_hour(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if(!checkParameterByKeys($request, array('id', 'shift', 'start_hour', 'end_hour', 'start_hour_saturday', 'end_hour_saturday'))){
            $respObj->setResponse(400, "failed", "Missing mandatory request");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour exist
        $office_hour = $this->office_hour_model->get_office_hour_by_id($request['id']);
        if(is_null($office_hour)){
            $respObj->setResponse(404, "failed", "Office hour not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($office_hour->site_code != $user->site_code){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['site_code'] = $user->site_code;
        $request['is_active'] = $office_hour->is_active;
        $request['datecreated'] = $office_hour->datecreated;
        $flag = $this->office_hour_model->update_office_hour($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", "Internal server error! Update office hour failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Update office hour success", $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function disable_office_hour($id){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour exist
        $office_hour = $this->office_hour_model->get_office_hour_by_id($id);
        if(is_null($office_hour)){
            $respObj->setResponse(404, "failed", "Office hour not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($office_hour->site_code != $user->site_code){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $flag = $this->office_hour_model->disable_office_hour($id);
        if(!$flag){
            $respObj->setResponse(500, "failed", "Internal server error! Update office hour failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Disable office hour success", $flag);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function enable_office_hour($id){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour
        $office_hour = $this->office_hour_model->get_office_hour_by_id($id);
        if(is_null($office_hour)){
            $respObj->setResponse(404, "failed", "Office hour not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($office_hour->site_code != $user->site_code){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $flag = $this->office_hour_model->enable_office_hour($id);
        if(!$flag){
            $respObj->setResponse(500, "failed", "Internal server error! Update office hour failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Enable office hour success", $flag);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*---------End of Web---------*/
    
/*---------Start of Apps---------*/
    function get_office_hours_apps(){
        $respObj = new ResponseApi();
        $dayname = strtolower(date("l"));
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $office_hours = $this->office_hour_model->get_office_hour(null, $site_code);
        $site_location = $this->site_location_model->get_site_location_by_site_code($site_code);

        foreach ($office_hours as &$office_hour) {
            if($dayname == 'saturday'){
                $office_hour->start_hour = $office_hour->start_hour_saturday;
                $office_hour->end_hour = $office_hour->end_hour_saturday;
            }
            unset($office_hour->start_hour_saturday);
            unset($office_hour->end_hour_saturday);
        }

        $resp_arr = array(
            "office_hour" => $office_hours,
            "site_location" => $site_location
        );

        #response
        $respObj->setResponse(200, "success", action_result("get office hour", "success"), $resp_arr);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_office_hour_by_id_apps($id){
        $respObj = new ResponseApi();
        $dayname = strtolower(date("l"));

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $office_hour = $this->office_hour_model->get_office_hour_by_id($id);
        if(is_null($office_hour)){
            $respObj->setResponse(404, "failed", action_result("office hour", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        if($dayname == 'saturday'){
            $office_hour->start_hour = $office_hour->start_hour_saturday;
            $office_hour->end_hour = $office_hour->end_hour_saturday;
        }
        unset($office_hour->start_hour_saturday);
        unset($office_hour->end_hour_saturday);

        $respObj->setResponse(200, "success", action_result("get office hour by id", "success"), $office_hour);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*---------End of Apps---------*/

}
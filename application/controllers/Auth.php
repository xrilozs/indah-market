<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    function get_auth_pegawai(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        if(!checkParameterByKeys($request, array('phone_number'))){
            $respObj->setResponse(400, "failed", message('missing_param'));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $phone_number = $request['phone_number'];

        $pegawai = $this->pegawai_model->get_pegawai_by_phone_number($phone_number, "PEGAWAI");
        if(is_null($pegawai)){
            $respObj->setResponse(404, "failed", action_result("employee", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $token = generateAppsToken($pegawai);
        $respObj->setResponse(200, "success", action_result("generate token", "success"), array('token' => $token));
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_auth_supervisor(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        if(!checkParameterByKeys($request, array('phone_number', 'password'))){
            $respObj->setResponse(400, "failed", message('missing_param'));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $phone_number = $request['phone_number'];
        $password = $request['password'];

        $supervisor = $this->pegawai_model->get_pegawai_by_phone_number_and_password($phone_number, $password, "SUPERVISOR");
        if(is_null($supervisor)){
            $respObj->setResponse(404, "failed", action_result("supervisor", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $token = generateAppsToken($supervisor);
        $respObj->setResponse(200, "success", action_result("generate token", "success"), array('token' => $token));
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_profile(){
        $respObj = new ResponseApi();

        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $pegawai = $jwt->data;

        $respObj->setResponse(200, "success", "Get profile success", $pegawai);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
}
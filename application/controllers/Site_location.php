<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Site_location extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

/*---------Start of Web---------*/
    function get_site_location(){
        $respObj = new ResponseApi();
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $site_code = $user->site_code;

        $site_location = $this->site_location_model->get_site_location_by_site_code($site_code);
        if(is_null($site_location)){
            $respObj->setResponse(404, "failed", "Site location not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #response
        $respObj->setResponse(200, "success", "Get site location by site code success", $site_location);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function create_site_location(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $site_code = $user->site_code;

        if(!checkParameterByKeys($request, array('lat', 'long'))){
            $respObj->setResponse(400, "failed", "Missing mandatory request");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['site_code'] = $site_code;
        $flag = $this->site_location_model->create_site_location($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", "Internal server error! Insert site location failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Create site location success", $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function update_site_location(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $site_code = $user->site_code;

        if(!checkParameterByKeys($request, array('id', 'lat', 'long'))){
            $respObj->setResponse(400, "failed", "Missing mandatory request");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $site_location = $this->site_location_model->get_site_location_by_id($request['id']);
        if(is_null($site_location)){
            $respObj->setResponse(404, "failed", "Site location not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($site_code != $site_location->site_code){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $request['site_code'] = $site_code;
        $request['datecreated'] = $site_location->datecreated;
        $flag = $this->site_location_model->update_site_location($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", "Internal server error! Update site location failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Update site location success", $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function delete_site_location($id){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check site location exist
        $site_location = $this->site_location_model->get_site_location_by_id($id);
        if(is_null($site_location)){
            $respObj->setResponse(404, "failed", "Site location not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($site_location->site_code != $user->site_code){
            $respObj->setResponse(401, "failed", "Unauthorized access");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $flag = $this->site_location_model->delete_site_location($id);
        if(!$flag){
            $respObj->setResponse(500, "failed", "Internal server error! Delete site location failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Delete site location success", $flag);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*---------End of Web---------*/
    
/*---------Start of Apps---------*/
    function get_site_location_apps(){
        $respObj = new ResponseApi();
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $site_location = $this->site_location_model->get_site_location_by_site_code($site_code);
        if(is_null($site_location)){
            $respObj->setResponse(404, "failed", action_result("site location", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #response
        $respObj->setResponse(200, "success", action_result("get site location", "success"),  $site_location);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*---------End of Apps---------*/

}
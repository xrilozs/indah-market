<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Absensi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }

    function start_working(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $dayname = strtolower(date("l"));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'PEGAWAI'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check param
        if(!checkParameterByKeys($request, array('office_hour_id'))){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour
        $office_hour = $this->office_hour_model->get_office_hour_by_id($request['office_hour_id'], $site_code, 1);
        if(is_null($office_hour)){
            $respObj->setResponse(400, "failed", action_result("working hour", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start_hour_timestamp = $dayname == 'saturday' ? strtotime($office_hour->start_hour_saturday) : strtotime($office_hour->start_hour);
        $min_start_hour_timestamp = $start_hour_timestamp - 60*60; 
        $max_start_hour_timestamp = $start_hour_timestamp + 60*60; 
        if($timestamp < $min_start_hour_timestamp || $timestamp > $max_start_hour_timestamp){
            $respObj->setResponse(400, "failed", message("cannot absensi"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check absensi
        $list_absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $now);
        if(count($list_absensi) > 0){
			$absensi = $list_absensi[0];
			if($absensi->office_hour_id == $request['office_hour_id']){
				$respObj->setResponse(400, "failed", message("already absensi"));
				$resp = $respObj->getResponse();
				setOutput($resp);
				return;
			}

			if(is_null($absensi->end_hour) || $absensi->absensi_status != "END WORKING"){
				$respObj->setResponse(400, "failed", message("no end working"));
				$resp = $respObj->getResponse();
				setOutput($resp);
				return;
			}
        }

        $rec_id = null;
        for ($i=0; $i < 3; $i++) { 
            $rec_id_temp = generateRecIdV2();
            $exist = $this->tx_absensi_harian_model->get_absensi_harian_by_rec_id_and_date_and_site_code($rec_id_temp, $now, $site_code);
            if($exist){
                continue;
            }else{
                $rec_id = $rec_id_temp;
                break;
            }
        }
        
        if(is_null($rec_id)){
            $respObj->setResponse(400, "failed", "COBA ABSEN SEKALI LAGI");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['pegawai_id'] = $pegawai->id;
        $request['shift'] = $office_hour->shift;
        $request['start_hour'] = $time;
        $request['absensi_date'] = $now;
        $request['rec_id'] = $rec_id;
        $request['actor'] = "SELF";
        $request['is_weekend'] = strtolower($dayname) == 'sunday' ? 1 : 0;
        $request['is_holiday'] = is_holiday($now);
        $flag = $this->tx_absensi_harian_model->create_absensi_harian($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #update current shift on pegawai data
        $data_pegawai = array("id"=>$pegawai->id, "current_shift"=>$office_hour->shift);
        $this->pegawai_model->update_pegawai_custom($data_pegawai);

        $respObj->setResponse(200, "success", action_result("start working", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function end_working(){
        $respObj = new ResponseApi();

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai->id);
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $yesterday = date("Y-m-d", strtotime('yesterday'));
        $is_yesterday = 0;
        $dayname_yesterday = strtolower(date("l", strtotime('yesterday')));
        $dayname = strtolower(date("l"));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'PEGAWAI'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check absensi
        $list_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $now, "START WORKING");
        if(count($list_absensi_harian) <= 0){
            //check if absensi pulang is on diff day
            if($pegawai->current_shift == 'MALAM'){
                $yesterday_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $yesterday, "START WORKING");
                if(count($yesterday_absensi_harian) <= 0){
                    $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
                    $resp = $respObj->getResponse();
                    setOutput($resp);
                    return;
                }
                $list_absensi_harian = $yesterday_absensi_harian;
                $is_yesterday = 1;
            }else{
                $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }
		$absensi_harian = $list_absensi_harian[0];

        if(!intval($absensi_harian->is_verified)){
            $respObj->setResponse(400, "failed", message("absensi not verified"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        // if($absensi_harian->end_hour && $absensi_harian->absensi_status == 'END WORKING'){
        //     $respObj->setResponse(400, "failed", message("already absensi"));
        //     $resp = $respObj->getResponse();
        //     setOutput($resp);
        //     return;
        // }

        #check office hour
        $dayname                = $is_yesterday ? strtolower(date("l", strtotime('yesterday'))) : strtolower(date("l"));
        $office_hour            = $this->office_hour_model->get_office_hour_by_id($absensi_harian->office_hour_id);
        $end_hour               = $dayname == 'saturday' ? $office_hour->end_hour_saturday : $office_hour->end_hour;
        $end_hour_timestamp     = strtotime($end_hour);
        $max_end_hour_timestamp = $end_hour_timestamp + 60*60;
        if($timestamp < $end_hour_timestamp){
            if($pegawai->current_shift == 'MALAM' && isIndicateOvertime($end_hour)){
                $yesterday_end_hour_timestamp   = strtotime($yesterday . " " . $end_hour);
                $max_end_hour_timestamp         = $yesterday_end_hour_timestamp + 60*60;
                if($timestamp < $yesterday_end_hour_timestamp){
                    $respObj->setResponse(400, "failed", message("cannot absensi"));
                    $resp = $respObj->getResponse();
                    setOutput($resp);
                    return;
                }
            }else{
                $respObj->setResponse(400, "failed", message("cannot absensi"));
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        $is_overtime =  $timestamp > $max_end_hour_timestamp ? 1 : 0;
        $overtime_hour = $is_overtime ? ceil( ($timestamp - $max_end_hour_timestamp) / (60*60) ) : 0;
        
        #update absensi harian
        $request = (array) $absensi_harian;
        $request['is_overtime'] = $is_overtime;
        $request['overtime_hour'] = $overtime_hour;
        $request['end_hour'] = $time;
        $request['absensi_status'] = "END WORKING";
        $flag = $this->tx_absensi_harian_model->update_absensi_harian($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", action_result("end working", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function spv_start_working(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $dayname = strtolower(date("l"));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check param
        if(!checkParameterByKeys($request, array('office_hour_id'))){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour
        $office_hour = $this->office_hour_model->get_office_hour_by_id($request['office_hour_id'], $site_code, 1);
        if(is_null($office_hour)){
            $respObj->setResponse(400, "failed", action_result("working hour", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start_hour_timestamp = $dayname == 'saturday' ? strtotime($office_hour->start_hour_saturday) : strtotime($office_hour->start_hour);
        $min_start_hour_timestamp = $start_hour_timestamp - 60*60; 
        if($timestamp < $min_start_hour_timestamp){
            $respObj->setResponse(400, "failed", message("cannot absensi"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $late_timestamp = $timestamp > $start_hour_timestamp ? $timestamp - $start_hour_timestamp : 0;
        $late_hour = $late_timestamp ? ceil( $late_timestamp / (60*60) ) : 0;

        #check absensi
        $list_absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $now);
        if(count($list_absensi) > 0){
			$absensi = $list_absensi[0];
            if($absensi->office_hour_id == $request['office_hour_id']){
				$respObj->setResponse(400, "failed", message("already absensi"));
				$resp = $respObj->getResponse();
				setOutput($resp);
				return;
			}

			if(is_null($absensi->end_hour) || $absensi->absensi_status != "END WORKING"){
				$respObj->setResponse(400, "failed", message("no end working"));
				$resp = $respObj->getResponse();
				setOutput($resp);
				return;
			}
        }

        $rec_id = null;
        for ($i=0; $i < 3; $i++) { 
            $rec_id_temp = generateRecIdV2();
            $exist = $this->tx_absensi_harian_model->get_absensi_harian_by_rec_id_and_date_and_site_code($rec_id_temp, $now, $site_code);
            if($exist){
                continue;
            }else{
                $rec_id = $rec_id_temp;
                break;
            }
        }
        
        if(is_null($rec_id)){
            $respObj->setResponse(400, "failed", "COBA ABSEN SEKALI LAGI");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['pegawai_id'] = $pegawai->id;
        $request['shift'] = $office_hour->shift;
        $request['start_hour'] = $time;
        $request['absensi_date'] = $now;
        $request['rec_id'] = $rec_id;
        $request['actor'] = "SELF";
        $request['is_weekend'] = strtolower($dayname) == 'sunday' ? 1 : 0;
        $request['is_holiday'] = is_holiday($now);
        $request['is_late'] = $late_hour > 0 ? 1 : 0;
        // $request['late_hour'] = $late_hour;
        $request['late_hour'] = null;
        $flag = $this->tx_absensi_harian_model->create_absensi_harian($request, 1);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #update current shift on pegawai data
        $data_pegawai = array("id"=>$pegawai->id, "current_shift"=>$office_hour->shift);
        $this->pegawai_model->update_pegawai_custom($data_pegawai);

        $respObj->setResponse(200, "success", action_result("start working", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function spv_end_working(){
        $respObj = new ResponseApi();

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai->id);
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $yesterday = date("Y-m-d", strtotime('yesterday'));
        $is_yesterday = 0;
        $dayname_yesterday = strtolower(date("l", strtotime('yesterday')));
        $dayname = strtolower(date("l"));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check absensi
        $list_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $now, "START WORKING");
        if(count($list_absensi_harian) <= 0){
            //check if absensi pulang is on diff day
            if($pegawai->current_shift == 'MALAM'){
                $yesterday_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $yesterday, "START WORKING");
                if(count($yesterday_absensi_harian) <= 0){
                    $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
                    $resp = $respObj->getResponse();
                    setOutput($resp);
                    return;
                }
                $list_absensi_harian = $yesterday_absensi_harian;
                $is_yesterday = 1;
            }else{
                $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }
		$absensi_harian = $list_absensi_harian[0];

        if(!intval($absensi_harian->is_verified)){
            $respObj->setResponse(400, "failed", message("absensi not verified"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        // if($absensi_harian->end_hour && $absensi_harian->absensi_status == 'END WORKING'){
        //     $respObj->setResponse(400, "failed", message("already absensi"));
        //     $resp = $respObj->getResponse();
        //     setOutput($resp);
        //     return;
        // }

        #check office hour
        $dayname                = $is_yesterday ? strtolower(date("l", strtotime('yesterday'))) : strtolower(date("l"));
        $office_hour            = $this->office_hour_model->get_office_hour_by_id($absensi_harian->office_hour_id);
        $end_hour               = $dayname == 'saturday' ? $office_hour->end_hour_saturday : $office_hour->end_hour;
        $end_hour_timestamp     = strtotime($end_hour);
        $max_end_hour_timestamp = $end_hour_timestamp + 60*60;
        if($timestamp < $end_hour_timestamp){
            if($pegawai->current_shift == 'MALAM' && isIndicateOvertime($end_hour)){
                $yesterday_end_hour_timestamp   = strtotime($yesterday . " " . $end_hour);
                $max_end_hour_timestamp         = $yesterday_end_hour_timestamp + 60*60;
                if($timestamp < $yesterday_end_hour_timestamp){
                    $respObj->setResponse(400, "failed", message("cannot absensi"));
                    $resp = $respObj->getResponse();
                    setOutput($resp);
                    return;
                }
            }else{
                $respObj->setResponse(400, "failed", message("cannot absensi"));
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        $is_overtime =  $timestamp > $max_end_hour_timestamp ? 1 : 0;
        $overtime_hour = $is_overtime ? ceil( ($timestamp - $max_end_hour_timestamp) / (60*60) ) : 0;
        
        #update absensi harian
        $request = (array) $absensi_harian;
        $request['is_overtime'] = $is_overtime;
        $request['overtime_hour'] = $overtime_hour;
        $request['end_hour'] = $time;
        $request['absensi_status'] = "END WORKING";
        $flag = $this->tx_absensi_harian_model->update_absensi_harian($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", action_result("end working", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function spv_help_start_working(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $dayname = strtolower(date("l"));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check param
        if(!checkParameterByKeys($request, array('office_hour_id', 'nip'))){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check nip
        if($site_code != strtolower($request['nip'][0])){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check pegawai
        $helped_pegawai = $this->pegawai_model->get_pegawai_by_nip($request['nip']);
        if(is_null($helped_pegawai)){
            $respObj->setResponse(400, "failed", action_result("employee", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour
        $office_hour = $this->office_hour_model->get_office_hour_by_id($request['office_hour_id'], $site_code, 1);
        if(is_null($office_hour)){
            $respObj->setResponse(400, "failed", action_result("working hour", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start_hour_timestamp = $dayname == 'saturday' ? strtotime($office_hour->start_hour_saturday) : strtotime($office_hour->start_hour);
        $min_start_hour_timestamp = $start_hour_timestamp - 60*60; 
        if($timestamp < $min_start_hour_timestamp){
            $respObj->setResponse(400, "failed", message("cannot absensi"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $late_timestamp = $timestamp > $start_hour_timestamp ? $timestamp - $start_hour_timestamp : 0;
        $late_hour = $late_timestamp ? ceil( $late_timestamp / (60*60) ) : 0;

        #check absensi
        $list_absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($helped_pegawai->id, $now);
        if(count($list_absensi) > 0){
			$absensi = $list_absensi[0];
            if($absensi->office_hour_id == $request['office_hour_id']){
				$respObj->setResponse(400, "failed", message("already absensi"));
				$resp = $respObj->getResponse();
				setOutput($resp);
				return;
			}

			if(is_null($absensi->end_hour) || $absensi->absensi_status != "END WORKING"){
				$respObj->setResponse(400, "failed", message("no end working"));
				$resp = $respObj->getResponse();
				setOutput($resp);
				return;
			}
        }

        $rec_id = null;
        for ($i=0; $i < 3; $i++) { 
            $rec_id_temp = generateRecIdV2();
            $exist = $this->tx_absensi_harian_model->get_absensi_harian_by_rec_id_and_date_and_site_code($rec_id_temp, $now, $site_code);
            if($exist){
                continue;
            }else{
                $rec_id = $rec_id_temp;
                break;
            }
        }
        
        if(is_null($rec_id)){
            $respObj->setResponse(400, "failed", "COBA ABSEN SEKALI LAGI");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['pegawai_id'] = $helped_pegawai->id;
        $request['shift'] = $office_hour->shift;
        $request['start_hour'] = $time;
        $request['absensi_date'] = $now;
        $request['rec_id'] = $rec_id;
        $request['actor'] = "SUPERVISOR";
        $request['is_weekend'] = $dayname == 'sunday' ? 1 : 0;
        $request['is_holiday'] = is_holiday($now);
        $request['is_late'] = $late_hour > 0 ? 1 : 0;
        // $request['late_hour'] = $late_hour;
        $request['late_hour'] = null;
        $request['supervisor_id'] = $pegawai->id;
        $flag = $this->tx_absensi_harian_model->create_absensi_harian($request, 1);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #update current shift on pegawai data
        $data_pegawai = array("id"=>$helped_pegawai->id, "current_shift"=>$office_hour->shift);
        $this->pegawai_model->update_pegawai_custom($data_pegawai);

        $respObj->setResponse(200, "success", action_result("start working", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function spv_help_end_working(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai->id);
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $dayname = strtolower(date("l"));
        $yesterday = date("Y-m-d", strtotime('yesterday'));
        $is_yesterday = 0;
        $dayname_yesterday = strtolower(date("l", strtotime('yesterday')));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        #check param
        if(!checkParameterByKeys($request, array('nip', 'reason'))){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $reason = $request['reason'];

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check nip
        if($site_code != strtolower($request['nip'][0])){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check pegawai
        $helped_pegawai = $this->pegawai_model->get_pegawai_by_nip($request['nip']);
        if(is_null($helped_pegawai)){
            $respObj->setResponse(400, "failed", action_result("employee", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check absensi
        $list_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($helped_pegawai->id, $now, "START WORKING");
        if(count($list_absensi_harian) <= 0){
            //check if absensi pulang is on diff day
            if($pegawai->current_shift == 'MALAM'){
                $yesterday_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($helped_pegawai->id, $yesterday, "START WORKING");
                if(count($yesterday_absensi_harian) <= 0){
                    $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
                    $resp = $respObj->getResponse();
                    setOutput($resp);
                    return;
                }
                $list_absensi_harian = $yesterday_absensi_harian;
                $is_yesterday   = 1;
            }else{
                $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }
		$absensi_harian = $list_absensi_harian[0];

        if(!intval($absensi_harian->is_verified)){
            $respObj->setResponse(400, "failed", message("absensi not verified"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        // if($absensi_harian->end_hour && $absensi_harian->absensi_status == 'END WORKING'){
        //     $respObj->setResponse(400, "failed", message("already absensi"));
        //     $resp = $respObj->getResponse();
        //     setOutput($resp);
        //     return;
        // }

        #check office hour
        $dayname                = $is_yesterday ? strtolower(date("l", strtotime('yesterday'))) : strtolower(date("l"));
        $office_hour            = $this->office_hour_model->get_office_hour_by_id($absensi_harian->office_hour_id);
        $end_hour               = $dayname == 'saturday' ? $office_hour->end_hour_saturday : $office_hour->end_hour;
        $end_hour_timestamp     = strtotime($end_hour);
        $max_end_hour_timestamp = $end_hour_timestamp + 60*60;
        if($timestamp < $end_hour_timestamp){
            if($helped_pegawai->current_shift == 'MALAM' && isIndicateOvertime($end_hour)){
                $yesterday_end_hour_timestamp   = strtotime($yesterday . " " . $end_hour);
                $max_end_hour_timestamp         = $yesterday_end_hour_timestamp + 60*60;
            }
        //     $respObj->setResponse(400, "failed", message("cannot absensi"));
        //     $resp = $respObj->getResponse();
        //     setOutput($resp);
        //     return;
        }
        
        $is_overtime =  $timestamp > $max_end_hour_timestamp ? 1 : 0;
        $overtime_hour = $is_overtime ? ceil( ($timestamp - $max_end_hour_timestamp) / (60*60) ) : 0;

        #update absensi harian
        $request = (array) $absensi_harian;
        $request['reason'] = $reason;
        $request['is_overtime'] = $is_overtime;
        $request['overtime_hour'] = $overtime_hour;
        $request['end_hour'] = $time;
        $request['absensi_status'] = "END WORKING";
        $flag = $this->tx_absensi_harian_model->update_absensi_harian($request);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", action_result("end working", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function verify_absensi_pegawai(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);
        $dayname = strtolower(date("l"));

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check param
        if(!checkParameterByKeys($request, array('pegawai_id', 'rec_id'))){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check absensi
        // $absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_rec_id_and_site_code($request['pegawai_id'], $request['rec_id'], $site_code);
		$absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_rec_id_and_date_and_site_code($request['rec_id'], $now, $site_code);
        if(is_null($absensi)){
            $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($absensi->is_verified){
            $respObj->setResponse(400, "failed", message("already verify"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour
        $office_hour = $this->office_hour_model->get_office_hour_by_id($absensi->office_hour_id);
        $start_hour_timestamp = $dayname == 'saturday' ? strtotime($absensi->absensi_date . " " . $office_hour->start_hour_saturday) : strtotime($absensi->absensi_date . " " . $office_hour->start_hour);
        $verify_hour_timestamp = $timestamp - 2*60*60; 
        if($verify_hour_timestamp < $start_hour_timestamp){
            $respObj->setResponse(400, "failed", message("cannot verify"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #verify absen
        $flag = $this->tx_absensi_harian_model->verify_absensi_harian($absensi->id, $pegawai->id);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", action_result("verify absensi", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function verify_absensi_pegawai_bulk(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);
        $dayname = strtolower(date("l"));

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check param
        if(!checkParameterByKeys($request, array('bulk'))){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $bulk = $request['bulk'];

        if(!is_array($bulk)){
            $respObj->setResponse(400, "failed", message("bad request"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $response_bulk = [];
        foreach ($bulk as $item) {
            #check absensi
            // $absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_rec_id_and_site_code($item['pegawai_id'], $item['rec_id'], $site_code);
			$absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_rec_id_and_date_and_site_code($item['rec_id'], $now, $site_code);
            if(is_null($absensi)){
                $item['status'] = 'failed';
                $item['message']= action_result("absensi", "not found");
                array_push($response_bulk, $item);
                continue;
            }
            $item['pegawai_name'] = $absensi->pegawai_name;
            $item['shift'] = $absensi->shift;
            $item['datecreated'] = $absensi->datecreated;

            if($absensi->is_verified){
                $item['status'] = 'failed';
                $item['message']= message("already verify");
                array_push($response_bulk, $item);
                continue;
            }

            #check office hour
            $office_hour = $this->office_hour_model->get_office_hour_by_id($absensi->office_hour_id);
            $start_hour_timestamp = $dayname == 'saturday' ? strtotime($absensi->absensi_date . " " . $office_hour->start_hour_saturday) : strtotime($absensi->absensi_date . " " . $office_hour->start_hour);
            $verify_hour_timestamp = $timestamp - 2*60*60; 
            if($verify_hour_timestamp < $start_hour_timestamp){
                $item['status'] = 'failed';
                $item['message']= message("cannot verify");
                array_push($response_bulk, $item);
                continue;
            }

            #verify absen
            $flag = $this->tx_absensi_harian_model->verify_absensi_harian($absensi->id, $pegawai->id);
            if(!$flag){
                $item['status'] = 'failed';
                $item['message']= message("internal server error");
                array_push($response_bulk, $item);
                continue;
            }

            $item['status'] = 'success';
            $item['message']= action_result("verify absensi", "success");
            array_push($response_bulk, $item);
        }

        $respObj->setResponse(200, "success", action_result("verify absensi bulk", "success"), $response_bulk);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function verify_absensi_spv(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check param
        if(!checkParameterByKeys($request, array('rec_id'))){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check absensi
        // $absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_rec_id_and_site_code($pegawai->id, $request['rec_id'], $site_code, $now);
		$absensi = $this->tx_absensi_harian_model->get_absensi_harian_by_rec_id_and_date_and_site_code($request['rec_id'], $now, $site_code);
        if(is_null($absensi)){
            $respObj->setResponse(400, "failed", action_result("absensi", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($absensi->is_verified){
            $respObj->setResponse(400, "failed", message("already verify"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($absensi->pegawai_id != $pegawai->id){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check office hour
        $office_hour = $this->office_hour_model->get_office_hour_by_id($absensi->office_hour_id);
        $start_hour_timestamp = strtotime($office_hour->start_hour);
        $verify_hour_timestamp = $start_hour_timestamp + 3*60*60; 
        if($timestamp < $verify_hour_timestamp){
            $respObj->setResponse(400, "failed", message("cannot verify"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #verify absen
        $flag = $this->tx_absensi_harian_model->verify_absensi_harian($absensi->id, $pegawai->id);
        if(!$flag){
            $respObj->setResponse(500, "failed", message("internal server error"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", action_result("verify absensi", "success"), $request);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_absensi_history(){
        $respObj = new ResponseApi();
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $date = $this->input->get('date');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get spl titipan
        $start = $page_number * $page_size;
        $order = array('field'=>"datecreated", 'order'=>"DESC");
        $limit = array('start'=>$start, 'size'=>$page_size);

        $absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id($pegawai->id, $date, $order, $limit);
        $absensi_harian_total = $this->tx_absensi_harian_model->count_absensi_harian_by_pegawai_id($pegawai->id, $date);

        #responses
        $respObj->setResponseDatatable(200, $absensi_harian, 1, $absensi_harian_total, $absensi_harian_total);
        $resp = $respObj->getResponseDatatable();
        setOutput($resp);
        return;
    }

    function get_spv_absensi_list(){
        $respObj = new ResponseApi();
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get spl titipan
        $start = $page_number * $page_size;
        $order = array('field'=>"datecreated", 'order'=>"DESC");
        $limit = array('start'=>$start, 'size'=>$page_size);

        $absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian($search, $now, $site_code, $order, $limit);
        $absensi_harian_total = $this->tx_absensi_harian_model->get_absensi_harian_total($search, $now, $site_code);

        #responses
        $respObj->setResponseDatatable(200, $absensi_harian, 1, $absensi_harian_total, $absensi_harian_total);
        $resp = $respObj->getResponseDatatable();
        setOutput($resp);
        return;
    }

    function get_spv_absensi_detail($id){
        $respObj = new ResponseApi();
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai->id);
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $yesterday = date("Y-m-d", strtotime('yesterday'));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get absensi harian
        $absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_id_and_site_code($id, $site_code);
        if(is_null($absensi_harian)){
			$respObj->setResponse(400, "failed", action_result("absensi", "not found"));
			$resp = $respObj->getResponse();
			setOutput($resp);
			return;
        }
        #responses
        $respObj->setResponse(200, "success", action_result("get absensi harian detail", "success"), $absensi_harian);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_absensi_detail_self(){
        $respObj = new ResponseApi();
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai->id);
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $yesterday = date("Y-m-d", strtotime('yesterday'));
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        #get absensi harian
		$absensi_harian = null;
		$today_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $now, "START WORKING");
		if(count($today_absensi_harian) <= 0){
			$today_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $now);
			if(count($today_absensi_harian) <= 0){
				if($pegawai->current_shift == 'MALAM'){
					$yesterday_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $yesterday, "START WORKING");
					if(count($yesterday_absensi_harian) <= 0){
						$yesterday_absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_pegawai_id_and_date($pegawai->id, $yesterday);
						if(count($yesterday_absensi_harian) <= 0){
							$respObj->setResponse(400, "failed", action_result("absensi", "not found"),1);
							$resp = $respObj->getResponse();
							setOutput($resp);
							return;
						}
					}
					$absensi_harian = $yesterday_absensi_harian[0];
				}else{
					$respObj->setResponse(400, "failed", action_result("absensi", "not found"),2);
					$resp = $respObj->getResponse();
					setOutput($resp);
					return;
				}
			}else{
				$absensi_harian = $today_absensi_harian[0];
			}
		}else{
			$absensi_harian = $today_absensi_harian[0];
		}

        #responses
        $respObj->setResponse(200, "success", action_result("get absensi harian detail self", "success"), $absensi_harian);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_absensi_by_code($code){
        $respObj = new ResponseApi();
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get absensi harian
        $absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_rec_id_and_date_and_site_code($code, $now, $site_code);
        if(is_null($absensi_harian)){
            $respObj->setResponse(404, "failed", action_result("absensi", "not found"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #responses
        $respObj->setResponse(200, "success", action_result("get absensi harian detail", "success"), $absensi_harian);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_pegawai_list(){
        $respObj = new ResponseApi();
        #authentication
        $jwt = verifyAppsToken();
        if(is_null($jwt)){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $jwt->data;
        $site_code = strtolower($pegawai->nip[0]);
        $now = date("Y-m-d");
        $timestamp = time(); 
        $time = date('H:i:s', $timestamp);

        if($pegawai->role != 'SUPERVISOR'){
            $respObj->setResponse(401, "failed", message("unauthorized access"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $nama = $this->input->get('nama');
        $params = array($page_number, $page_size, $nama);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get spl titipan
        $start = $page_number * $page_size;
        $order = array('field'=>"datecreated", 'order'=>"DESC");
        $limit = array('start'=>$start, 'size'=>$page_size);

        $pegawai = $this->pegawai_model->get_pegawai_by_name_and_site_code($nama, $site_code, $order, $limit);
        $pegawai_total = $this->pegawai_model->count_pegawai_by_name_and_site_code($nama, $site_code);

        #responses
        $respObj->setResponseDatatable(200, $pegawai, 1, $pegawai_total, $pegawai_total);
        $resp = $respObj->getResponseDatatable();
        setOutput($resp);
        return;
    }

    #CRON
    function overtime_reminder(){
        $timestamp = time();
        $now = date("Y-m-d");
        $sites = array('a','c','r','s','t');

        foreach ($sites as $site) {
            $office_hour = $this->office_hour_model->get_office_hour(null, $site);
            $list_supervisor = $this->pegawai_model->get_pegawai_by_role_and_site('SUPERVISOR', $site);
            foreach ($office_hour as $shift) {
                $end_hour = $shift->end_hour;
                $end_hour_timestamp = strtotime($end_hour);
                $min_overtime_hour_timestamp = $end_hour_timestamp + 60*60; 

                $a = array(
                    "min_overtime_hour_timestamp" => $min_overtime_hour_timestamp,
                    "timestamp" => $timestamp
                );
                if($timestamp < $min_overtime_hour_timestamp){
                    continue;
                }

                $absensi_overtime = $this->tx_absensi_harian_model->get_absensi_harian_by_date_and_office_hour($now, $shift->id);
                foreach ($absensi_overtime as $item) {
                    if($item->absensi_status != 'END WORKING'){
                        foreach ($list_supervisor as $supervisor) {
                            $pegawai_name = $item->pegawai_name;
                            $message = "Pegawai $pegawai_name belum melakukan ABSENSI PULANG. Harap pastikan apakah pegawai melakukan lembur atau tidak.";
                            $wa_response = send_message($supervisor->telepon, $message);
                        }
                    }
                }
            }
        }

        
    }
}

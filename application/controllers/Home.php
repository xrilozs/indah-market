<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  function __construct(){
    parent::__construct();
    date_default_timezone_set("Asia/Jakarta");
  }
  
	public function index()
	{
		auth_redirect();
		$data['title']					= 'Dashboard';
		$data['content']				= 'dashboard';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function login(){
		if(empty($this->session->userdata('user_login')))
		$this->load->view('login');
		else redirect('dashboard');
	}

	function hutang()
	{
		auth_redirect();
		$data['title']					= 'Hutang';
		$data['content']				= 'hutang/hutang_input';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function user_manager()
	{
		auth_redirect();
		$data['title']					= 'User Manager';
		$data['content']				= 'user_manager';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function produksi_harian()
	{
		auth_redirect();
		$data['title']					= 'Produksi Harian';
		$data['content']				= 'produksi/produksi_harian';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function returan()
	{
		auth_redirect();
		$data['title']					= 'Returan';
		$data['content']				= 'produksi/returan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function pelunasan_hutang()
	{
		auth_redirect();
		$data['title']					= 'Update Pelunasan Hutang';
		$data['content']				= 'hutang/pelunasan_hutang';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function pelunasan_faktur()
	{
		auth_redirect();
		$data['title']					= 'Update Pelunasan Faktur';
		$data['content']				= 'piutang/update_pelunasan_faktur';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function nota_penjualan()
	{
		auth_redirect();
		$data['title']					= 'Nota Penjualan';
		$data['content']				= 'piutang/nota_penjualan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function report_produksi_bulanan()
	{
		auth_redirect();
		$data['title']					= 'Laporan Produksi Bulanan';
		$data['content']				= 'produksi/report_produksi_bulanan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
    
    function report_nota_penjualan()
	{
		auth_redirect();
		$data['title']					= 'Laporan Nota Penjualan';
		$data['content']				= 'piutang/report_nota_penjualan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
    
    function report_faktur_harian()
	{
		auth_redirect();
		$data['title']					= 'Laporan Faktur Harian';
		$data['content']				= 'piutang/report_faktur_harian';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function report_produksi_harian()
	{
		auth_redirect();
		$data['title']					= 'Laporan Produksi Harian';
		$data['content']				= 'produksi/report_produksi_harian';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function report_returan_bulanan()
	{
		auth_redirect();
		$data['title']					= 'Laporan Returan Bulanan';
		$data['content']				= 'produksi/report_returan_bulanan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function report_returan_harian()
	{
		auth_redirect();
		$data['title']					= 'Laporan Returan Harian';
		$data['content']				= 'produksi/report_returan_harian';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  	function report_pembelian_bulanan()
	{
		auth_redirect();
		$data['title']					= 'Laporan Pembelian Bulanan';
		$data['content']				= 'hutang/report_pembelian_perbulan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  	function report_pembelian_harian()
	{
		auth_redirect();
		$data['title']					= 'Laporan Produksi Harian';
		$data['content']				= 'hutang/report_pembelian_harian';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  
  function update_returan()
	{
		auth_redirect();
		$data['title']					= 'Update Returan';
		$data['content']				= 'produksi/update_returan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function myprofile()
	{
		auth_redirect();
		$data['title']					= 'Profilku';
		$data['content']				= 'myprofile';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function customer()
	{
		auth_redirect();
		$data['title']					= 'Customer';
		$data['content']				= 'piutang/customer';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
    function faktur()
	{
		auth_redirect();
		$data['title']					= 'Faktur';
		$data['content']				= 'piutang/faktur';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
    
    function surat_jalan()
	{
		auth_redirect();
		$data['title']					= 'Faktur';
		$data['content']				= 'delivery/surat_jalan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function edit_user($id_user)
	{
		auth_redirect();
		$where									= array(
			'id'	=> $id_user
		);
		$user_qry								= $this->user_model->read_user($where);
		if($user_qry->num_rows()!=0){
      $data_user = $user_qry->row();
      $data_json = array(
        'info'=>'success',
        'name'=>$data_user->name,
        'username'=>$data_user->username,
        'phone'=>$data_user->phone,
        'type'=>$data_user->type,
        'id'=>$data_user->id,
        'site_code'=>$data_user->site_code
      );
      die(json_encode($data_json));
    } else {
      $data_json = array(
        'info'=>'error'
      );
      die(json_encode($data_json));
    }
	}
  
  	function edit_returan($retur_id){
		#authentication
		$roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

		$where = "A.id='".$retur_id."'";
		$retur_qry								= $this->retur_model->read_retur($where);
		if($retur_qry->num_rows()!=0){
			$data_retur = $retur_qry->row();
			$data_json = array(
				'info'=>'success',
				'id'=>$data_retur->id,
				'site_code'=>$data_retur->site_code,
				'customer_id'=>$data_retur->customer_id,
				'tanggal_diterima'=>$data_retur->tanggal_diterima,
				'ukuran'=>$data_retur->ukuran,
				'jumlah_retur_kg'=>$data_retur->jumlah_retur_kg,
				'jumlah_retur_lembar'=>$data_retur->jumlah_retur_lembar,
				'alasan_retur'=>$data_retur->alasan_retur,
				'tindak_lanjut'=>$data_retur->tindak_lanjut,
				'tanda_terima'=>$data_retur->tanda_terima
			);
			die(json_encode($data_json));
		} else {
			$data_json = array(
				'info'=>'error'
			);
			die(json_encode($data_json));
		}
	}

	function edit_fakturin($fakturin_id){
		#authentication
		$roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }
		
		$where = "A.id='".$fakturin_id."'";
		$fakturin_qry								= $this->fakturin_model->read_fakturin($where);
		if($fakturin_qry->num_rows()!=0){
     		$data_fakturin = $fakturin_qry->row();
			$data_json = array(
				'info'=>'success',
				'id'=>$data_fakturin->id,
				'status'=>$data_fakturin->status,
				'jumlah_bayar'=>$data_fakturin->jumlah_bayar,
				'tanggal_bayar'=>$data_fakturin->tanggal_bayar,
				'keterangan_pembayaran'=>$data_fakturin->keterangan_pembayaran,
			);
			die(json_encode($data_json));
		} else {
			$data_json = array(
				'info'=>'error'
			);
			die(json_encode($data_json));
		}
	}
  
  	function edit_faktur($faktur_id){
		#authentication
		$roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

		$where = "A.id='".$faktur_id."'";
		$fakturin_qry = $this->faktur_model->read_faktur($where);
		if($fakturin_qry->num_rows()!=0){
			$data_fakturin = $fakturin_qry->row();
			$data_json = array(
				'info'=>'success',
				'id'=>$data_fakturin->id,
				'status_lunas'=>$data_fakturin->status,
				'jumlah_bayar'=>$data_fakturin->jumlah_bayar,
				'tanggal_bayar'=>$data_fakturin->tanggal_bayar,
				'keterangan_pembayaran'=>$data_fakturin->keterangan_pembayaran,
			);
			die(json_encode($data_json));
		} else {
			$data_json = array(
				'info'=>'error'
			);
			die(json_encode($data_json));
		}
	}
  
	function edit_customer($customer_id){
		#authentication
		$roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

		$where = array(
			'id' => $customer_id
		);
		$customer_qry = $this->customer_model->read_customer($where);
		if($customer_qry->num_rows()!=0){
			$data_customer = $customer_qry->row();
			$data_json = array(
				'info'=>'success',
				'id'=>$data_customer->id,
				'name'=>$data_customer->name,
				'work_phone'=>$data_customer->work_phone,
				'contact_person'=>$data_customer->contact_person,
				'address_1'=>$data_customer->address_1,
				'address_2'=>$data_customer->address_2,
				'tax_non_tax'=>$data_customer->tax_non_tax,
				'limit_plafon'=>$data_customer->limit_plafon,
				'business_type'=>$data_customer->business_type,
				'nik_ktp'=>$data_customer->nik_ktp,
				'no_npwp'=>$data_customer->no_npwp,
				'no_hp'=>$data_customer->no_hp,
				'sumber_info'=>$data_customer->sumber_info,
				'tipe_lampiran'=>$data_customer->tipe_lampiran,
				'cara_bayar'=>$data_customer->cara_bayar,
				'mkt'=>$data_customer->mkt,
				'pembayaran_ekspedisi'=>$data_customer->pembayaran_ekspedisi,
				'acc_oleh'=>$data_customer->acc_oleh,
				'rating'=>$data_customer->rating
			);
			die(json_encode($data_json));
		} else {
			$data_json = array(
				'info'=>'error'
			);
			die(json_encode($data_json));
		}
	}

	function edit_prewed($id_prewed)
	{
		auth_redirect();
		$where									= array(
			'id'	=> $id_prewed
		);
		$prewed_qry								= $this->prewed_model->read_prewed($where);
		if($prewed_qry->num_rows()!=0){
		$data['prewed']						=	$prewed_qry->row();
		$data['title']					= 'Edit Prewed';
		$data['content']				= 'edit_prewed';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	} else {
		redirect('404');
	}
	}

	function error_page()
	{
		$data['title']					= '404 Page not found';
		$data['content']				= '404';
		$data['member'] 				= get_current_member();
		$
		$this->load->view('template', $data);
	}

	function order()
	{
		auth_redirect();
		$data['title']					= 'Pemesanan';
		$data['content']				= 'pemesanan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function register()
	{
		$data['title']					= 'Register';
		$data['content']				= 'Register';
		$this->load->view('register', $data);
	}

	function cart()
	{
		auth_redirect();
		$cart_details = array();
		$total_harga	= 0;
		if(!empty($this->session->userdata('cart'))){
      foreach($this->session->userdata('cart') as $cart_content){
				$where 									= array('id'=>$cart_content['book_id']);
        $product_data           = $this->book_model->read_book($where)->row();
				$cart_details[] 				= array('book_id'=>$product_data->id, 'name'=>$product_data->name, 'price'=>$product_data->price, 'pict'=>$product_data->pict);
			}
		}
		foreach($cart_details as $cart_details_item){
			$total_harga+=$cart_details_item['price'];
		}
		$current_member         = get_current_member();
    $cart_details           = array();
    $total_price            = 0;
    $price                  = 0;
		$product_qty 						= array();
    if(!empty($this->session->userdata('cart'))){
      foreach($this->session->userdata('cart') as $cart_content){
				$where 									= array('id'=>$cart_content['book_id']);
        $product_data           = $this->book_model->read_book($where)->row();
        // $product_data           = $this->model_member->read_product_option_by_product_key($cart_content['product_key'])->row();
        $price                  = $product_data->price;
        // echo $product_data->product_name;
        $cart_details[]         = array(
					'product_key'					=> $product_data->id,
          'product_name'                => $product_data->name,
          'product_image'       => $product_data->pict,
          'product_price'       => $price
        );
				//menghitung qty per kode produk
				if(empty($product_qty[$cart_content['book_id']])) {
					$product_qty[$cart_content['book_id']]=array('qty'=>1, 'product_key'=>$product_data->id, 'product_price'=>$price, 'product_image'=>$product_data->pict,  'product_name'=>$product_data->name,'sub_total'=>$price);
				}
				else {
					$product_qty[$cart_content['book_id']]['qty']+=1;
					$product_qty[$cart_content['book_id']]['sub_total']+=$price;
				}
      }
    }
    // print_r($cart_details);
    // menghitung total harga
    foreach($cart_details as $cart_item){
      $total_price  += $cart_item['product_price'];
    }
    $data['title']          = 'Cart';
    $data['member']         = $current_member;
    $data['cart_details']   = $cart_details;
    $data['total_price']    = $total_price;
		$data['cart_product']		=	 $product_qty;
    $data['content']   			= 'cart';
    $this->load->view('template', $data);
	}
	function checkout(){
		auth_redirect();
    $total_price = 0;
		$total_price = 0;
    $current_member         = get_current_member();
    if(!empty($this->session->userdata('cart'))){
      foreach($this->session->userdata('cart') as $cart_content){
        // echo $cart_content;
				$where 									= array('id'=>$cart_content['book_id']);
        $product_data           = $this->book_model->read_book($where)->row();
        $price                  = $product_data->price;
        // echo $product_data->product_name;
        $cart_details[]         = array(
          'name'                => $product_data->name,
          'product_image'       => $product_data->pict,
          'product_price'       => $price
        );
      }
    }
    // print_r($cart_details);
    // menghitung total harga
    foreach($cart_details as $cart_item){
      $total_price  += $cart_item['product_price'];
    }
    $total_product          = count($this->session->userdata('cart'));
    $shipment_cost          = SHIPMENT_COST;
    $total_shipment_cost    = $total_product*$shipment_cost;
		$total_cost 					  = $total_price+$total_shipment_cost;
    $data['title']          = 'Checkout';
    $data['member']         = $current_member;
    $data['total_shipment_cost'] = $total_shipment_cost;
    $data['total_product']  = $total_product;
    $data['total_price']    = $total_price;
		$data['total_cost']			= $total_cost;
    $data['content']   = 'checkout';
    $this->load->view('template', $data);
  }

	function detail_order($order_id)
	{
		auth_redirect();
		$data['title']					= 'Order Detail';
		$data['content']				= 'detail_order';
		$data['member'] 				= get_current_member();
		$data['order_id']				= $order_id;
		$this->load->view('template', $data);
	}

	function prewed_order_manager()
	{
		auth_redirect();
		$data['title']					= 'Prewed Order Manager';
		$data['content']				= 'prewed_order_manager';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function others_order_manager()
	{
		auth_redirect();
		$data['title']					= 'Others Order Manager';
		$data['content']				= 'others_order_manager';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function penjualan_piutang_bulanan()
	{
		auth_redirect();
		$data['title']					= 'Laporan Penjualan Bulanan';
		$data['content']				= 'piutang/report_penjualan_perbulan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
  function penjualan_piutang_customer()
	{
		auth_redirect();
		$data['title']					= 'Laporan Penjualan PerCustomer';
		$data['content']				= 'piutang/report_penjualan_percustomer';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
  
    function faktur_pajak_bulanan()
	{
		auth_redirect();
		$data['title']					= 'Laporan Faktur Pajak';
		$data['content']				= 'piutang/report_faktur_pajak_perbulan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

    function edit_produksi($id=0){
        #authentication
		$roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $where                          = "A.id='".$id."'";
        $data['produksi']               = $this->produksi_model->read_produksi($where)->row();
        $data['title']					= 'Edit Produksi';
		$data['content']				= 'produksi/edit_produksi';
		$data['member'] 				= $user;
		$this->load->view('template', $data);
    }
	
    function kwitansi()
	{
		auth_redirect();
		$data['title']					= 'Kwitansi';
		$data['content']				= 'piutang/kwitansi';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
	function spl()
	{
		auth_redirect();
		$data['title']					= 'Surat Pesanan Langganan';
		$data['content']				= 'spl/list';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	} 
	function spl_titipan()
	{
		auth_redirect();
		$data['title']					= 'SPL Titipan';
		$data['content']				= 'spl/list_titipan';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	} 
	function spl_monthly_report()
	{
		auth_redirect();
		$data['title']					= 'SPL - Laporan Bulanan';
		$data['content']				= 'spl/monthly_report';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function spl_delete()
	{
		auth_redirect();
		$data['title']					= 'SPL - Hapus Data';
		$data['content']				= 'spl/delete';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
	
	function spl_limit()
	{
		auth_redirect();
		$data['title']					= 'SPL - Limit';
		$data['content']				= 'spl/limit';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	} 

	function spl_burn()
	{
		auth_redirect();
		$data['title']					= 'SPL - Burn';
		$data['content']				= 'spl/burn';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	} 

	function pegawai()
	{
		auth_redirect();
		$data['title']					= 'Pegawai';
		$data['content']				= 'absensi/pegawai';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function absensi()
	{
		auth_redirect();
		$data['title']					= 'Absensi';
		$data['content']				= 'absensi/absensi';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	} 

	function histori_absensi()
	{
		auth_redirect();
		$data['title']					= 'Histori Absensi';
		$data['content']				= 'absensi/histori_absensi';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function kasbon_pegawai()
	{
		auth_redirect();
		$data['title']					= 'Kasbon Pegawai';
		$data['content']				= 'absensi/kasbon_pegawai';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function setor_barang_jadi()
	{
		auth_redirect();
		$data['title']					= 'Setor Barang Jadi';
		$data['content']				= 'delivery/setor_barang_jadi';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function terima_barang_jadi()
	{
		auth_redirect();
		$data['title']					= 'Terima Barang Jadi';
		$data['content']				= 'delivery/terima_barang_jadi';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function data_barang()
	{
		auth_redirect();
		$data['title']					= 'Data Barang';
		$data['content']				= 'delivery/data_barang';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function keyword()
	{
		auth_redirect();
		$data['title']					= 'Keyword';
		$data['content']				= 'keyword/list_keyword';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function office_hour()
	{
		auth_redirect();
		$data['title']					= 'Office Hour';
		$data['content']				= 'office_hour/list_office_hour';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function jurnal_absensi()
	{
		auth_redirect();
		$data['title']					= 'Jurnal Absensi';
		$data['content']				= 'absensi/jurnal_absensi';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function delete_jurnal_absensi()
	{
		auth_redirect();
		$data['title']					= 'Delete Jurnal Absensi';
		$data['content']				= 'absensi/delete_jurnal_absensi';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function site_location()
	{
		auth_redirect();
		$data['title']					= 'Site Location';
		$data['content']				= 'site_location/page';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}

	function hari_libur()
	{
		auth_redirect();
		$data['title']					= 'Hari Libur';
		$data['content']				= 'hari_libur/list_hari_libur';
		$data['member'] 				= get_current_member();
		$this->load->view('template', $data);
	}
}

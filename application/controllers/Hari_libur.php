<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hari_libur extends CI_Controller
{
  function __construct(){
      parent::__construct();
      date_default_timezone_set("Asia/Jakarta");
  }

/*---------Start of hari libur---------*/
  function get_hari_libur(){
    $respObj = new ResponseApi();
    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
        $respObj->setResponse(401, "failed", "Unauthorized access");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
    $site_code = $user->site_code;

    $page_number = $this->input->get('page_number');
    $page_size = $this->input->get('page_size');
    $search = $this->input->get('search');
    $year = $this->input->get('year');
    $sort = $this->input->get('sort');
    $order_by = $this->input->get('order_by');
    $draw = $this->input->get('draw');
    $params = array($page_number, $page_size);

    #check request params
    if(!checkParameter($params)){
        $respObj->setResponse(400, "failed", "Missing parameter.", $params);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $start = $page_number * $page_size;
    $order = array('field'=>$order_by, 'order'=>$sort);
    $limit = array('start'=>$start, 'size'=>$page_size);
    $hari_liburs = $this->hari_libur_model->get_hari_libur($search, $year, $order, $limit);
    $total = $this->hari_libur_model->get_hari_libur_total($search, $year);

    #response
    $respObj->setResponseDatatable(200, $hari_liburs, $draw, $total, $total);
    $resp = $respObj->getResponseDatatable();
    setOutput($resp);
    return;
  }

  function get_hari_libur_by_id($id){
    $respObj = new ResponseApi();
    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
        $respObj->setResponse(401, "failed", "Unauthorized access");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $hari_libur = $this->hari_libur_model->get_hari_libur_by_id($id);
    $respObj->setResponse(200, "success", "Get hari libur by id success", $hari_libur);
    $resp = $respObj->getResponse();
    setOutput($resp);
    return;
  }

  function create_hari_libur(){
    $respObj = new ResponseApi();
    $request = json_decode($this->input->raw_input_stream, true);

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
        $respObj->setResponse(401, "failed", "Unauthorized access");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    if(!checkParameterByKeys($request, array('date', 'description'))){
        $respObj->setResponse(400, "failed", "Missing mandatory request");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $flag = $this->hari_libur_model->create_hari_libur($request);
    if(!$flag){
        $respObj->setResponse(500, "failed", "Internal server error! Insert hari libur failed");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $respObj->setResponse(200, "success", "Create hari libur success", $request);
    $resp = $respObj->getResponse();
    setOutput($resp);
    return;
  }

  function update_hari_libur(){
    $respObj = new ResponseApi();
    $request = json_decode($this->input->raw_input_stream, true);

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
        $respObj->setResponse(401, "failed", "Unauthorized access");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    if(!checkParameterByKeys($request, array('id', 'date', 'description'))){
        $respObj->setResponse(400, "failed", "Missing mandatory request");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    #check hari libur exist
    $hari_libur = $this->hari_libur_model->get_hari_libur_by_id($request['id']);
    if(is_null($hari_libur)){
        $respObj->setResponse(404, "failed", "Hari Libur not found");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $request['datecreated'] = $hari_libur->datecreated;
    $flag = $this->hari_libur_model->update_hari_libur($request);
    if(!$flag){
        $respObj->setResponse(500, "failed", "Internal server error! Update hari libur failed");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $respObj->setResponse(200, "success", "Update hari libur success", $request);
    $resp = $respObj->getResponse();
    setOutput($resp);
    return;
  }

  function delete_hari_libur($id){
    $respObj = new ResponseApi();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
        $respObj->setResponse(401, "failed", "Unauthorized access");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    #check hari libur exist
    $hari_libur = $this->hari_libur_model->get_hari_libur_by_id($id);
    if(is_null($hari_libur)){
        $respObj->setResponse(404, "failed", "Hari libur not found");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $flag = $this->hari_libur_model->delete_hari_libur($id);
    if(!$flag){
        $respObj->setResponse(500, "failed", "Internal server error! Delete hari libur failed");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    $respObj->setResponse(200, "success", "Delete hari libur success", $flag);
    $resp = $respObj->getResponse();
    setOutput($resp);
    return;
  }
/*---------End of hari libur---------*/
}
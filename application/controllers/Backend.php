<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Backend extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    
/*------Start of User-------*/
    function delete_token(){
        $username = $this->input->get('username');
        $user     = $this->user_model->get_user_by_username($username);
        if ($user) {
            $flag = $this->user_model->logout($username);

            if($flag){
                die('<div class="alert alert-success"><strong>Sukses!</strong> Token berhasil dihapus</div>');
            }else{
                die('<div class="alert alert-danger"><strong>Error!</strong> Token gagal dihapus!</div>');
            }
        } else {
            die('<div class="alert alert-danger"><strong>Error!</strong> Hapus token gagal! Silahkan Cek username Anda!</div>');
        }
    }
    function login_process(){
        $username = $this->input->post('username');
        $password = hash('md5', $this->input->post('password'));
        $user     = $this->user_model->login($username, $password);
        if ($user) {
            if($user->token){
                $vrfToken = verifyToken($user->token);

                if(!$vrfToken['is_valid']){
                    $this->session->set_userdata('user_login', $username);
                    #generate new token
                    $token = generateToken($username);
                    $data = array(
                        "id" => $user->id,
                        "token" => $token['access_token'],
                        "refresh_token" => $token['refresh_token'],
                    );
                    $this->user_model->update_user($data);

                    $data = array(
                        'info' => 'success',
                        'message' => '<div class="alert alert-success"><strong>Sukses!</strong> Sesaat lagi anda akan masuk..</div>',
                        "token" => $token['access_token'],
                        "refresh_token" => $token['refresh_token'],
                    );
                    die(json_encode($data));
                }else{
                    $data = array(
                        'info' => 'failed',
                        'message' => '<div class="alert alert-danger"><strong>Error!</strong> Login Gagal! Anda sedang login di device lain!</div>'
                    );
                    die(json_encode($data));
                }
            }else{
                #generate token
                $token = generateToken($username);
                $data = array(
                    "id" => $user->id,
                    "token" => $token['access_token'],
                    "refresh_token" => $token['refresh_token'],
                );
                $this->user_model->update_user($data);

                $this->session->set_userdata('user_login', $username);
                $data = array(
                    'info' => 'success',
                    'message' => '<div class="alert alert-success"><strong>Sukses!</strong> Sesaat lagi anda akan masuk..</div>',
                    "token" => $token['access_token'],
                    "refresh_token" => $token['refresh_token'],
                );
                die(json_encode($data));
            }
        } else {
            $data = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger"><strong>Error!</strong> Login Gagal! Silahkan Cek Email/Password Anda!</div>'
            );
            die(json_encode($data));
        }
    }

    function logout_process(){
        $respObj = new ResponseApi();

        #check header
        $header = $this->input->request_headers();
        if(isset($header['Authorization'])){
            list(, $token) = explode(' ', $header['Authorization']);

            $vrfToken = verifyToken($token);
            if($vrfToken['is_valid']){
                $this->user_model->logout($vrfToken['jwt']->username);
                $data = array(
                    'info' => 'success',
                    'message' => 'success logout'
                );
                die(json_encode($data));
            }
        }

        $data = array(
            'info' => 'success',
            'message' => 'No token'
        );
        die(json_encode($data));     
    }
    
    function logout(){
        $this->session->set_userdata('user_login', "");
        $this->session->sess_destroy();
        redirect(BASE_URL);
    }
    
    function profile_process(){
        #authentication
        $roles=array('super_admin','proret','hut_put','delivery','adm_prod');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $name   = $this->input->post('name');
        $phone  = $this->input->post('phone');
        $alamat = $this->input->post('alamat');
        $user   = get_current_member();
        $data   = array(
            'name' => $name,
            'phone' => $phone,
            'id' => $user->id
        );
        $flag   = $this->user_model->update_user($data);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', 'success');
            redirect(base_url("myprofile"));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', 'error');
            redirect(base_url("myprofile"));
        }
    }

    function edit_password_process(){
        $old_password      = hash('md5', $this->input->post('old_password'));
        $new_password      = hash('md5', $this->input->post('new_password'));
        $new_password_real = $this->input->post('new_password');
        $member            = get_current_member();
        //checking 
        $where             = array(
            'id' => $member->id,
            'password' => $old_password
        );
        $flag              = $this->user_model->read_user($where)->num_rows();
        if ($flag) {
            $update_member = array(
                'id' => $member->id,
                'password' => $new_password
            );
            $success       = $this->user_model->update_user($update_member);
            if ($success) {
                //set json
                $data_json = array(
                    'info' => 'success',
                    'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Pengubahan password berhasil dilakukan, Password Baru anda : <b>' . $new_password_real . '</b></a>.</div>'
                );
                die(json_encode($data_json));
            } else {
                //set json
                $data_json = array(
                    'info' => 'failed',
                    'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Pengubahan password gagal dilakukan, Terjadi Kesalahan saat penulisan di database</a>.</div>'
                );
                die(json_encode($data_json));
            }
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Pengubahan password gagal dilakukan, password lama tidak cocok</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }
    
    function create_user_process(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $name         = $this->input->post('name');
        $username     = $this->input->post('username');
        $phone        = $this->input->post('phone');
        $type         = $this->input->post('type');
        $password     = $this->input->post('password');
        $site_code    = $this->input->post('site_code');
        $datetime     = date("Y-m-d H:i:s");
        //check availability
        $where        = array(
            'username' => $username
        );
        $availability = $this->user_model->read_user($where);
        if ($availability->num_rows() == 0) {
            $data = array(
                'name' => $name,
                'phone' => $phone,
                'username' => $username,
                'type' => $type,
                'password' => hash('md5', $password),
                'site_code' => $site_code,
                'datecreated' => $datetime,
                'datemodified' => $datetime
            );
            $flag = $this->user_model->create_user($data);
            if ($flag) {
                //set json
                $data_json = array(
                    'info' => 'success',
                    'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> User berhasil ditambahkan</a>.</div>'
                );
                die(json_encode($data_json));
            } else {
                //set json
                $data_json = array(
                    'info' => 'failed',
                    'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> User gagal ditambahkan!</a>.</div>'
                );
                die(json_encode($data_json));
            }
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Useername sudah digunakan!</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }
    
    function edit_user_process(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $name      = $this->input->post('name');
        $username  = $this->input->post('username');
        $phone     = $this->input->post('phone');
        $type      = $this->input->post('type');
        $id        = $this->input->post('id');
        $site_code = $this->input->post('site_code');
        $datetime  = date("Y-m-d H:i:s");
        $data      = array(
            'name' => $name,
            'phone' => $phone,
            'username' => $username,
            'type' => $type,
            'id' => $id,
            'site_code' => $site_code,
            'datemodified' => $datetime
        );
        $flag      = $this->user_model->update_user($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> User berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> User gagal diubah</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function delete_user($user_id = ""){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        if ($user_id != "") {
            $flag = $this->user_model->delete_user($user_id);
            if ($flag) {
                $this->session->set_flashdata('message', 'success_delete');
                redirect(base_url("user_manager"));
            } else {
                $this->session->set_flashdata('message', 'error_delete');
                redirect(base_url("user_manager"));
            }
        }
    }

    function get_login(){
        $respObj = new ResponseApi();

        if(empty($this->session->userdata('user_login'))){
            $respObj->setResponse(400, "failed", "session not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $where = array('username'=> $this->session->userdata('user_login'));
        $data_user = $this->user_model->read_user($where);
        if($data_user->num_rows()==0){
            $respObj->setResponse(400, "failed", "user not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $login = $data_user->row();

        $respObj->setResponse(200, "success", "Get login success", $login);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*------End of User-------*/

/*------Start of SPL-------*/
    function get_spl_titipan(){
        #init variable
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get spl titipan
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        if(is_null($date)) $date = date("Y-m-d");
        $output = array();
        $spl_titipan = $this->spl_titipan_model->get_spl_titipan($search, $date, $order, $limit);
        $output['data'] = $spl_titipan;
        $output['recordsTotal'] = $this->spl_titipan_model->get_spl_titipan_total($search, $date);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
            $respObj->setResponse(200, "success", "Get spl titipan is success", $output['data']);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }else{
            $output['draw'] = $draw;
            $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
            $resp = $respObj->getResponseDatatable();
            setOutput($resp);
            return;
        } 
    }
    function create_spltitipan_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $spl_id = $this->input->post('no_spl');
        $spl = $this->spl_model->get_spl_by_id($spl_id);
        if(is_null($spl)){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }

        $site_titipan = $this->input->post('site_titipan');
        $total_spl_titipan = $this->spl_titipan_model->get_spl_titipan_total(null, date("Y-m-d"));
        $no_spl_titipan = $site_titipan . substr($spl->no_spl, 1);

        $data = array(
            "spl_id" => $spl->id,
            "no_spl_titipan" => $no_spl_titipan,
            "marketing_penerima" => $this->input->post('marketing_penerima'),
            "site_titipan" => $site_titipan,
            "site_awal" => $this->input->post('site_awal'),
            "tanggal_spl_titipan" => date("Y-m-d"),
            "tanggal_spl_asli" => $spl->tanggal_spl,
            "status" => 'BELUM SELESAI'
        );
        $flag = $this->spl_titipan_model->create_spl_titipan($data);
        if ($flag) {
            #update spl awal
            $this->spl_model->dititipkan_spl($spl->id);
            #send message WA
            $wa_message = "Anda Mendapat TITIPAN SPL baru, Wajib Lihat di SPL TITIPAN $no_spl_titipan";
            $destination_number = $this->input->post('no_hp_marketing_penerima');
            $response = send_message($destination_number, $wa_message);
            log_message('error', $response);
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }
    function update_spltitipan_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id = $this->input->post('id');

        #check spl titipan
        $spl_titipan = $this->spl_titipan_model->get_spl_titipan_by_id($id);
        if(is_null($spl_titipan)){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> SPL tidak ditemukan</a>.</div>'
            );
            die(json_encode($data_json));
            return;
        }

        $spl = $this->spl_model->get_spl_by_id($spl_titipan->spl_id);

        $status = $this->input->post('status') && $user->type == 'super_admin' ? $this->input->post('status') : $spl_titipan->status;

        $data = array(
            "id" => $id, 
            "spl_id" => $spl_titipan->spl_id,
            "no_spl_titipan" => $spl_titipan->no_spl_titipan,
            "marketing_penerima" => $this->input->post('marketing_penerima'),
            "site_titipan" => $spl_titipan->site_titipan,
            "site_awal" => $spl_titipan->site_awal,
            "ket_kirim1" => $this->input->post('ket_kirim1'),
            "ket_kirim2" => $this->input->post('ket_kirim2'),
            "ket_kirim3" => $this->input->post('ket_kirim3'),
            "ket_kirim4" => $this->input->post('ket_kirim4'),
            "ket_kirim5" => $this->input->post('ket_kirim5'),
            "ket_retur" => $this->input->post('ket_retur'),
            "tanggal_spl_titipan" => $spl_titipan->tanggal_spl_titipan,
            "tanggal_spl_asli" => $spl->tanggal_spl,
            "status" => $status,
            "datecreated" => $spl_titipan->datecreated
        );
        
        $flag = $this->spl_titipan_model->update_spl_titipan($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }
    function get_spltitipan_detail(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id = $this->input->get('id');
        #check spl titipan
        $spl_titipan = $this->spl_titipan_model->get_spl_titipan_by_id($id);
        if(is_null($spl_titipan)){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> SPL tidak ditemukan</a>.</div>'
            );
            die(json_encode($data_json));
        }else{
            $data_json = array(
                'info' => 'success',
                'data' => $spl_titipan
            );
            die(json_encode($data_json));
        }
    }

    function burn_spl_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $tanggal = $this->input->post('tanggal');
        $tanggal_text = $this->input->post('tanggal_text');
        $phone_number = array(
            $this->input->post('phone_number_1'),
            $this->input->post('phone_number_2'),
            $this->input->post('phone_number_3'),
            $this->input->post('phone_number_4'),
            $this->input->post('phone_number_5'),
            $this->input->post('phone_number_6')
        );

        #get spl which can be burn by month
        $spl_list = $this->spl_model->get_burn_spl_by_month($tanggal, $user->site_code);
        if(sizeof($spl_list) <= 0){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Tidak ada data SPL yang bisa dihanguskan</a>.</div>'
            );
            die(json_encode($data_json));
        }

        #burn spl
        $flag = $this->spl_model->burn_spl_by_month($tanggal, $user->site_code);
        if(!$flag){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Sistem gagal menghanguskan SPL</a>.</div>'
            );
            die(json_encode($data_json));
        }

        #send message
        $url = generate_spl_hangus($spl_list, $tanggal_text);
        $full_url = BASE_URL . $url;
        $message  = "Berikut adalah data SPL yang dihanguskan\n" . $full_url;
        foreach($phone_number as $item){
            $phone_number_exist = $this->phone_number_model->get_phone_number_by_phone_number($item);
            if(is_null($phone_number_exist)){
                $this->phone_number_model->create_phone_number($item);
            }
            send_message($item, $message);
        }

        #success response
        $data_json = array(
            'info' => 'success',
            'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL Berhasil dihanguskan</a>.</div>',
            "url" => $full_url
        );
        die(json_encode($data_json));
    }

    function burn_spl_satuan_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $user = authRole($roles);
        if(is_null($user)){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Authentication Failed!'
            );
            die(json_encode($data_json));
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> ID is mandatory!'
            );
            die(json_encode($data_json));
        }
        $id = $request['id'];

        #get spl which can be burn by month
        $spl = $this->spl_model->get_spl_by_id($id);
        if(is_null($spl)){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> SPL tidak ditemukan'
            );
            die(json_encode($data_json));
        }

        #burn spl
        $flag = $this->spl_model->burn_spl_by_id($id);
        if(!$flag){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Sistem gagal menghanguskan SPL</a>.</div>'
            );
            die(json_encode($data_json));
        }

        #success response
        $data_json = array(
            'info' => 'success',
            'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> SPL Berhasil dihanguskan</a>.</div>'
        );
        die(json_encode($data_json));
    }

    function get_spl(){
        #init variable
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $status = $this->input->get('status');
        $approval_status = $this->input->get('approval_status');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        if(is_null($date)) $date = date("Y-m-d");
        if($status == 'ALL') $status = null;
        $output = array();
        $spls = $this->spl_model->get_spl($search, $status, $date, $approval_status, $user->site_code, $order, $limit);
        $output['data'] = $spls;
        $output['recordsTotal'] = $this->spl_model->get_spl_total($search, $status, $date, $approval_status, $user->site_code);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get user is success", $output['data']);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function get_spl_monthly(){
        #init variable
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $status = $this->input->get('status');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');

        #get user
        $limit = null;
        if(!is_null($page_number) && $page_size){
            $start = $page_number * $page_size;
            $limit = array('start'=>$start, 'size'=>$page_size);
        }
        $order = array('field'=>$order_by, 'order'=>$sort);
        if(is_null($date)) $date = date("Y-m");
        if($status == 'ALL') $status = null;
        $output = array();
        $spls = $this->spl_model->get_spl_monthly($search, $status, $date, $user->site_code, $order, $limit);
        $output['data'] = $spls;
        $output['recordsTotal'] = $this->spl_model->get_spl_monthly_total($search, $status, $date, $user->site_code);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get user is success", $output['data']);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function get_spl_monthly_and_print(){
        #init variable
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $search = $this->input->get('search');
        $status = $this->input->get('status');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');

        #get spl
        $order = array('field'=>$order_by, 'order'=>$sort);
        if(is_null($date)) $date = date("Y-m");
        if($status == 'ALL') $status = null;
        $spls = $this->spl_model->get_spl_monthly($search, $status, $date, $user->site_code, $order);
        $data = array('spls' => $spls);
        $this->load->view('spl/print_spl_monthly', $data);
    }

    function get_spl_monthly_and_export(){
        #init variable
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $search = $this->input->get('search');
        $status = $this->input->get('status');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');

        #get spl
        $order = array('field'=>$order_by, 'order'=>$sort);
        if(is_null($date)) $date = date("Y-m");
        if($status == 'ALL') $status = null;
        $spls = $this->spl_model->get_spl_monthly($search, $status, $date, $user->site_code, $order);
        $data = array('spls' => $spls, 'date'=>$date);
        $this->load->view('spl/export_spl_monthly', $data);
    }
    
    function create_spl_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $total_spl = $this->spl_model->get_spl_monthly_total(null, null, date("Y-m-d"), $user->site_code);

        $no_spl = strtoupper($user->site_code) . date('mY') . strval(intval($total_spl)+1);
        $tanggal_spl = date("Y-m-d");
        $user_id = $user->id;
        $customer_id = $this->input->post('customer_id');
        $lebar_bahan = $this->input->post('lebar_bahan');
        $tebal_bahan = $this->input->post('tebal_bahan');
        $ukuran_potong = $this->input->post('ukuran_potong');
        $satuan = $this->input->post('satuan');
        $keterangan = $this->input->post('keterangan');
        $jumlah_order = $this->input->post('jumlah_order');
        $harga_satuan = $this->input->post('harga_satuan');
        $komposisi = $this->input->post('komposisi');
        $jumlah_roll = $this->input->post('jumlah_roll');
        $hasil_rumus = $this->input->post('hasil_rumus');
        $approval_status = 'REQUESTED';
        $spl_status = 'BELUM';

        $data = array(
            "no_spl" => $no_spl,
            "tanggal_spl" => $tanggal_spl,
            "user_id" => $user_id,
            "customer_id" => $customer_id,
            "lebar_bahan" => $lebar_bahan,
            "tebal_bahan" => $tebal_bahan,
            "ukuran_potong" => $ukuran_potong,
            "satuan" => $satuan,
            "keterangan" => $keterangan,
            "jumlah_order" => $jumlah_order,
            "harga_satuan" => $harga_satuan,
            "komposisi" => $komposisi,
            "jumlah_roll" => $jumlah_roll,
            "hasil_rumus" => $hasil_rumus,
            "approval_status" => $approval_status,
            "spl_status" => $spl_status,
        );
        $flag = $this->spl_model->create_spl($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function update_spl_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id = $this->input->post('id');
        $customer_id = $this->input->post('customer_id');
        $lebar_bahan = $this->input->post('lebar_bahan');
        $tebal_bahan = $this->input->post('tebal_bahan');
        $ukuran_potong = $this->input->post('ukuran_potong');
        $satuan = $this->input->post('satuan');
        $keterangan = $this->input->post('keterangan');
        $jumlah_order = $this->input->post('jumlah_order');
        $harga_satuan = $this->input->post('harga_satuan');
        $komposisi = $this->input->post('komposisi');
        $jumlah_roll = $this->input->post('jumlah_roll');
        $hasil_rumus = $this->input->post('hasil_rumus');
        $tanggal_kirim1 = $this->input->post('tanggal_kirim1');
        $jumlah_kirim1 = $this->input->post('jumlah_kirim1');
        $tanggal_kirim2 = $this->input->post('tanggal_kirim2');
        $jumlah_kirim2 = $this->input->post('jumlah_kirim2');
        $tanggal_kirim3 = $this->input->post('tanggal_kirim3');
        $jumlah_kirim3 = $this->input->post('jumlah_kirim3');
        $sjkirim1 = $this->input->post('sjkirim1');
        $sjkirim2 = $this->input->post('sjkirim2');
        $sjkirim3 = $this->input->post('sjkirim3');

        #check spl
        $spl = $this->spl_model->get_spl_by_id($id);
        if(is_null($spl)){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> SPL tidak ditemukan</a>.</div>'
            );
            die(json_encode($data_json));
            return;
        }

        $data = array(
            "id" => $id,
            "no_spl" => $spl->no_spl,
            "tanggal_spl" => $spl->tanggal_spl,
            "user_id" => $spl->user_id,
            "customer_id" => $customer_id,
            "lebar_bahan" => $lebar_bahan,
            "tebal_bahan" => $tebal_bahan,
            "ukuran_potong" => $ukuran_potong,
            "satuan" => $satuan,
            "keterangan" => $keterangan,
            "jumlah_order" => $jumlah_order,
            "harga_satuan" => $harga_satuan,
            "komposisi" => $komposisi,
            "jumlah_roll" => $jumlah_roll,
            "hasil_rumus" => $hasil_rumus,
            "spl_status" => $spl->spl_status,
            "datecreated" => $spl->datecreated
        );
        if($spl->approval_status == 'TIDAK_APPROVED') $data['approval_status'] = 'REQUESTED';
        else $data['approval_status'] = $spl->approval_status;

        if($tanggal_kirim1) $data['tanggal_kirim1'] = $tanggal_kirim1;
        else $data['tanggal_kirim1'] = $spl->tanggal_kirim1;
        if($jumlah_kirim1) $data['jumlah_kirim1'] = $jumlah_kirim1;
        else $data['jumlah_kirim1'] = $spl->jumlah_kirim1;
        if($tanggal_kirim2) $data['tanggal_kirim2'] = $tanggal_kirim2;
        else $data['tanggal_kirim2'] = $spl->tanggal_kirim2;
        if($jumlah_kirim2) $data['jumlah_kirim2'] = $jumlah_kirim2;
        else $data['jumlah_kirim2'] = $spl->jumlah_kirim2;
        if($tanggal_kirim3) $data['tanggal_kirim3'] = $tanggal_kirim3;
        else $data['tanggal_kirim3'] = $spl->tanggal_kirim3;
        if($jumlah_kirim3) $data['jumlah_kirim3'] = $jumlah_kirim3;
        else $data['jumlah_kirim3'] = $spl->jumlah_kirim3;
        if($sjkirim1) $data['sjkirim1'] = $sjkirim1;
        else $data['sjkirim1'] = $spl->sjkirim1;
        if($sjkirim2) $data['sjkirim2'] = $sjkirim2;
        else $data['sjkirim2'] = $spl->sjkirim2;
        if($sjkirim3) $data['sjkirim3'] = $sjkirim3;
        else $data['sjkirim3'] = $spl->sjkirim3;
        
        $flag = $this->spl_model->update_spl($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function approve_spl_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check spl
        $spl = $this->spl_model->get_spl_by_id($id);
        if(is_null($spl)){
            $respObj->setResponse(400, "failed", "SPL not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->spl_model->approve_spl($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Failed to approved SPL");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Approve SPL success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function multiple_approve_spl_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('ids'))){
            $respObj->setResponse(400, "failed", "ids is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $ids = $request['ids'];

        if(!is_array($ids)){
            $respObj->setResponse(400, "failed", "ids should be array");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->spl_model->approve_batch_spl($ids);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Failed to approved batch SPL");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Approve batch SPL success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function noapprove_spl_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check spl
        $spl = $this->spl_model->get_spl_by_id($id);
        if(is_null($spl)){
            $respObj->setResponse(400, "failed", "SPL not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->spl_model->noapprove_spl($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Failed to reject SPL", $flag);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Reject SPL success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function done_spl_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check spl
        $spl = $this->spl_model->get_spl_by_id($id);
        if(is_null($spl)){
            $respObj->setResponse(400, "failed", "SPL not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->spl_model->done_spl($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Failed to done SPL");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Done SPL success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function delete_spl_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check spl
        $spl = $this->spl_model->get_spl_by_id($id);
        if(is_null($spl)){
            $respObj->setResponse(400, "failed", "SPL not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->spl_model->delete_spl($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Failed to delete SPL");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Delete SPL success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function delete_spl_batch(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        $mandatory_param = array('date', 'status', 'search');
        if(!checkParameterByKeys($request, $mandatory_param)){
            $respObj->setResponse(400, "failed", "date, status, and search is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['status'] = $request['status'] == 'ALL' ? null : $request['status'];

        #check spl
        $flag = $this->spl_model-> delete_spl_batch($request['search'], $request['status'], $request['date'], $user->site_code);        
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Failed to delete batch SPL");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Delete batch SPL success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function autodone_spl_process(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $marketing_id = $this->input->post('marketing_id');
        $date = $this->input->post('date');

        $spl_limit = $this->limit_model->get_limit_by_site($user->site_code);
        if(is_null($spl_limit)){
            $respObj->setResponse(400, "failed", "SPL Limit belum diset! Untuk melakukan auto done SPL Limit harus diset");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check marketing
        $spls = $this->spl_model->get_spl_by_user_id_and_date($marketing_id, $date);
        foreach($spls as $spl){
            $hasil_rumus = $spl->hasil_rumus;
            $jumlah_kirim1 = is_null($spl->jumlah_kirim1) ? 0 : $spl->jumlah_kirim1;
            $jumlah_kirim2 = is_null($spl->jumlah_kirim2) ? 0 : $spl->jumlah_kirim2;
            $jumlah_kirim3 = is_null($spl->jumlah_kirim3) ? 0 : $spl->jumlah_kirim3;
            $total_kirim = $jumlah_kirim1 + $jumlah_kirim2 + $jumlah_kirim3;
            if($total_kirim >= ($hasil_rumus*($spl_limit->nilai/100))){
                $this->spl_model->done_spl($spl->id);
            }
        }

        $respObj->setResponse(200, "success", "Auto done SPL success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_spl_detail(){
        #authentication
        $roles=array('super_admin', 'proret', 'delivery', 'adm_prod');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id = $this->input->get('id');
        #check spl
        $spl = $this->spl_model->get_spl_by_id($id);
        if(is_null($spl)){
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> SPL tidak ditemukan</a>.</div>'
            );
            die(json_encode($data_json));
        }else{
            $data_json = array(
                'info' => 'success',
                'data' => $spl
            );
            die(json_encode($data_json));
        }
    }

    function print_spl(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id = $this->input->get('id');
        #check spl
        $spl = $this->spl_model->get_spl_by_id($id);
        $data = array('spl'=>$spl);
        $this->load->view('spl/print_spl', $data);
    }

    function print_spl_titipan(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id = $this->input->get('id');
        #check spl
        $spl_titipan = $this->spl_titipan_model->get_spl_titipan_by_id($id);
        $spl = $this->spl_model->get_spl_by_id($spl_titipan->spl_id);
        $data = array(
            'spl' => $spl,
            'spl_titipan' => $spl_titipan
        );
        $this->load->view('spl/print_spl_titipan', $data);
    }

    function get_spl_limit(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get histori absensi
        $spl_limit = $this->limit_model->get_limit_by_site($user->site_code);
        
        #response
        $respObj->setResponse(200, "success", "Get spl limit is success", $spl_limit);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function update_spl_limit(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        #check payload
        $mandatory_param = array('nilai');
        if(!checkParameterByKeys($request, $mandatory_param)){
            $respObj->setResponse(400, "failed", "nilai is mandatory");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if(array_key_exists("id", $request)){
            $limit = $this->limit_model->get_limit_by_id($request['id']);
            if(is_null($limit)){
                $respObj->setResponse(404, "failed", "spl limit not found");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
            if($limit->site != $user->site_code){
                $respObj->setResponse(400, "failed", "Unauthorized access to SPL limit");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
            $flag = $this->limit_model->update_limit($request);
        }else{
            $request['site'] = $user->site_code;
            $flag = $this->limit_model->create_limit($request);
        }

        if(!$flag){
            $respObj->setResponse(400, "failed", "update spl limit failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "update spl limit success");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*------End of SPL-------*/

/*------Start of Phone Number------*/
    function get_phone_number(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        #init variable
        $query = $this->input->get('q');
        $phone_number = $this->phone_number_model->get_phone_number_like($query);

        $options = array();
        foreach($phone_number as $item){
            $name = $item->name ? "($item->name) " : "";
            $full_text = $name . $item->phone_number;
            $option = array(
                "id" => $item->phone_number,
                "text" => "$full_text"
            );
            array_push($options, $option);
        }

        die(json_encode($options));
    }
/*------End of Phone Number------*/

/*------Start of Pegawai-------*/
    function get_pegawai(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #init variable
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $pegawai = $this->pegawai_model->get_pegawai($search, $user->site_code, $order, $limit);
        $output['data'] = $pegawai;
        $output['recordsTotal'] = $this->pegawai_model->get_pegawai_total($search, $user->site_code);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get pegawai is success", $output['data']);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function get_pegawai_all(){
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        #init variable
        $query = $this->input->get('q');
        $pegawai = $this->pegawai_model->get_pegawai(null, $user->site_code, null, null, $query);

        $options = array();
        foreach($pegawai as $item){
            $kasbon_amount = 0;
            if($item->jumlah_kasbon > 0){
                $latest_kasbon = $this->kasbon_pegawai_model->get_last_kasbon_by_pegawai($item->id);
                if($latest_kasbon){
                    $kasbon_amount = $latest_kasbon->potong_mingguan;
                }
            }
            $option = array(
                "id" => $item->id,
                "text" => "($item->nip) $item->nama",
                "jumlah_kasbon" => $item->jumlah_kasbon,
                "potongan_kasbon" => $kasbon_amount
            );
            array_push($options, $option);
        }

        die(json_encode($options));
    }

    function get_pegawai_detail(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check pegawai
        $id = $this->input->get('id');
        $pegawai = $this->pegawai_model->get_pegawai_by_id($id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "Get pegawai detail is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }else{
            $respObj->setResponse(200, "success", "Get pegawai detail is success", $pegawai);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function get_pegawai_and_print(){
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }
        
        #init variable
        $search = $this->input->get('search');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');

        #get spl
        $order = array('field'=>$order_by, 'order'=>$sort);
        $pegawais = $this->pegawai_model->get_pegawai($search, $user->site_code, $order);
        $data = array('pegawais' => $pegawais);
        $this->load->view('absensi/print_pegawai', $data);
    }

    function create_pegawai_process(){
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $total_pegawai = $this->pegawai_model->get_pegawai_total();

        $nama = $this->input->post('nama');
        $nip = strtoupper($user->site_code) . strtoupper(substr($nama,0,1)) . strval(intval($total_pegawai)+1);
        $alamat = $this->input->post('alamat');
        $telepon = $this->input->post('telepon');
        $jabatan = $this->input->post('jabatan');
        $role = $this->input->post('role');
        $tanggal_masuk = $this->input->post('tanggal_masuk');
        $jumlah_kasbon = $this->input->post('jumlah_kasbon');
        $upah_harian = $this->input->post('upah_harian');
        $upah_bulanan = $this->input->post('upah_bulanan');
        $upah_lembur = $this->input->post('upah_lembur');
        $upah_libur = $this->input->post('upah_libur');

        $data = array(
            "nip" => $nip,
            "nama" => $nama,
            "alamat" => $alamat,
            "telepon" => $telepon,
            "jabatan" => $jabatan,
            "role" => $role,
            "tanggal_masuk" => $tanggal_masuk,
            "jumlah_kasbon" => preg_replace( '/[^0-9]/', '', $jumlah_kasbon),
            "upah_harian" => preg_replace( '/[^0-9]/', '', $upah_harian),
            "upah_bulanan" => preg_replace( '/[^0-9]/', '', $upah_bulanan),
            "upah_lembur" => preg_replace( '/[^0-9]/', '', $upah_lembur),
            "upah_libur" => preg_replace( '/[^0-9]/', '', $upah_libur)
        );
        if($user->type == 'super_admin'){
            $data['password'] = $this->input->post('password');
        }
        $flag = $this->pegawai_model->create_pegawai($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Create pegawai is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } else {
            $respObj->setResponse(200, "success", "Create pegawai is success", array($data, $user));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function update_pegawai_process(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $alamat = $this->input->post('alamat');
        $telepon = $this->input->post('telepon');
        $jabatan = $this->input->post('jabatan');
        $role = $this->input->post('role');
        $tanggal_masuk = $this->input->post('tanggal_masuk');
        $jumlah_kasbon = $this->input->post('jumlah_kasbon');
        $upah_harian = $this->input->post('upah_harian');
        $upah_bulanan = $this->input->post('upah_bulanan');
        $upah_lembur = $this->input->post('upah_lembur');
        $upah_libur = $this->input->post('upah_libur');
        $keyword = $this->input->post('keyword');

        #check keyword
        $keyword_obj = $this->keyword_model->get_keyword_by_module("PEGAWAI", "ACTIVE");
        if($keyword_obj){
            $verify = password_verify($keyword, $keyword_obj->keyword_hash);
            if(!$verify){
                $respObj->setResponse(400, "failed", "Invalid keyword");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        #check pegawai
        $pegawai = $this->pegawai_model->get_pegawai_by_id($id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "Pegawai not found!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $data = array(
            "id" => $id,
            "nip" => $pegawai->nip,
            "nama" => $nama,
            "alamat" => $alamat,
            "telepon" => $telepon,
            "jabatan" => $jabatan,
            "role" => $role,
            "tanggal_masuk" => $tanggal_masuk,
            "jumlah_kasbon" => preg_replace( '/[^0-9]/', '', $jumlah_kasbon),
            "upah_harian" => preg_replace( '/[^0-9]/', '', $upah_harian),
            "upah_bulanan" => preg_replace( '/[^0-9]/', '', $upah_bulanan),
            "upah_lembur" => preg_replace( '/[^0-9]/', '', $upah_lembur),
            "upah_libur" => preg_replace( '/[^0-9]/', '', $upah_libur),
            "datecreated" => $pegawai->datecreated
        );
        if($user->type == 'super_admin'){
            $data['password'] = $this->input->post('password');
        }else{
            $data['password'] = $pegawai->password;
        }
        $flag = $this->pegawai_model->update_pegawai($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Update pegawai is failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } else {
            $respObj->setResponse(200, "success", "Update pegawai is success");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function delete_pegawai_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id and keyword are mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check keyword
        $keyword = $this->keyword_model->get_keyword_by_module("PEGAWAI", "ACTIVE");
        if($keyword){
            $verify = password_verify($request['keyword'], $keyword->keyword_hash);
            if(!$verify){
                $respObj->setResponse(400, "failed", "Invalid keyword");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        #check pegawai
        $pegawai = $this->pegawai_model->get_pegawai_by_id($id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "Pegawai not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->pegawai_model->delete_pegawai($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Delete pegawai is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Delete Pegawai is success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_kasbon_pegawai(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #init variable
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $kasbon_pegawais = $this->kasbon_pegawai_model->get_kasbon_pegawai($search, $user->site_code, $start_date, $end_date, $order, $limit);
        $output['data'] = $kasbon_pegawais;
        $output['recordsTotal'] = $this->kasbon_pegawai_model->get_kasbon_pegawai_total($search, $user->site_code, $start_date, $end_date);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get kasbon pegawai is success", $output['data']);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function get_kasbon_pegawai_and_print(){
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        #init variable
        $search = $this->input->get('search');
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');

        #get spl
        $order = array('field'=>$order_by, 'order'=>$sort);
        $pegawais = $this->kasbon_pegawai_model->get_kasbon_pegawai($search, $user->site_code, $start_date, $end_date, $order);
        $data = array('kasbon_pegawais' => $pegawais);
        $this->load->view('absensi/print_kasbon_pegawai', $data);
    }

    function get_kasbon_pegawai_detail(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check pegawai
        $id = $this->input->get('id');
        $kasbon_pegawai = $this->kasbon_pegawai_model->get_kasbon_pegawai_by_id($id);
        if(is_null($kasbon_pegawai)){
            $respObj->setResponse(400, "failed", "Get kasbon pegawai detail is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }else{
            $respObj->setResponse(200, "success", "Get kasbon pegawai detail is success", $kasbon_pegawai);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function create_kasbon_pegawai_process(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $pegawai_id = $this->input->post('pegawai_id');
        $tanggal_kasbon = $this->input->post('tanggal_kasbon');
        $jumlah_kasbon = $this->input->post('jumlah_kasbon');
        $alasan_kasbon = $this->input->post('alasan_kasbon');
        $potong_mingguan = $this->input->post('potong_mingguan');
        $potong_bulanan = $this->input->post('potong_bulanan');
        $keyword = $this->input->post('keyword');

        #check keyword
        $keyword_obj = $this->keyword_model->get_keyword_by_module("KASBON", "ACTIVE");
        if($keyword_obj){
            $verify = password_verify($keyword, $keyword_obj->keyword_hash);
            if(!$verify){
                $respObj->setResponse(400, "failed", "Invalid keyword");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        #check pegawai
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai_id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "pegawai is not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #update kasbon pegawai
        $new_kasbon = $pegawai->jumlah_kasbon + intval(preg_replace( '/[^0-9]/', '', $jumlah_kasbon));
        $data_pegawai = array(
            "id" => $pegawai->id,
            "jumlah_kasbon" => $new_kasbon
        );
        $flag = $this->pegawai_model->update_pegawai_custom($data_pegawai);
        if(!$flag){
            $respObj->setResponse(400, "failed", "Update kasbon pegawai failed! 1");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #create kasbon
        $data = array(
            "pegawai_id" => $pegawai_id,
            "tanggal_kasbon" => $tanggal_kasbon,
            "alasan_kasbon" => $alasan_kasbon,
            "potong_mingguan" => $potong_mingguan,
            "potong_bulanan" => $potong_bulanan,
            "jumlah_kasbon" => preg_replace( '/[^0-9]/', '', $jumlah_kasbon),
            "potong_mingguan" => preg_replace( '/[^0-9]/', '', $potong_mingguan),
            "potong_bulanan" => preg_replace( '/[^0-9]/', '', $potong_bulanan)
        );
        $flag = $this->kasbon_pegawai_model->create_kasbon_pegawai($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Create kasbon pegawai is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } else {
            $respObj->setResponse(200, "success", "Create kasbon pegawai is success");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function update_kasbon_pegawai_process(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $id = $this->input->post('id');
        $pegawai_id = $this->input->post('pegawai_id');
        $tanggal_kasbon = $this->input->post('tanggal_kasbon');
        $jumlah_kasbon = $this->input->post('jumlah_kasbon');
        $alasan_kasbon = $this->input->post('alasan_kasbon');
        $potong_mingguan = $this->input->post('potong_mingguan');
        $potong_bulanan = $this->input->post('potong_bulanan');
        $keyword = $this->input->post('keyword');

        #check keyword
        $keyword_obj = $this->keyword_model->get_keyword_by_module("KASBON", "ACTIVE");
        if($keyword_obj){
            $verify = password_verify($keyword, $keyword_obj->keyword_hash);
            if(!$verify){
                $respObj->setResponse(400, "failed", "Invalid keyword");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        #check pegawai
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai_id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "Pegawai not found!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check kasbon pegawai
        $kasbon_pegawai = $this->kasbon_pegawai_model->get_kasbon_pegawai_by_id($id);
        if(is_null($kasbon_pegawai)){
            $respObj->setResponse(400, "failed", "Kasbon pegawai not found!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #update kasbon pegawai
        if($kasbon_pegawai->pegawai_id == $pegawai_id){
            $new_kasbon = ($pegawai->jumlah_kasbon-$kasbon_pegawai->jumlah_kasbon) + intval(preg_replace( '/[^0-9]/', '', $jumlah_kasbon));
            $data_pegawai = array(
                "id" => $pegawai->id,
                "jumlah_kasbon" => $new_kasbon
            );
            $this->pegawai_model->update_pegawai_custom($data_pegawai);
            // if(!$flag){
            //     $respObj->setResponse(400, "failed", "Update kasbon pegawai failed!");
            //     $resp = $respObj->getResponse();
            //     setOutput($resp);
            //     return;
            // }
        }else{
            #restore pegawai kasbon
            $old_pegawai = $this->pegawai_model->get_pegawai_by_id($kasbon_pegawai->pegawai_id);
            $old_kasbon = $old_pegawai->jumlah_kasbon - $kasbon_pegawai->jumlah_kasbon;
            $data_old_pegawai = array(
                "id" => $old_pegawai->id,
                "jumlah_kasbon" => $old_kasbon
            );
            $flag = $this->pegawai_model->update_pegawai_custom($data_old_pegawai);
            if(!$flag){
                $respObj->setResponse(400, "failed", "Failed update jumlah kasbon");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
            #update kasbon pegawai
            $new_kasbon = $pegawai->jumlah_kasbon + intval(preg_replace( '/[^0-9]/', '', $jumlah_kasbon));
            $data_new_pegawai = array(
                "id" => $pegawai->id,
                "jumlah_kasbon" => $new_kasbon
            );
            $flag = $this->pegawai_model->update_pegawai_custom($data_new_pegawai);
            if(!$flag){
                $respObj->setResponse(400, "failed", "Failed update jumlah kasbon");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        $data = array(
            "id" => $id,
            "pegawai_id" => $pegawai_id,
            "tanggal_kasbon" => $tanggal_kasbon,
            "alasan_kasbon" => $alasan_kasbon,
            "potong_mingguan" => $potong_mingguan,
            "potong_bulanan" => $potong_bulanan,
            "jumlah_kasbon" => preg_replace( '/[^0-9]/', '', $jumlah_kasbon),
            "potong_mingguan" => preg_replace( '/[^0-9]/', '', $potong_mingguan),
            "potong_bulanan" => preg_replace( '/[^0-9]/', '', $potong_bulanan),
            "datecreated" => $kasbon_pegawai->datecreated
        );
        $flag = $this->kasbon_pegawai_model->update_kasbon_pegawai($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Update kasbon pegawai is failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } else {
            $respObj->setResponse(200, "success", "Update kasbon pegawai is success");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function delete_kasbon_pegawai_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id and keyword are mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check keyword
        $keyword_obj = $this->keyword_model->get_keyword_by_module("KASBON", "ACTIVE");
        if($keyword_obj){
            $verify = password_verify($request['keyword'], $keyword_obj->keyword_hash);
            if(!$verify){
                $respObj->setResponse(400, "failed", "Invalid keyword");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        #check kasbon pegawai
        $kasbon_pegawai = $this->kasbon_pegawai_model->get_kasbon_pegawai_by_id($id);
        if(is_null($kasbon_pegawai)){
            $respObj->setResponse(400, "failed", "Kasbon pegawai not found!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check pegawai
        $pegawai = $this->pegawai_model->get_pegawai_by_id($kasbon_pegawai->pegawai_id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "Pegawai not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #restore pegawai kasbon
        $old_kasbon = $pegawai->jumlah_kasbon - $kasbon_pegawai->jumlah_kasbon;
        $data_pegawai = array(
            "id" => $pegawai->id,
            "jumlah_kasbon" => $old_kasbon
        );
        $flag = $this->pegawai_model->update_pegawai_custom($data_pegawai);
        if(!$flag){
            $respObj->setResponse(400, "failed", "Failed update jumlah kasbon");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->kasbon_pegawai_model->delete_kasbon_pegawai($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Delete kasbon pegawai is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Delete kasbon pegawai is success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_absensi(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $jabatan = $this->input->get('jabatan');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');  
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size, $sort, $order_by);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $absensi = $this->absensi_model->get_absensi($search, $date, $user->site_code, $order, $limit, $jabatan);
        $output['data'] = $absensi;
        $output['recordsTotal'] = $this->absensi_model->get_absensi_total($search, $date, $user->site_code, $jabatan);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get absensi is success", $output['data']);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function get_absensi_detail(){
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $id = $this->input->get('id');
        #check absensi
        $absensi = $this->absensi_model->get_absensi_by_id($id);
        if(is_null($absensi)){
            $respObj->setResponse(400, "failed", "Get absensi detail is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }else{
            $respObj->setResponse(200, "success", "Get absensi detail is success", $absensi);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function get_absensi_and_print(){
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        #init variable
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $jabatan = $this->input->get('jabatan');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');

        #get absensi
        $order = array('field'=>$order_by, 'order'=>$sort);
        $absensis = $this->absensi_model->get_absensi($search, $date, $user->site_code, null, null, $jabatan);
        $data = array('absensis' => $absensis, 'date'=>$date);
        $this->load->view('absensi/print_absensi', $data);
    }

    function get_absensi_and_print_rekap(){
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        #init variable
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $jabatan = $this->input->get('jabatan');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');

        #get absensi
        $order = array('field'=>$order_by, 'order'=>$sort);
        $rekap_absensi = $this->absensi_model->get_absensi($search, $date, $user->site_code, $order, null, $jabatan);
        $data = array('rekap_absensi' => $rekap_absensi, 'date'=>$date);
        $this->load->view('absensi/print_rekap_absensi', $data);
    }

    function create_absensi_process(){
        #init req and res
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $pegawai_id = $this->input->post('pegawai_id');
        $jumlah_hadir_biasa = $this->input->post('jumlah_hadir_biasa');
        $jumlah_hari_libur = $this->input->post('jumlah_hari_libur');
        $jumlah_jam_lembur = $this->input->post('jumlah_jam_lembur');
        $jumlah_telat = $this->input->post('jumlah_telat');
        $jumlah_telat_hari = $this->input->post('jumlah_telat_hari');
        $jumlah_absen = $this->input->post('jumlah_absen');
        $potongan_kasbon = $this->input->post('potongan_kasbon');
        $uang_tunjangan = $this->input->post('uang_tunjangan');
        $tanggal_periode_absensi = $this->input->post('tanggal_periode_absensi');
        $jumlah_telat = preg_replace( '/[^0-9]/', '', $jumlah_telat);
        $potongan_kasbon = preg_replace( '/[^0-9]/', '', $potongan_kasbon);
        $uang_tunjangan = preg_replace( '/[^0-9]/', '', $uang_tunjangan);

        #check pegawai
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai_id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "Pegawai is not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $data = array(
            "pegawai_id" => $pegawai_id,
            "jumlah_hadir_biasa" => $jumlah_hadir_biasa,
            "jumlah_hari_libur" => $jumlah_hari_libur,
            "jumlah_jam_lembur" => $jumlah_jam_lembur,
            "jumlah_telat" => $jumlah_telat,
            "jumlah_telat_hari" => $jumlah_telat_hari,
            "jumlah_absen" => $jumlah_absen,
            "potongan_kasbon" => $potongan_kasbon,
            "uang_tunjangan" => $uang_tunjangan,
            "tanggal_periode_absensi" => $tanggal_periode_absensi
        );
        $flag = $this->absensi_model->create_absensi($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Create absensi is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #update pegawai kasbon
        $new_kasbon = $pegawai->jumlah_kasbon - $potongan_kasbon;
        $flag = $this->pegawai_model->update_pegawai_custom(
            array(
                'id'=>$pegawai->id, 
                'jumlah_kasbon'=> $new_kasbon, 
                'datemodified' => date("Y-m-d H:i:s")
            )
        );
        if (!$flag) {
            $respObj->setResponse(500, "failed", "update kasbon is failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }


        $respObj->setResponse(200, "success", "Create pegawai is success");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function update_absensi_process(){
        #init req and res
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $id = $this->input->post('id');
        $pegawai_id = $this->input->post('pegawai_id');
        $jumlah_hadir_biasa = $this->input->post('jumlah_hadir_biasa');
        $jumlah_hari_libur = $this->input->post('jumlah_hari_libur');
        $jumlah_jam_lembur = $this->input->post('jumlah_jam_lembur');
        $jumlah_telat = $this->input->post('jumlah_telat');
        $jumlah_telat_hari = $this->input->post('jumlah_telat_hari');
        $jumlah_absen = $this->input->post('jumlah_absen');
        $potongan_kasbon = $this->input->post('potongan_kasbon');
        $uang_tunjangan = $this->input->post('uang_tunjangan');
        $tanggal_periode_absensi = $this->input->post('tanggal_periode_absensi');
        $jumlah_telat = preg_replace( '/[^0-9]/', '', $jumlah_telat);
        $potongan_kasbon = preg_replace( '/[^0-9]/', '', $potongan_kasbon);
        $uang_tunjangan = preg_replace( '/[^0-9]/', '', $uang_tunjangan);

        #check absensi
        $absensi = $this->absensi_model->get_absensi_by_id($id);
        if(is_null($absensi)){
            $respObj->setResponse(400, "failed", "Absensi is not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check pegawai
        $pegawai = $this->pegawai_model->get_pegawai_by_id($pegawai_id);
        if(is_null($pegawai)){
            $respObj->setResponse(400, "failed", "Pegawai is not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        if($absensi->pegawai_id != $pegawai_id){
            #restore previous kasbon
            $old_pegawai = $this->pegawai_model->get_pegawai_by_id($absensi->pegawai_id);
            $old_kasbon = $old_pegawai->jumlah_kasbon + $absensi->potongan_kasbon;
            $flag = $this->pegawai_model->update_pegawai_custom(
                array(
                    'id'=>$old_pegawai->id, 
                    'jumlah_kasbon'=> $old_kasbon, 
                    'datemodified' => date("Y-m-d H:i:s")
                )
            );
            if(!$flag){
                $respObj->setResponse(400, "failed", "Update Old Pegawai is failed");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }

            #update new pegawai kasbon
            $new_kasbon = $pegawai->jumlah_kasbon - $potongan_kasbon;
            $flag = $this->pegawai_model->update_pegawai_custom(
                array(
                    'id'=>$pegawai->id, 
                    'jumlah_kasbon'=> $new_kasbon, 
                    'datemodified' => date("Y-m-d H:i:s")
                )
            );
            if (!$flag) {
                $respObj->setResponse(500, "failed", "update new pegawai kasbon is failed!");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }else{
            if(intval($absensi->potongan_kasbon) != intval($potongan_kasbon)){
                $diff = intval($potongan_kasbon) - intval($absensi->potongan_kasbon);
                $new_kasbon = $pegawai->jumlah_kasbon - $diff;
                $flag = $this->pegawai_model->update_pegawai_custom(
                    array(
                        'id'=>$pegawai->id, 
                        'jumlah_kasbon'=> $new_kasbon, 
                        'datemodified' => date("Y-m-d H:i:s")
                    )
                );
                if (!$flag) {
                    $respObj->setResponse(500, "failed", "update new pegawai kasbon is failed!");
                    $resp = $respObj->getResponse();
                    setOutput($resp);
                    return;
                }
            }
        }

        #update absensi
        $data = array(
            "id" => $id,
            "pegawai_id" => $pegawai_id,
            "jumlah_hadir_biasa" => $absensi->jumlah_hadir_biasa,
            "jumlah_hari_libur" => $absensi->jumlah_hari_libur,
            "jumlah_jam_lembur" => $jumlah_jam_lembur,
            "jumlah_telat" => $jumlah_telat,
            "jumlah_telat_hari" => $jumlah_telat_hari,
            "jumlah_absen" => $jumlah_absen,
            "potongan_kasbon" => preg_replace( '/[^0-9]/', '', $potongan_kasbon),
            "uang_tunjangan" => $uang_tunjangan,
            "tanggal_periode_absensi" => $tanggal_periode_absensi,
            "datecreated" => $absensi->datecreated
        );
        $flag = $this->absensi_model->update_absensi($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Update absensi is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Update abensi is success");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function delete_absensi_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check absensi
        $absensi = $this->absensi_model->get_absensi_by_id($id);
        if(is_null($absensi)){
            $respObj->setResponse(400, "failed", "Absensi not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $pegawai = $this->pegawai_model->get_pegawai_by_id($absensi->pegawai_id);

        #check authority
        $nip = $pegawai->nip;
        $site_code = $nip[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->absensi_model->delete_absensi($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Delete absensi is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #update pegawai kasbon
        $new_kasbon = $pegawai->jumlah_kasbon + $absensi->potongan_kasbon;
        $flag = $this->pegawai_model->update_pegawai_custom(
            array(
                'id'=>$pegawai->id, 
                'jumlah_kasbon'=> $new_kasbon, 
                'datemodified' => date("Y-m-d H:i:s")
            )
        );
        if (!$flag) {
            $respObj->setResponse(500, "failed", "update pegawai kasbon is failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Delete absensi is success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_histori_absensi(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');  
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size, $sort, $order_by);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get histori absensi
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $histori_absensi = $this->histori_absensi_model->get_histori_absensi($search, $date, $user->site_code, $order, $limit);
        $output['data'] = $histori_absensi;
        $output['recordsTotal'] = $this->histori_absensi_model->get_histori_absensi_total($search, $date, $user->site_code);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get histori absensi is success", $histori_absensi);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function update_histori_absensi_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('date'))){
            $respObj->setResponse(400, "failed", "search and date is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $date = $request['date'];

        $akumulasi_absensi = $this->absensi_model->get_akumulasi_absensi($date, $user->site_code);
        foreach($akumulasi_absensi as $item){
            $histori_absensi = $this->histori_absensi_model->get_histori_absensi_by_pegawai_id_and_date($item->pegawai_id, $date);

            if(is_null($histori_absensi)){ #create new histori absensi
                $fulldate = "$date-01";
                $data = array(
                    "pegawai_id" => $item->pegawai_id,
                    "tanggal_periode" => $fulldate,
                    "jumlah_akumulasi_hadir_biasa" => $item->akumulasi_hadir,
                    "jumlah_akumulasi_hari_libur" => $item->akumulasi_libur,
                    "jumlah_akumulasi_jam_lembur" => $item->akumulasi_jam_lembur,
                    "jumlah_akumulasi_telat" => $item->akumulasi_telat,
                    "jumlah_akumulasi_absen" => $item->akumulasi_absen
                );
                $this->histori_absensi_model->create_histori_absensi($data);
            }else{ #update histori absensi
                $data = array(
                    "id" => $histori_absensi->id,
                    "pegawai_id" => $histori_absensi->pegawai_id,
                    "tanggal_periode" => $histori_absensi->tanggal_periode,
                    "jumlah_akumulasi_hadir_biasa" => $item->akumulasi_hadir,
                    "jumlah_akumulasi_hari_libur" => $item->akumulasi_libur,
                    "jumlah_akumulasi_jam_lembur" => $item->akumulasi_jam_lembur,
                    "jumlah_akumulasi_telat" => $item->akumulasi_telat,
                    "jumlah_akumulasi_absen" => $item->akumulasi_absen,
                    "datecreated" => $histori_absensi->datecreated
                );
                $this->histori_absensi_model->update_histori_absensi($data);
            }
        }

        if(sizeof($akumulasi_absensi) > 0){
            $pegawai_ids = array_map(function ($item) {
                return $item->pegawai_id;
            }, $akumulasi_absensi);
            $flag = $this->absensi_model->update_absensi_on_histori_batch($pegawai_ids, $date);
            if (!$flag) {
                $respObj->setResponse(400, "failed", "Update Histori Absensi failed");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        $respObj->setResponse(200, "success", "Update Histori Absensi success");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_histori_absensi_and_print(){        
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        #init variable
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');

        #get absensi
        $order = array('field'=>$order_by, 'order'=>$sort);
        $histori_absensi = $this->histori_absensi_model->get_histori_absensi($search, $date, $user->site_code, $order);

        $months = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $date_arr = (explode("-", $date));
        $new_date = $months[intval($date_arr[1])-1] . " " . $date_arr[0];

        $data = array('histori_absensis' => $histori_absensi, 'date'=>$new_date);
        $this->load->view('absensi/print_histori_absensi', $data);
    }

    function get_absensi_harian(){
        $respObj = new ResponseApi();
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $site_code = $user->site_code;
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');  
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get spl titipan
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);

        $absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_range($start_date, $end_date, $search, $site_code, $order, $limit);
        $absensi_harian_total = $this->tx_absensi_harian_model->count_absensi_harian_range($start_date, $end_date, $search, $site_code);

        #responses
        $respObj->setResponseDatatable(200, $absensi_harian, $draw, $absensi_harian_total, $absensi_harian_total);
        $resp = $respObj->getResponseDatatable();
        setOutput($resp);
        return;
    }

    function get_absensi_harian_by_id(){
        $respObj = new ResponseApi();
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $site_code = $user->site_code;
        $jurnal_id = $this->input->get('id');
        $params = array($jurnal_id);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", message("missing param"));
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get jurnal absensi
        $absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_by_id($jurnal_id);
        if(is_null($absensi_harian)){
            $respObj->setResponse(400, "failed", "Get jurnal absensi detail is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #responses
        $respObj->setResponse(200, "success", "Get jurnal absensi detail is success", $absensi_harian);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function update_absensi_harian(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id', 'start_hour', 'end_hour'))){
            $respObj->setResponse(400, "failed", "start hour and end hour is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $request['datemodified'] = date("Y-m-d H:i:s");

        $flag = $this->tx_absensi_harian_model->update_absensi_harianV2($request);
        if(!$flag){
            $respObj->setResponse(400, "failed", "Update jurnal absensi gagal!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "update jurnal absensi berhasil");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function delete_absensi_harian(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('start_date', 'end_date'))){
            $respObj->setResponse(400, "failed", "start date and end date is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start_date = $request['start_date'];
        $end_date = $request['end_date'];
        $site_code = $user->site_code;

        #delete absensi harian
        $this->tx_absensi_harian_model->delete_absensi_harian_range($start_date, $end_date, $site_code);

        $respObj->setResponse(200, "success", "delete jurnal absensi is success");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function sync_absensi_harian(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('start_date', 'end_date'))){
            $respObj->setResponse(400, "failed", "start date and end date is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start_date = $request['start_date'];
        $end_date = $request['end_date'];
        $site_code = $user->site_code;

        $exist_absensi = $this->absensi_model->get_absensi_by_tanggal_periode($start_date, $site_code);
        if($exist_absensi){
            $respObj->setResponse(400, "failed", "Rekap Absensi harian dengan periode tanggal $start_date sudah ada. Silahkan pilih periode tanggal lainnya.");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        #count diff day
        $start_date_timestamp = strtotime($start_date);
        $end_date_timestamp = strtotime($end_date);
        $diff_day = 0;
        if($start_date_timestamp != $end_date_timestamp){
            $diff_timestamp = $end_date_timestamp - $start_date_timestamp;
            $diff_day = round($diff_timestamp / (60*60*24));
        }

        $absensi_harian_rekap_list = $this->tx_absensi_harian_model->get_absensi_harian_range_recap($start_date, $end_date, $site_code);

        foreach ($absensi_harian_rekap_list as $absensi_harian_rekap) {
            #get absen day
            $absen_day = 0;
            $week_total = 0;
            $jumlah_hari_biasa = $absensi_harian_rekap->jumlah_hadir - ($absensi_harian_rekap->weekend_total + $absensi_harian_rekap->holiday_total);
            if($diff_day > 0){
                $week_total = floor($diff_day / 7);
                $normal_work_day = ($diff_day % 7) + ($week_total * 6);
                $absen_day = $normal_work_day - $jumlah_hari_biasa;
            }
            #get potongan kasbon
            $pegawai = $this->pegawai_model->get_pegawai_by_id($absensi_harian_rekap->pegawai_id);
            if($pegawai->jumlah_kasbon > 0){
                $kasbon_pegawai = $this->kasbon_pegawai_model->get_last_kasbon_by_pegawai($absensi_harian_rekap->pegawai_id);
                $potongan_kasbon = $week_total <= 1 ? $kasbon_pegawai->potong_mingguan : $kasbon_pegawai->potong_mingguan * $week_total;
            }else{
                $potongan_kasbon = 0;
            }

            #save absen mingguan
            $data_absen = array(
                'pegawai_id' => $absensi_harian_rekap->pegawai_id,
                'jumlah_hadir_biasa' => $jumlah_hari_biasa,
                'jumlah_hari_libur' => intval($absensi_harian_rekap->weekend_total) + intval($absensi_harian_rekap->holiday_total),
                'jumlah_jam_lembur' => $absensi_harian_rekap->overtime_hour_total,
                'jumlah_telat' => intval($absensi_harian_rekap->late_total) * 10000,
                'jumlah_telat_hari' => $absensi_harian_rekap->late_total,
                'jumlah_absen' => $absen_day,
                'potongan_kasbon' => $potongan_kasbon,
                'uang_tunjangan' => 0,
                'tanggal_periode_absensi' => $start_date
            );
            $this->absensi_model->create_absensi($data_absen);

            #update pegawai kasbon
            $new_kasbon = $pegawai->jumlah_kasbon - $potongan_kasbon;
            $this->pegawai_model->update_pegawai_custom(
                array(
                    'id'=>$pegawai->id, 
                    'jumlah_kasbon'=> $new_kasbon, 
                    'datemodified' => date("Y-m-d H:i:s")
                )
            );
        }

        #update sync status
        $this->tx_absensi_harian_model->sync_absensi_harian_range($start_date, $end_date, $site_code);

        $respObj->setResponse(200, "success", "recap jurnal absensi is success");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function print_absensi_harian(){
        $respObj = new ResponseApi();
        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $site_code = $user->site_code;
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');
        $order = array('field'=>'pegawai_name', 'order'=>'ASC');

        $absensi_harian = $this->tx_absensi_harian_model->get_absensi_harian_range($start_date, $end_date, null, $site_code, $order);

        $index      = 0;
        $last_name  = "";
        foreach ($absensi_harian as &$item) {
            $index      = $last_name == $item->pegawai_name ? $index+1 : 1;
            $last_name  = $item->pegawai_name;
            $item->no   = $index;
        }

        #responses
        $data = array('absensi_harian' => $absensi_harian, 'start_date'=>$start_date, 'end_date' => $end_date);
        $this->load->view('absensi/print_jurnal_absensi', $data);
    }
/*------End of Pegawai-------*/

/*------Start of Delivery-------*/
    function get_delivery(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'delivery');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');  
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $delivery = $this->delivery_model->get_delivery($search, $date, $user->site_code, $order, $limit);
        $output['data'] = $delivery;
        $output['recordsTotal'] = $this->delivery_model->get_delivery_total($search, $date, $user->site_code);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get delivery is success", $delivery);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function get_delivery_montlhy(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'proret', 'delivery', 'adm_prod');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $date = $this->input->get('date');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');  
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $histori_absensi = $this->delivery_model->get_delivery_monthly($search, $date, $user->site_code, $order, $limit);
        $output['data'] = $histori_absensi;
        $output['recordsTotal'] = $this->delivery_model->get_delivery_monthly_total($search, $date, $user->site_code);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
          $respObj->setResponse(200, "success", "Get delivery is success", $histori_absensi);
          $resp = $respObj->getResponse();
          setOutput($resp);
          return;
        }else{
          $output['draw'] = $draw;
          $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
          $resp = $respObj->getResponseDatatable();
          setOutput($resp);
          return;
        } 
    }

    function get_delivery_by_spl_no(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'delivery', 'adm_prod');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $spl_no = $this->input->get('spl_no');
        $params = array($spl_no);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check spl
        $spl = $this->spl_model->get_spl_by_spl_no($spl_no);
        if(is_null($spl)){
            $respObj->setResponse(400, "failed", "SPL not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $delivery = $this->delivery_model->get_delivery_by_spl_id($spl->id, strtoupper($user->site_code));
        $response = array('delivery'=>$delivery, 'spl'=>$spl);        
        $respObj->setResponse(200, "success", "Get delivery success", $response);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function get_spl_by_spl_no(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $spl_no = $this->input->get('q');
        $approval_status = $this->input->get('approval_status');

        #check spl
        $spl = $this->spl_model->get_spl_by_like_spl_no($spl_no, null, $approval_status);
        $options = array();
        foreach($spl as &$item){
            $item->text = $item->no_spl;
        }
        die(json_encode($spl));
    }

    function get_delivery_by_spl_id(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $id = $this->input->get('id');
        $params = array($id);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check spl
        $delivery = $this->delivery_model->get_delivery_by_id($id, strtoupper($user->site_code));
        if(is_null($delivery)){
            $respObj->setResponse(400, "failed", "Delivery not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $spl = $this->spl_model->get_spl_by_id($delivery->spl_id, strtoupper($user->site_code));
        $response = array('delivery'=>$delivery, 'spl'=>$spl);        
        $respObj->setResponse(200, "success", "Get delivery success", $response);
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function create_update_delivery(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #authentication
        $roles=array('super_admin', 'delivery', 'adm_prod');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('spl_id', 'kodeCust', 'tglSetorBarang1', 'jlhYgDisetor1'))){
            $respObj->setResponse(400, "failed", "missing mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check spl
        $spl = $this->spl_model->get_spl_by_id($request['spl_id']);
        if(is_null($spl)){
            $respObj->setResponse(400, "failed", "spl not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check authority
        $no_spl = $spl->no_spl;
        $site_code = $no_spl[0];
        if(strtoupper($user->site_code) !== $site_code){
            $respObj->setResponse(400, "failed", "Do not have authority!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #create/update delivery
        if(array_key_exists("id", $request)){
            $flag = $this->delivery_model->update_delivery_custom($request);
        }else{
            if($user->type == 'delivery'){
                $respObj->setResponse(401, "failed", "Authentication Failed!");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
            $flag = $this->delivery_model->create_delivery($request);
        }
        if (!$flag) {
            $respObj->setResponse(500, "failed", "create/update delivery data failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $respObj->setResponse(200, "success", "Create/update delivery data success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function update_deliveries(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #authentication
        $roles=array('super_admin', 'delivery');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('received', 'delivered', 'returned'))){
            $respObj->setResponse(400, "failed", "missing mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check spl
        if(sizeof($request['received']) > 0){
            $flag = $this->delivery_model->received_delivery_batch($request['received']);
            if(!$flag){
                $respObj->setResponse(400, "failed", "Update received failed");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        if(sizeof($request['delivered']) > 0){
            $flag = $this->delivery_model->delivered_delivery_batch($request['delivered']);
            if(!$flag){
                $respObj->setResponse(400, "failed", "Update delivered failed");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }

        if(sizeof($request['returned']) > 0){
            $flag = $this->delivery_model->returned_delivery_batch($request['returned']);
            if(!$flag){
                $respObj->setResponse(400, "failed", "Update returned failed");
                $resp = $respObj->getResponse();
                setOutput($resp);
                return;
            }
        }


        $respObj->setResponse(200, "success", "Update delivery status success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function delete_delivery(){
        #init variable
        $respObj = new ResponseApi();
        
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $id = $this->input->get('id');
        $params = array($id);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check delivery
        $delivery = $this->delivery_model->get_delivery_by_id($id, strtoupper($user->site_code));
        if(is_null($delivery)){
            $respObj->setResponse(400, "failed", "Delivery not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $flag = $this->delivery_model->delete_delivery($delivery->id);
        if(!$flag){
            $respObj->setResponse(400, "failed", "Delete delivery failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $respObj->setResponse(200, "success", "Delete delivery success");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*------End of Delivery-------*/
    
/*------Start of Produksi & Return-------*/
    function produksi_harian_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $no_mesin         = $this->input->post('no_mesin');
        $ukuran           = $this->input->post('ukuran');
        $jml_kg           = $this->input->post('jml_prod_kg');
        $jml_lbr          = $this->input->post('jml_prod_lbr');
        $ket              = $this->input->post('keterangan');
        $tanggal_produksi = $this->input->post('tanggal_produksi');
        $datetime         = date("Y-m-d H:i:s");
        $member           = get_current_member();
        $site_code        = $member->site_code;
        $data             = array(
            'site_code' => $site_code,
            'user_id' => $member->id,
            'no_machine' => $no_mesin,
            'tanggal_produksi' => $tanggal_produksi,
            'number_of_production_kg' => $jml_kg,
            'number_of_production_sheet' => $jml_lbr,
            'size' => $ukuran,
            'description' => $ket,
            'datecreated' => $datetime,
            'datemodified' => $datetime
        );
        $flag             = $this->produksi_model->create_produksi($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function returan_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }
        
        $tanggal_diterima = $this->input->post('tanggal_diterima');
        $customer_id      = $this->input->post('customer_id');
        $jml_kg           = $this->input->post('jml_retur_kg');
        $jml_lbr          = $this->input->post('jml_retur_lbr');
        $ukuran           = $this->input->post('ukuran');
        $alasan           = $this->input->post('alasan_retur');
        $tanda_terima     = $this->input->post('tanda_terima');
        $tindak_lanjut    = $this->input->post('tindak_lanjut');
        $datetime         = date("Y-m-d H:i:s");
        $member           = get_current_member();
        $site_code        = $member->site_code;
        $data             = array(
            'site_code' => $site_code,
            'tanggal_diterima' => $tanggal_diterima,
            'user_id' => $member->id,
            'customer_id' => $customer_id,
            'jumlah_retur_kg' => $jml_kg,
            'jumlah_retur_lembar' => $jml_lbr,
            'ukuran' => $ukuran,
            'alasan_retur' => $alasan,
            'tanda_terima' => $tanda_terima,
            'tindak_lanjut' => $tindak_lanjut,
            'datecreated' => $datetime,
            'datemodified' => $datetime
        );
        $flag             = $this->retur_model->create_retur($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function create_customer_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $name           = $this->input->post('name');
        $work_phone     = $this->input->post('work_phone');
        $contact_person = $this->input->post('contact_person');
        $address_1      = $this->input->post('address_1');
        $address_2      = $this->input->post('address_2');
        $tax_non_tax    = $this->input->post('tax_non_tax');
        $limit_plafon   = $this->input->post('limit_plafon');
        $business_type  = $this->input->post('business_type');
        $nik_ktp        = $this->input->post('nik_ktp');
        $no_npwp        = $this->input->post('no_npwp');
        $no_hp          = $this->input->post('no_hp');
        $sumber_info    = $this->input->post('sumber_info');
        $tipe_lampiran  = $this->input->post('tipe_lampiran');
        $cara_bayar     = $this->input->post('cara_bayar');
        $mkt            = $this->input->post('mkt');
        $pembayaran_ekspedisi  = $this->input->post('pembayaran_ekspedisi');
        $acc_oleh       = $this->input->post('acc_oleh');
        $rating         = $this->input->post('rating');
        $member         = get_current_member();
        $datetime       = date("Y-m-d H:i:s");
        $data           = array(
            'user_id' => $member->id,
            'name' => $name,
            'work_phone' => $work_phone,
            'contact_person' => $contact_person,
            'address_1' => $address_1,
            'address_2' => $address_2,
            'tax_non_tax' => $tax_non_tax,
            'limit_plafon' => $limit_plafon,
            'business_type' => $business_type,
            'nik_ktp' => $nik_ktp,
            'no_npwp' => $no_npwp,
            'no_hp' => $no_hp,
            'sumber_info' => $sumber_info,
            'tipe_lampiran' => $tipe_lampiran,
            'cara_bayar' => $cara_bayar,
            'mkt' => $mkt,
            'pembayaran_ekspedisi' => $pembayaran_ekspedisi,
            'acc_oleh' => $acc_oleh,
            'rating' => $rating,
            'datecreated' => $datetime,
            'datemodified' => $datetime
        );
        $flag           = $this->customer_model->create_customer($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Customer berhasil ditambahkan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Customer gagal ditambahkan!</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }
    
    function edit_customer_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }
        
        $name           = $this->input->post('name');
        $work_phone     = $this->input->post('work_phone');
        $contact_person = $this->input->post('contact_person');
        $address_1      = $this->input->post('address_1');
        $address_2      = $this->input->post('address_2');
        $tax_non_tax    = $this->input->post('tax_non_tax');
        $limit_plafon   = $this->input->post('limit_plafon');
        $business_type  = $this->input->post('business_type');
        $nik_ktp        = $this->input->post('nik_ktp');
        $no_npwp        = $this->input->post('no_npwp');
        $no_hp          = $this->input->post('no_hp');
        $sumber_info    = $this->input->post('sumber_info');
        $tipe_lampiran  = $this->input->post('tipe_lampiran');
        $cara_bayar     = $this->input->post('cara_bayar');
        $mkt            = $this->input->post('mkt');
        $pembayaran_ekspedisi  = $this->input->post('pembayaran_ekspedisi');
        $acc_oleh       = $this->input->post('acc_oleh');
        $rating         = $this->input->post('rating');
        $id             = $this->input->post('id');
        $member         = get_current_member();
        $datetime       = date("Y-m-d H:i:s");
        $data           = array(
            'id' => $id,
            'name' => $name,
            'work_phone' => $work_phone,
            'contact_person' => $contact_person,
            'address_1' => $address_1,
            'address_2' => $address_2,
            'tax_non_tax' => $tax_non_tax,
            'limit_plafon' => $limit_plafon,
            'business_type' => $business_type,
            'nik_ktp' => $nik_ktp,
            'no_npwp' => $no_npwp,
            'no_hp' => $no_hp,
            'sumber_info' => $sumber_info,
            'tipe_lampiran' => $tipe_lampiran,
            'cara_bayar' => $cara_bayar,
            'mkt' => $mkt,
            'pembayaran_ekspedisi' => $pembayaran_ekspedisi,
            'acc_oleh' => $acc_oleh,
            'rating' => $rating,
            'datecreated' => $datetime,
            'datemodified' => $datetime
        );
        $flag           = $this->customer_model->update_customer($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Customer berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Customer gagal diubah!</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function delete_customer($customer_id = ""){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        if ($customer_id != "") {
            $flag = $this->customer_model->delete_customer($customer_id);
            if ($flag) {
                $this->session->set_flashdata('message', 'success_delete');
                redirect(base_url("customer"));
            } else {
                $this->session->set_flashdata('message', 'error_delete');
                redirect(base_url("customer"));
            }
        }
    }

    function report_produksi_bulanan_monthly(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $year             = $this->input->get('year');
        $month            = $this->input->get('month');
        $no_mesin         = $this->input->get('no_mesin');
        $date             = $year . "-" . $month;
        $data['date']     = $date;
        $data['member']   = $user;
        $data['no_mesin'] = $no_mesin;
        $this->load->view('produksi/report_produksi_bulanan_change', $data);
    }

    function send_email_produksi_bulanan(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $member        = $user;
        $site_code     = $member->site_code;
        $email         = $this->input->post('email');
        $year          = $this->input->post('year');
        $month         = $this->input->post('month');
        $no_mesin      = $this->input->post('no_mesin');
        $link_download = base_url('laporan/download_produksi_bulanan?site_code=' . $site_code . "&year=" . $year . "&month=" . $month . "&no_mesin=" . $no_mesin);
        $data_email    = array(
            'judul_laporan' => 'Produksi Bulanan',
            'link_download' => '<a href="' . $link_download . '">Disini!</a>'
        );
        //        print_r($data_email);
        $this->mailer->send_email_report($email, $data_email);
        //        //set flash data
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Email Telah Terkirim!</div>');
        redirect(base_url('report/produksi_bulanan'));
    }

    function download_produksi_bulanan(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $site_code = $this->input->get('site_code');
        $year      = $this->input->get('year');
        $month     = $this->input->get('month');
        $no_mesin  = $this->input->get('no_mesin');
        if ($site_code == "") {
            $member    = $user;
            $site_code = $member->site_code;
        }
        $year     = ($year == '' ? $this->input->post('year') : $year);
        $month    = ($month == '' ? $this->input->post('month') : $month);
        $no_mesin = ($no_mesin == '' ? $this->input->post('no_mesin') : $no_mesin);
        $date     = $year . "-" . $month;
        if ($no_mesin == "") {
            $produksi    = get_total_produksi_by_month($date, $site_code, $no_mesin);
            $total_kg    = 0;
            $total_sheet = 0;
            if ($produksi != false) {
                $no  = 0;
                //PDF Processing 
                $pdf = new FPDF('l', 'mm', 'A5');
                // membuat halaman baru
                $pdf->AddPage();
                // setting jenis font yang akan digunakan
                $pdf->SetFont('Arial', 'B', 10);
                // mencetak string 
                $pdf->Cell(60, 2, 'Laporan Produksi ' . strtoupper($site_code) . ' Bulan ' . date("M", strtotime($date)) . ' ' . $year, 0, 1, 'C');
                $pdf->SetFont('Arial', 'B', 12);
                // Memberikan space kebawah agar tidak terlalu rapat
                $pdf->Cell(10, 7, '', 0, 1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(20, 6, 'No', 1, 0);
                $pdf->Cell(15, 6, 'Site', 1, 0);
                $pdf->Cell(27, 6, 'No . Mesin', 1, 0);
                $pdf->Cell(33, 6, 'Ukuran', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah KG', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah Lbr', 1, 0);
                $pdf->Cell(57, 6, 'Deskripsi', 1, 1);
                $pdf->SetFont('Arial', '', 10);
                foreach ($produksi->result() as $item) {
                    $no++;
                    $pdf->Cell(20, 6, $no, 1, 0);
                    $pdf->Cell(15, 6, $item->site_code, 1, 0);
                    $pdf->Cell(27, 6, $item->no_machine, 1, 0);
                    $pdf->Cell(33, 6, $item->size, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_kg, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_sheet, 1, 0);
                    $pdf->Cell(57, 6, $item->description, 1, 1);
                    $total_kg += $item->number_of_production_kg;
                    $total_sheet += $item->number_of_production_sheet;
                }
                $pdf->Cell(20, 6, '', 1, 0);
                $pdf->Cell(42, 6, 'TOTAL', 1, 0);
                $pdf->Cell(33, 6, '', 1, 0);
                $pdf->Cell(22, 6, $total_kg, 1, 0);
                $pdf->Cell(22, 6, $total_sheet, 1, 0);
                $pdf->Cell(57, 6, '', 1, 0);
                $file_name_save = 'laporan_produksi_bulanan_' . date('Y-m-d-H-i-s') . ".pdf";
                $pdf->Output();
            }
        } else {
            $produksi    = get_produksi_byday($date, $site_code, $no_mesin);
            $total_kg    = 0;
            $total_sheet = 0;
            if ($produksi != false) {
                $no  = 0;
                //PDF Processing 
                $pdf = new FPDF('l', 'mm', 'A5');
                // membuat halaman baru
                $pdf->AddPage();
                // setting jenis font yang akan digunakan
                $pdf->SetFont('Arial', 'B', 14);
                // mencetak string 
                $pdf->Cell(190, 2, $produksi->row()->no_machine, 0, 1, 'C');
                $pdf->SetFont('Arial', 'B', 12);
                // Memberikan space kebawah agar tidak terlalu rapat
                $pdf->Cell(10, 7, '', 0, 1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(20, 6, 'TGL', 1, 0);
                $pdf->Cell(15, 6, 'Site', 1, 0);
                $pdf->Cell(33, 6, 'Ukuran', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah KG', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah Lbr', 1, 0);
                $pdf->Cell(57, 6, 'Deskripsi', 1, 1);
                $pdf->SetFont('Arial', '', 10);
                foreach ($produksi->result() as $item) {
                    $pdf->Cell(20, 6, $item->tanggal_produksi, 1, 0);
                    $pdf->Cell(15, 6, $item->site_code, 1, 0);
                    $pdf->Cell(33, 6, $item->size, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_kg, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_sheet, 1, 0);
                    $pdf->Cell(57, 6, $item->description, 1, 1);
                    $total_kg += $item->number_of_production_kg;
                    $total_sheet += $item->number_of_production_sheet;
                }
                $pdf->Cell(20, 6, 'TOTAL', 1, 0);
                $pdf->Cell(15, 6, '', 1, 0);
                $pdf->Cell(33, 6, '', 1, 0);
                $pdf->Cell(22, 6, $total_kg, 1, 0);
                $pdf->Cell(22, 6, $total_sheet, 1, 0);
                $pdf->Cell(57, 6, '', 1, 1);
                $file_name_save = 'laporan_produksi_bulanan_' . date('Y-m-d-H-i-s') . ".pdf";
                $pdf->Output();
            }
        }
    }

    function report_produksi_daily(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $date           = $this->input->get('date');
        $no_mesin       = $this->input->get('no_mesin');
        $data['date']   = $date;
        $data['member'] = $user;
        $data['no_mesin'] = $no_mesin;
        $this->load->view('produksi/report_produksi_harian_change', $data);
    }

    function send_email_produksi_harian(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $member        = $user;
        $site_code     = $member->site_code;
        $email         = $this->input->post('email');
        $date          = $this->input->post('date');
        $no_mesin      = $this->input->post('no_mesin');
        $link_download = base_url('laporan/download_produksi_harian?site_code=' . $site_code . "&date=" . $date . "&no_mesin=" . $no_mesin);
        $data_email    = array(
            'judul_laporan' => 'Produksi Harian',
            'link_download' => '<a href="' . $link_download . '">Disini!</a>'
        );
        //        print_r($data_email);
        $this->mailer->send_email_report($email, $data_email);
        //        //set flash data
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Email Telah Terkirim!</div>');
        redirect(base_url('report/produksi_harian'));
    }

    function download_produksi_harian(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $site_code = $this->input->get('site_code');
        $date      = $this->input->get('date');
        $no_mesin  = $this->input->get('no_mesin');
        if ($site_code == "") {
            $member    = get_current_member();
            $site_code = $member->site_code;
        }
        $date     = ($date == '' ? $this->input->post('date') : $date);
        $no_mesin = ($no_mesin == '' ? $this->input->post('no_mesin') : $no_mesin);
        if ($no_mesin == "") {
            $produksi    = get_produksi_by_date($date, $site_code, $no_mesin);
            $total_kg    = 0;
            $total_sheet = 0;
            if ($produksi != false) {
                $no  = 0;
                //PDF Processing 
                $pdf = new FPDF('l', 'mm', 'A5');
                // membuat halaman baru
                $pdf->AddPage();
                // setting jenis font yang akan digunakan
                $pdf->SetFont('Arial', 'B', 10);
                // mencetak string 
                $pdf->Cell(60, 2, 'Laporan Produksi ' . strtoupper($site_code) . ' Tanggal ' . ($date), 0, 1, 'C');
                $pdf->SetFont('Arial', 'B', 12);
                // Memberikan space kebawah agar tidak terlalu rapat
                $pdf->Cell(10, 7, '', 0, 1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(20, 6, 'No', 1, 0);
                $pdf->Cell(15, 6, 'Site', 1, 0);
                $pdf->Cell(27, 6, 'No . Mesin', 1, 0);
                $pdf->Cell(33, 6, 'Ukuran', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah KG', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah Lbr', 1, 0);
                $pdf->Cell(57, 6, 'Deskripsi', 1, 1);
                $pdf->SetFont('Arial', '', 10);
                foreach ($produksi->result() as $item) {
                    $no++;
                    $pdf->Cell(20, 6, $no, 1, 0);
                    $pdf->Cell(15, 6, $item->site_code, 1, 0);
                    $pdf->Cell(27, 6, $item->no_machine, 1, 0);
                    $pdf->Cell(33, 6, $item->size, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_kg, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_sheet, 1, 0);
                    $pdf->Cell(57, 6, $item->description, 1, 1);
                    $total_kg += $item->number_of_production_kg;
                    $total_sheet += $item->number_of_production_sheet;
                }
                $pdf->Cell(20, 6, '', 1, 0);
                $pdf->Cell(42, 6, 'TOTAL', 1, 0);
                $pdf->Cell(33, 6, '', 1, 0);
                $pdf->Cell(22, 6, $total_kg, 1, 0);
                $pdf->Cell(22, 6, $total_sheet, 1, 0);
                $pdf->Cell(57, 6, '', 1, 0);
                $file_name_save = 'laporan_produksi_harian_' . date('Y-m-d-H-i-s') . ".pdf";
                $pdf->Output();
            }
        } else {
            $produksi    = get_produksi_by_date($date, $site_code, $no_mesin);
            $total_kg    = 0;
            $total_sheet = 0;
            if ($produksi != false) {
                $no  = 0;
                //PDF Processing 
                $pdf = new FPDF('l', 'mm', 'A5');
                // membuat halaman baru
                $pdf->AddPage();
                // setting jenis font yang akan digunakan
                $pdf->SetFont('Arial', 'B', 14);
                // mencetak string 
                $pdf->Cell(190, 2, $produksi->row()->no_machine, 0, 1, 'C');
                $pdf->SetFont('Arial', 'B', 12);
                // Memberikan space kebawah agar tidak terlalu rapat
                $pdf->Cell(10, 7, '', 0, 1);
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(20, 6, 'TGL', 1, 0);
                $pdf->Cell(15, 6, 'Site', 1, 0);
                $pdf->Cell(33, 6, 'Ukuran', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah KG', 1, 0);
                $pdf->Cell(22, 6, 'Jumlah Lbr', 1, 0);
                $pdf->Cell(57, 6, 'Deskripsi', 1, 1);
                $pdf->SetFont('Arial', '', 10);
                foreach ($produksi->result() as $item) {
                    $pdf->Cell(20, 6, $item->tanggal_produksi, 1, 0);
                    $pdf->Cell(15, 6, $item->site_code, 1, 0);
                    $pdf->Cell(33, 6, $item->size, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_kg, 1, 0);
                    $pdf->Cell(22, 6, $item->number_of_production_sheet, 1, 0);
                    $pdf->Cell(57, 6, $item->description, 1, 1);
                    $total_kg += $item->number_of_production_kg;
                    $total_sheet += $item->number_of_production_sheet;
                }
                $pdf->Cell(20, 6, 'TOTAL', 1, 0);
                $pdf->Cell(15, 6, '', 1, 0);
                $pdf->Cell(33, 6, '', 1, 0);
                $pdf->Cell(22, 6, $total_kg, 1, 0);
                $pdf->Cell(22, 6, $total_sheet, 1, 0);
                $pdf->Cell(57, 6, '', 1, 1);
                $file_name_save = 'laporan_produksi_harian_' . date('Y-m-d-H-i-s') . ".pdf";
                $pdf->Output();
            } else {
                echo 'Data tidak ditemukan!';
            }
        }
    }

    function produksi_edit_process(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id               = $this->input->post('id');
        $no_mesin         = $this->input->post('no_mesin');
        $ukuran           = $this->input->post('ukuran');
        $jml_kg           = $this->input->post('jml_prod_kg');
        $jml_lbr          = $this->input->post('jml_prod_lbr');
        $ket              = $this->input->post('keterangan');
        $tanggal_produksi = $this->input->post('tanggal_produksi');
        $datetime         = date("Y-m-d H:i:s");
        $member           = get_current_member();
        $site_code        = $member->site_code;
        $data             = array(
            'id' => $id,
            'site_code' => $site_code,
            'user_id' => $member->id,
            'no_machine' => $no_mesin,
            'tanggal_produksi' => $tanggal_produksi,
            'number_of_production_kg' => $jml_kg,
            'number_of_production_sheet' => $jml_lbr,
            'size' => $ukuran,
            'description' => $ket,
            'datecreated' => $datetime,
            'datemodified' => $datetime
        );
        $flag             = $this->produksi_model->update_produksi($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Perubahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Perubahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function report_returan_bulanan_monthly(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $year           = $this->input->get('year');
        $month          = $this->input->get('month');
        $flag           = $this->input->get('flag');
        $date           = $year . "-" . $month;
        $data['date']   = $date;
        $data['flag']   = $flag;
        $data['member'] = $user;
        $this->load->view('produksi/report_returan_bulanan_change', $data);
    }

    function send_email_returan_bulanan(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $member        = $user;
        $site_code     = $member->site_code;
        $email         = $this->input->post('email');
        $year          = $this->input->post('year');
        $month         = $this->input->post('month');
        $flag          = $this->input->post('flag');
        $link_download = base_url('laporan/download_returan_bulanan?site_code=' . $site_code . "&year=" . $year . "&month=" . $month . "&flag" . $flag);
        $data_email    = array(
            'judul_laporan' => 'Returan Bulanan',
            'link_download' => '<a href="' . $link_download . '">Disini!</a>'
        );
        //        print_r($data_email);
        $this->mailer->send_email_report($email, $data_email);
        //        //set flash data
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Email Telah Terkirim!</div>');
        redirect(base_url('report/returan_bulanan'));
    }

    function download_returan_bulanan(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $site_code = $this->input->get('site_code');
        $year      = $this->input->get('year');
        $month     = $this->input->get('month');
        $flag      = $this->input->get('flag');
        if ($site_code == "") {
            $member    = $user;
            $site_code = $member->site_code;
        }
        $year      = ($year == '' ? $this->input->post('year') : $year);
        $month     = ($month == '' ? $this->input->post('month') : $month);
        $flag      = ($flag == '' ? $this->input->post('flag') : $flag);
        $date      = $year . "-" . $month;
        //        $group = "A.customer_id";
        $returan   = get_returan_by_month($date, $site_code, $flag);
        $total_kg  = 0;
        $total_lbr = 0;
        if ($returan != false) {
            $no  = 0;
            //PDF Processing 
            $pdf = new FPDF('l', 'mm', 'A5');
            // membuat halaman baru
            $pdf->AddPage();
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial', 'B', 10);
            // mencetak string 
            $pdf->Cell(60, 2, 'Laporan Returan ' . strtoupper($site_code) . ' Bulan ' . date("M", strtotime($date)) . ' ' . $year, 0, 1, 'C');
            $pdf->SetFont('Arial', 'B', 12);
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10, 7, '', 0, 1);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(8, 6, 'No', 1, 0);
            $pdf->Cell(40, 6, 'Customer', 1, 0);
            $pdf->Cell(8, 6, 'Site', 1, 0);
            $pdf->Cell(22, 6, 'Tgl Diterima', 1, 0);
            $pdf->Cell(27, 6, 'Ukuran', 1, 0);
            $pdf->Cell(22, 6, 'Jumlah KG', 1, 0);
            $pdf->Cell(22, 6, 'Jumlah Lbr', 1, 0);
            $pdf->Cell(15, 6, 'Flag', 1, 0);
            $pdf->Cell(32, 6, 'Tindakan', 1, 1);
            $pdf->SetFont('Arial', '', 10);
            foreach ($returan->result() as $item) {
                $no++;
                $pdf->Cell(8, 6, $no, 1, 0);
                $pdf->Cell(40, 6, $item->retur_name, 1, 0);
                $pdf->Cell(8, 6, $item->site_code, 1, 0);
                $pdf->Cell(22, 6, $item->tanggal_diterima, 1, 0);
                $pdf->Cell(27, 6, $item->ukuran, 1, 0);
                $pdf->Cell(22, 6, $item->jumlah_retur_kg, 1, 0);
                $pdf->Cell(22, 6, $item->jumlah_retur_lembar, 1, 0);
                $pdf->Cell(15, 6, $item->tanda_terima, 1, 0);
                $pdf->Cell(32, 6, $item->tindak_lanjut, 1, 1);
                $total_kg += $item->jumlah_retur_kg;
                $total_lbr += $item->jumlah_retur_lembar;
            }
            $pdf->Cell(8, 6, '', 1, 0);
            $pdf->Cell(40, 6, 'TOTAL', 1, 0);
            $pdf->Cell(8, 6, $item->site_code, 1, 0);
            $pdf->Cell(22, 6, '', 1, 0);
            $pdf->Cell(27, 6, '', 1, 0);
            $pdf->Cell(22, 6, $total_kg, 1, 0);
            $pdf->Cell(22, 6, $total_lbr, 1, 0);
            $pdf->Cell(15, 6, '', 1, 0);
            $pdf->Cell(32, 6, '', 1, 1);
            $file_name_save = 'laporan_returan_bulanan_' . date('Y-m-d-H-i-s') . ".pdf";
            $pdf->Output();
        }
    }

    function report_returan_daily(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $date           = $this->input->get('date');
        $flag           = $this->input->get('flag');
        $data['date']   = $date;
        $data['member'] = $user;
        $data['flag']   = $flag;
        $this->load->view('produksi/report_returan_harian_change', $data);
    }

    function send_email_returan_harian(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $member        = $user;
        $site_code     = $member->site_code;
        $email         = $this->input->post('email');
        $date          = $this->input->post('date');
        $flag          = $this->input->post('flag');
        $link_download = base_url('laporan/download_returan_harian?site_code=' . $site_code . "&date=" . $date . "&flag" . $flag);
        $data_email    = array(
            'judul_laporan' => 'Returan Harian',
            'link_download' => '<a href="' . $link_download . '">Disini!</a>'
        );
        //        print_r($data_email);
        $this->mailer->send_email_report($email, $data_email);
        //        //set flash data
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Email Telah Terkirim!</div>');
        redirect(base_url('report/returan_harian'));
    }

    function download_returan_harian(){
        #authentication
        $roles=array('super_admin', 'proret');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $site_code = $this->input->get('site_code');
        $date      = $this->input->get('date');
        $flag      = $this->input->get('flag');
        if ($site_code == "") {
            $member    = $user;
            $site_code = $member->site_code;
        }
        $date      = ($date == '' ? $this->input->post('date') : $date);
        $flag      = ($flag == '' ? $this->input->post('flag') : $flag);
        //        $group = "A.customer_id";
        $returan   = get_returan_by_date($date, $site_code, $flag);
        $total_kg  = 0;
        $total_lbr = 0;
        if ($returan != false) {
            $no  = 0;
            //PDF Processing 
            $pdf = new FPDF('l', 'mm', 'A5');
            // membuat halaman baru
            $pdf->AddPage();
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial', 'B', 10);
            // mencetak string 
            $pdf->Cell(60, 2, 'Laporan Returan ' . strtoupper($site_code) . ' Tanggal ' . $date, 0, 1, 'C');
            $pdf->SetFont('Arial', 'B', 12);
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10, 7, '', 0, 1);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(8, 6, 'No', 1, 0);
            $pdf->Cell(40, 6, 'Customer', 1, 0);
            $pdf->Cell(8, 6, 'Site', 1, 0);
            $pdf->Cell(22, 6, 'Tgl Diterima', 1, 0);
            $pdf->Cell(27, 6, 'Ukuran', 1, 0);
            $pdf->Cell(22, 6, 'Jumlah KG', 1, 0);
            $pdf->Cell(22, 6, 'Jumlah Lbr', 1, 0);
            $pdf->Cell(15, 6, 'Flag', 1, 0);
            $pdf->Cell(32, 6, 'Tindakan', 1, 1);
            $pdf->SetFont('Arial', '', 10);
            foreach ($returan->result() as $item) {
                $no++;
                $pdf->Cell(8, 6, $no, 1, 0);
                $pdf->Cell(40, 6, $item->retur_name, 1, 0);
                $pdf->Cell(8, 6, $item->site_code, 1, 0);
                $pdf->Cell(22, 6, $item->tanggal_diterima, 1, 0);
                $pdf->Cell(27, 6, $item->ukuran, 1, 0);
                $pdf->Cell(22, 6, $item->jumlah_retur_kg, 1, 0);
                $pdf->Cell(22, 6, $item->jumlah_retur_lembar, 1, 0);
                $pdf->Cell(15, 6, $item->tanda_terima, 1, 0);
                $pdf->Cell(32, 6, $item->tindak_lanjut, 1, 1);
                $total_kg += $item->jumlah_retur_kg;
                $total_lbr += $item->jumlah_retur_lembar;
            }
            $pdf->Cell(8, 6, '', 1, 0);
            $pdf->Cell(40, 6, 'TOTAL', 1, 0);
            $pdf->Cell(8, 6, '', 1, 0);
            $pdf->Cell(22, 6, '', 1, 0);
            $pdf->Cell(27, 6, '', 1, 0);
            $pdf->Cell(22, 6, $total_kg, 1, 0);
            $pdf->Cell(22, 6, $total_lbr, 1, 0);
            $pdf->Cell(15, 6, '', 1, 0);
            $pdf->Cell(32, 6, '', 1, 1);
            $file_name_save = 'laporan_returan_harian_' . date('Y-m-d-H-i-s') . ".pdf";
            $pdf->Output();
        }
    }

    function update_returan_list(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];
		$total=$this->db->count_all_results("retur");
		$output=array();
		$output['draw']=$draw;
		$output['recordsTotal']=$output['recordsFiltered']=$total;
		$output['data']=array();
        $this->db->select("retur.*, customer.name customer_name");
		if($search!=""){
            $this->db->like("retur.customer_id",$search);
            $this->db->or_like('customer.name',$search);
            $this->db->or_like('retur.ukuran',$search);
            $this->db->or_like('retur.jumlah_retur_kg',$search);
            $this->db->or_like('retur.jumlah_retur_lembar',$search);
            $this->db->or_like('retur.tanda_terima',$search);
            $this->db->or_like('retur.tindak_lanjut',$search);
            $this->db->or_like('retur.tanggal_diterima',$search);
            $this->db->or_like('retur.datecreated',$search);
		}
		$this->db->limit($length,$start);
		$this->db->order_by('datecreated','DESC');
        $this->db->from('retur');
        $this->db->join('customer', 'customer.id = retur.customer_id');
		$query=$this->db->get();
		$nomor_urut=$start+1;
		foreach ($query->result_array() as $faktur) {
			$edit = '<button url="'.base_url('home/edit_returan/'.$faktur['id']).'" class="btn btn-success edit-returan btn-block" data-toggle="modal" data-target="#edit_returan_modal">Edit</button>';
            $delete = '<button url="'.base_url('backend/delete_returan/'.$faktur['id']).'" class="btn btn-danger delete-ajax btn-block" " data-toggle="modal" data-target="#delete_returan_modal">Delete</button></td>';
            $output['data'][]=array($nomor_urut,$faktur['customer_name'],$faktur['ukuran'], $faktur['jumlah_retur_kg'], $faktur['jumlah_retur_lembar'],$faktur['tanggal_diterima'],$faktur['tanda_terima'],$faktur['tindak_lanjut'], $edit.$delete);
		    $nomor_urut++;
		}

		echo json_encode($output);
    }
    
    function edit_returan_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $site_code        = $this->input->post('site_code');
        $tanggal_diterima = $this->input->post('tanggal_diterima');
        $customer_id      = $this->input->post('customer_id');
        $jml_kg           = $this->input->post('jml_retur_kg');
        $jml_lbr          = $this->input->post('jml_retur_lbr');
        $ukuran           = $this->input->post('ukuran');
        $alasan           = $this->input->post('alasan_retur');
        $tanda_terima     = $this->input->post('tanda_terima');
        $tindak_lanjut    = $this->input->post('tindak_lanjut');
        $id               = $this->input->post('id');
        $datetime         = date("Y-m-d H:i:s");
        $data             = array(
            'id' => $id,
            'site_code' => $site_code,
            'tanggal_diterima' => $tanggal_diterima,
            'user_id' => $user->id,
            'customer_id' => $customer_id,
            'jumlah_retur_kg' => $jml_kg,
            'jumlah_retur_lembar' => $jml_lbr,
            'ukuran' => $ukuran,
            'alasan_retur' => $alasan,
            'tanda_terima' => $tanda_terima,
            'tindak_lanjut' => $tindak_lanjut,
            'datemodified' => $datetime
        );
        $flag             = $this->retur_model->update_retur($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Data berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Data berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function delete_returan($retur_id = ""){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        if ($retur_id != "") {
            $flag = $this->retur_model->delete_retur($retur_id);
            if ($flag) {
                $this->session->set_flashdata('message', 'success_delete');
                redirect(base_url("update_returan"));
            } else {
                $this->session->set_flashdata('message', 'error_delete');
                redirect(base_url("update_returan"));
            }
        }
    }
/*------End of Produksi & Return-------*/

/*------Start of Hutang-------*/
    function hutang_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $customer_id     = $this->input->post('customer_id');
        $tanda_terima    = $this->input->post('tanda_terima');
        $tanggal_faktur  = $this->input->post('tanggal_faktur');
        $tanggal_kembali = $this->input->post('tanggal_kembali');
        $no_faktur_pajak = $this->input->post('no_faktur_pajak');
        $no_po           = $this->input->post('no_po');
        $cetak           = $this->input->post('cetak');
        $jumlah_produk   = (INT) $this->input->post('jumlah_produk');
        $jumlah_satuan   = "";
        $nama_barang     = "";
        $harga_barang    = "";
        $no_faktur       = "";
        $jumlah_harga    = "";
        $tipe_berat      = "";
        for ($i = 1; $i <= $jumlah_produk; $i++) {
            if ($i < $jumlah_produk) {
                $tipe_berat .= $this->input->post('tipe_berat_barang_' . $i) . ";";
                $jumlah_satuan .= $this->input->post('banyaknya_barang_' . $i) . ";";
                $no_faktur .= $this->input->post('no_faktur_' . $i) . ";";
                $nama_barang .= $this->input->post('nama_barang_' . $i) . ";";
                $harga_barang .= $this->input->post('harga_barang_' . $i) . ";";
                $jumlah_harga .= ((FLOAT) $this->input->post('banyaknya_barang_' . $i)) * ((FLOAT) $this->input->post('harga_barang_' . $i)) . ";";
            } else {
                $tipe_berat .= $this->input->post('tipe_berat_barang_' . $i);
                $jumlah_satuan .= $this->input->post('banyaknya_barang_' . $i);
                $no_faktur .= $this->input->post('no_faktur_' . $i) . ";";
                $nama_barang .= $this->input->post('nama_barang_' . $i);
                $harga_barang .= $this->input->post('harga_barang_' . $i);
                $jumlah_harga .= ((FLOAT) $this->input->post('banyaknya_barang_' . $i)) * ((FLOAT) $this->input->post('harga_barang_' . $i));
            }
        }
        $jumlah_tagihan    = 0;
        $jumlah_tagihan_ex = explode(";", $jumlah_harga);
        foreach ($jumlah_tagihan_ex as $item) {
            $jumlah_tagihan += (FLOAT) $item;
        }
        $where        = array(
            'id' => $customer_id
        );
        $customer_qry = $this->customer_model->read_customer($where);
        if ($customer_qry->num_rows() != 0) {
            $customer_data = $customer_qry->row();
            $customer_name = $customer_data->name;
            $ppn_persen    = (FLOAT) $this->input->post('ppn_type');
            $ppn_value     = $ppn_persen * $jumlah_tagihan;
            $dpp_pajak     = $jumlah_tagihan + $ppn_value;
            $grand_total   = $jumlah_tagihan + $ppn_value;
            $datetime      = date("Y-m-d H:i:s");
            $member        = $user;
            $data          = array(
                'user_id' => $member->id,
                'customer_id' => $customer_id,
                'tanda_terima' => $tanda_terima,
                'tanggal_faktur' => $tanggal_faktur,
                'tanggal_kembali' => $tanggal_kembali,
                'jumlah_satuan' => $jumlah_satuan,
                'nama_barang' => $nama_barang,
                'harga_barang' => $harga_barang,
                'no_faktur' => $no_faktur,
                'tipe_berat' => $tipe_berat,
                'jumlah_total' => $jumlah_harga,
                'jumlah_tagihan' => $jumlah_tagihan,
                'no_faktur_pajak' => $no_faktur_pajak,
                'persen_pajak' => $ppn_persen*100,
                'dpp_pajak' => $dpp_pajak,
                'pajak' => $ppn_value,
                'no_po' => $no_po,
                'status' => 'BELUM',
                'datecreated' => $datetime,
                'datemodified' => $datetime
            );
            $flag = $this->fakturin_model->create_fakturin($data);
            if ($flag) {
                $data_print = array(
                    'jumlah_produk' => $jumlah_produk,
                    'tipe_berat' => explode(";", $tipe_berat),
                    'no_faktur' => explode(";", $no_faktur),
                    'jumlah_satuan' => explode(";", $jumlah_satuan),
                    'nama_barang' => explode(";", $nama_barang),
                    'harga_barang' => explode(";", $harga_barang),
                    'jumlah_harga' => explode(";", $jumlah_harga),
                    'tanda_terima' => $tanda_terima,
                    'customer_name' => $customer_name,
                    'persen_pajak' => $ppn_persen,
                    'member_name' => $member->name,
                    'tanggal_faktur' => $tanggal_faktur,
                    'subtotal' => $jumlah_tagihan,
                    'ppn' => $ppn_value,
                    'total' => $grand_total
                );
                if($cetak=='ya'){
                    $this->load->view('hutang/fakturin_print', $data_print);
                } else {
                    //set flashdata
                    $this->session->set_userdata('message', '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>');
                    redirect(base_url('hutang'));
                }
            } else {
                //set flashdata
                $this->session->set_userdata('message', '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>');
                redirect(base_url('hutang'));
            }
        } else {
            echo 'Invalid Customer id';
        }
    }

    function edit_fakturin_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }
        
        $id_hutang             = $this->input->post('id');
        $status                = $this->input->post('status');
        $tanggal_bayar         = $this->input->post('tanggal_bayar');
        $jumlah_bayar          = $this->input->post('jumlah_bayar');
        $keterangan_pembayaran = $this->input->post('keterangan_pembayaran');
        $datetime              = date("Y-m-d H:i:s");
        $data                  = array(
            'id' => $id_hutang,
            'status' => $status,
            'tanggal_bayar' => $tanggal_bayar,
            'jumlah_bayar' => $jumlah_bayar,
            'keterangan_pembayaran' => $keterangan_pembayaran,
            'datemodified' => $datetime
        );
        $flag                  = $this->fakturin_model->update_fakturin($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Data berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Data berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function send_email_pembelian_bulanan(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $member        = $user;
        $email         = $this->input->post('email');
        $year          = $this->input->post('year');
        $month         = $this->input->post('month');
        $customer_id   = $this->input->post('customer_id');
        $link_download = base_url('laporan/download_pembelian_bulanan?customer_id=' . $customer_id . "&year=" . $year . "&month=" . $month);
        $data_email    = array(
            'judul_laporan' => 'Pembelian Bulanan (Fakturin)',
            'link_download' => '<a href="' . $link_download . '">Disini!</a>'
        );
        //        print_r($data_email);
        $this->mailer->send_email_report($email, $data_email);
        //        //set flash data
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Email Telah Terkirim!</div>');
        redirect(base_url('report/pembelian_hutang_bulanan'));
    }
    
    function download_pembelian_bulanan(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $customer_id   = $this->input->get('customer_id');
        $year          = $this->input->get('year');
        $month         = $this->input->get('month');
        $customer_id   = $this->input->get('customer_id');
        $year          = ($year == '' ? $this->input->post('year') : $year);
        $month         = ($month == '' ? $this->input->post('month') : $month);
        $customer_id   = ($customer_id == '' ? $this->input->post('customer_id') : $customer_id);
        $date          = $year . "-" . $month;
        $fakturin      = get_fakturin_list_by_month($date, $customer_id);
        $total_tagihan = 0;
        $total_pajak   = 0;
        if ($fakturin != false) {
            $no  = 0;
            //PDF Processing 
            $pdf = new FPDF('l', 'mm', 'LETTER');
            // membuat halaman baru
            $pdf->AddPage();
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial', 'B', 10);
            // mencetak string 
            $pdf->Cell(75, 2, 'Laporan Pembelian Fakturin Bulan ' . date("M", strtotime($date)) . ' ' . $year, 0, 1, 'C');
            $pdf->SetFont('Arial', 'B', 12);
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10, 7, '', 0, 1);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(12, 6, 'No', 1, 0);
            $pdf->Cell(40, 6, 'Customer', 1, 0);
            $pdf->Cell(30, 6, 'No.Faktur', 1, 0);
            $pdf->Cell(22, 6, 'Tgl. Faktur', 1, 0);
            $pdf->Cell(22, 6, 'Tgl. Kembali', 1, 0);
            $pdf->Cell(25, 6, 'Jml Tagihan', 1, 0);
            $pdf->Cell(35, 6, 'No.Fak Pajak', 1, 0);
            $pdf->Cell(25, 6, 'Dpp Pajak', 1, 0);
            $pdf->Cell(35, 6, 'No.PO', 1, 1);
            $pdf->SetFont('Arial', '', 10);
            foreach ($fakturin->result() as $item) {
                $no++;
                $pdf->Cell(12, 6, $no, 1, 0);
                $pdf->Cell(40, 6, $item->customer_name, 1, 0);
                $pdf->Cell(30, 6, $item->no_faktur, 1, 0);
                $pdf->Cell(22, 6, $item->tanggal_faktur, 1, 0);
                $pdf->Cell(22, 6, $item->tanggal_kembali, 1, 0);
                $pdf->Cell(25, 6, $item->jumlah_tagihan, 1, 0);
                $pdf->Cell(35, 6, $item->no_faktur_pajak, 1, 0);
                $pdf->Cell(25, 6, $item->dpp_pajak, 1, 0);
                $pdf->Cell(35, 6, $item->no_po, 1, 1);
                $total_tagihan += $item->jumlah_tagihan;
                $total_pajak += $item->dpp_pajak;
            }
            $pdf->Cell(12, 6, '', 1, 0);
            $pdf->Cell(40, 6, 'TOTAL', 1, 0);
            $pdf->Cell(30, 6, '', 1, 0);
            $pdf->Cell(22, 6, '', 1, 0);
            $pdf->Cell(22, 6, '', 1, 0);
            $pdf->Cell(25, 6, $total_tagihan, 1, 0);
            $pdf->Cell(35, 6, '', 1, 0);
            $pdf->Cell(25, 6, $total_pajak, 1, 0);
            $pdf->Cell(35, 6, '', 1, 1);
            $file_name_save = 'laporan_pembelian_bulanan_' . date('Y-m-d-H-i-s') . ".pdf";
            $pdf->Output();
        } else {
            echo 'Belum ada data';
        }
    }

    function report_fakturin_monthly(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $year                = $this->input->get('year');
        $month               = $this->input->get('month');
        $customer_id         = $this->input->get('customer_id');
        $date                = $year . "-" . $month;
        $data['date']        = $date;
        $data['member']      = $user;
        $data['customer_id'] = $customer_id;
        $this->load->view('hutang/report_pembelian_perbulan_change', $data);
    }

    function edit_fakturin($id_fakturin){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $where         = "A.id='" . $id_fakturin . "'";
        $data_fakturin = $this->fakturin_model->read_fakturin($where);
        if ($data_fakturin->num_rows() != 0) {
            $fakturin         = $data_fakturin->row();
            $data['title']    = 'Hutang';
            $data['content']  = 'hutang/edit_fakturin';
            $data['fakturin'] = $fakturin;
            $data['member']   = $user;
            $this->load->view('template', $data);
        }
    }

    function edit_hutang_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $fakturin_id     = (INT) $this->input->post('fakturin_id');
        $jumlah_produk   = (INT) $this->input->post('jumlah_produk');
        $customer_id     = $this->input->post('customer_id');
        $no_faktur       = $this->input->post('no_faktur');
        $tanggal_faktur  = $this->input->post('tanggal_faktur');
        $tanggal_kembali = $this->input->post('tanggal_kembali');
        $no_faktur_pajak = $this->input->post('no_faktur_pajak');
        $no_po           = $this->input->post('no_po');
        $jumlah_satuan   = "";
        $nama_barang     = "";
        $harga_barang    = "";
        $jumlah_harga    = "";
        $tipe_berat      = "";
        $no_spl          = "";
        for ($i = 1; $i <= $jumlah_produk; $i++) {
            if ($i < $jumlah_produk) {
                $tipe_berat .= $this->input->post('tipe_berat_barang_' . $i) . ";";
                $jumlah_satuan .= $this->input->post('banyaknya_barang_' . $i) . ";";
                $nama_barang .= $this->input->post('nama_barang_' . $i) . ";";
                $no_spl .= $this->input->post('no_spl_barang_' . $i) . ";";
                $harga_barang .= $this->input->post('harga_barang_' . $i) . ";";
                $jumlah_harga .= ((FLOAT) $this->input->post('banyaknya_barang_' . $i)) * ((FLOAT) $this->input->post('harga_barang_' . $i)) . ";";
            } else {
                $tipe_berat .= $this->input->post('tipe_berat_barang_' . $i);
                $jumlah_satuan .= $this->input->post('banyaknya_barang_' . $i);
                $nama_barang .= $this->input->post('nama_barang_' . $i);
                $no_spl .= $this->input->post('no_spl_barang_' . $i);
                $harga_barang .= $this->input->post('harga_barang_' . $i);
                $jumlah_harga .= ((FLOAT) $this->input->post('banyaknya_barang_' . $i)) * ((FLOAT) $this->input->post('harga_barang_' . $i));
            }
        }
        $jumlah_tagihan    = 0;
        $jumlah_tagihan_ex = explode(";", $jumlah_harga);
        foreach ($jumlah_tagihan_ex as $item) {
            $jumlah_tagihan += (FLOAT) $item;
        }
        $ppn_persen = (FLOAT) $this->input->post('ppn_type');
        $ppn_value  = $ppn_persen * $jumlah_tagihan;
        $dpp_pajak  = $jumlah_tagihan + $ppn_value;
        $datetime   = date("Y-m-d H:i:s");
        $member     = $user;
        $data       = array(
            'id' => $fakturin_id,
            'customer_id' => $customer_id,
            'no_faktur' => $no_faktur,
            'tanggal_faktur' => $tanggal_faktur,
            'tanggal_kembali' => $tanggal_kembali,
            'jumlah_satuan' => $jumlah_satuan,
            'nama_barang' => $nama_barang,
            'harga_barang' => $harga_barang,
            'tipe_berat' => $tipe_berat,
            'no_spl' => $no_spl,
            'jumlah_total' => $jumlah_harga,
            'jumlah_tagihan' => $jumlah_tagihan,
            'no_faktur_pajak' => $no_faktur_pajak,
            'pajak' => $ppn_value,
            'dpp_pajak' => $dpp_pajak,
            'no_po' => $no_po,
            'datemodified' => $datetime
        );
        $flag       = $this->fakturin_model->update_fakturin($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Edit data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Edit data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function print_fakturin($fakturin_id){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $where = "A.id='".$fakturin_id."'";
        $fakturin_qry = $this->fakturin_model->read_fakturin($where);
        if ($fakturin_qry->num_rows() != 0) {
            $fakturin_data = $fakturin_qry->row();
            $customer_id   = $fakturin_data->customer_id;
            $where         = array(
                'id' => $customer_id
            );
            $customer_qry  = $this->customer_model->read_customer($where);
            if ($customer_qry->num_rows() != 0) {
                $customer_data = $customer_qry->row();
                $customer_name = $customer_data->name;
                $ppn_persen    = ($fakturin_data->dpp_pajak==$fakturin_data->jumlah_tagihan?0:0.10);
                $ppn           = $fakturin_data->jumlah_tagihan*$ppn_persen;
                $grand_total   = $fakturin_data->jumlah_tagihan-$ppn;
                $jumlah_produk = count(explode(";", $fakturin_data->nama_barang));
                $datetime      = date("Y-m-d H:i:s");

                    $data_print = array(
                        'jumlah_produk' => $jumlah_produk,
                        'tanda_terima' => $fakturin_data->tanda_terima,
                        'tipe_berat' => explode(";", $fakturin_data->tipe_berat),
                        'jumlah_satuan' => explode(";", $fakturin_data->jumlah_satuan),
                        'nama_barang' => explode(";", $fakturin_data->nama_barang),
                        'no_spl' => explode(";", $fakturin_data->no_spl),
                        'harga_barang' => explode(";", $fakturin_data->harga_barang),
                        'jumlah_harga' => explode(";", $fakturin_data->jumlah_total),
                        'no_faktur' => explode(";", $fakturin_data->no_faktur),
                        'persen_pajak' => $fakturin_data->persen_pajak,
                        'customer_name' => $customer_name,
                        'tanggal_faktur' => $fakturin_data->tanggal_faktur,
                        'subtotal' => $fakturin_data->jumlah_tagihan,
                        'ppn' => $fakturin_data->pajak,
                        'total' => $fakturin_data->dpp_pajak
                    );
                    $this->load->view('hutang/fakturin_print', $data_print);
            }
        }
    }

    function send_email_pembelian_harian(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $member        = $user;
        $email         = $this->input->post('email');
        $date          = $this->input->post('date');
        $customer_id   = $this->input->post('customer_id');
        $link_download = base_url('laporan/download_pembelian_harian?customer_id=' . $customer_id . "&date=" . $date );
        $data_email    = array(
            'judul_laporan' => 'Pembelian Harian (Fakturin)',
            'link_download' => '<a href="' . $link_download . '">Disini!</a>'
        );
        //        print_r($data_email);
        $this->mailer->send_email_report($email, $data_email);
        //        //set flash data
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Email Telah Terkirim!</div>');
        redirect(base_url('report/pembelian_hutang_harian'));
    }
    
    function download_pembelian_harian(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $date          = $this->input->get('date');
        $customer_id   = $this->input->get('customer_id');
        $date          = ($date == '' ? $this->input->post('date') : $date);
        $customer_id   = ($customer_id == '' ? $this->input->post('customer_id') : $customer_id);
        $fakturin      = get_fakturin_by_date($date, $customer_id);
        $total_tagihan = 0;
        $total_pajak   = 0;
        if ($fakturin != false) {
            $no  = 0;
            //PDF Processing 
            $pdf = new FPDF('l', 'mm', 'LETTER');
            // membuat halaman baru
            $pdf->AddPage();
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial', 'B', 10);
            // mencetak string 
            $pdf->Cell(75, 2, 'Laporan Pembelian Fakturin Harian ' . $date . ' ' , 0, 1, 'C');
            $pdf->SetFont('Arial', 'B', 12);
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10, 7, '', 0, 1);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(12, 6, 'No', 1, 0);
            $pdf->Cell(40, 6, 'Customer', 1, 0);
            $pdf->Cell(30, 6, 'No.Faktur', 1, 0);
            $pdf->Cell(22, 6, 'Tgl. Faktur', 1, 0);
            $pdf->Cell(22, 6, 'Tgl. Kembali', 1, 0);
            $pdf->Cell(25, 6, 'Jml Tagihan', 1, 0);
            $pdf->Cell(35, 6, 'No.Fak Pajak', 1, 0);
            $pdf->Cell(25, 6, 'Dpp Pajak', 1, 0);
            $pdf->Cell(46, 6, 'No.PO', 1, 1);
            $pdf->SetFont('Arial', '', 10);
            foreach ($fakturin->result() as $item) {
                $no++;
                $pdf->Cell(12, 6, $no, 1, 0);
                $pdf->Cell(40, 6, $item->customer_name, 1, 0);
                $pdf->Cell(30, 6, $item->no_faktur, 1, 0);
                $pdf->Cell(22, 6, $item->tanggal_faktur, 1, 0);
                $pdf->Cell(22, 6, $item->tanggal_kembali, 1, 0);
                $pdf->Cell(25, 6, $item->jumlah_tagihan, 1, 0);
                $pdf->Cell(35, 6, $item->no_faktur_pajak, 1, 0);
                $pdf->Cell(25, 6, $item->dpp_pajak, 1, 0);
                $pdf->Cell(46, 6, $item->no_po, 1, 1);
                $total_tagihan += $item->jumlah_tagihan;
                $total_pajak += $item->dpp_pajak;
            }
            $pdf->Cell(12, 6, '', 1, 0);
            $pdf->Cell(40, 6, 'TOTAL', 1, 0);
            $pdf->Cell(30, 6, '', 1, 0);
            $pdf->Cell(22, 6, '', 1, 0);
            $pdf->Cell(22, 6, '', 1, 0);
            $pdf->Cell(25, 6, $total_tagihan, 1, 0);
            $pdf->Cell(35, 6, '', 1, 0);
            $pdf->Cell(25, 6, $total_pajak, 1, 0);
            $pdf->Cell(46, 6, '', 1, 1);
            $file_name_save = 'laporan_pembelian_bulanan_' . date('Y-m-d-H-i-s') . ".pdf";
            $pdf->Output();
        } else {
            echo 'Belum ada data';
        }
    }

    function report_fakturin_daily(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $date           = $this->input->get('date');
        $customer_id    = $this->input->get('customer_id');
        $data['customer_id'] = $customer_id;
        $data['date']   = $date;
        $data['member'] = $user;
        $this->load->view('hutang/report_pembelian_harian_change', $data);
    }
/*------End of Hutang-------*/

/*------Start of Piutang-------*/
    function faktur_piutang_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $customer_id     = $this->input->post('customer_id');
        $site_code       = $this->input->post('site_code');
        $tanggal         = $this->input->post('tanggal_faktur');
        $no_sj           = $this->input->post('sj');
        $no_faktur_pajak = $this->input->post('no_faktur_pajak');
        $cetak           = $this->input->post('cetak');
        $no_faktur       = '';
        $member          = $user;
        $besar_ppn       = 0.10;
        $ppn             = 0;
        $subtotal        = 0;
        $total           = 0;
        $barang_det      = '
      <table class="table">
      <thead>
      <tr>
        <th>No.SJ</th>
        <th>BANYAKNYA</th>
        <th></th>
        <th>NAMA BARANG</th>
        <th></th>
        <th>HARGA @Rp.</th>
        <th>JUMLAH</th>
      </tr>
      </thead>
      <tbody>
      ';
        //checking customer
        $where           = array(
            'id' => $customer_id
        );
        $customer_qry    = $this->customer_model->read_customer($where);
        if ($customer_qry->num_rows() != 0) {
            $customer_data = $customer_qry->row();
            $customer_name = $customer_data->name;
            $curdate       = date('Y-m-d H:i:s');
            //penghitungan
            $no_sj_ex      = explode(',', $no_sj);
            $cur_sj        = "";
            foreach ($no_sj_ex as $sj) {
                $where  = array(
                    'sj_no' => $sj,
                    'no_faktur' => ''
                );
                $sj_qry = $this->sj_model->read_sj($where);
                if ($sj_qry->num_rows() != 0) {
                    $sj_data          = $sj_qry->row();
                    $jumlah_satuan_ex = explode(';', $sj_data->jumlah_satuan);
                    $tipe_berat_ex    = explode(';', $sj_data->tipe_berat);
                    $nama_barang_ex   = explode(';', $sj_data->nama_barang);
                    $spl_ex           = explode(';', $sj_data->no_spl);
                    $harga_ex         = explode(';', $sj_data->harga_barang);
                    $jumlah_ex        = explode(';', $sj_data->jumlah_total);
                    $jumlah_barang    = count($nama_barang_ex);
                    for ($i = 0; $i < $jumlah_barang; $i++) {
                        $barang_det .= '
            <tr>
            <td>
            ' . $sj . '
            </td>
            <td>
            ' . money($jumlah_satuan_ex[$i]) . '
            </td>
            <td>
            ' . $tipe_berat_ex[$i] . '
            </td>
            <td>
            ' . $nama_barang_ex[$i] . '
            </td>
            <td>
            ' . $spl_ex[$i] . '
            </td>
            <td>
            ' . money($harga_ex[$i]) . '
            </td>
            <td>
            ' . money($jumlah_ex[$i]) . '
            </td>
            </tr>
            ';
                        $cur_sj = $sj;
                        $subtotal += (FLOAT) $jumlah_ex[$i];
                    }
                }
            }
            $barang_det .= '</tbody></table>';
            $ppn       = ($no_faktur_pajak != "" ? $besar_ppn * $subtotal : 0);
            $total     = $subtotal + $ppn;
            $faktur_in = array(
                'id_sj' => $no_sj,
                'customer_id' => $customer_id,
                'site_code' => $site_code,
                'user_id' => $member->id,
                'subtotal' => $subtotal,
                'ppn' => $ppn,
                'total' => $total,
                'no_faktur' => $no_faktur,
                'no_faktur_pajak' => $no_faktur_pajak,
                'tanggal_faktur' => $tanggal,
                'datecreated' => $curdate,
                'datemodified' => $curdate
            );
            $insert_id = $this->faktur_model->create_faktur($faktur_in);
            if (!empty($insert_id)) {
                $no_faktur   = $site_code . $insert_id;
                //update faktur
                $data_update = array(
                    'id' => $insert_id,
                    'no_faktur' => $no_faktur
                );
                $this->faktur_model->update_faktur($data_update);
                //update SJ
                foreach ($no_sj_ex as $sj) {
                    $update_sj = array(
                        'sj_no' => $sj,
                        'no_faktur' => $no_faktur,
                        'datemodified' => $curdate
                    );
                    $this->sj_model->update_sj_by_nosj($update_sj);
                }
                //set json
                $data = array(
                    'barang_det' => $barang_det,
                    'subtotal' => $subtotal,
                    'customer_name' => $customer_name,
                    'tanggal_faktur' => $tanggal,
                    'faktur_no' => $no_faktur,
                    'total' => $total,
                    'ppn' => $ppn,
                    'npn' => $no_faktur_pajak,
                    'member_name' => $member->name
                );
                if($cetak=='ya'){
                $this->load->view('piutang/faktur_print', $data);
                $this->session->set_flashdata('msg', '<div class="alert alert-success"><strong>Sukses!</strong> Penyimpanan dan pencetakan faktur berhasil.</div>');
                } else {
                    $this->session->set_flashdata('msg', '<div class="alert alert-success"><strong>Sukses!</strong> Penyimpanan dan pencetakan faktur berhasil.</div>');
                redirect(base_url('faktur'));
                }
            } else {
                //set flash data
                $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> Penyimpanan dan pencetakan faktur gagal.</div>');
                redirect(base_url('faktur'));
            }
        } else {
            //set flash data
            $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> ID CUstomer tidak valid.</div>');
            redirect(base_url('faktur'));
        }
    }

    function sj_search(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $no_sj  = $this->input->post('no_sj');
        $where  = array(
            'sj_no' => $no_sj,
            'no_faktur' => '0'
        );
        $sj_qry = $this->sj_model->read_sj($where);
        if ($sj_qry->num_rows() != 0) {
            $sj_data          = $sj_qry->row();
            $jumlah_satuan_ex = explode(';', $sj_data->jumlah_satuan);
            $tipe_berat_ex    = explode(';', $sj_data->tipe_berat);
            $nama_barang_ex   = explode(';', $sj_data->nama_barang);
            $spl_ex           = explode(';', $sj_data->no_spl);
            $harga_ex         = explode(';', $sj_data->harga_barang);
            $jumlah_ex        = explode(';', $sj_data->jumlah_total);
            $jumlah_barang    = count($nama_barang_ex);
            $barang_det       = '
      <table class="table">
      <thead>
      <tr>
        <th>Banyaknya</th>
        <th>Tipe Berat</th>
        <th>Nama Barang</th>
        <th>SPL</th>
        <th>Harga</th>
        <th>Jumlah</th>
      </tr>
      </thead>
      <tbody id="produk_add_table">
      ';
            for ($i = 0; $i < $jumlah_barang; $i++) {
                $barang_det .= '
        <tr>
        <td>
        ' . $jumlah_satuan_ex[$i] . '
        </td>
        <td>
        ' . $tipe_berat_ex[$i] . '
        </td>
        <td>
        ' . $nama_barang_ex[$i] . '
        </td>
        <td>
        ' . $spl_ex[$i] . '
        </td>
        <td>
        ' . money($harga_ex[$i]) . '
        </td>
        <td>
        ' . money($jumlah_ex[$i]) . '
        </td>
        </tr>
        ';
            }
            $barang_det .= '</tbody></table>';
            $data_json = array(
                'info' => 'success',
                'message' => '
        <div class="panel panel-default" id="sj_' . $sj_data->sj_no . '">
        <div class="panel-heading">' . $sj_data->sj_no . ' <a class="pull-right delete-sj text-danger" href="#" sj_no="' . $sj_data->sj_no . '" sj_id="sj_' . $sj_data->sj_no . '"><span class="fa fa-trash"></span> Hapus</a></div>
        <div class="panel-body">
        ' . $barang_det . '
        </div>
        </div>
        ',
                'success_message' => '<div class="alert alert-success">
          <strong>Sukses!</strong> No.SJ berhasil ditambahkan.
        </div>'
            );
            die(json_encode($data_json));
        } else {
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger">
          <strong>Error!</strong> No.SJ Tidak Ditemukan.
        </div>'
            );
            die(json_encode($data_json));
        }
    }

    function nota_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $no_nota               = $this->input->post('no_nota');
        $tanggal_nota          = $this->input->post('tanggal_nota');
        $customer_id           = $this->input->post('customer_id');
        $pajak_non_pajak       = $this->input->post('pajak_non_pajak');
        $faktur_id             = $this->input->post('faktur');
        $cetak                 = $this->input->post('cetak');
        $grand_total           = 0;
        $ppn                   = 0;
        $keterangan_pembayaran = $this->input->post('keterangan_pembayaran');
        $tanggal_bayar         = $this->input->post('tanggal_bayar');
        $jumlah_bayar          = $this->input->post('jumlah_bayar');
        $datetime              = date("Y-m-d H:i:s");
        $where                 = array(
            'id' => $customer_id
        );
        $customer_qry          = $this->customer_model->read_customer($where);
        $besar_ppn             = 0.10;
        $total                 = 0;
        if ($customer_qry->num_rows() != 0) {
            $customer_data  = $customer_qry->row();
            $customer_name  = $customer_data->name;
            //menghitung grandtotal
            $faktur_id_list = explode(",", $faktur_id);
            foreach ($faktur_id_list as $item) {
                $where      = "A.no_faktur='" . $item . "'";
                $faktur_qry = $this->faktur_model->read_faktur($where);
                if ($faktur_qry->num_rows() != 0) {
                    $faktur_data = $faktur_qry->row();
                    $grand_total += $faktur_data->total;
                }
            }
            $subtotal = $grand_total;
            $total = $subtotal;
            if($pajak_non_pajak != 'n'){
                $ppn   = $besar_ppn * $grand_total;
                $total += $ppn;
            }
            
            $data  = array(
                'no_nota' => $no_nota,
                'tanggal_nota' => $tanggal_nota,
                'customer_id' => $customer_id,
                'pajak_non_pajak' => $pajak_non_pajak,
                'faktur_id' => $faktur_id,
                'grand_total' => $total,
                'keterangan_pembayaran' => $keterangan_pembayaran,
                'tanggal_bayar' => $tanggal_bayar,
                'jumlah_bayar' => $jumlah_bayar,
                'datecreated' => $datetime,
                'datemodified' => $datetime
            );
            $flag  = $this->nota_penjualan_model->create_nota_penjualan($data);
            if ($flag) {
                $faktur_det  = '
                  <table class="table">
                  <thead>
                  <tr>
                    <th>No Faktur</th>
                    <th>Surat Jalan</th>
                    <th>Tanggal Faktur</th>
                    <th>Jumlah</th>
                  </tr>
                  </thead>
                  <tbody>
                  ';
                $faktur_list = explode(',', $faktur_id);
                foreach ($faktur_list as $faktur) {
                    $where       = 'A.no_faktur="' . $faktur . '"';
                    $faktur_qry  = $this->faktur_model->read_faktur($where);
                    $faktur_data = $faktur_qry->row();
                    if ($faktur_qry->num_rows() != 0) {
                        $faktur_det .= "
                <tr>";
                        $faktur_det .= "<td>" . $faktur_data->no_faktur . "</td>";
                        $faktur_det .= "<td>";
                        $sj     = explode(',', $faktur_data->id_sj);
                        $jumlah = count($sj);
                        for ($i = 0; $i < $jumlah; $i++) {
                            $faktur_det .= '
                    ' . $sj[$i] . ',
                    ';
                        }
                        $faktur_det .= ' 
                    </td>
                    <td>
                    ' . $faktur_data->tanggal_faktur . '
                    </td>
                    <td>
                    ' . money($faktur_data->total) . '
                    </td>
                    </tr>';
                    }
                }
                $faktur_det .= '</tbody></table>';
                $data_display = array(
                    'tanggal_nota' => $tanggal_nota,
                    'nota_no' => $no_nota,
                    'customer_name' => $customer_name,
                    'subtotal' => $grand_total,
                    'faktur_det' => $faktur_det,
                    'ppn' => $ppn,
                    'total' => $total,
                    'subtotal' => $subtotal,
                    'npn' => $pajak_non_pajak
                );
                if($cetak=='ya'){
                $this->load->view('piutang/nota_penjualan_print', $data_display);
                } else {
                   $this->session->set_flashdata('msg', '<div class="alert alert-success"><strong>Sukses!</strong> Data berhasil disimpan!.</div>');
                redirect(base_url('nota_penjualan')); 
                }
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> Data tidak valid.</div>');
                redirect(base_url('nota_penjualan'));
        } 
        }   else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> Data tidak valid.</div>');
                redirect(base_url('nota_penjualan'));
        } 
    }

    function faktur_search(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $faktur_no  = $this->input->post('faktur_no_search');
        $where      = 'A.no_faktur="' . $faktur_no . '"';
        $faktur_qry = $this->faktur_model->read_faktur($where);
        if ($faktur_qry->num_rows() != 0) {
            $faktur_data = $faktur_qry->row();
            $sj          = explode(',', $faktur_data->id_sj);
            $jumlah      = count($sj);
            $faktur_det  = '
      <table class="table">
      <thead>
      <tr>
        <th>Surat Jalan</th>
        <th>Tanggal Faktur</th>
        <th>Jumlah</th>
      </tr>
      </thead>
      <tbody id="faktur_add_table">
      <tr>
      <td>
      ';
            for ($i = 0; $i < $jumlah; $i++) {
                $faktur_det .= '
        <span class="label label-primary">' . $sj[$i] . '</span> &nbsp;
        ';
            }
            $faktur_det .= ' 
        </td>
        <td>
        ' . $faktur_data->tanggal_faktur . '
        </td>
        <td>
        ' . money($faktur_data->total) . '
        </td>
        </tr>';
            $faktur_det .= '</tbody></table>';
            $data_json = array(
                'info' => 'success',
                'message' => '
        <div class="panel panel-default" id="faktur_' . $faktur_data->no_faktur . '">
        <div class="panel-heading">' . $faktur_data->no_faktur . ' <a class="pull-right delete-faktur text-danger" href="#" faktur_no="' . $faktur_data->no_faktur . '" faktur_id="faktur_' . $faktur_data->no_faktur . '"><span class="fa fa-trash"></span> Hapus</a></div>
        <div class="panel-body">
        ' . $faktur_det . '
        </div>
        </div>
        ',
                'success_message' => '<div class="alert alert-success">
          <strong>Sukses!</strong> No.Faktur berhasil ditambahkan.
        </div>'
            );
            die(json_encode($data_json));
        } else {
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger">
          <strong>Error!</strong> No.Faktur Tidak Ditemukan.
        </div>'
            );
            die(json_encode($data_json));
        }
    }

    function cetak_kwitansi(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $diterima_dari      = $this->input->post('diterima_dari');
        $banyaknya_uang     = $this->input->post('banyaknya_uang');
        $untuk_pembayaran_1 = $this->input->post('untuk_pembayaran_1');
        $untuk_pembayaran_2 = $this->input->post('untuk_pembayaran_2');
        $jumlah_rp          = $this->input->post('jumlah_rp');
        $tempat_dan_tanggal = $this->input->post('tempat_dan_tanggal');
        $data               = array(
            'diterima_dari' => $diterima_dari,
            'banyaknya_uang' => terbilang($jumlah_rp),
            'untuk_pembayaran_1' => $untuk_pembayaran_1,
            'untuk_pembayaran_2' => $untuk_pembayaran_2,
            'jumlah_rp' => $jumlah_rp,
            'tempat_dan_tanggal' => $tempat_dan_tanggal
        );
        $this->load->view('piutang/cetak_kwitansi', $data);
    }

    function edit_faktur_pelunasan_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id_hutang             = $this->input->post('id');
        $status                = $this->input->post('status_lunas');
        $tanggal_bayar         = $this->input->post('tanggal_bayar');
        $jumlah_bayar          = $this->input->post('jumlah_bayar');
        $keterangan_pembayaran = $this->input->post('keterangan_pembayaran');
        $datetime              = date("Y-m-d H:i:s");
        $data                  = array(
            'id' => $id_hutang,
            'status' => $status,
            'tanggal_bayar' => $tanggal_bayar,
            'jumlah_bayar' => $jumlah_bayar,
            'keterangan_pembayaran' => $keterangan_pembayaran,
            'datemodified' => $datetime
        );
        $flag                  = $this->faktur_model->update_faktur($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Data berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Data berhasil diubah</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }

    function send_email_penjualan_bulanan(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $member        = $user;
        $email         = $this->input->post('email');
        $year          = $this->input->post('year');
        $month         = $this->input->post('month');
        $customer_id   = $this->input->post('customer_id');
        $link_download = base_url('laporan/download_penjualan_bulanan?customer_id=' . $customer_id . "&year=" . $year."&month=".$month );
        $data_email    = array(
            'judul_laporan' => 'Penjualan Bulanan (Faktur)',
            'link_download' => '<a href="' . $link_download . '">Disini!</a>'
        );
        //        print_r($data_email);
        $this->mailer->send_email_report($email, $data_email);
        //        //set flash data
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Email Telah Terkirim!</div>');
        redirect(base_url('report/penjualan_piutang_bulanan'));
    }
    
    function download_penjualan_bulanan(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $customer_id   = $this->input->get('customer_id');
        $year          = $this->input->get('year');
        $month         = $this->input->get('month');
        $customer_id   = $this->input->get('customer_id');
        $year          = ($year == '' ? $this->input->post('year') : $year);
        $month         = ($month == '' ? $this->input->post('month') : $month);
        $customer_id   = ($customer_id == '' ? $this->input->post('customer_id') : $customer_id);
        $date          = $year . "-" . $month;
        $fakturin      = get_faktur_list_by_month($date, $customer_id);
        $total_tagihan = 0;
        $total_pajak   = 0;
        if ($fakturin != false) {
            $no  = 0;
            //PDF Processing 
            $pdf = new FPDF('l', 'mm', 'LETTER');
            // membuat halaman baru
            $pdf->AddPage();
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial', 'B', 10);
            // mencetak string 
            $pdf->Cell(75, 2, 'Laporan Penjualan Fakturin Bulan '. date("M", strtotime($date)) . ' ' . $year, 0, 1, 'C');
            $pdf->SetFont('Arial', 'B', 12);
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10, 7, '', 0, 1);
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(12, 6, 'No', 1, 0);
            $pdf->Cell(40, 6, 'Customer', 1, 0);
            $pdf->Cell(30, 6, 'No.Faktur', 1, 0);
            $pdf->Cell(22, 6, 'Tgl. Faktur', 1, 0);
            $pdf->Cell(25, 6, 'Jml Tagihan', 1, 0);
            $pdf->Cell(35, 6, 'No.Fak Pajak', 1, 0);
            $pdf->Cell(25, 6, 'PPN', 1, 0);
            $pdf->Cell(15, 6, 'Status', 1, 0);
            $pdf->Cell(20, 6, 'Ket.Bayar', 1, 0);
            $pdf->Cell(20, 6, 'Tgl. Bayar', 1, 0);
            $pdf->Cell(20, 6, 'Jml Bayar', 1, 1);
            $pdf->SetFont('Arial', '', 10);
            foreach ($fakturin->result() as $item) {
                $no++;
                $pdf->Cell(12, 6, $no, 1, 0);
                $pdf->Cell(40, 6, $item->customer_name, 1, 0);
                $pdf->Cell(30, 6, $item->no_faktur, 1, 0);
                $pdf->Cell(22, 6, $item->tanggal_faktur, 1, 0);
                $pdf->Cell(25, 6, money($item->total), 1, 0);
                $pdf->Cell(35, 6, $item->no_faktur_pajak, 1, 0);
                $pdf->Cell(25, 6, money($item->ppn), 1, 0);
                $pdf->Cell(15, 6, $item->status, 1, 0);
                $pdf->Cell(20, 6, ($item->keterangan_pembayaran==''?'Belum ada':decode_enum($item->keterangan_pembayaran)), 1, 0);
                $pdf->Cell(20, 6, ($item->tanggal_bayar=='0000:00:00'?'Belum ada':$item->tanggal_bayar), 1, 0);
                $pdf->Cell(20, 6, ($item->jumlah_bayar=='00,0'?'Belum ada':money($item->jumlah_bayar)), 1, 1);
                $total_tagihan += $item->total;
                $total_pajak += $item->ppn;
            }
            $pdf->Cell(12, 6, '', 1, 0);
            $pdf->Cell(40, 6, 'TOTAL', 1, 0);
            $pdf->Cell(30, 6, '', 1, 0);
            $pdf->Cell(22, 6, '', 1, 0);
            $pdf->Cell(25, 6, money($total_tagihan), 1, 0);
            $pdf->Cell(35, 6, '', 1, 0);
            $pdf->Cell(25, 6, money($total_pajak), 1, 0);
            $pdf->Cell(15, 6, '', 1, 0);
            $pdf->Cell(20, 6, '', 1, 0);
            $pdf->Cell(20, 6, '', 1, 0);
            $pdf->Cell(20, 6, '', 1, 1);
            $file_name_save = 'laporan_penjualan_bulanan_' . date('Y-m-d-H-i-s') . ".pdf";
            $pdf->Output();
        } else {
            echo 'Belum ada data';
        }
    }

    function report_faktur_monthly(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $year                = $this->input->get('year');
        $month               = $this->input->get('month');
        $customer_id         = $this->input->get('customer_id');
        $date                = $year . "-" . $month;
        $data['date']        = $date;
        $data['member']      = $user;
        $data['customer_id'] = $customer_id;
        $this->load->view('piutang/report_penjualan_perbulan_change', $data);
    }

    function edit_nota_penjualan($id_nota){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $where          = "A.id='" . $id_nota . "'";
        $nota_data      = $this->nota_penjualan_model->read_nota_penjualan($where);
        $faktur_det_isi = "";
        if ($nota_data->num_rows() != 0) {
            $nota        = $nota_data->row();
            $faktur_list = explode(',', $nota->faktur_id);
            foreach ($faktur_list as $faktur) {
                $faktur_no  = $faktur;
                $where      = 'A.no_faktur="' . $faktur_no . '"';
                $faktur_qry = $this->faktur_model->read_faktur($where);
                if ($faktur_qry->num_rows() != 0) {
                    $faktur_data = $faktur_qry->row();
                    $sj          = explode(',', $faktur_data->id_sj);
                    $jumlah      = count($sj);
                    $faktur_det  = '
              <table class="table">
              <thead>
              <tr>
                <th>Surat Jalan</th>
                <th>Tanggal Faktur</th>
                <th>Jumlah</th>
              </tr>
              </thead>
              <tbody id="faktur_add_table">
              <tr>
              <td>
              ';
                    for ($i = 0; $i < $jumlah; $i++) {
                        $faktur_det .= '
                <span class="label label-primary">' . $sj[$i] . '</span> &nbsp;
                ';
                    }
                    $faktur_det .= ' 
                </td>
                <td>
                ' . $faktur_data->tanggal_faktur . '
                </td>
                <td>
                ' . money($faktur_data->total) . '
                </td>
                </tr>';
                    $faktur_det .= '</tbody></table>';
                    $faktur_det_isi .= '
                <div class="panel panel-default" id="faktur_' . $faktur_data->no_faktur . '">
                <div class="panel-heading">' . $faktur_data->no_faktur . ' <a class="pull-right delete-faktur text-danger" href="#" faktur_no="' . $faktur_data->no_faktur . '" faktur_id="faktur_' . $faktur_data->no_faktur . '"><span class="fa fa-trash"></span> Hapus</a></div>
                <div class="panel-body">
                ' . $faktur_det . '
                </div>
                </div>
                ';
                }
            }
            $data['title']   = 'Edit Nota Penjualan';
            $data['content'] = 'piutang/edit_nota_penjualan';
            $data['member']  = $user;
            $data['nota']    = $nota_data->row();
            $data['faktur']  = $faktur_det_isi;
            $this->load->view('template', $data);
        }
    }

    function edit_nota_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $id_nota               = $this->input->post('id_nota');
        $no_nota               = $this->input->post('no_nota');
        $tanggal_nota          = $this->input->post('tanggal_nota');
        $customer_id           = $this->input->post('customer_id');
        $pajak_non_pajak       = $this->input->post('pajak_non_pajak');
        $faktur_id             = $this->input->post('faktur');
        $grand_total           = 0;
        $keterangan_pembayaran = $this->input->post('keterangan_pembayaran');
        $tanggal_bayar         = $this->input->post('tanggal_bayar');
        $jumlah_bayar          = $this->input->post('jumlah_bayar');
        $datetime              = date("Y-m-d H:i:s");
        $besar_ppn             = 0.10;
        $total                 = 0;
        $where                 = array(
            'id' => $customer_id
        );
        $customer_qry          = $this->customer_model->read_customer($where);
        if ($customer_qry->num_rows() != 0) {
            $customer_data  = $customer_qry->row();
            $customer_name  = $customer_data->name;
            //menghitung grandtotal
            $faktur_id_list = explode(",", $faktur_id);
            foreach ($faktur_id_list as $item) {
                $where      = "A.no_faktur='" . $faktur_id . "'";
                $faktur_qry = $this->faktur_model->read_faktur($where);
                if ($faktur_qry->num_rows() != 0) {
                    $faktur_data = $faktur_qry->row();
                    $grand_total += $faktur_data->total;
                }
            }
            $ppn   = $besar_ppn * $grand_total;
            $total = $grand_total - $ppn;
            $data  = array(
                'no_nota' => $no_nota,
                'tanggal_nota' => $tanggal_nota,
                'customer_id' => $customer_id,
                'pajak_non_pajak' => $pajak_non_pajak,
                'faktur_id' => $faktur_id,
                'grand_total' => $grand_total,
                'keterangan_pembayaran' => $keterangan_pembayaran,
                'tanggal_bayar' => $tanggal_bayar,
                'jumlah_bayar' => $jumlah_bayar,
                'datecreated' => $datetime,
                'datemodified' => $datetime
            );
            $flag  = $this->nota_penjualan_model->create_nota_penjualan($data);
            if ($flag) {
                //set flashdata
                $this->session->set_flashdata('msg', '<div class="alert alert-success"><strong>Sukses!</strong> Edit Nota Berhasil!.</div>');
                redirect(base_url('report/nota_penjualan'));
            } else {
                echo 'Data tidak valid';
            }
        } else {
            echo 'Customer ID Tidak Valid';
        }
    }

    function print_nota_penjualan($nota_id){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $where       = "A.id = '" . $nota_id . "'";
        $nota_qry    = $this->nota_penjualan_model->read_nota_penjualan($where);
        $ppn         = 0;
        $besar_ppn   = 0.10;
        $grand_total = 0;
        if ($nota_qry->num_rows() != 0) {
            $data_nota   = $nota_qry->row();
            $faktur_id   = $data_nota->faktur_id;
            $faktur_det  = '
                  <table class="table">
                  <thead>
                  <tr>
                    <th>No Faktur</th>
                    <th>Surat Jalan</th>
                    <th>Tanggal Faktur</th>
                    <th>Jumlah</th>
                  </tr>
                  </thead>
                  <tbody>
                  ';
            $faktur_list = explode(',', $faktur_id);
            foreach ($faktur_list as $item) {
                $where      = "A.no_faktur='" . $faktur_id . "'";
                $faktur_qry = $this->faktur_model->read_faktur($where);
                if ($faktur_qry->num_rows() != 0) {
                    $faktur_data = $faktur_qry->row();
                    $grand_total += $faktur_data->total;
                }
            }
            $total = $grand_total;
            foreach ($faktur_list as $faktur) {
                $where       = 'A.no_faktur="' . $faktur . '"';
                $faktur_qry  = $this->faktur_model->read_faktur($where);
                $faktur_data = $faktur_qry->row();
                if ($faktur_qry->num_rows() != 0) {
                    $faktur_det .= "
                <tr>";
                    $faktur_det .= "<td>" . $faktur_data->no_faktur . "</td>";
                    $faktur_det .= "<td>";
                    $sj     = explode(',', $faktur_data->id_sj);
                    $jumlah = count($sj);
                    for ($i = 0; $i < $jumlah; $i++) {
                        $faktur_det .= '
                    ' . $sj[$i] . ',
                    ';
                    }
                    $faktur_det .= ' 
                    </td>
                    <td>
                    ' . $faktur_data->tanggal_faktur . '
                    </td>
                    <td>
                    ' . money($faktur_data->total) . '
                    </td>
                    </tr>';
                }
            }
            $faktur_det .= '</tbody></table>';
            $data_display = array(
                'tanggal_nota' => $nota_qry->row()->tanggal_nota,
                'nota_no' => $data_nota->no_nota,
                'customer_name' => $data_nota->customer_name,
                'subtotal' => $data_nota->grand_total,
                'faktur_det' => $faktur_det,
                'ppn' => $ppn,
                'total' => $total,
                'npn' => $data_nota->pajak_non_pajak
            );
            $this->load->view('piutang/nota_penjualan_print', $data_display);
        } else {
            echo 'Id Nota tidak valid';
        }
    }

    function report_nota_penjualan_daily(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $date           = $this->input->get('date');
        $data['date']   = $date;
        $data['member'] = $user;
        $this->load->view('piutang/report_nota_penjualan_change', $data);
    }

    function edit_faktur($id_faktur){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $where       = "A.id='" . $id_faktur . "'";
        $data_faktur = $this->faktur_model->read_faktur($where);
        $isi_sj      = "";
        if ($data_faktur->num_rows() != 0) {
            $faktur   = $data_faktur->row();
            $no_sj_ex = explode(',', $faktur->id_sj);
            $cur_sj   = "";
            foreach ($no_sj_ex as $sj) {
                $where  = array(
                    'sj_no' => $sj,
                    'no_faktur' => ''
                );
                $sj_qry = $this->sj_model->read_sj($where);
                if ($sj_qry->num_rows() != 0) {
                    $sj_data          = $sj_qry->row();
                    $jumlah_satuan_ex = explode(';', $sj_data->jumlah_satuan);
                    $tipe_berat_ex    = explode(';', $sj_data->tipe_berat);
                    $nama_barang_ex   = explode(';', $sj_data->nama_barang);
                    $spl_ex           = explode(';', $sj_data->no_spl);
                    $harga_ex         = explode(';', $sj_data->harga_barang);
                    $jumlah_ex        = explode(';', $sj_data->jumlah_total);
                    $jumlah_barang    = count($nama_barang_ex);
                    $barang_det       = '
          <table class="table">
          <thead>
          <tr>
            <th>Banyaknya</th>
            <th>Tipe Berat</th>
            <th>Nama Barang</th>
            <th>SPL</th>
            <th>Harga</th>
            <th>Jumlah</th>
          </tr>
          </thead>
          <tbody id="produk_add_table">
          ';
                    for ($i = 0; $i < $jumlah_barang; $i++) {
                        $barang_det .= '
            <tr>
            <td>
            ' . $jumlah_satuan_ex[$i] . '
            </td>
            <td>
            ' . $tipe_berat_ex[$i] . '
            </td>
            <td>
            ' . $nama_barang_ex[$i] . '
            </td>
            <td>
            ' . $spl_ex[$i] . '
            </td>
            <td>
            ' . $harga_ex[$i] . '
            </td>
            <td>
            ' . $jumlah_ex[$i] . '
            </td>
            </tr>
            ';
                    }
                    $barang_det .= '</tbody></table>';
                    $isi_sj .= '
            <div class="panel panel-default" id="sj_' . $sj_data->sj_no . '">
            <div class="panel-heading">' . $sj_data->sj_no . ' <a class="pull-right delete-sj text-danger" href="#" sj_no="' . $sj_data->sj_no . '" sj_id="sj_' . $sj_data->sj_no . '"><span class="fa fa-trash"></span> Hapus</a></div>
            <div class="panel-body">
            ' . $barang_det . '
            </div>
            </div>
            ';
                }
            }
            $data['title']   = 'Edit Faktur';
            $data['content'] = 'piutang/edit_faktur';
            $data['member']  = $user;
            $data['faktur']  = $faktur;
            $data['sj']      = $isi_sj;
            $this->load->view('template', $data);
        } else {
            echo 'ID tidak valid';
        }
    }

    function edit_faktur_process(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $customer_id     = $this->input->post('customer_id');
        $site_code       = $this->input->post('site_code');
        $tanggal         = $this->input->post('tanggal_faktur');
        $no_sj           = $this->input->post('sj');
        $no_faktur_pajak = $this->input->post('no_faktur_pajak');
        $id_faktur       = $this->input->post('id_faktur');
        $no_faktur       = '';
        $member          = get_current_member();
        $besar_ppn       = 0.10;
        $ppn             = 0;
        $subtotal        = 0;
        $total           = 0;
        $barang_det      = '
      <table class="table">
      <thead>
      <tr>
        <th>No.SJ</th>
        <th>BANYAKNYA</th>
        <th></th>
        <th>NAMA BARANG</th>
        <th></th>
        <th>HARGA @Rp.</th>
        <th>JUMLAH</th>
      </tr>
      </thead>
      <tbody>
      ';
        //checking customer
        $where           = array(
            'id' => $customer_id
        );
        $customer_qry    = $this->customer_model->read_customer($where);
        if ($customer_qry->num_rows() != 0) {
            $customer_data = $customer_qry->row();
            $customer_name = $customer_data->name;
            $curdate       = date('Y-m-d H:i:s');
            //penghitungan
            $no_sj_ex      = explode(',', $no_sj);
            $cur_sj        = "";
            foreach ($no_sj_ex as $sj) {
                $where  = array(
                    'sj_no' => $sj,
                    'no_faktur' => ''
                );
                $sj_qry = $this->sj_model->read_sj($where);
                if ($sj_qry->num_rows() != 0) {
                    $sj_data          = $sj_qry->row();
                    $jumlah_satuan_ex = explode(';', $sj_data->jumlah_satuan);
                    $tipe_berat_ex    = explode(';', $sj_data->tipe_berat);
                    $nama_barang_ex   = explode(';', $sj_data->nama_barang);
                    $spl_ex           = explode(';', $sj_data->no_spl);
                    $harga_ex         = explode(';', $sj_data->harga_barang);
                    $jumlah_ex        = explode(';', $sj_data->jumlah_total);
                    $jumlah_barang    = count($nama_barang_ex);
                    for ($i = 0; $i < $jumlah_barang; $i++) {
                        $barang_det .= '
            <tr>
            <td>
            ' . ($cur_sj == $sj ? '' : $sj) . '
            </td>
            <td>
            ' . $jumlah_satuan_ex[$i] . '
            </td>
            <td>
            ' . $tipe_berat_ex[$i] . '
            </td>
            <td>
            ' . $nama_barang_ex[$i] . '
            </td>
            <td>
            ' . $spl_ex[$i] . '
            </td>
            <td>
            ' . $harga_ex[$i] . '
            </td>
            <td>
            ' . money($jumlah_ex[$i]) . '
            </td>
            </tr>
            ';
                        $cur_sj = $sj;
                        $subtotal += (FLOAT) $jumlah_ex[$i];
                    }
                }
            }
            $barang_det .= '</tbody></table>';
            $ppn       = ($no_faktur_pajak != "" ? $besar_ppn * $subtotal : 0);
            $total     = $subtotal - $ppn;
            $faktur_in = array(
                'id' => $id_faktur,
                'id_sj' => $no_sj,
                'customer_id' => $customer_id,
                'user_id' => $member->id,
                'site_code' => $site_code,
                'subtotal' => $subtotal,
                'ppn' => $ppn,
                'total' => $total,
                'no_faktur' => $site_code . $id_faktur,
                'no_faktur_pajak' => $no_faktur_pajak,
                'tanggal_faktur' => $tanggal,
                'datemodified' => $curdate
            );
            $flag      = $this->faktur_model->update_faktur($faktur_in);
            if ($flag == 1) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success"><strong>Sukses!</strong> Penyimpanan dan pencetakan faktur berhasil.</div>');
                redirect(base_url('report/faktur_harian'));
            } else {
                //set flash data
                $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> Penyimpanan dan pencetakan faktur gagal.</div>');
                redirect(base_url('report/faktur_harian'));
            }
        } else {
            //set flash data
            $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> Data Cusomer tidak ditemukan</div>');
            redirect(base_url('report/faktur_harian'));
        }
    }

    function print_faktur($id_faktur){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $where      = "A.id='" . $id_faktur . "'";
        $faktur_qry = $this->faktur_model->read_faktur($where);
        $subtotal   = 0;
        $besar_ppn  = 0.10;
        if ($faktur_qry->num_rows() != 0) {
            $faktur_data  = $faktur_qry->row();
            $barang_det   = '
      <table class="table">
      <thead>
      <tr>
        <th>No.SJ</th>
        <th>BANYAKNYA</th>
        <th></th>
        <th>NAMA BARANG</th>
        <th></th>
        <th>HARGA @Rp.</th>
        <th>JUMLAH</th>
      </tr>
      </thead>
      <tbody>
      ';
            //checking customer
            $where        = array(
                'id' => $faktur_data->customer_id
            );
            $customer_qry = $this->customer_model->read_customer($where);
            if ($customer_qry->num_rows() != 0) {
                $customer_data = $customer_qry->row();
                $customer_name = $customer_data->name;
                $curdate       = date('Y-m-d H:i:s');
                //penghitungan
                $no_sj_ex      = explode(',', $faktur_data->id_sj);
                $cur_sj        = "";
                foreach ($no_sj_ex as $sj) {
                    $where  = array(
                        'sj_no' => $sj,
                        'no_faktur' => ''
                    );
                    $sj_qry = $this->sj_model->read_sj($where);
                    if ($sj_qry->num_rows() != 0) {
                        $sj_data          = $sj_qry->row();
                        $jumlah_satuan_ex = explode(';', $sj_data->jumlah_satuan);
                        $tipe_berat_ex    = explode(';', $sj_data->tipe_berat);
                        $nama_barang_ex   = explode(';', $sj_data->nama_barang);
                        $spl_ex           = explode(';', $sj_data->no_spl);
                        $harga_ex         = explode(';', $sj_data->harga_barang);
                        $jumlah_ex        = explode(';', $sj_data->jumlah_total);
                        $jumlah_barang    = count($nama_barang_ex);
                        for ($i = 0; $i < $jumlah_barang; $i++) {
                            $barang_det .= '
            <tr>
            <td>
            ' . $sj . '
            </td>
            <td>
            ' . $jumlah_satuan_ex[$i] . '
            </td>
            <td>
            ' . $tipe_berat_ex[$i] . '
            </td>
            <td>
            ' . $nama_barang_ex[$i] . '
            </td>
            <td>
            ' . $spl_ex[$i] . '
            </td>
            <td>
            ' . $harga_ex[$i] . '
            </td>
            <td>
            ' . money($jumlah_ex[$i]) . '
            </td>
            </tr>
            ';
                            $cur_sj = $sj;
                            $subtotal += (FLOAT) $jumlah_ex[$i];
                        }
                    }
                }
                $barang_det .= '</tbody></table>';
                $ppn   = ($faktur_data->no_faktur_pajak != "" ? $besar_ppn * $subtotal : 0);
                $total = $subtotal + $ppn;
                $data  = array(
                    'barang_det' => $barang_det,
                    'subtotal' => $subtotal,
                    'customer_name' => $customer_name,
                    'tanggal_faktur' => $faktur_data->tanggal_faktur,
                    'faktur_no' => $faktur_data->no_faktur,
                    'total' => $total,
                    'ppn' => $ppn,
                    'npn' => $faktur_data->no_faktur_pajak
                );
                $this->load->view('piutang/faktur_print', $data);
            } else
                echo 'invalid id';
        } else
            echo 'invalid customer id';
    }

    function report_faktur_daily(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }

        $date           = $this->input->get('date');
        $id_customer    = $this->input->get('id_customer'); 
        $data['date']   = $date;
        $data['id_customer'] = $id_customer;
        $data['member'] = $user;
        $this->load->view('piutang/report_faktur_harian_change', $data);
    }

    function get_customer_and_print(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }
        
        #init variable
        // $search = $this->input->get('search');
        // $sort = $this->input->get('sort');
        // $order_by = $this->input->get('order_by');

        #get spl
        // $order = array('field'=>$order_by, 'order'=>$sort);
        // $pegawais = $this->pegawai_model->get_pegawai($search, $user->site_code, $order);
        $customer = get_customer_list();
        $data = array('customers' => $customer->result());
        $this->load->view('piutang/print_customer', $data);
    }

    function get_customer_and_export(){
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $this->load->view('errors/html/error_auth');
            return;
        }
        
        #init variable
        // $search = $this->input->get('search');
        // $sort = $this->input->get('sort');
        // $order_by = $this->input->get('order_by');

        #get spl
        // $order = array('field'=>$order_by, 'order'=>$sort);
        // $pegawais = $this->pegawai_model->get_pegawai($search, $user->site_code, $order);
        $customer = get_customer_list();
        $data = array('customers' => $customer->result());
        $this->load->view('piutang/export_customer', $data);
    }
/*------End of Piutang-------*/

/*------Start of Keyword-------*/
    function get_keyword(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #init variable
        $page_number = $this->input->get('page_number');
        $page_size = $this->input->get('page_size');
        $search = $this->input->get('search');
        $sort = $this->input->get('sort');
        $order_by = $this->input->get('order_by');
        $draw = $this->input->get('draw');
        $params = array($page_number, $page_size);

        #check request params
        if(!checkParameter($params)){
            $respObj->setResponse(400, "failed", "Missing parameter.", $params);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #get user
        $start = $page_number * $page_size;
        $order = array('field'=>$order_by, 'order'=>$sort);
        $limit = array('start'=>$start, 'size'=>$page_size);
        $output = array();
        $keyword = $this->keyword_model->get_keyword($search, $order, $limit);
        $output['data'] = $keyword;
        $output['recordsTotal'] = $this->keyword_model->get_keyword_total($search);
        $output['recordsFiltered'] = $output['recordsTotal'];
        
        #response
        if(empty($draw)){
            $respObj->setResponse(200, "success", "Get keyword is success", $output['data']);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }else{
            $output['draw'] = $draw;
            $respObj->setResponseDatatable(200, $output['data'], $draw, $output['recordsTotal'], $output['recordsFiltered']);
            $resp = $respObj->getResponseDatatable();
            setOutput($resp);
            return;
        } 
    }

    function get_keyword_detail(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check keyword
        $id = $this->input->get('id');
        $keyword = $this->keyword_model->get_keyword_by_id($id);
        if(is_null($keyword)){
            $respObj->setResponse(400, "failed", "Get keyword detail is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }else{
            $respObj->setResponse(200, "success", "Get keyword detail is success", $keyword);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function get_keyword_by_module(){
        $respObj = new ResponseApi();

        #authentication
        $roles=array('super_admin', 'hut_put');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check keyword
        $module = $this->input->get('module');
        $keyword = $this->keyword_model->get_keyword_by_module($module, "ACTIVE");
        if(is_null($keyword)){
            $respObj->setResponse(400, "failed", "Get keyword by module is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }else{
            $respObj->setResponse(200, "success", "Get keyword by module is success", $keyword);
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function create_keyword_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('module', 'description', 'keyword'))){
            $respObj->setResponse(400, "failed", "module, description, and keyword is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $module = $request['module'];
        $description = $request['description'];
        $keyword = $request['keyword'];
        $keyword_hash = password_hash($keyword, PASSWORD_DEFAULT);

        $data = array(
            "module" => $module,
            "description" => $description,
            "keyword_hash" => $keyword_hash
        );
        $flag = $this->keyword_model->create_keyword($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Create keyword is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } else {
            $respObj->setResponse(200, "success", "Create keyword is success");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function update_keyword_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id', 'description', 'keyword'))){
            $respObj->setResponse(400, "failed", "id, description, and keyword is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $id = $request['id'];
        $description = $request['description'];
        $keyword = $request['keyword'];
        $keyword_hash = password_hash($keyword, PASSWORD_DEFAULT);

        #check keyword
        $keyword = $this->keyword_model->get_keyword_by_id($id);
        if(is_null($keyword)){
            $respObj->setResponse(400, "failed", "keyword not found!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        $data = array(
            "id" => $id,
            "module" => $keyword->module,
            "description" => $description,
            "keyword_hash" => $keyword_hash,
            "status" => $keyword->status,
            "datecreated" => $keyword->datecreated
        );
        $flag = $this->keyword_model->update_keyword($data);
        if (!$flag) {
            $respObj->setResponse(500, "failed", "Update keyword is failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } else {
            $respObj->setResponse(200, "success", "Update keyword is success");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
    }

    function active_keyword_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check keyword
        $keyword = $this->keyword_model->get_keyword_by_id($id);
        if(is_null($keyword)){
            $respObj->setResponse(400, "failed", "keyword not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->keyword_model->active_keyword($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Active keyword is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Active keyword is success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }

    function inactive_keyword_process(){
        $respObj = new ResponseApi();
        $request = json_decode($this->input->raw_input_stream, true);

        #authentication
        $roles=array('super_admin');
        $user = authRole($roles);
        if(is_null($user)){
            $respObj->setResponse(401, "failed", "Authentication Failed!");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }

        #check payload
        if(!checkParameterByKeys($request, array('id'))){
            $respObj->setResponse(400, "failed", "id is mandatory in payload");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        $id = $request['id'];

        #check keyword
        $keyword = $this->keyword_model->get_keyword_by_id($id);
        if(is_null($keyword)){
            $respObj->setResponse(400, "failed", "keyword not found");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        }
        
        $flag = $this->keyword_model->inactive_keyword($id);
        if (!$flag) {
            $respObj->setResponse(400, "failed", "Inactive keyword is failed");
            $resp = $respObj->getResponse();
            setOutput($resp);
            return;
        } 

        $respObj->setResponse(200, "success", "Inactive keyword is success!");
        $resp = $respObj->getResponse();
        setOutput($resp);
        return;
    }
/*------End of Keyword-------*/

/*------Start of Unused endpoint-------*/
    function sj_process()
    {
        $jumlah_produk    = (INT) $this->input->post('jumlah_produk');
        $sj_no            = $this->input->post('no_sj');
        $nama_barang      = "";
        $banyaknya_barang = "";
        $tipe_berat       = "";
        $no_spl           = "";
        $harga_barang     = "";
        $jumlah_satuan    = "";
        $jumlah_harga     = "";
        for ($i = 1; $i <= $jumlah_produk; $i++) {
            if ($i < $jumlah_produk) {
                $tipe_berat .= $this->input->post('tipe_berat_barang_' . $i) . ";";
                $jumlah_satuan .= $this->input->post('banyaknya_barang_' . $i) . ";";
                $nama_barang .= $this->input->post('nama_barang_' . $i) . ";";
                $no_spl .= $this->input->post('no_spl_barang_' . $i) . ";";
                $harga_barang .= $this->input->post('harga_barang_' . $i) . ";";
                $jumlah_harga .= ((FLOAT) $this->input->post('banyaknya_barang_' . $i)) * ((FLOAT) $this->input->post('harga_barang_' . $i)) . ";";
            } else {
                $tipe_berat .= $this->input->post('tipe_berat_barang_' . $i);
                $jumlah_satuan .= $this->input->post('banyaknya_barang_' . $i);
                $nama_barang .= $this->input->post('nama_barang_' . $i);
                $no_spl .= $this->input->post('no_spl_barang_' . $i);
                $harga_barang .= $this->input->post('harga_barang_' . $i);
                $jumlah_harga .= ((FLOAT) $this->input->post('banyaknya_barang_' . $i)) * ((FLOAT) $this->input->post('harga_barang_' . $i));
            }
        }
        $jumlah_tagihan    = 0;
        $jumlah_tagihan_ex = explode(";", $jumlah_harga);
        foreach ($jumlah_tagihan_ex as $item) {
            $jumlah_tagihan += (FLOAT) $item;
        }
        $besar_ppn       = 0.10;
        $customer_id     = $this->input->post('customer_id');
        $tanggal_sj      = $this->input->post('tanggal_sj');
        $ppn             = $besar_ppn * $jumlah_tagihan;
        $no_faktur_pajak = $this->input->post('no_faktur_pajak');
        $datetime        = date("Y-m-d H:i:s");
        $member          = get_current_member();
        $data            = array(
            'user_id' => $member->id,
            'customer_id' => $customer_id,
            'sj_no' => $sj_no,
            'no_spl' => $no_spl,
            'tanggal_sj' => $tanggal_sj,
            'jumlah_satuan' => $jumlah_satuan,
            'nama_barang' => $nama_barang,
            'harga_barang' => $harga_barang,
            'tipe_berat' => $tipe_berat,
            'jumlah_total' => $jumlah_harga,
            'jumlah_tagihan' => $jumlah_tagihan,
            'no_faktur_pajak' => $no_faktur_pajak,
            'ppn' => $ppn,
            'status_lunas' => 'belum_lunas',
            'datecreated' => $datetime,
            'datemodified' => $datetime
        );
        $flag            = $this->sj_model->create_sj($data);
        if ($flag) {
            //set json
            $data_json = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Sukses!</strong> Penambahan data berhasil dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        } else {
            //set json
            $data_json = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gagal!</strong> Penambahan data gagal dilakukan</a>.</div>'
            );
            die(json_encode($data_json));
        }
    }
    
    function faktur_piutang_process_simpan()
    {
        $customer_id     = $this->input->post('customer_id');
        $site_code       = $this->input->post('site_code');
        $tanggal         = $this->input->post('tanggal_faktur');
        $no_sj           = $this->input->post('sj');
        $no_faktur_pajak = $this->input->post('no_faktur_pajak');
        $no_faktur       = '';
        $member          = get_current_member();
        $besar_ppn       = 0.10;
        $ppn             = 0;
        $subtotal        = 0;
        $total           = 0;
        //checking customer
        $where           = array(
            'id' => $customer_id
        );
        $customer_qry    = $this->customer_model->read_customer($where);
        if ($customer_qry->num_rows() != 0) {
            $customer_data = $customer_qry->row();
            $customer_name = $customer_data->name;
            $curdate       = date('Y-m-d H:i:s');
            //penghitungan
            $no_sj_ex      = explode(',', $no_sj);
            $cur_sj        = "";
            foreach ($no_sj_ex as $sj) {
                $where  = array(
                    'sj_no' => $sj,
                    'no_faktur' => ''
                );
                $sj_qry = $this->sj_model->read_sj($where);
                if ($sj_qry->num_rows() != 0) {
                    $sj_data          = $sj_qry->row();
                    $jumlah_satuan_ex = explode(';', $sj_data->jumlah_satuan);
                    $tipe_berat_ex    = explode(';', $sj_data->tipe_berat);
                    $nama_barang_ex   = explode(';', $sj_data->nama_barang);
                    $spl_ex           = explode(';', $sj_data->no_spl);
                    $harga_ex         = explode(';', $sj_data->harga_barang);
                    $jumlah_ex        = explode(';', $sj_data->jumlah_total);
                    $jumlah_barang    = count($nama_barang_ex);
                    for ($i = 0; $i < $jumlah_barang; $i++) {
                        $cur_sj = $sj;
                        $subtotal += (FLOAT) $jumlah_ex[$i];
                    }
                }
            }
            $ppn       = ($no_faktur_pajak != "" ? $besar_ppn * $subtotal : 0);
            $total     = $subtotal + $ppn;
            $faktur_in = array(
                'id_sj' => $no_sj,
                'customer_id' => $customer_id,
                'site_code' => $site_code,
                'user_id' => $member->id,
                'subtotal' => $subtotal,
                'ppn' => $ppn,
                'total' => $total,
                'no_faktur' => $no_faktur,
                'no_faktur_pajak' => $no_faktur_pajak,
                'tanggal_faktur' => $tanggal,
                'datecreated' => $curdate,
                'datemodified' => $curdate
            );
            $insert_id = $this->faktur_model->create_faktur($faktur_in);
            if (!empty($insert_id)) {
                $no_faktur   = $site_code . $insert_id;
                //update faktur
                $data_update = array(
                    'id' => $insert_id,
                    'no_faktur' => $no_faktur
                );
                $this->faktur_model->update_faktur($data_update);
                //update SJ
                foreach ($no_sj_ex as $sj) {
                    $update_sj = array(
                        'sj_no' => $sj,
                        'no_faktur' => $no_faktur,
                        'datemodified' => $curdate
                    );
                    $this->sj_model->update_sj_by_nosj($update_sj);
                }
                $this->session->set_flashdata('msg', '<div class="alert alert-success"><strong>Sukses!</strong> Penyimpanan dan pencetakan faktur berhasil.</div>');
                redirect(base_url('faktur'));
            } else {
                //set flash data
                $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> Penyimpanan dan pencetakan faktur gagal.</div>');
                redirect(base_url('faktur'));
            }
        } else {
            //set flash data
            $this->session->set_flashdata('msg', '<div class="alert alert-danger"><strong>Gagal!</strong> ID CUstomer tidak valid.</div>');
            redirect(base_url('faktur'));
        }
    }

    function export_produksi_bulanan()
    {
        //    ob_start();
        $month    = '09';
        $year     = '2018';
        $email    = $this->input->post('email');
        $date     = $year . "-" . $month;
        $member   = get_current_member();
        $produksi = get_produksi_by_month($date, $member->site_code);
        if ($produksi != false) {
            $no  = 0;
            //PDF Processing 
            $pdf = new FPDF('l', 'mm', 'A5');
            // membuat halaman baru
            $pdf->AddPage();
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial', 'B', 16);
            // mencetak string 
            $pdf->Cell(190, 7, 'Laporan Produksi Buanan', 0, 1, 'C');
            $pdf->SetFont('Arial', 'B', 12);
            // Memberikan space kebawah agar tidak terlalu rapat
            $pdf->Cell(10, 7, '', 0, 1);
            $pdf->SetFont('Arial', 'B', 10);
            $pdf->Cell(20, 6, 'No', 1, 0);
            $pdf->Cell(20, 6, 'No.Mesin', 1, 0);
            $pdf->Cell(27, 6, 'Ukuran', 1, 0);
            $pdf->Cell(25, 6, 'Jumlah(Kg)', 1, 0);
            $pdf->Cell(25, 6, 'Jumlah(Lbr)', 1, 1);
            $pdf->SetFont('Arial', '', 10);
            $no = 1;
            foreach ($produksi->result() as $item) {
                $no++;
                $pdf->Cell(20, 6, $no, 1, 0);
                $pdf->Cell(20, 6, $item->no_machine, 1, 0);
                $pdf->Cell(27, 6, $item->size, 1, 0);
                $pdf->Cell(25, 6, $item->number_of_production_kg, 1, 0);
                $pdf->Cell(25, 6, $item->number_of_production_sheet, 1, 1);
            }
            $file_name_save = 'laporan_produksi_buanan_' . date('Y-m-d-H-i-s') . ".pdf";
            $pdf->Output();
            //        $assets_export_len = strlen(BASE_URL().'/');
            //        $attachment_url = substr($export, strlen(BASE_URL()));
            //        $file_name = $file_name_save;
            //        $data_email = array(
            //        'email'=>$email,
            //        'judul_laporan'=>'Laporan Produksi Buanan',
            //        'nama_file'=>$file_name,
            //        'attachment_url'=>$attachment_url,
            //        'tanggal'=>$date
            //        );
            //        $this->mailer->send_email_report_attachment($data_email);
            ////        ob_end_flush(); 
            //        echo 'Berhasil';
        }
    }
/*------End of Unused endpoint-------*/
}
?>

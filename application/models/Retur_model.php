<?php
class retur_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_retur($data){
        $this->db->insert($this->retur,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_retur($where="", $group="", $limit=''){
        $sql = "SELECT A.*, B.name username, B.site_code, C.name retur_name ";
        if($group!="") $sql.=", IFNULL(SUM(A.jumlah_retur_kg),0) total_kg, IFNULL(SUM(A.jumlah_retur_lembar),0) total_sheet ";
        $sql.="FROM ".$this->retur." AS A 
        JOIN ".$this->user." AS B on B.id=A.user_id
        JOIN ".$this->customer." AS C on C.id=A.customer_id ";
        if($where!="") $sql.="WHERE ".$where;
        if($group!="") $sql.=" GROUP BY ".$group;
        if($limit!="") $sql.=" LIMIT ".$limit['start'].", ".$limit['length'];
        $query=$this->db->query($sql);
        return $query;
    }
    function update_retur($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->retur,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_retur($id){
        $this->db->where('id',$id);
        $this->db->delete($this->retur);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function read_returails($where=""){
      $sql = " SELECT A.*, B.name, B.price FROM ".$this->retur." AS A
      JOIN ".$this->book." AS B ON B.id=A.book_id
      ";
      if($where!="") $sql.=" WHERE ".$where;
      return $this->db->query($sql);
    }
}
?>

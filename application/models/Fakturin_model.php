<?php
class fakturin_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_fakturin($data){
        $this->db->insert($this->fakturin,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_fakturin($where){
        $sql = "SELECT A.*, B.name username, C.name customer_name FROM ".$this->fakturin." AS A 
        JOIN ".$this->user." AS B on B.id=A.user_id
        JOIN ".$this->customer." AS C on C.id=A.customer_id ";
        if($where!="") $sql.="WHERE ".$where;
        $query=$this->db->query($sql);
        return $query;
    }
    function update_fakturin($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->fakturin,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_fakturin($id){
        $this->db->where('id',$id);
        $this->db->delete($this->fakturin);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>

<?php
class Kasbon_pegawai_model extends CI_Model{
    public $id;
    public $pegawai_id;
    public $tanggal_kasbon;
    public $jumlah_kasbon;
    public $alasan_kasbon;
    public $potong_mingguan;
    public $potong_bulanan;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_kasbon_pegawai($search=null, $site_code=null, $start_date=null, $end_date=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nip FROM pegawai as p where p.id=pegawai_id) as pegawai_nip
        ");
        $this->db->from("kasbon_pegawai");
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', tanggal_kasbon, jumlah_kasbon, alasan_kasbon, potong_mingguan, potong_bulanan) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        if($start_date){
            $this->db->where("tanggal_kasbon >=", $start_date);
        }
        if($end_date){
            $this->db->where("tanggal_kasbon <=", $end_date);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_kasbon_pegawai_total($search=null, $site_code=null, $start_date=null, $end_date=null){
        $this->db->select("*");
        $this->db->from('kasbon_pegawai');
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', tanggal_kasbon, jumlah_kasbon, alasan_kasbon, potong_mingguan, potong_bulanan) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        if($start_date){
            $this->db->where("tanggal_kasbon >=", $start_date);
        }
        if($end_date){
            $this->db->where("tanggal_kasbon <=", $end_date);
        }

        return $this->db->count_all_results();
    }

    function get_kasbon_pegawai_by_id($id){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nip FROM pegawai as p where p.id=pegawai_id) as pegawai_nip
        ");
        $this->db->from('kasbon_pegawai');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_last_kasbon_by_pegawai($pegawai_id){
        $this->db->select("*");
        $this->db->from('kasbon_pegawai');
        $this->db->where("pegawai_id", $pegawai_id);
        $this->db->order_by("tanggal_kasbon", "DESC"); 
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function create_kasbon_pegawai($data){
        $this->pegawai_id = $data['pegawai_id'];
        $this->tanggal_kasbon = $data['tanggal_kasbon'];
        $this->jumlah_kasbon = $data['jumlah_kasbon'];
        $this->alasan_kasbon = $data['alasan_kasbon'];
        $this->potong_mingguan = $data['potong_mingguan'];
        $this->potong_bulanan = $data['potong_bulanan'];
        $this->datecreated = date("Y-m-d H:i:s");
  
        $this->db->insert('kasbon_pegawai', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_kasbon_pegawai($data){
        $this->id = $data['id'];
        $this->pegawai_id = $data['pegawai_id'];
        $this->tanggal_kasbon = $data['tanggal_kasbon'];
        $this->jumlah_kasbon = $data['jumlah_kasbon'];
        $this->alasan_kasbon = $data['alasan_kasbon'];
        $this->potong_mingguan = $data['potong_mingguan'];
        $this->potong_bulanan = $data['potong_bulanan'];
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('kasbon_pegawai', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function delete_kasbon_pegawai($id){
        $this->db->where('id', $id);
        $this->db->delete('kasbon_pegawai');
        return $this->db->affected_rows();
    }
}
?>
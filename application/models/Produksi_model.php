<?php
class produksi_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_produksi($data){
        $this->db->insert($this->produksi,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_produksi($where=""){
        $sql = "SELECT A.*, B.name username, B.site_code FROM ".$this->produksi." AS A 
        JOIN ".$this->user." AS B on B.id=A.user_id
        ";
        if($where!="") $sql.="WHERE ".$where;
        $sql.="ORDER BY A.datecreated DESC";
        $query=$this->db->query($sql);
        return $query;
    }
    function read_produksi_group($where=""){
        $sql = "SELECT A.*, B.name username, B.site_code FROM ".$this->produksi." AS A 
        JOIN ".$this->user." AS B on B.id=A.user_id
        ";
        if($where!="") $sql.="WHERE ".$where;
        $query=$this->db->query($sql);
        return $query;
    }
    function read_produksi_groupday($where=""){
        $sql = "SELECT A.*, B.name username, B.site_code FROM ".$this->produksi." AS A 
        JOIN ".$this->user." AS B on B.id=A.user_id
        ";
        if($where!="") $sql.="WHERE ".$where;
        $query=$this->db->query($sql);
        return $query;
    }
    function update_produksi($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->produksi,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_produksi($id){
        $this->db->where('id',$id);
        $this->db->delete($this->produksi);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>

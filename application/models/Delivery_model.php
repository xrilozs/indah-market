<?php
class Delivery_model extends CI_Model{
    public $id;
    public $spl_id;
    public $kodeCust;
    public $tglSetorBarang1;
    public $jlhYgDisetor1;
    public $tglSetorBarang2;
    public $jlhYgDisetor2;
    public $received;
    public $delivered;
    public $returned;
    public $ret_desc;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_delivery($search=null, $date=null, $site_code=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT s.no_spl FROM spl as s where s.id=spl_id) as spl_no,
            (SELECT s.ukuran_potong FROM spl as s where s.id=spl_id) as ukuran
        ");
        $this->db->from("delivery");
        if($search){
            $where_search = "(
                ((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                ((SELECT s.ukuran_potong FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', kodeCust, tglSetorBarang1, jlhYgDisetor1, tglSetorBarang2, jlhYgDisetor2, ret_desc) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $where_date = "DATE(datecreated) = '$date'";
            $this->db->where($where_date);
        }
        if($site_code){
            $where_site_code = "((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_delivery_monthly($search=null, $date=null, $site_code=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT s.no_spl FROM spl as s where s.id=spl_id) as spl_no,
            (SELECT s.ukuran_potong FROM spl as s where s.id=spl_id) as ukuran
        ");
        $this->db->from("delivery");
        if($search){
            $where_search = "(
                ((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                ((SELECT s.ukuran_potong FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', kodeCust, tglSetorBarang1, jlhYgDisetor1, tglSetorBarang2, jlhYgDisetor2, ret_desc) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $date_arr = explode("-",$date);
            $where_date = '(YEAR(datecreated) = '.$date_arr[0].' AND MONTH(datecreated) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $where_site_code = "((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_delivery_by_id($id, $site_code){
        $this->db->select("*");
        $this->db->from('delivery');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_delivery_by_spl_id($spl_id, $site_code){
        $this->db->select("*");
        $this->db->from('delivery');
        $where_site_code = "((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '$site_code%')";
        $this->db->where($where_site_code);
        $this->db->where("spl_id", $spl_id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_delivery_total($search=null, $date=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('delivery');
        if($search){
            $where_search = "(
                ((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                ((SELECT s.ukuran_potong FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', kodeCust, tglSetorBarang1, jlhYgDisetor1, tglSetorBarang2, jlhYgDisetor2, ret_desc) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $where_date = "DATE(datecreated) = '$date'";
            $this->db->where($where_date);
        }
        if($site_code){
            $where_site_code = "((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        return $this->db->count_all_results();
    }

    function get_delivery_monthly_total($search=null, $date=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('delivery');
        if($search){
            $where_search = "(
                ((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                ((SELECT s.ukuran_potong FROM spl as s where s.id=spl_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', kodeCust, tglSetorBarang1, jlhYgDisetor1, tglSetorBarang2, jlhYgDisetor2, ret_desc) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $date_arr = explode("-",$date);
            $where_date = '(YEAR(datecreated) = '.$date_arr[0].' AND MONTH(datecreated) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $where_site_code = "((SELECT s.no_spl FROM spl as s where s.id=spl_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        return $this->db->count_all_results();
    }

    function create_delivery($data){
        $this->spl_id = $data['spl_id'];
        $this->kodeCust = $data['kodeCust'];
        $this->tglSetorBarang1 = $data['tglSetorBarang1'];
        $this->jlhYgDisetor1 = $data['jlhYgDisetor1'];
        $this->received = 0;
        $this->delivered = 0;
        $this->returned = 0;
        $this->datecreated = date("Y-m-d H:i:s");
  
        $this->db->insert('delivery', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_delivery_custom($data){
        $data['datemodified'] = date("Y-m-d H:i:s");
        $this->db->update('delivery', $data, array('id'=>$data['id']));
        return $this->db->affected_rows() > 0;
    }

    function received_delivery_batch($ids){
        $data = array();
        foreach ($ids as $id) {
            $data[] = array(
               'id' => $id,
               'received' => 1,
               'datemodified' => date("Y-m-d H:i:s")
            );
         }    
        $this->db->update_batch('delivery', $data, 'id');
        return $this->db->affected_rows() > 0;
    }

    function delivered_delivery_batch($ids){
        $data = array();
        foreach ($ids as $id) {
            $data[] = array(
               'id' => $id,
               'delivered' => 1,
               'datemodified' => date("Y-m-d H:i:s")
            );
         }    
        $this->db->update_batch('delivery', $data, 'id');
        return $this->db->affected_rows() > 0;
    }

    function returned_delivery_batch($arr){
        $data = array();
        foreach ($arr as $r) {
            $data[] = array(
               'id' => $r['id'],
               'returned' => 1,
               'ret_desc' => $r['text'],
               'datemodified' => date("Y-m-d H:i:s")
            );
         }    
        $this->db->update_batch('delivery', $data, 'id');
        return $this->db->affected_rows() > 0;
    }

    function delete_delivery($id){
        $this->db->where('id', $id);
        $this->db->delete('delivery');
        return $this->db->affected_rows();
    }
}
?>
<?php
class settings_model extends CI_Model{

  var $campaign                 = 'campaign';
  var $charity                  = 'charity';
  var $provinces                = 'faktur';
  var $regencies                = 'retur';
  var $settings                 = 'settings';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_settings($data){
        $this->db->insert($this->settings,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_settings($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->settings);
        $query=$this->db->get();
        return $query;
    }
    function update_settings($data){
        $this->db->where('name',$data['name']);
        $this->db->update($this->settings,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_settings($id){
        $this->db->where('id',$id);
        $this->db->delete($this->chasettingsity);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
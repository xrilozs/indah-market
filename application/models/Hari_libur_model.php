<?php
class Hari_libur_model extends CI_Model{
    public $id;
    public $date;
    public $description;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_hari_libur($search=null, $year=null, $order=null, $limit=null){
        $this->db->select("*");
        $this->db->from("hari_libur");
        if($search){
            $where_search = "(
                (CONCAT_WS(',', date, description) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($year){
            $this->db->where("YEAR(date)", $year);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_hari_libur_by_id($id){
        $this->db->from('hari_libur');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_hari_libur_by_date($date){
        $this->db->from('hari_libur');
        $this->db->where("date", $date);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_hari_libur_total($year){
        $this->db->select("*");
        $this->db->from('hari_libur');
        if($year){
            $this->db->where("YEAR(date)", $year);
        }
        return $this->db->count_all_results();
    }

    function create_hari_libur($data){
        $this->date         = $data['date'];
        $this->description  = $data['description'];
        $this->datecreated  = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('hari_libur', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_hari_libur($data){
        $this->id           = $data['id'];
        $this->date         = $data['date'];
        $this->description  = $data['description'];
        $this->datecreated  = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('hari_libur', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function delete_hari_libur($id){   
        $this->db->where('id', $id);
        $this->db->delete('hari_libur');
        return $this->db->affected_rows();
    }
}
?>
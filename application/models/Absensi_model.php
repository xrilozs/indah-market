<?php
class Absensi_model extends CI_Model{
    public $id;
    public $pegawai_id;
    public $jumlah_hadir_biasa;
    public $jumlah_hari_libur;
    public $jumlah_jam_lembur;
    public $jumlah_telat;
    public $jumlah_telat_hari;
    public $jumlah_absen;
    public $potongan_kasbon;
    public $uang_tunjangan;
    public $tanggal_periode_absensi;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_absensi($search=null, $date=null, $site_code=null, $order=null, $limit=null, $jabatan=null){
        $this->db->select("a.*,
            p.nama as pegawai_name,
            p.nip as pegawai_nip,
            p.jumlah_kasbon as pegawai_jumlah_kasbon,
            p.upah_harian as pegawai_upah_harian,
            p.upah_lembur as pegawai_upah_lembur,
            p.upah_libur as pegawai_upah_libur
        ");
        $this->db->from("absensi a");
        $this->db->join("pegawai p", "p.id = a.pegawai_id");
        if($search){
            $where_search = "(
                p.nama LIKE '%".$search."%' OR 
                p.nip LIKE '%".$search."%' OR 
                (CONCAT_WS(',', a.jumlah_hadir_biasa, a.jumlah_hari_libur, a.jumlah_jam_lembur, a.jumlah_telat, a.jumlah_telat_hari, a.jumlah_absen, a.potongan_kasbon, a.uang_tunjangan) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $this->db->where("a.tanggal_periode_absensi", $date);
        }
        if($site_code){
            $where_site_code = "p.nip LIKE '$site_code%'";
            $this->db->where($where_site_code);
        }
        if($jabatan){
            $this->db->where("p.jabatan", $jabatan);
        }
        if($order){
            $this->db->order_by("a.".$order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_absensi_by_id($id){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nip FROM pegawai as p where p.id=pegawai_id) as pegawai_nip
        ");
        $this->db->from('absensi');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_absensi_by_tanggal_periode($tanggal_periode_absensi, $site_code){
        $this->db->select("a.*");
        $this->db->from('absensi a');
        $this->db->join("pegawai p", "p.id = a.pegawai_id");
        $this->db->where("tanggal_periode_absensi", $tanggal_periode_absensi);
        if($site_code){
            $where_site_code = "p.nip LIKE '$site_code%'";
            $this->db->where($where_site_code);
        }
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_absensi_total($search=null, $date=null, $site_code=null, $jabatan=null){
        $this->db->select("a.*");
        $this->db->from('absensi a');
        $this->db->join("pegawai p", "p.id = a.pegawai_id");
        if($search){
            $where_search = "(
                (CONCAT_WS(',', a.jumlah_hadir_biasa, a.jumlah_hari_libur, a.jumlah_jam_lembur, a.jumlah_telat, a.jumlah_telat_hari, a.jumlah_absen, a.potongan_kasbon, a.uang_tunjangan, p.nama) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $this->db->where("a.tanggal_periode_absensi", $date);
        }
        if($site_code){
            $where_site_code = "p.nip LIKE '$site_code%'";
            $this->db->where($where_site_code);
        }
        if($jabatan){
            $this->db->where("p.jabatan", $jabatan);
        }
        return $this->db->count_all_results();
    }

    function get_akumulasi_absensi($date, $site_code=null){
        $this->db->select("pegawai_id, SUM(jumlah_hadir_biasa) as akumulasi_hadir,
            SUM(jumlah_hari_libur) as akumulasi_libur, SUM(jumlah_jam_lembur) as akumulasi_jam_lembur,
            SUM(jumlah_telat_hari) as akumulasi_telat, SUM(jumlah_absen) as akumulasi_absen
        ");
        $this->db->from("absensi");
        $date_arr = explode("-",$date);
        $where_date = 'YEAR(tanggal_periode_absensi) = '.$date_arr[0].' AND MONTH(tanggal_periode_absensi) = '.$date_arr[1].'';
        $this->db->where($where_date);
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        $this->db->group_by("pegawai_id");
        $query = $this->db->get();
        return $query->result();
    }

    function create_absensi($data){
        $this->pegawai_id = $data['pegawai_id'];
        $this->jumlah_hadir_biasa = $data['jumlah_hadir_biasa'];
        $this->jumlah_hari_libur = $data['jumlah_hari_libur'];
        $this->jumlah_jam_lembur = $data['jumlah_jam_lembur'];
        $this->jumlah_telat = $data['jumlah_telat'];
        $this->jumlah_telat_hari = $data['jumlah_telat_hari'];
        $this->jumlah_absen = $data['jumlah_absen'];
        $this->potongan_kasbon = $data['potongan_kasbon'];
        $this->uang_tunjangan = $data['uang_tunjangan'];
        $this->tanggal_periode_absensi = $data['tanggal_periode_absensi'];
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('absensi', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_absensi($data){
        $this->id = $data['id'];
        $this->pegawai_id = $data['pegawai_id'];
        $this->jumlah_hadir_biasa = $data['jumlah_hadir_biasa'];
        $this->jumlah_hari_libur = $data['jumlah_hari_libur'];
        $this->jumlah_jam_lembur = $data['jumlah_jam_lembur'];
        $this->jumlah_telat = $data['jumlah_telat'];
        $this->jumlah_telat_hari = $data['jumlah_telat_hari'];
        $this->jumlah_absen = $data['jumlah_absen'];
        $this->potongan_kasbon = $data['potongan_kasbon'];
        $this->uang_tunjangan = $data['uang_tunjangan'];
        $this->tanggal_periode_absensi = $data['tanggal_periode_absensi'];
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('absensi', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function update_absensi_custom($data){  
        $this->db->update('absensi', $data, array('id'=>$data['id']));
        return $this->db->affected_rows() > 0;
    }

    function update_absensi_on_histori_batch($pegawai_ids, $date){
        $data = array(
            "is_update_on_history" => 1,
            "datemodified" => date("Y-m-d H:i:s")
        );
        $date_arr = explode("-",$date);
        $where_date = 'YEAR(tanggal_periode_absensi) = '.$date_arr[0].' AND MONTH(tanggal_periode_absensi) = '.$date_arr[1].'';
        $this->db->where($where_date);
        $this->db->where_in('pegawai_id', $pegawai_ids);
        $this->db->update('absensi', $data);
        return $this->db->affected_rows();
    }

    function delete_absensi($id){
        $this->db->where('id', $id);
        $this->db->delete('absensi');
        return $this->db->affected_rows();
    }
}
?>
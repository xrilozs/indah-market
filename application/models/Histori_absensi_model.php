<?php
class Histori_absensi_model extends CI_Model{
    public $id;
    public $pegawai_id;
    public $tanggal_periode;
    public $jumlah_akumulasi_hadir_biasa;
    public $jumlah_akumulasi_hari_libur;
    public $jumlah_akumulasi_jam_lembur;
    public $jumlah_akumulasi_telat;
    public $jumlah_akumulasi_absen;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_histori_absensi($search=null, $date=null, $site_code=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nip FROM pegawai as p where p.id=pegawai_id) as pegawai_nip
        ");
        $this->db->from("histori_absensi");
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', jumlah_akumulasi_hadir_biasa, jumlah_akumulasi_hari_libur, jumlah_akumulasi_jam_lembur, jumlah_akumulasi_telat, jumlah_akumulasi_absen) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $date_arr = explode("-",$date);
            $where_date = '(YEAR(tanggal_periode) = '.$date_arr[0].' AND MONTH(tanggal_periode) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_histori_absensi_by_pegawai_id_and_date($pegawai_id, $date){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nip FROM pegawai as p where p.id=pegawai_id) as pegawai_nip
        ");
        $this->db->from('histori_absensi');
        $date_arr = explode("-",$date);
        $where_date = 'YEAR(tanggal_periode) = '.$date_arr[0].' AND MONTH(tanggal_periode) = '.$date_arr[1].'';
        $this->db->where($where_date);
        $this->db->where("pegawai_id", $pegawai_id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_histori_absensi_total($search=null, $date=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('histori_absensi');
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', jumlah_akumulasi_hadir_biasa, jumlah_akumulasi_hari_libur, jumlah_akumulasi_jam_lembur, jumlah_akumulasi_telat, jumlah_akumulasi_absen) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($date){
            $date_arr = explode("-",$date);
            $where_date = '(YEAR(tanggal_periode) = '.$date_arr[0].' AND MONTH(tanggal_periode) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        return $this->db->count_all_results();
    }

    function create_histori_absensi($data){
        $this->id = null;
        $this->pegawai_id = $data['pegawai_id'];
        $this->tanggal_periode = $data['tanggal_periode'];
        $this->jumlah_akumulasi_hadir_biasa = $data['jumlah_akumulasi_hadir_biasa'];
        $this->jumlah_akumulasi_hari_libur = $data['jumlah_akumulasi_hari_libur'];
        $this->jumlah_akumulasi_jam_lembur = $data['jumlah_akumulasi_jam_lembur'];
        $this->jumlah_akumulasi_telat = $data['jumlah_akumulasi_telat'];
        $this->jumlah_akumulasi_absen = $data['jumlah_akumulasi_absen'];
        $this->datecreated = date("Y-m-d H:i:s");
  
        $this->db->insert('histori_absensi', $this);
        return $this->db->affected_rows();
    }

    function update_histori_absensi($data){
        $this->id = $data['id'];
        $this->pegawai_id = $data['pegawai_id'];
        $this->tanggal_periode = $data['tanggal_periode'];
        $this->jumlah_akumulasi_hadir_biasa = $data['jumlah_akumulasi_hadir_biasa'];
        $this->jumlah_akumulasi_hari_libur = $data['jumlah_akumulasi_hari_libur'];
        $this->jumlah_akumulasi_jam_lembur = $data['jumlah_akumulasi_jam_lembur'];
        $this->jumlah_akumulasi_telat = $data['jumlah_akumulasi_telat'];
        $this->jumlah_akumulasi_absen = $data['jumlah_akumulasi_absen'];
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('histori_absensi', $this, array('id'=>$this->id));
        return $this->db->affected_rows();
    }
}
?>
<?php
class spl_titipan_model extends CI_Model{
    public $id;
    public $spl_id;
    public $no_spl_titipan;
    public $marketing_penerima;
    public $site_titipan;
    public $site_awal;
    public $ket_kirim1;
    public $ket_kirim2;
    public $ket_kirim3;
    public $ket_kirim4;
    public $ket_kirim5;
    public $ket_retur;
    public $tanggal_spl_titipan;
    public $tanggal_spl_asli;
    public $status;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_spl_titipan($search=null, $tanggal_spl_titipan=null, $order=null, $limit=null){
        $this->db->select("spl_titipan.*,
            spl.no_spl as no_spl,
            customer.name as customer_name,
            customer.rating as customer_rating,
        ");
        $this->db->from("spl_titipan");
        $this->db->join('spl', 'spl.id = spl_titipan.spl_id', 'left');
        $this->db->join('customer', 'customer.id = spl.customer_id', 'left');

        if($search){
            $where_search = "(
                (CONCAT_WS(',', spl.no_spl, customer.name, customer.rating, no_spl_titipan, marketing_penerima, site_titipan, site_awal, tanggal_spl_titipan, status) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($tanggal_spl_titipan){
            $date_arr = explode("-",$tanggal_spl_titipan);
            $where_date = "( YEAR(tanggal_spl_titipan) = $date_arr[0] AND MONTH(tanggal_spl_titipan) = $date_arr[1])";
            $this->db->where($where_date);
        }
        $this->db->where("is_deleted", 0);
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_spl_titipan_by_id($id){
        $this->db->select("spl_titipan.*,
            spl.no_spl as no_spl,
            spl.tebal_bahan as tebal_bahan,
            spl.lebar_bahan as lebar_bahan,
            spl.ukuran_potong as ukuran_potong,
            spl.keterangan as keterangan,
            spl.komposisi as komposisi,
            spl.harga_satuan as harga_satuan,
            spl.jumlah_roll as jumlah_roll,
            spl.hasil_rumus as hasil_rumus,
            customer.name as customer_name,
            customer.rating as customer_rating,
        ");
        $this->db->from("spl_titipan");
        $this->db->join('spl', 'spl.id = spl_titipan.spl_id', 'left');
        $this->db->join('customer', 'customer.id = spl.customer_id', 'left');
        $this->db->where("spl_titipan.id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_spl_titipan_total($search=null, $tanggal_spl_titipan=null){
        $this->db->from('spl_titipan');
        if($search){
            $where_search = "(
                ((SELECT c.name FROM customer as c where c.id=customer_id ) LIKE '%".$search."%') OR 
                ((SELECT u.name FROM user as u where u.id=user_id ) LIKE '%".$search."%') OR
                (CONCAT_WS(',', no_spl_titipan, jumlah_order, hasil_rumus, approval_status, spl_titipan_status, tanggal_spl_titipan, lebar_bahan, tebal_bahan, komposisi, keterangan, ukuran_potong, jumlah_roll) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($search){
            $where_search = "(
                (CONCAT_WS(',', no_spl, customer_name, customer_rating, no_spl_titipan, marketing_penerima, site_titipan, site_awal, tanggal_spl_titipan, status) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($tanggal_spl_titipan){
            $date_arr = explode("-",$tanggal_spl_titipan);
            $where_date = "( YEAR(tanggal_spl_titipan) = $date_arr[0] AND MONTH(tanggal_spl_titipan) = $date_arr[1])";
            $this->db->where($where_date);
        }
        return $this->db->count_all_results();
    }

    function create_spl_titipan($data){
        $this->spl_id = $data['spl_id'];
        $this->no_spl_titipan = $data['no_spl_titipan'];
        $this->marketing_penerima = $data['marketing_penerima'];
        $this->site_titipan = $data['site_titipan'];
        $this->site_awal = $data['site_awal'];
        $this->tanggal_spl_titipan = $data['tanggal_spl_titipan'];
        $this->tanggal_spl_asli = $data['tanggal_spl_asli'];
        $this->status = $data['status'];
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('spl_titipan', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_spl_titipan($data){
        $this->id = $data['id'];
        $this->spl_id = $data['spl_id'];
        $this->no_spl_titipan = $data['no_spl_titipan'];
        $this->marketing_penerima = $data['marketing_penerima'];
        $this->site_titipan = $data['site_titipan'];
        $this->site_awal = $data['site_awal'];
        $this->ket_kirim1 = $data['ket_kirim1'];
        $this->ket_kirim2 = $data['ket_kirim2'];
        $this->ket_kirim3 = $data['ket_kirim3'];
        $this->ket_kirim4 = $data['ket_kirim4'];
        $this->ket_kirim5 = $data['ket_kirim5'];
        $this->ket_retur = $data['ket_retur'];
        $this->tanggal_spl_titipan = $data['tanggal_spl_titipan'];
        $this->tanggal_spl_asli = $data['tanggal_spl_asli'];
        $this->status = $data['status'];
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('spl_titipan', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

}
?>
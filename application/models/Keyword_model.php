<?php
class Keyword_model extends CI_Model{
    public $id;
    public $module;
    public $description;
    public $keyword_hash;
    public $status;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_keyword($search=null, $order=null, $limit=null){
        $this->db->select("*");
        $this->db->from("keyword");
        if($search){
            $where_search = "(
                (CONCAT_WS(',', module, description, status) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_keyword_by_id($id){
        $this->db->select("*");
        $this->db->from('keyword');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_keyword_by_module($module, $status=null){
        $this->db->select("*");
        $this->db->from('keyword');
        if($status){
            $this->db->where("status", $status);
        }
        $this->db->where("module", $module);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_keyword_total($search=null){
        $this->db->select("*");
        $this->db->from('keyword');
        if($search){
            $where_search = "(
                (CONCAT_WS(',', module, description, status) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        return $this->db->count_all_results();
    }

    function create_keyword($data){
        $this->module = $data['module'];
        $this->description = $data['description'];
        $this->keyword_hash = $data['keyword_hash'];
        $this->status = "ACTIVE";
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('keyword', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_keyword($data){
        $this->id = $data['id'];
        $this->module = $data['module'];
        $this->description = $data['description'];
        $this->keyword_hash = $data['keyword_hash'];
        $this->status = $data['status'];
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('keyword', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function active_keyword($id){
        $this->id = $id;
        $data = array(
            "status" => "ACTIVE",
            "datemodified" => date("Y-m-d H:i:s")
        );
  
        $this->db->update('keyword', $data, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function inactive_keyword($id){
        $this->id = $id;
        $data = array(
            "status" => "INACTIVE",
            "datemodified" => date("Y-m-d H:i:s")
        );
  
        $this->db->update('keyword', $data, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }
}
?>
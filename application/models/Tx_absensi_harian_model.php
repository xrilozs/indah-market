<?php
class Tx_absensi_harian_model extends CI_Model{
    public $id;
    public $pegawai_id;
    public $office_hour_id;
    public $shift;
    public $start_hour;
    public $end_hour;
    public $absensi_date;
    public $rec_id;
    public $absensi_status;
    public $actor;
    public $supervisor_id;
    public $is_sync;
    public $is_verified;
    public $is_overtime;
    public $is_weekend;
    public $is_holiday;
    public $is_late;
    public $late_hour;
    public $overtime_hour;
    public $reason;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_absensi_harian($search=null, $absensi_date=null, $site_code=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from("tx_absensi_harian");
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nama FROM pegawai as p where p.id=supervisor_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', shift, start_hour, end_hour, rec_id, absensi_status, actor, late_hour, overtime_hour) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($absensi_date){
            $this->db->where("absensi_date", $absensi_date);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_absensi_harian_range($start_date, $end_date, $search=null, $site_code=null, $order=null, $limit=null){
        $start_datetime = "$start_date 00:00:00";
        $end_datetime = "$end_date 23:59:59";

        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from("tx_absensi_harian");
        $this->db->where("absensi_date BETWEEN '$start_datetime' and '$end_datetime'");
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nama FROM pegawai as p where p.id=supervisor_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', shift, start_hour, end_hour, rec_id, absensi_status, actor, late_hour, overtime_hour) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_absensi_harian_range_recap($start_date, $end_date, $site_code){
        $start_datetime = "$start_date 00:00:00";
        $end_datetime = "$end_date 23:59:59";

        $this->db->select("pegawai_id, 
            count(pegawai_id) jumlah_hadir, 
            sum(is_overtime) overtime_total,
            sum(overtime_hour) overtime_hour_total,
            IFNULL(sum(is_late), 0) late_total,
            IFNULL(sum(late_hour), 0) late_hour_total,
            IFNULL(sum(is_weekend), 0) weekend_total,
            IFNULL(sum(is_holiday), 0) holiday_total
        ");
        $this->db->from("tx_absensi_harian");
        $this->db->where("absensi_date BETWEEN '$start_datetime' and '$end_datetime'");
        $this->db->where("((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')");
        $this->db->where("(is_sync IS NULL OR is_sync = 0)");
        $this->db->group_by("pegawai_id");
        $query = $this->db->get();
        return $query->result();
    }

    function get_absensi_harian_by_id($id){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from('tx_absensi_harian');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_absensi_harian_by_id_and_site_code($id, $site_code){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from('tx_absensi_harian');
        $this->db->where("id", $id);
        $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
        $this->db->where($where_site_code);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_absensi_harian_by_pegawai_id($pegawai_id, $absensi_date=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from('tx_absensi_harian');
        $this->db->where("pegawai_id", $pegawai_id);
        if($absensi_date){
            $this->db->where("absensi_date", $absensi_date);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_absensi_harian_by_pegawai_id_and_date($pegawai_id, $absensi_date, $absensi_status=null){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from('tx_absensi_harian');
        $this->db->where("pegawai_id", $pegawai_id);
        $this->db->where("absensi_date", $absensi_date);
		if($absensi_status){
			$this->db->where("absensi_status", $absensi_status);
		}
		$this->db->order_by("id", "DESC"); 
		$query = $this->db->get();
		return $query->result();
    }

    function get_absensi_harian_by_rec_id_and_date($rec_id, $absensi_date){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from('tx_absensi_harian');
        $this->db->where("rec_id", $rec_id);
        $this->db->where("absensi_date", $absensi_date);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_absensi_harian_by_rec_id_and_date_and_site_code($rec_id, $absensi_date, $site_code){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from('tx_absensi_harian');
        $this->db->where("rec_id", $rec_id);
        $this->db->where("absensi_date", $absensi_date);
        $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
        $this->db->where($where_site_code);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }


    function get_absensi_harian_by_pegawai_id_and_rec_id_and_site_code($pegawai_id, $rec_id, $site_code, $absensi_date=null){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from('tx_absensi_harian');
        $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
        $this->db->where($where_site_code);
        $this->db->where("pegawai_id", $pegawai_id);
        $this->db->where("rec_id", $rec_id);
        if(!is_null($absensi_date)){
            $this->db->where("absensi_date", $absensi_date);
        }
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_absensi_harian_by_date_and_office_hour($absensi_date, $office_hour_id){
        $this->db->select("*,
            (SELECT p.nama FROM pegawai as p where p.id=pegawai_id) as pegawai_name,
            (SELECT p.nama FROM pegawai as p where p.id=supervisor_id) as supervisor_name
        ");
        $this->db->from("tx_absensi_harian");
        $this->db->where("absensi_date", $absensi_date);
        $this->db->where("office_hour_id", $office_hour_id);
        $query = $this->db->get();
        return $query->result();
    }

    function get_absensi_harian_total($search=null, $absensi_date=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('tx_absensi_harian');
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nama FROM pegawai as p where p.id=supervisor_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', shift, start_hour, end_hour, rec_id, absensi_status, actor, late_hour, overtime_hour) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($absensi_date){
            $this->db->where("absensi_date", $absensi_date);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        return $this->db->count_all_results();
    }

    function count_absensi_harian_by_site_code_and_date($site_code, $absensi_date){
        $this->db->from('tx_absensi_harian');
        $this->db->where("absensi_date", $absensi_date);
        $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
        $this->db->where($where_site_code);
        return $this->db->count_all_results();
    }

    function count_absensi_harian_by_pegawai_id($pegawai_id, $absensi_date=null){
        $this->db->from('tx_absensi_harian');
        $this->db->where("pegawai_id", $pegawai_id);
        if($absensi_date){
            $this->db->where("absensi_date", $absensi_date);
        }
        return $this->db->count_all_results();
    }

    function count_absensi_harian_range($start_date, $end_date, $search=null, $site_code=null){
        $start_datetime = "$start_date 00:00:00";
        $end_datetime = "$end_date 23:59:59";

        $this->db->from("tx_absensi_harian");
        $this->db->where("absensi_date BETWEEN '$start_datetime' and '$end_datetime'");
        if($search){
            $where_search = "(
                ((SELECT p.nama FROM pegawai as p where p.id=pegawai_id) LIKE '%".$search."%') OR 
                ((SELECT p.nama FROM pegawai as p where p.id=supervisor_id) LIKE '%".$search."%') OR 
                (CONCAT_WS(',', shift, start_hour, end_hour, rec_id, absensi_status, actor, late_hour, overtime_hour) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
            $this->db->where($where_site_code);
        }
        return $this->db->count_all_results();
    }

    function get_last_absensi_harian_by_site_code($site_code){
        $this->db->from('tx_absensi_harian');
        $where_site_code = "((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')";
        $this->db->where($where_site_code);
        $this->db->order_by("datecreated", "DESC");
        $this->db->limit(1,0);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_absensi_harian($data, $is_spv=0){
        $this->pegawai_id = $data['pegawai_id'];
        $this->office_hour_id = $data['office_hour_id'];
        $this->shift = $data['shift'];
        $this->start_hour = $data['start_hour'];
        $this->absensi_date = $data['absensi_date'];
        $this->rec_id = $data['rec_id'];
        $this->is_sync = 0;
        $this->is_overtime = 0;
        $this->is_weekend = $data['is_weekend'];
        $this->is_holiday = $data['is_holiday'];
        $this->is_verified = $is_spv ? 1 : 0;
        $this->is_late = array_key_exists('is_late', $data) ? $data['is_late'] : 0;
        $this->absensi_status = 'START WORKING';
        $this->actor = $data['actor'];
        if(array_key_exists('late_hour', $data)){
            $this->late_hour = $data['late_hour'];
        }
        $this->supervisor_id = array_key_exists("supervisor_id", $data) ? $data['supervisor_id'] : null;
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('tx_absensi_harian', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_absensi_harian($data){
        $this->id = $data['id'];
        $this->pegawai_id = $data['pegawai_id'];
        $this->office_hour_id = $data['office_hour_id'];
        $this->shift = $data['shift'];
        $this->start_hour = $data['start_hour'];
        $this->end_hour = $data['end_hour'];
        $this->absensi_date = $data['absensi_date'];
        $this->rec_id = $data['rec_id'];
        $this->absensi_status = $data['absensi_status'];
        $this->actor = $data['actor'];
        $this->supervisor_id = $data['supervisor_id'];
        $this->is_sync = $data['is_sync'];
        $this->is_overtime = $data['is_overtime'];
        $this->is_verified = $data['is_verified'];
        $this->is_weekend = $data['is_weekend'];
        $this->is_holiday = $data['is_holiday'];
        $this->is_late = $data['is_late'];
        $this->late_hour = $data['late_hour'];
        $this->overtime_hour = $data['overtime_hour'];
        $this->reason = array_key_exists('reason', $data) ? $data['reason'] : null;
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('tx_absensi_harian', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function update_absensi_harianV2($data){
        $this->db->update('tx_absensi_harian', $data, array('id'=>$data['id']));
        return $this->db->affected_rows() > 0;
    }

    function verify_absensi_harian($id, $supervisor_id){
        $data = array(
            'supervisor_id' => $supervisor_id,
            'is_verified' => 1, 
            'datemodified' => date("Y-m-d H:i:s")
        );
        $this->db->update('tx_absensi_harian', $data, array('id'=>$id));
        return $this->db->affected_rows() > 0;
    }

    function sync_absensi_harian_range($start_date, $end_date, $site_code){
        $start_datetime = "$start_date 00:00:00";
        $end_datetime = "$end_date 23:59:59";

        $this->db->set('is_sync', 1);
        $this->db->set('datemodified', date("Y-m-d H:i:s"));
        $this->db->where("absensi_date BETWEEN '$start_datetime' and '$end_datetime'");
        $this->db->where("((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')");
        $this->db->where("(is_sync IS NULL OR is_sync = 0)");
        $this->db->update('tx_absensi_harian');
        return $this->db->affected_rows() > 0;
    }

    function delete_absensi_harian_range($start_date, $end_date, $site_code){
        $start_datetime = "$start_date 00:00:00";
        $end_datetime = "$end_date 23:59:59";
        $this->db->where("absensi_date BETWEEN '$start_datetime' and '$end_datetime'");
        $this->db->where("((SELECT p.nip FROM pegawai as p where p.id=pegawai_id) LIKE '$site_code%')");
        // $this->db->where('is_sync', 1);
        $this->db->delete('tx_absensi_harian');
        return $this->db->affected_rows();
    }
}
?>

<?php
class Office_hour_model extends CI_Model{
    public $id;
    public $site_code;
    public $shift;
    public $start_hour;
    public $end_hour;
    public $start_hour_saturday;
    public $end_hour_saturday;
    public $is_active;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_office_hour($search=null, $site_code=null, $order=null, $limit=null){
        $this->db->select("*");
        $this->db->from("office_hour");
        if($search){
            $where_search = "(
                (CONCAT_WS(',', site_code, shift, start_hour, end_hour, start_hour_saturday, end_hour_saturday) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $this->db->where("site_code", $site_code);
        }
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_office_hour_by_id($id, $site_code=null, $is_active=null){
        $this->db->from('office_hour');
        $this->db->where("id", $id);
        if(!is_null($site_code)){
            $this->db->where("site_code", $site_code);
        }
        if(!is_null($is_active)){
            $this->db->where("is_active", $is_active);
        }
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_office_hour_total($search=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('office_hour');
        if($search){
            $where_search = "(
                (CONCAT_WS(',', site_code, shift, start_hour, end_hour, start_hour_saturday, end_hour_saturday) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $this->db->where("site_code", $site_code);
        }
        return $this->db->count_all_results();
    }

    function create_office_hour($data){
        $this->site_code = $data['site_code'];
        $this->shift = $data['shift'];
        $this->start_hour = $data['start_hour'];
        $this->end_hour = $data['end_hour'];
        $this->start_hour_saturday = $data['start_hour_saturday'];
        $this->end_hour_saturday = $data['end_hour_saturday'];
        $this->is_active = 1;
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('office_hour', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_office_hour($data){
        $this->id = $data['id'];
        $this->site_code = $data['site_code'];
        $this->shift = $data['shift'];
        $this->start_hour = $data['start_hour'];
        $this->end_hour = $data['end_hour'];
        $this->start_hour_saturday = $data['start_hour_saturday'];
        $this->end_hour_saturday = $data['end_hour_saturday'];
        $this->is_active = $data['is_active'];
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('office_hour', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function disable_office_hour($id){
        $data = array('is_active'=>0, 'datemodified' => date("Y-m-d H:i:s"));
        $this->db->update('office_hour', $data, array('id'=>$id));
        return $this->db->affected_rows();
    }

    function enable_office_hour($id){
        $data = array('is_active'=>1, 'datemodified' => date("Y-m-d H:i:s"));
        $this->db->update('office_hour', $data, array('id'=>$id));
        return $this->db->affected_rows();
    }
}
?>
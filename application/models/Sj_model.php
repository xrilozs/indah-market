<?php
class sj_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $sj                       = 'sj';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_sj($data){
        $this->db->insert($this->sj,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_sj($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->sj);
        $query=$this->db->get();
        return $query;
    }
    function update_sj($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->sj,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function update_sj_by_nosj($data){
        $this->db->where('sj_no',$data['sj_no']);
        $this->db->update($this->sj,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_sj($id){
        $this->db->where('id',$id);
        $this->db->delete($this->sj);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>

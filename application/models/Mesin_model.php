<?php
class mesin_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $mesin                    = 'mesin';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_mesin($data){
        $this->db->insert($this->mesin,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_mesin($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->mesin);
        $query=$this->db->get();
        return $query;
    }
    function update_mesin($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->mesin,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_mesin($id){
        $this->db->where('id',$id);
        $this->db->delete($this->mesin);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>

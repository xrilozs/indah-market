<?php
class customer_model extends CI_Model{

 var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_customer($data){
        $this->db->insert($this->customer,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_customer($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->customer);
        $query=$this->db->get();
        return $query;
    }
    function update_customer($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->customer,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_customer($id){
        $this->db->where('id',$id);
        $this->db->delete($this->customer);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>

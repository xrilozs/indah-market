<?php
class Phone_number_model extends CI_Model{
  public $id;
  public $phone_number;
  public $datecreated;
  public $datemodified;

  function get_phone_number_like($like){
    $this->db->like("phone_number", $like);
    $query = $this->db->get("phone_number");
    return $query->result();
  }

  function get_phone_number_by_phone_number($phone_number){
    $this->db->where("phone_number", $phone_number);
    $query = $this->db->get("phone_number");
    return $query->num_rows() > 0 ? $query->row() : null;
  }

  function create_phone_number($phone_number){
    $this->phone_number = $phone_number;
    $this->datecreated = date("Y-m-d H:i:s");
    $this->datemodified = date("Y-m-d H:i:s");

    $this->db->insert('phone_number', $this);
    return $this->db->affected_rows() > 0;
  }
}
?>

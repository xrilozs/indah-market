<?php
class user_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_user($data){
        $this->db->insert($this->user,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_user($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->user);
        $query=$this->db->get();
        return $query;
    }
    function get_user_by_id($id){
      $this->db->select("*");
      $this->db->from($this->user);
      $this->db->where("id", $id);
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }
    function update_user($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->user,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_user($id){
        $this->db->where('id',$id);
        $this->db->delete($this->user);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function login($username, $password){
      $this->db->where("username", $username);
      $this->db->where("password", $password);
      $query = $this->db->get($this->user);
      return $query->num_rows() > 0 ? $query->row() : null;
    }
    function logout($username){
      $data = array(
        "token" => null,
        "refresh_token" => null
      );
      $this->db->where("username", $username);
      $this->db->update($this->user, $data);
      $flag=$this->db->affected_rows();
      return $flag;
    }
    function get_user_by_username($username){
      $this->db->where("username", $username);
      $query = $this->db->get($this->user);
      return $query->num_rows() > 0 ? $query->row() : null;
    }
}
?>

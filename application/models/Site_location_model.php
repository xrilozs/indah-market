<?php
class Site_location_model extends CI_Model{
    public $id;
    public $lat;
    public $long;
    public $site_code;
    public $address;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_site_location_by_site_code($site_code){
        $this->db->select("*");
        $this->db->from("site_location");
        $this->db->where("site_code", $site_code);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_site_location_by_id($id){
        $this->db->from('site_location');
        $this->db->where("id", $id);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function create_site_location($data){
        $this->lat = $data['lat'];
        $this->long = $data['long'];
        $this->site_code = $data['site_code'];
        if(array_key_exists("address", $data)) $this->address = $data['address'];
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('site_location', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_site_location($data){
        $this->id = $data['id'];
        $this->lat = $data['lat'];
        $this->long = $data['long'];
        $this->site_code = $data['site_code'];
        if(array_key_exists("address", $data)) $this->address = $data['address'];
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('site_location', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function delete_site_location($id){
        $this->db->where('id', $id);
        $this->db->delete('site_location');
        return $this->db->affected_rows();
    }
}
?>
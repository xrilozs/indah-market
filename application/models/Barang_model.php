<?php
class barang_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_barang($data){
        $this->db->insert($this->barang,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_barang($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->barang);
        $query=$this->db->get();
        return $query;
    }
    function update_barang($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->barang,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_barang($id){
        $this->db->where('id',$id);
        $this->db->delete($this->barang);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>

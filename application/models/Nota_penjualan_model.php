<?php
class nota_penjualan_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_nota_penjualan($data){
        $this->db->insert($this->nota_penjualan,$data);
        $id=$this->db->insert_id();
        return $id;
    }
    function read_nota_penjualan($where=""){
        $sql = "SELECT A.*, B.name customer_name FROM ".$this->nota_penjualan." AS A 
        JOIN ".$this->customer." AS B ON B.id = A.customer_id";
        if($where!="") $sql.=" WHERE ".$where;
        $query=$this->db->query($sql);
        return $query;
    }
    function update_nota_penjualan($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->nota_penjualan,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_nota_penjualan($id){
        $this->db->where('id',$id);
        $this->db->delete($this->nota_penjualan);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function read_nota_penjualan_det($where=""){
      $sql = "SELECT A.*, B.name, B.phone, B.alamat FROM ".$this->nota_penjualan." AS A
      JOIN ".$this->member." AS B ON B.id = A.member_id
      ";
      return $this->db->query($sql);
    }
}
?>

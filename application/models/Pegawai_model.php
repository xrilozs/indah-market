<?php
class Pegawai_model extends CI_Model{
    public $id;
    public $nip;
    public $nama;
    public $alamat;
    public $telepon;
    public $jabatan;
    public $tanggal_masuk;
    public $jumlah_kasbon;
    public $upah_harian;
    public $upah_bulanan;
    public $upah_lembur;
    public $upah_libur;
    public $is_deleted;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_pegawai($search=null, $site_code=null, $order=null, $limit=null, $query=null){
        $this->db->select("*,
            FORMAT(jumlah_kasbon,'id_ID') as jumlah_kasbon_formatted,
            FORMAT(upah_harian,'id_ID') as upah_harian_formatted,
            FORMAT(upah_bulanan,'id_ID') as upah_bulanan_formatted,
            FORMAT(upah_lembur,'id_ID') as upah_lembur_formatted,
            FORMAT(upah_libur,'id_ID') as upah_libur_formatted
        ");
        $this->db->from("pegawai");
        if($search){
            $where_search = "(
                (CONCAT_WS(',', nip, nama, alamat, jabatan, tanggal_masuk, jumlah_kasbon, upah_harian, upah_bulanan, upah_lembur, upah_libur) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $this->db->like("nip", $site_code, 'after');
        }
        if($query){
            $where_query = "(
                (CONCAT_WS(',', nip, nama) LIKE '%".$query."%')
            )";
            $this->db->where($where_query);
        }
        $this->db->where("is_deleted", 0);
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_pegawai_by_id($id){
        $this->db->select("*");
        $this->db->from('pegawai');
        $this->db->where("id", $id);
        $this->db->where("is_deleted", 0);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_pegawai_by_name_and_site_code($nama, $site_code, $order=null, $limit=null){
        $this->db->select("*,
            FORMAT(jumlah_kasbon,'id_ID') as jumlah_kasbon_formatted,
            FORMAT(upah_harian,'id_ID') as upah_harian_formatted,
            FORMAT(upah_bulanan,'id_ID') as upah_bulanan_formatted,
            FORMAT(upah_lembur,'id_ID') as upah_lembur_formatted,
            FORMAT(upah_libur,'id_ID') as upah_libur_formatted
        ");
        $this->db->from("pegawai");
        $this->db->like("nama", $nama);
        $this->db->like("nip", $site_code, 'after');
        $this->db->where("is_deleted", 0);
        $this->db->order_by($order['field'], $order['order']); 
        $this->db->limit($limit['size'], $limit['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function get_pegawai_by_phone_number($phone_number, $role=null){
        $this->db->select("*");
        $this->db->from('pegawai');
        $this->db->where("telepon", $phone_number);
        $this->db->where("is_deleted", 0);
        if($role){
            $this->db->where("role", $role);
        }
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_pegawai_by_nip($nip){
        $this->db->select("*");
        $this->db->from('pegawai');
        $this->db->where("nip", $nip);
        $this->db->where("is_deleted", 0);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_pegawai_by_phone_number_and_password($phone_number, $password, $role=null){
        $this->db->select("*");
        $this->db->from('pegawai');
        $this->db->where("telepon", $phone_number);
        $this->db->where("password", $password);
        $this->db->where("is_deleted", 0);
        if($role){
            $this->db->where("role", $role);
        }
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_pegawai_total($search=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('pegawai');
        if($search){
            $where_search = "(
                (CONCAT_WS(',', nip, nama, alamat, jabatan, tanggal_masuk, jumlah_kasbon, upah_harian, upah_bulanan, upah_lembur, upah_libur) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($site_code){
            $this->db->like("nip", $site_code, 'after');
        }
        return $this->db->count_all_results();
    }

    function get_pegawai_by_role_and_site($role, $site_code){
        $this->db->select("*,
            FORMAT(jumlah_kasbon,'id_ID') as jumlah_kasbon_formatted,
            FORMAT(upah_harian,'id_ID') as upah_harian_formatted,
            FORMAT(upah_bulanan,'id_ID') as upah_bulanan_formatted,
            FORMAT(upah_lembur,'id_ID') as upah_lembur_formatted,
            FORMAT(upah_libur,'id_ID') as upah_libur_formatted
        ");
        $this->db->from("pegawai");
        $this->db->where("role", $role);
        $this->db->like("nip", $site_code, 'after');
        $query = $this->db->get();
        return $query->result();
    }

    function count_pegawai_by_name_and_site_code($nama, $site_code){
        $this->db->select("*");
        $this->db->from('pegawai');
        $this->db->like("nama", $nama);
        $this->db->like("nip", $site_code, 'after');
        $this->db->where("is_deleted", 0);
        return $this->db->count_all_results();
    }

    function create_pegawai($data){
        $this->nip = $data['nip'];
        $this->nama = $data['nama'];
        $this->alamat = $data['alamat'];
        $this->telepon = $data['telepon'];
        $this->jabatan = $data['jabatan'];
        $this->role = $data['role'];
        $this->tanggal_masuk = $data['tanggal_masuk'];
        $this->jumlah_kasbon = $data['jumlah_kasbon'];
        $this->upah_harian = $data['upah_harian'];
        $this->upah_bulanan = $data['upah_bulanan'];
        $this->upah_lembur = $data['upah_lembur'];
        $this->upah_libur = $data['upah_libur'];
        $this->password = array_key_exists("password", $data) ? $data['password'] : null;
        $this->is_deleted = 0;
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('pegawai', $this);
        return $this->db->affected_rows() > 0;
    }

    function update_pegawai($data){
        $this->id = $data['id'];
        $this->nip = $data['nip'];
        $this->nama = $data['nama'];
        $this->alamat = $data['alamat'];
        $this->telepon = $data['telepon'];
        $this->jabatan = $data['jabatan'];
        $this->role = $data['role'];
        $this->tanggal_masuk = $data['tanggal_masuk'];
        $this->jumlah_kasbon = $data['jumlah_kasbon'];
        $this->upah_harian = $data['upah_harian'];
        $this->upah_bulanan = $data['upah_bulanan'];
        $this->upah_lembur = $data['upah_lembur'];
        $this->upah_libur = $data['upah_libur'];
        $this->password = $data['password'];
        $this->is_deleted = 0;
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('pegawai', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function update_pegawai_custom($data){  
        $this->db->update('pegawai', $data, array('id'=>$data['id']));
        return $this->db->affected_rows() > 0;
    }

    function delete_pegawai($id){
        $data = array('is_deleted'=>1, 'datemodified' => date("Y-m-d H:i:s"));
        $this->db->update('pegawai', $data, array('id'=>$id));
        return $this->db->affected_rows();
    }
}
?>
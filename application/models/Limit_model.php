<?php
class Limit_model extends CI_Model{
    public $id;
    public $nilai;
    public $site;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_limit_by_id($id){
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from('limit');
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_limit_by_site($site){
        $this->db->select("*");
        $this->db->where("site", $site);
        $this->db->from('limit');
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function create_limit($data){
        $data['datecreated'] = date("Y-m-d H:i:s");
        $this->db->insert('limit', $data);
        return $this->db->affected_rows() > 0;
    }

    function update_limit($data){
        $data['datemodified'] = date("Y-m-d H:i:s");
        $this->db->update('limit', $data, array('id'=>$data['id']));
        return $this->db->affected_rows() > 0;
    }
}

?>
<?php
class Spl_model extends CI_Model{
    public $id;
    public $no_spl;
    public $tanggal_spl;
    public $customer_id;
    public $user_id;
    public $lebar_bahan;
    public $tebal_bahan;
    public $ukuran_potong;
    public $satuan;
    public $keterangan;
    public $jumlah_order;
    public $harga_satuan;
    public $komposisi;
    public $jumlah_roll;
    public $hasil_rumus;
    public $tanggal_kirim1;
    public $jumlah_kirim1;
    public $tanggal_kirim2;
    public $jumlah_kirim2;
    public $tanggal_kirim3;
    public $jumlah_kirim3;
    public $sjkirim1;
    public $sjkirim2;
    public $sjkirim3;
    public $approval_status;
    public $spl_status;
    public $is_deleted;
    public $datecreated;
    public $datemodified;

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    function get_spl($search=null, $spl_status=null, $spl_date=null, $approval_status=null, $site_code=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT u.name FROM user as u where u.id=user_id) as marketing_name,
            (SELECT c.name FROM customer as c where c.id=customer_id) as customer_name,
            (SELECT c.rating FROM customer as c where c.id=customer_id) as customer_rating,
        ");
        $this->db->from("spl");
        if($search){
            $where_search = "(
                ((SELECT c.name FROM customer as c where c.id=customer_id ) LIKE '%".$search."%') OR 
                ((SELECT u.name FROM user as u where u.id=user_id ) LIKE '%".$search."%') OR
                (CONCAT_WS(',', no_spl, jumlah_order, hasil_rumus, approval_status, spl_status, tanggal_spl, lebar_bahan, tebal_bahan, komposisi, keterangan, ukuran_potong, jumlah_roll, harga_satuan) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($spl_status){
            $this->db->where("spl_status", $spl_status);
        }
        if($spl_date){
            $this->db->where("tanggal_spl", $spl_date);
        }
        if($approval_status){
            $this->db->where("approval_status", $approval_status);
        }
        if($site_code){
            $this->db->like("no_spl", $site_code, 'after');
        }
        $this->db->where("is_deleted", 0);
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_spl_monthly($search=null, $spl_status=null, $spl_date=null, $site_code=null, $order=null, $limit=null){
        $this->db->select("*,
            (SELECT u.name FROM user as u where u.id=user_id) as marketing_name,
            (SELECT c.name FROM customer as c where c.id=customer_id) as customer_name,
            (SELECT c.rating FROM customer as c where c.id=customer_id) as customer_rating,
        ");
        $this->db->from("spl");
        if($search){
            $where_search = "(
                ((SELECT c.name FROM customer as c where c.id=customer_id ) LIKE '%".$search."%') OR 
                ((SELECT u.name FROM user as u where u.id=user_id ) LIKE '%".$search."%') OR
                (CONCAT_WS(',', no_spl, jumlah_order, hasil_rumus, approval_status, spl_status, tanggal_spl, lebar_bahan, tebal_bahan, komposisi, keterangan, ukuran_potong, jumlah_roll) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($spl_status){
            $this->db->where("spl_status", $spl_status);
        }
        if($spl_date){
            $date_arr = explode("-",$spl_date);
            $where_date = '(YEAR(tanggal_spl) = '.$date_arr[0].' AND MONTH(tanggal_spl) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $this->db->like("no_spl", $site_code, 'after');
        }
        $this->db->where("is_deleted", 0);
        if($order){
            $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
            $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_burn_spl_by_month($spl_date=null, $site_code=null){
        $this->db->select("*,
            (SELECT u.name FROM user as u where u.id=user_id) as marketing_name,
            (SELECT c.name FROM customer as c where c.id=customer_id) as customer_name
        ");
        $this->db->from("spl");
        if($spl_date){
            $date_arr = explode("-",$spl_date);
            $where_date = '(YEAR(tanggal_spl) = '.$date_arr[0].' AND MONTH(tanggal_spl) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $this->db->like("no_spl", $site_code, 'after');
        }
        $this->db->where("is_deleted", 0);
        $this->db->where_not_in("spl_status", array("DITITIPKAN", "HANGUS"));
        $this->db->where("abs(datediff(NOW(), tanggal_spl)) >= 60");
        $query = $this->db->get();
        return $query->result();
    }

    function burn_spl_by_month($spl_date=null, $site_code=null){
        $this->db->set('spl_status', 'HANGUS');
        if($spl_date){
            $date_arr = explode("-",$spl_date);
            $where_date = '(YEAR(tanggal_spl) = '.$date_arr[0].' AND MONTH(tanggal_spl) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $this->db->like("no_spl", $site_code, 'after');
        }
        $this->db->where("is_deleted", 0);
        $this->db->where_not_in("spl_status", array("DITITIPKAN", "HANGUS"));
        $this->db->where("abs(datediff(NOW(), tanggal_spl)) >= 60");
        $this->db->update('spl');
        return $this->db->affected_rows() > 0;
    }

    function burn_spl_by_id($id){
        $this->db->set('spl_status', 'HANGUS');
        $this->db->where("id", $id);
        $this->db->where("is_deleted", 0);
        $this->db->where_not_in("spl_status", array("DITITIPKAN", "HANGUS"));
        $this->db->update('spl');
        return $this->db->affected_rows() > 0;
    }

    function get_spl_by_id($id){
        $this->db->select("*,
            (SELECT u.name FROM user as u where u.id=user_id) as marketing_name,
            (SELECT c.name FROM customer as c where c.id=customer_id) as customer_name,
            (SELECT c.rating FROM customer as c where c.id=customer_id) as customer_rating,
        ");
        $this->db->from('spl');
        $this->db->where("id", $id);
        $this->db->where("is_deleted", 0);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_spl_by_spl_no($spl_no){
        $this->db->select("*,
            (SELECT u.name FROM user as u where u.id=user_id) as marketing_name,
            (SELECT c.name FROM customer as c where c.id=customer_id) as customer_name,
            (SELECT c.rating FROM customer as c where c.id=customer_id) as customer_rating,
        ");
        $this->db->from('spl');
        $this->db->where("no_spl", $spl_no);
        $this->db->where("is_deleted", 0);
        $query = $this->db->get();
        return $query->num_rows() ? $query->row() : null;
    }

    function get_spl_by_like_spl_no($spl_no=null, $site_code=null, $approval_status=null){
        $this->db->select("*,
            (SELECT u.name FROM user as u where u.id=user_id) as marketing_name,
            (SELECT c.name FROM customer as c where c.id=customer_id) as customer_name,
            (SELECT c.rating FROM customer as c where c.id=customer_id) as customer_rating,
        ");
        $this->db->from('spl');
        if($spl_no) $this->db->like("no_spl", $spl_no);
        if($site_code) $this->db->like("no_spl", $site_code, 'after');
        if($approval_status) $this->db->where("approval_status", $approval_status);
        $this->db->where("is_deleted", 0);
        $this->db->where("spl_status", "BELUM");
        $this->db->limit(50, 0);
		$this->db->order_by("no_spl", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function get_spl_by_user_id_and_date($user_id, $spl_date){
        $this->db->select("*,
            (SELECT u.name FROM user as u where u.id=user_id) as marketing_name,
            (SELECT c.name FROM customer as c where c.id=customer_id) as customer_name,
            (SELECT c.rating FROM customer as c where c.id=customer_id) as customer_rating,
        ");
        $this->db->from('spl');
        $this->db->where("user_id", $user_id);
        $date_arr = explode("-",$spl_date);
        $where_date = "(YEAR(tanggal_spl) = $date_arr[0] AND MONTH(tanggal_spl) = $date_arr[1])";
        $this->db->where($where_date);
        $this->db->where("is_deleted", 0);
        $query = $this->db->get();
        return $query->result();
    }

    function get_spl_total($search=null, $spl_status=null, $spl_date=null, $approval_status=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('spl');
        if($search){
            $where_search = "(
                ((SELECT c.name FROM customer as c where c.id=customer_id ) LIKE '%".$search."%') OR 
                ((SELECT u.name FROM user as u where u.id=user_id ) LIKE '%".$search."%') OR
                (CONCAT_WS(',', no_spl, jumlah_order, hasil_rumus, approval_status, spl_status, tanggal_spl, lebar_bahan, tebal_bahan, komposisi, keterangan, ukuran_potong, jumlah_roll, harga_satuan) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($spl_status){
            $this->db->where("spl_status", $spl_status);
        }
        if($spl_date){
            $this->db->where("tanggal_spl", $spl_date);
        }
        if($approval_status){
            $this->db->where("approval_status", $approval_status);
        }
        if($site_code){
            $this->db->like("no_spl", $site_code, 'after');
        }
        return $this->db->count_all_results();
    }

    function get_spl_monthly_total($search=null, $spl_status=null, $spl_date=null, $site_code=null){
        $this->db->select("*");
        $this->db->from('spl');
        if($search){
            $where_search = "(
                ((SELECT c.name FROM customer as c where c.id=customer_id ) LIKE '%".$search."%') OR 
                ((SELECT u.name FROM user as u where u.id=user_id ) LIKE '%".$search."%') OR
                (CONCAT_WS(',', no_spl, jumlah_order, hasil_rumus, approval_status, spl_status, tanggal_spl, lebar_bahan, tebal_bahan, komposisi, keterangan, ukuran_potong, jumlah_roll) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($spl_status){
            $this->db->where("spl_status", $spl_status);
        }
        if($spl_date){
            $date_arr = explode("-",$spl_date);
            $where_date = '(YEAR(tanggal_spl) = '.$date_arr[0].' AND MONTH(tanggal_spl) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $this->db->like("no_spl", $site_code, 'after');
        }
        return $this->db->count_all_results();
    }

    function create_spl($data){
        $this->no_spl = $data['no_spl'];
        $this->tanggal_spl = $data['tanggal_spl'];
        $this->customer_id = $data['customer_id'];
        $this->user_id = $data['user_id'];
        $this->lebar_bahan = $data['lebar_bahan'];
        $this->tebal_bahan = $data['tebal_bahan'];
        $this->ukuran_potong = $data['ukuran_potong'];
        $this->satuan = $data['satuan'];
        $this->keterangan = $data['keterangan'];
        $this->jumlah_order = $data['jumlah_order'];
        $this->harga_satuan = $data['harga_satuan'];
        $this->komposisi = $data['komposisi'];
        $this->jumlah_roll = $data['jumlah_roll'];
        $this->hasil_rumus = $data['hasil_rumus'];
        $this->approval_status = $data['approval_status'];
        $this->spl_status = $data['spl_status'];
        $this->is_deleted = 0;
        $this->datecreated = date("Y-m-d H:i:s");
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->insert('spl', $this);
        return $this->db->affected_rows() > 0;
    }
    function update_spl($data){
        $this->id = $data['id'];
        $this->no_spl = $data['no_spl'];
        $this->tanggal_spl = $data['tanggal_spl'];
        $this->customer_id = $data['customer_id'];
        $this->user_id = $data['user_id'];
        $this->lebar_bahan = $data['lebar_bahan'];
        $this->tebal_bahan = $data['tebal_bahan'];
        $this->ukuran_potong = $data['ukuran_potong'];
        $this->satuan = $data['satuan'];
        $this->keterangan = $data['keterangan'];
        $this->jumlah_order = $data['jumlah_order'];
        $this->harga_satuan = $data['harga_satuan'];
        $this->komposisi = $data['komposisi'];
        $this->jumlah_roll = $data['jumlah_roll'];
        $this->hasil_rumus = $data['hasil_rumus'];
        if(array_key_exists('tanggal_kirim1', $data)) $this->tanggal_kirim1 = $data['tanggal_kirim1'];
        if(array_key_exists('jumlah_kirim1', $data)) $this->jumlah_kirim1 = $data['jumlah_kirim1'];
        if(array_key_exists('tanggal_kirim2', $data)) $this->tanggal_kirim2 = $data['tanggal_kirim2'];
        if(array_key_exists('jumlah_kirim2', $data)) $this->jumlah_kirim2 = $data['jumlah_kirim2'];
        if(array_key_exists('tanggal_kirim3', $data)) $this->tanggal_kirim3 = $data['tanggal_kirim3'];
        if(array_key_exists('jumlah_kirim3', $data)) $this->jumlah_kirim3 = $data['jumlah_kirim3'];
        if(array_key_exists('sjkirim1', $data)) $this->sjkirim1 = $data['sjkirim1'];
        if(array_key_exists('sjkirim2', $data)) $this->sjkirim2 = $data['sjkirim2'];
        if(array_key_exists('sjkirim3', $data)) $this->sjkirim3 = $data['sjkirim3'];
        $this->approval_status = $data['approval_status'];
        $this->spl_status = $data['spl_status'];
        $this->is_deleted = 0;
        $this->datecreated = $data['datecreated'];
        $this->datemodified = date("Y-m-d H:i:s");
  
        $this->db->update('spl', $this, array('id'=>$this->id));
        return $this->db->affected_rows() > 0;
    }

    function approve_spl($id){
        $data = array('approval_status'=>'APPROVED', 'datemodified' => date("Y-m-d H:i:s"));
        $this->db->update('spl', $data, array('id'=>$id));
        return $this->db->affected_rows() > 0;
    }

    function approve_batch_spl($ids){
        $data = array();
        foreach ($ids as $id) {
            $data[] = array(
               'id' => $id,
               'approval_status' => "APPROVED",
               'datemodified' => date("Y-m-d H:i:s")
            );
         }    
        $this->db->update_batch('spl', $data, 'id');
        return $this->db->affected_rows() > 0;
    }

    function noapprove_spl($id){
        $data = array('approval_status'=>'TIDAK_APPROVED', 'datemodified' => date("Y-m-d H:i:s"));
        $this->db->update('spl', $data, array('id'=>$id));
        return $this->db->affected_rows() > 0;
    }

    function done_spl($id){
        $data = array('spl_status'=>'SELESAI', 'datemodified' => date("Y-m-d H:i:s"));
        $this->db->update('spl', $data, array('id'=>$id));
        return $this->db->affected_rows() > 0;
    }

    function dititipkan_spl($id){
        $data = array('spl_status'=>'DITITIPKAN', 'datemodified' => date("Y-m-d H:i:s"));
        $this->db->update('spl', $data, array('id'=>$id));
        return $this->db->affected_rows() > 0;
    }

    function delete_spl($id){
        $this->db->where('id', $id);
        $this->db->delete('spl');
        return $this->db->affected_rows();
    }

    function delete_spl_batch($search=null, $spl_status=null, $spl_date=null, $site_code=null){
        if($search){
            $where_search = "(
                ((SELECT c.name FROM customer as c where c.id=customer_id ) LIKE '%".$search."%') OR 
                ((SELECT u.name FROM user as u where u.id=user_id ) LIKE '%".$search."%') OR
                (CONCAT_WS(',', no_spl, jumlah_order, hasil_rumus, approval_status, spl_status, tanggal_spl, lebar_bahan, tebal_bahan, komposisi, keterangan, ukuran_potong, jumlah_roll) LIKE '%".$search."%')
            )";
            $this->db->where($where_search);
        }
        if($spl_status){
            $this->db->where("spl_status", $spl_status);
        }
        if($spl_date){
            $date_arr = explode("-",$spl_date);
            $where_date = '(YEAR(tanggal_spl) = '.$date_arr[0].' AND MONTH(tanggal_spl) = '.$date_arr[1].')';
            $this->db->where($where_date);
        }
        if($site_code){
            $this->db->like("no_spl", $site_code, 'after');
        }
        $this->db->delete('spl');
        return $this->db->affected_rows();
    }
}
?>

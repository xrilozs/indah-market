<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResponseApi {
  private $httpCode;
  private $status;
  private $message;
  private $data = null;
  private $draw;
  private $recordsTotal;
  private $recordsFiltered;

  public function setResponse($httpCode, $status, $message, $data=null){
    $this->httpCode = $httpCode;
    $this->status = $status;
    $this->message = $message;
    $this->data = $data;
  }
  
  public function getResponse(){
    $response = array(
      "code" => $this->httpCode,
      "status" => $this->status,
      "message" => $this->message,
      "data" => $this->data
    );
    return $response;
  }
  
  public function setResponseDatatable($httpCode, $data, $draw, $recordsTotal, $recordsFiltered){
    $this->httpCode = $httpCode;
    $this->data = $data;
    $this->draw = $draw;
    $this->recordsTotal = $recordsTotal;
    $this->recordsFiltered = $recordsFiltered;
  }
  
  public function getResponseDatatable(){
    $response = array(
      "code" => $this->httpCode,
      "data" => $this->data,
      "draw" => $this->draw,
      "recordsTotal" => $this->recordsTotal,
      "recordsFiltered" => $this->recordsFiltered
    );
    return $response;
  }
}

?>
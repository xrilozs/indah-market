<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Artomoro Jaya Email class.
 *
 * @class am_email
 * @author Iqbal
 */
class Mailer
{
	var $CI;
    var $active;

	/**
	 * Constructor - Sets up the object properties.
	 */
	function __construct()
    {
        $this->CI       =& get_instance();
        $this->active	= TRUE;

        require_once SWIFT_MAILSERVER;
	}

    /**
	 * Send email function.
	 *
     * @param string    $to         (Required)  To email destination
     * @param string    $subject    (Required)  Subject of email
     * @param string    $message    (Required)  Message of email
     * @param string    $from       (Optional)  From email
     * @param string    $from_name  (Optional)  From name email
	 * @return Mixed
	 */
	function send($to, $subject, $message, $from = '', $from_name = ''){
        if (!$this->active) return false;

		$transport = false;
		if ($mailserver_host = '') {
			$transport = Swift_SmtpTransport::newInstance($mailserver_host, 25)
	    		->setUsername('')
	     		->setPassword('');
		}

		try{
            //Create the Transport
            if(!$transport) $transport  = Swift_MailTransport::newInstance();
			else $transport  = Swift_MailTransport::newInstance($transport);
            //Create the message
            $mail       = Swift_Message::newInstance();
            //Give the message a subject
            $mail->setSubject($subject)
                 ->setFrom(array($from => $from_name))
                 ->setTo($to)
                 ->setBody($message->plain)
                 ->addPart($message->html, 'text/html');

	        //Create the Mailer using your created Transport
	        $mailer     = Swift_Mailer::newInstance($transport);
	        //Send the message
	        $result     = $mailer->send($mail);

			return $result;
		}catch (Exception $e){
			// should be database log in here
			return $e->getMessage(); // 'failed to gather MAILDATA';
		}

		return false;
	}
	/**
     * Send email with attachment function.
     *
         * @param string    $to         (Required)  To email destination
         * @param string    $subject    (Required)  Subject of email
         * @param string    $message    (Required)  Message of email
         * @param string    $attachment_url    (Required)  Message of email
         * @param string    $from       (Optional)  From email
         * @param string    $from_name  (Optional)  From name email
     * @return Mixed
     */
    function send_with_attachment($to, $subject, $message, $attachment_url, $from = '', $from_name = ''){
                if (!$this->active) return false;

        $transport = false;
        if ($mailserver_host = '') {
            $transport = Swift_SmtpTransport::newInstance($mailserver_host, 25)
                    ->setUsername('')
                    ->setPassword('');
        }

        try{
                        //Create the Transport
                        if(!$transport) $transport  = Swift_MailTransport::newInstance();
            else $transport  = Swift_MailTransport::newInstance($transport);
                        //Create the message
                        $mail       = Swift_Message::newInstance();
                        //Give the message a subject
                        $mail->setSubject($subject)
                                 ->setFrom(array($from => $from_name))
                                 ->setTo($to)
                                 ->setBody($message->plain)
                                 ->attach(Swift_Attachment::fromPath($attachment_url))
                                 ->addPart($message->html, 'text/html');

                    //Create the Mailer using your created Transport
                    $mailer     = Swift_Mailer::newInstance($transport);
                    //Send the message
                    $result     = $mailer->send($mail);

            return $result;
        }catch (Exception $e){
            // should be database log in here
            return $e->getMessage(); // 'failed to gather MAILDATA';
        }

        return false;
    }
    function send_email_report($email, $data_laporan){
        $email              = trim($email);

        $plain_down     = get_settings('send_email_report');
        $html_down      = get_settings('send_email_report_html');

        $plain_down         = str_replace("%judul_laporan%",             $data_laporan['judul_laporan'], $plain_down);
        $plain_down         = str_replace("%link_download%",             $data_laporan['link_download'], $plain_down);
        $plain_down         = str_replace("%website_url%",          base_url(), $plain_down);
        $plain_down         = str_replace("%company_name%",     		COMPANY_NAME, $plain_down);
        $plain_down         = str_replace("%year%",     		date('Y'), $plain_down);
        
        $html_down         = str_replace("%judul_laporan%",             $data_laporan['judul_laporan'], $html_down);
        $html_down         = str_replace("%link_download%",             $data_laporan['link_download'], $html_down);
        $html_down         = str_replace("%website_url%",          base_url(), $html_down);
        $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
        $html_down         = str_replace("%year%",     		date('Y'), $html_down);

        $message            = new stdClass();
        $message->plain     = $plain_down;
        $message->html      = $html_down;

        $send               = $this->send($email, 'Unduh Laporan '.$data_laporan['judul_laporan'], $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

            return $send;
    }
    
    function send_email_change_password($email,$password){

        $plain_down     = get_settings('send_email_change_password');
        $html_down      = get_settings('send_email_change_password_html');
        $admin_contact  = get_settings('admin_contact');
        $plain_down         = str_replace("%email%",             $email, $plain_down);
        $plain_down         = str_replace("%password%",             $password, $plain_down);
        $plain_down         = str_replace("%website_url%",          base_url(), $plain_down);
        $plain_spon         = str_replace("%company_name%",     		COMPANY_NAME, $plain_down);
        $plain_spon         = str_replace("%year%",     			date('Y'), $plain_down);
        $plain_spon         = str_replace("%admin_contact%",     			$admin_contact, $plain_down);
        
        
        $html_down         = str_replace("%email%",             $email, $html_down);
        $html_down         = str_replace("%password%",             $password, $html_down);
        $html_down         = str_replace("%website_url%",          base_url(), $html_down);
        $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
        $html_down         = str_replace("%year%",     			date('Y'), $html_down);
        $html_down         = str_replace("%admin_contact%",     			$admin_contact, $html_down);
        
        $message            = new stdClass();
        $message->plain     = $plain_down;
        $message->html      = $html_down;

        $send               = $this->send($email, 'Informasi Ubah Password', $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

            return $send;
    }
    
    function send_email_reset_password($email,$password){

        $plain_down     = get_settings('send_email_forgot');
        $html_down      = get_settings('send_email_forgot_html');
        $admin_contact  = get_settings('admin_contact');
        $plain_down         = str_replace("%email%",             $email, $plain_down);
        $plain_down         = str_replace("%password%",             $password, $plain_down);
        $plain_down         = str_replace("%website_url%",          base_url(), $plain_down);
        $plain_spon         = str_replace("%company_name%",     		COMPANY_NAME, $plain_down);
        $plain_spon         = str_replace("%year%",     			date('Y'), $plain_down);
        $plain_spon         = str_replace("%admin_contact%",     			$admin_contact, $plain_down);
        
        
        $html_down         = str_replace("%email%",             $email, $html_down);
        $html_down         = str_replace("%password%",             $password, $html_down);
        $html_down         = str_replace("%website_url%",          base_url(), $html_down);
        $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
        $html_down         = str_replace("%year%",     			date('Y'), $html_down);
        $html_down         = str_replace("%admin_contact%",     			$admin_contact, $html_down);
        
        $message            = new stdClass();
        $message->plain     = $plain_down;
        $message->html      = $html_down;

        $send               = $this->send($email, 'Informasi Reset Password', $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

            return $send;
    }
    function send_email_report_attachment($data){
        if ( !$data ) return false;
				$email              = $data['email'];

        $plain_spon         = '\n' .
        $data['judul_laporan'].'\n' .
        '-------------------------------------------------'.'\n\n' .
        'Tanggal : '.$data['tanggal'].' \n' .
				'Nama File : '.$data['nama_file'].'\n\n '.
        '-------------------------------------------------'.'\n\n' .
        'Salam Sukses,'.'\n' .
        'Manajemen '.get_option('company_name');
        $html_spon          = '
        <div style="width: 80%; border: 2px solid #FCB322; padding: 0; margin: 0 auto;">
            <div style="background-color: #FCB322; padding: 5px; color: #FFF; text-align: center; font: bold 13px Arial;">'.$data['judul_laporan'].'</div>

            <div style="padding 10px; color: #666666; font: 12px/20px Arial;">
            <p style="padding: 0 10px;">Tanggal : <strong>'.$data['tanggal'].'</strong></p>
						<p style="padding: 0 10px;">Nama File : <strong>'.$data['nama_file'].'</strong></p>
            <p style="padding: 10px 10px 0 10px; color: #888888; font-size: 11px;">-------------------------------------------------<br />
            Salam Sukses,<br />
            Manajemen '.get_option('company_name').'</p>

            <p style="text-align: center; margin: 15px 0 0 0; font: 10px Arial; color: #888888; border-top: 1px solid #EEE; padding: 15px 0; background-color: #F7F7F7;">Copyright &copy; '.date('Y').'. '.get_option('company_name').'</p>
            </div>
        </div>';
        $message            = new stdClass();
        $message->plain     = $plain_spon;
        $message->html      = $html_spon;

        $send_to_recipient    = $this->send_with_attachment($email, $data['judul_laporan'], $message,$data['attachment_url'], 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

        if( !empty($debug ) ){
            print_r($send_to_recipient);
        }else{
            if($send_to_recipient){
                return true;
            }
        }

        return false;
    }
}


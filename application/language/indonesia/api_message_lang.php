<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  #common idiom
  $lang['office hour']    = 'jam kerja';
  $lang['absensi']        = 'absensi';
  $lang['supervisor']     = 'supervisor';
  $lang['employee']       = 'pekerja';
  $lang['site location']  = 'Lokasi situs';
  $lang['authectication'] = 'otentikasi';
  $lang['success']        = 'sukses';
  $lang['failed']         = 'gagal';
  $lang['not found']      = 'tidak ditemukan';
  #error message
  $lang['missing param']        = 'payload tidak memiliki parameter wajib';
  $lang['unauthorized access']  = 'akses tidak sah';
  $lang['invalid token']        = 'token tidak valid';
  $lang['internal server error']= 'terjadi kesalahan pada server';
  $lang['bad request']          = 'Format data request tidak valid';
  #action message
  $lang['generate token']                 = 'mendapatkan akses';
  $lang['get profile']                    = 'mendapatkan profil';
  $lang['get office hour']                = 'mendapatkan jam kerja';
  $lang['get office hour by id']          = 'mendapatkan jam kerja berdasarkan ID';
  $lang['get absensi harian']             = 'mendapatkan absensi harian';
  $lang['get absensi harian detail']      = 'mendapatkan detail absensi harian';
  $lang['get absensi harian detail self'] = 'mendapatkan detail absensi harian hari ini';
  $lang['get site location']              = 'mendapatkan lokasi situs';

  $lang['start working']          = 'absensi masuk';
  $lang['end working']            = 'absensi pulang';
  $lang['verify absensi']         = 'verifikasi absensi';
  $lang['verify absensi bulk']    = 'verifikasi absensi masal';
  $lang['already absensi']        = 'absensi sudah dilakukan';
  $lang['already verify']         = 'verifikasi sudah dilakukan';
  $lang['absensi need approval']  = 'absensi memerlukan persetujuan dari supervisor';
  $lang['no start working']       = 'belum melakukan absensi masuk';
  $lang['no end working']       	= 'belum melakukan absensi keluar';
  $lang['absensi not verified']   = 'absensi masuk belum diverifikasi';
  $lang['cannot absensi']         = 'belum/tidak masuk waktu absensi';
  $lang['cannot verify']          = 'belum masuk waktu verifikasi';
?>

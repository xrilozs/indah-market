<?php

function send_message($phone, $message){

  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://app.ruangwa.id/api/send_message',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => 'token='.WA_TOKEN.'&number='.$phone.'&message='.$message,
  ));
  $response = curl_exec($curl);
  curl_close($curl);

  return $response;
}

function send_document($phone, $url2){

  $url = "https://zonaintegritas.com/AssetsSite/img/berita/Berita-20210706085548.jpg";
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://app.ruangwa.id/api/send_document',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => 'token='.WA_TOKEN.'&number='.$phone.'&file='.$url,
  ));
  $response = curl_exec($curl);
  curl_close($curl);

  return $response;
}

?>
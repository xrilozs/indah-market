<?php
    function get_all_spl_list(){
        $CI =& get_instance();
        $data_spl = $CI->spl_model->get_spl();
        return $data_spl;
    }

    function get_total_spl(){
        $CI =& get_instance();
        $total = $CI->spl_model->get_spl_total();
        return $total;
    }
?>
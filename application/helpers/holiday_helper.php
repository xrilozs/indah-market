<?php
  function is_holiday($date){
    $CI =& get_instance();
    $hari_libur = $CI->hari_libur_model->get_hari_libur_by_date($date);
    return is_null($hari_libur) ? 0 : 1; 
  }
?>
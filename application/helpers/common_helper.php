<?php
require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

  function checkParameter($params){
    foreach($params as $param){
      if(is_null($param)){
        return false;
      }
    }
    return true;
  }

  function checkParameterByKeys($data, $keys){
    foreach($keys as $key){
      if(!array_key_exists($key, $data)){
        return false;
      }
    }
    return true;
  }

  function setOutput($resp){
    $CI =& get_instance();
    $CI->output
        ->set_header('Access-Control-Allow-Origin: *')
        ->set_status_header($resp['code'])
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($resp));
  }

  function authRole($roles){
    $user_login = get_current_member();
    if(!$user_login){
      return null;
    }else{
      if(in_array($user_login->type, $roles)){
        return $user_login;
      }else{
        return null;
      }
    }
  }

  function generateAppsToken($data){
    $token_expired = time() + (30 * 24 * 60 * 60);
    // $token_expired = time() + (60);
    $payload = array(
      'data' => $data,
      'exp' => $token_expired
    );
    #access token
    $access_token = JWT::encode($payload, ACCESS_TOKEN_SECRET, 'HS256');
    return $access_token;
  }

  function verifyAppsToken(){
    $CI =& get_instance();

    #check header
    $header = $CI->input->request_headers();
    if(isset($header['Authorization'])){
      list(, $token) = explode(' ', $header['Authorization']);
      #check token
      try {
        $jwt = JWT::decode($token, new Key(ACCESS_TOKEN_SECRET, 'HS256'));
        return $jwt;
      } catch (Exception $e) {
        return null;
      }
    }else{
      return null;
    }
  }

  function generateToken($username){
    $token_expired = time() + (120 * 60);
    $payload = array(
      'username' => $username,
      'exp' => $token_expired
    );
    #access token
    $access_token = JWT::encode($payload, ACCESS_TOKEN_SECRET, 'HS256');
    #refresh token
    $payload['exp'] = time() + (120 * 60);
    $refresh_token = JWT::encode($payload, REFRESH_TOKEN_SECRET, 'HS256');
  
    $response = array(
      'access_token' => $access_token,
      'expiry' => date(DATE_ISO8601, $token_expired),
      'refresh_token' => $refresh_token
    );

    return $response;
  }

  function verifyToken($token){
    $CI =& get_instance();
    
    #check token
    try {
      $jwt = JWT::decode($token, new Key(ACCESS_TOKEN_SECRET, 'HS256'));

      return array(
        "is_valid" => true,
        "message" => "Authorized access",
        "jwt" => $jwt
      );
    } catch (Exception $e) {
      return array(
        "is_valid" => false,
        "message" => "Invalid requested token"
      );
    }
  }

  function generateRecId($site_code){
    $CI =& get_instance();
    $now = date("Y-m-d");

    $last_absensi_harian = $CI->tx_absensi_harian_model->get_last_absensi_harian_by_site_code($site_code);
    $rec_id = is_null($last_absensi_harian) ? 1 : $last_absensi_harian->rec_id + 1;
    $rec_id = $rec_id > 100 ? 1 : $rec_id;
    return $rec_id;
  }

  function generateRecIdV2() {
    $characters = '0123456789';
    $code = '';

    for ($i = 0; $i < 5; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $code .= $characters[$index];
    }

    return $code;
  }

  function isIndicateOvertime($end_hour){
    $currentDateTime  = time();
    
    $startTime        = strtotime('00:00');
    $endTime          = strtotime('08:00');
    $endHourStartTime = strtotime('08:00');
    $endHourEndTime   = strtotime('23:59');
    $endHourTime      = strtotime($end_hour);
    
    if ($currentDateTime > $startTime && $currentDateTime < $endTime && $endHourTime > $endHourStartTime && $endHourTime <= $endHourEndTime) {
      return true;
    } else {
      return false;
    }
  }

?>

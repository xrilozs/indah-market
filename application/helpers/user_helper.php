<?php
if ( !function_exists('auth_redirect') ){
  function auth_redirect(){
    $CI =& get_instance();
    if(empty($CI->session->userdata('user_login'))) redirect('login');
    else return true;
  }
}

if ( !function_exists('get_current_member') ){
  function get_current_member(){
    $CI =& get_instance();
    if(!empty($CI->session->userdata('user_login'))){
      $where = array(
        'username'=> $CI->session->userdata('user_login')
      );
      $data_user = $CI->user_model->read_user($where);
      if($data_user->num_rows()!=0){
        return $data_user->row();
      } else return false;
    } else return false;
  }
}

if ( !function_exists('is_admin') ){
  function is_admin(){
    $CI =& get_instance();
    if(!empty($CI->session->userdata('user_login'))){
      $where = array(
        'username'=> $CI->session->userdata('user_login')
      );
      $data_user = $CI->user_model->read_user($where);
      if($data_user->num_rows()!=0){
        if($data_user->row()->type=='super_admin')
        return 1;
        else return 0;
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_all_member_list') ){
  function get_all_member_list(){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = array('type!='=>'super_admin');
    $data_user = $CI->user_model->read_user($where);
    if($data_user->num_rows()!=0){
      return $data_user;
    } else return false;
  }
}

if ( !function_exists('get_all_member_list_site') ){
  function get_all_member_list_site(){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = array('type!='=>'super_admin', 'site_code' => $user->site_code);
    $data_user = $CI->user_model->read_user($where);
    if($data_user->num_rows()!=0){
      return $data_user;
    } else return false;
  }
}


if ( !function_exists('get_fakturin_list') ){
  function get_fakturin_list($status='BELUM'){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = "A.status='".$status."'";
    $data_fakturin = $CI->fakturin_model->read_fakturin($where);
    if($data_fakturin->num_rows()!=0){
      return $data_fakturin;
    } else return false;
  }
}

if ( !function_exists('get_faktur_list') ){
  function get_faktur_list($status='belum_lunas'){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = " A.status='".$status."'";
    $data_fakturin = $CI->faktur_model->read_faktur($where);
    if($data_fakturin->num_rows()!=0){
      return $data_fakturin;
    } else return false;
  }
}

if ( !function_exists('get_all_customer') ){
  function get_all_customer(){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin', 'proret');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $data_customer = $CI->customer_model->read_customer();
    if($data_customer->num_rows()!=0){
      return $data_customer;
    } else return false;
  }
}
if ( !function_exists('get_all_faktur') ){
  function get_all_faktur(){
    $CI =& get_instance();
      $data_faktur = $CI->faktur_model->read_faktur();
      if($data_faktur->num_rows()!=0){
        return $data_faktur;
      } else return false;
  }
}
  
if ( !function_exists('get_all_returan') ){
  function get_all_returan($site_code){
    $CI =& get_instance();
    $where ="B.site_code='".$site_code."'";
    $data_customer = $CI->retur_model->read_retur($where);
    if($data_customer->num_rows()!=0){
        return $data_customer;
      } else return false;
  }
}

if ( !function_exists('get_all_produksi') ){
  function get_all_produksi(){
    $CI =& get_instance();
      $data_produksi = $CI->produksi_model->read_produksi();
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}

if ( !function_exists('get_produksi_by_month') ){
  function get_produksi_by_month($date, $site_code, $no_mesin=""){
    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin', 'proret');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = "B.site_code='".$site_code."' AND YEAR(A.tanggal_produksi)='".$year."' AND MONTH(A.tanggal_produksi)='".$month."'";
      if($no_mesin!="undefined" && $no_mesin!="") $where.=" AND A.no_machine = '".$no_mesin."'";
      $data_produksi = $CI->produksi_model->read_produksi($where);
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}
if ( !function_exists('get_total_produksi_by_month') ){
  function get_total_produksi_by_month($date, $site_code, $no_mesin=""){
    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();
    $where = "B.site_code='".$site_code."' AND YEAR(A.tanggal_produksi)='".$year."' AND MONTH(A.tanggal_produksi)='".$month."'";
      if($no_mesin!="") $where.=" AND A.no_machine = '".$no_mesin."'";
      $data_produksi = $CI->produksi_model->read_produksi_group($where);
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}
if ( !function_exists('get_produksi_byday') ){
  function get_produksi_byday($date, $site_code, $no_mesin=""){
    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();
    $where = "B.site_code='".$site_code."' AND YEAR(A.tanggal_produksi)='".$year."' AND MONTH(A.tanggal_produksi)='".$month."'";
      if($no_mesin!="undefined" && $no_mesin!="") $where.=" AND A.no_machine = '".$no_mesin."'";
      $data_produksi = $CI->produksi_model->read_produksi_groupday($where);
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}
if ( !function_exists('get_returan_by_month') ){
  function get_returan_by_month($date, $site_code, $flag="", $group=""){
    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin', 'proret');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = "B.site_code='".$site_code."' AND YEAR(A.datecreated)='".$year."' AND MONTH(A.datecreated)='".$month."'";
    if($flag!="") $where.="AND A.tanda_terima='".$flag."'";
      $data_retur = $CI->retur_model->read_retur($where, $group);
      if($data_retur->num_rows()!=0){
        return $data_retur;
      } else return false;
  }
}

if ( !function_exists('get_returan_by_date') ){
  function get_returan_by_date($date, $site_code, $flag=""){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin', 'proret');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }
    
    $where = "B.site_code='".$site_code."' AND DATE(A.datecreated)='".$date."'";
      if($flag!="") $where.="AND A.tanda_terima='".$flag."'";
      $data_retur = $CI->retur_model->read_retur($where);
      if($data_retur->num_rows()!=0){
        return $data_retur;
      } else return false;
  }
}

if ( !function_exists('get_produksi_by_date') ){
  function get_produksi_by_date($date, $site_code, $no_mesin=""){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin', 'proret');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = "B.site_code='".$site_code."' AND A.tanggal_produksi='".$date."'";
    if($no_mesin!="") $where.=" AND A.no_machine='".$no_mesin."'";
      $data_produksi = $CI->produksi_model->read_produksi($where);
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}



if ( !function_exists('get_fakturin_list_by_month') ){
  function get_fakturin_list_by_month($date, $customer_id=""){
    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();
    $where = "YEAR(A.tanggal_faktur)='".$year."' AND MONTH(A.tanggal_faktur)='".$month."' ";
    if($customer_id!="") $where.=" AND A.customer_id='".$customer_id."'";
      $data_fakturin = $CI->fakturin_model->read_fakturin($where);
      if($data_fakturin->num_rows()!=0){
        return $data_fakturin;
      } else return false;
  }
}

if ( !function_exists('get_fakturin_by_date') ){
  function get_fakturin_by_date($date, $id_customer=""){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = "DATE(A.tanggal_faktur)='".$date."'";
    if($id_customer!="") $where.=" AND A.customer_id='".$id_customer."'";
    $data_fakturin = $CI->fakturin_model->read_fakturin($where);
    if($data_fakturin->num_rows()!=0){
      return $data_fakturin;
    } else return false;
  }
}

if ( !function_exists('get_nota_penjualan_by_date') ){
  function get_nota_penjualan_by_date($date){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $where = "DATE(A.tanggal_nota)='".$date."'";
    $data_nota = $CI->nota_penjualan_model->read_nota_penjualan($where);
    if($data_nota->num_rows()!=0){
      return $data_nota;
    } else return false;
  }
}

if ( !function_exists('get_faktur_list_by_date') ){
  function get_faktur_list_by_date($date){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }
    
    $where = "DATE(A.tanggal_faktur)='".$date."'";
    $data_faktur = $CI->faktur_model->read_faktur($where);
    if($data_faktur->num_rows()!=0){
      return $data_faktur;
    } else return false;
  }
}

if ( !function_exists('get_faktur_list_by_month') ){
  function get_faktur_list_by_month($date, $customer_id=""){
    #authentication
    $roles=array('super_admin');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();
    $where = "YEAR(A.tanggal_faktur)='".$year."' AND MONTH(A.tanggal_faktur)='".$month."' ";
    if($customer_id!="") $where.=" AND A.customer_id='".$customer_id."'";
    $data_fakturin = $CI->faktur_model->read_faktur($where);
    if($data_fakturin->num_rows()!=0){
      return $data_fakturin;
    } else return false;
  }
}

if ( !function_exists('get_faktur_by_date') ){
  function get_faktur_by_date($date){
    $CI =& get_instance();
    $where = "DATE(A.tanggal_faktur)='".$date."'";
      $data_fakturin = $CI->faktur_model->read_faktur($where);
      if($data_fakturin->num_rows()!=0){
        return $data_fakturin;
      } else return false;
  }
}

// if ( !function_exists('get_fakturin_list') ){
//   function get_fakturin_list($date){
//     $CI =& get_instance();
//       $data_fakturin = $CI->fakturin_model->read_fakturin();
//       if($data_fakturin->num_rows()!=0){
//         return $data_fakturin;
//       } else return false;
//   }
// }

if ( !function_exists('get_customer_list') ){
  function get_customer_list(){
    $CI =& get_instance();

    #authentication
    $roles=array('super_admin', 'proret');
    $user = authRole($roles);
    if(is_null($user)){
      return false;
    }

    $data_customer = $CI->customer_model->read_customer();
    if($data_customer->num_rows()!=0){
      return $data_customer;
    } else return false;
  }
}

if ( !function_exists('get_no_mesin') ){
  function get_no_mesin($site_code){
    $CI =& get_instance();
    $where = array('site_code'=>$site_code);
      $data_mesin = $CI->mesin_model->read_mesin($where);
      if($data_mesin->num_rows()!=0){
        return $data_mesin;
      } else return false;
  }
}


if ( !function_exists('money') ){
  function money($number){
    return number_format($number,2,",",".");
  }
}



if ( !function_exists('generate_unique') ){
  function generate_unique(){
    return rand(1,9).rand(1,9).rand(1,9);
  }
}

if( !function_exists('hari')){
  function hari($hari){
    $harinya = "";
    switch ($hari) {
        case 'Sun':
        $harinya = "Minggu";
        break;
        case 'Mon':
        $harinya = "Senin";
        break;
        case 'Tue':
        $harinya = "Selasa";
        break;
        case 'Wed':
        $harinya = "Rabu";
        break;
        case 'Thu':
        $harinya = "Kamis";
        break;
        case 'Fri':
        $harinya = "Jumat";
        break;
        case 'Sat':
        $harinya = "Sabtu";
        break;
    }
    return $harinya;
  }
}

if( !function_exists('bulan')){
  function bulan($bulan){
    $bulannya = "";
    switch ($bulan) {
        case 'Jan':
        $bulannya = "Januari";
        break;
        case 'Feb':
        $bulannya = "Februari";
        break;
        case 'Mar':
        $bulannya = "Maret";
        break;
        case 'Apr':
        $bulannya = "April";
        break;
        case 'May':
        $bulannya = "Mei";
        break;
        case 'Jun':
        $bulannya = "Juni";
        break;
        case 'Jul':
        $bulannya = "Juli";
        break;
        case 'Aug':
        $bulannya = "Agustus";
        break;
        case 'Sep':
        $bulannya = "September";
        break;
        case 'Oct':
        $bulannya = "Oktober";
        break;
        case 'Nov':
        $bulannya = "November";
        break;
        case 'Dec':
        $bulannya = "Desember";
        break;
    }
    return $bulannya;
  }
}
if( !function_exists('decode_enum')){
  function decode_enum($enum){
    $enum = str_replace("_"," ",$enum);
    $enum = strtoupper($enum);
    return $enum;
  }
}
if ( !function_exists('get_settings') ){
  function get_settings(){
    $CI =& get_instance();
      $data = $CI->settings_model->read_settings();
      if($data->num_rows()!=0){
        return $data->row()->value;
      } else return false;
  }
}
function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " Belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " Seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut($nilai));
		} else {
			$hasil = trim(penyebut($nilai));
		}     		
		return $hasil." Rupiah";
	}
 


?>

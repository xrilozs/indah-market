<?php
  function generate_spl_hangus($items, $tanggal){
    require_once APPPATH . '../vendor/autoload.php';
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);

    $CI =& get_instance();
    $data['items'] = $items;
    $data['tanggal'] = $tanggal;
    $html = $CI->load->view('spl/print_spl_burn', $data, true);

    $mpdf->WriteHTML($html);
    $attachment = $mpdf->Output('', 'S');
    $output_url = "assets/export/spl-burn-$tanggal.pdf";
    $mpdf->Output($output_url, '');
    // $mpdf->Output("spl-burn-$tanggal.pdf", 'D');
    
    return $output_url;
  }
?>
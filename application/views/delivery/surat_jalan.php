<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Surat Jalan
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-pencil"></i> Surat Jalan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <form method="post" id="faktur_piutang_form" action="<?php echo base_url('backend/sj_process');?>">
    <div id="faktur_piutang_form_warning">
    </div>
    <div class="form-group">
      <label>Kepada YTH : </label><br>
              <select name="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer
          </option>';
          }
          ?>
          
        </select>
    </div>
    <div class="form-group">
      <label>Tanggal : </label>
      <input type="text" class="form-control datepicker" name="tanggal_sj">
    </div>
    <div class="form-group">
      <label>SJ No : </label><br>
      <input type="text" class="form-control" name="no_sj">
    </div>
    <div class="form-group">
      <label>Barang : </label><br>
      <input type="button" class="btn btn-primary produk_add_piutang" value='Tambah'>
      <input type="hidden" id="jumlah_produk" name="jumlah_produk" value="1">
    <table class=" table-condensed">
    <thead>
      <tr>
        <th>Banyaknya</th>
        <th>Tipe Berat</th>
        <th>Nama Barang</th>
        <th>SPL</th>
        <th>Harga</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody id="produk_add_table">
      <tr>
        <td><input type="number"  step="0.01" class="form-control" name="banyaknya_barang_1"></td>
        <td><select name="tipe_berat_barang_1" class="form-control">
          <option value="">
            Choose..
          </option>
          <option value="kg">
            Kg
          </option>
          <option value="lb">
            Lb
          </option>
          <option value="rl">
            Rl
          </option>
        </select></td>
        <td><input type="text" class="form-control" name="nama_barang_1"></td>
        <td><input type="text" class="form-control" name="no_spl_barang_1"></td>
        <td><input type="number" class="form-control" step="0.01"  name="harga_barang_1" ></td>
        <td></td>
      </tr>
    </tbody>
  </table>
    </div>
    <div class="form-group">
      <label>No. Faktur Pajak: </label>
      <input type="text" class="form-control" name="no_faktur_pajak">
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success submit-ajax btn-block" form-id="faktur_piutang_form"  warning_message="faktur_piutang_form_warning">Simpan</button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->

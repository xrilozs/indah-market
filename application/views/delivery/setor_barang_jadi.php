<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Setor Barang Jadi
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-archive"></i> Setor Barang Jadi</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12" id="search-delivery-warning"></div>
        <div class="col-lg-12">
            <form id="search-delivery-form">
                <div class="form-group">
                    <label>Cari SPL</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="No SPL.." name="no_spl" required>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default" type="button">Cari</button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <hr style="border: 1px solid;" />
        </div>
    </div>
    <div class="row" id="delivery-section" style="display:none;">
        <div class="col-lg-12" id="delivery-warning"></div>
        <div class="col-lg-12">
            <form id="delivery-form">
                <div class="form-group">
                    <label>No SPL</label>
                    <input type="text" class="form-control" name="no_spl" readonly>
                </div>
                <div class="form-group">
                    <label>Customer</label>
                    <input type="text" class="form-control" name="customer" readonly>
                </div>
                <div class="form-group">
                    <label>Ukuran</label>
                    <input type="text" class="form-control" name="ukuran" readonly>
                </div>
                <div class="form-group">
                    <label>Hasil Rumus</label>
                    <input type="text" class="form-control" name="hasil_rumus" readonly>
                </div>
                <div class="form-group">
                  <label>Tanggal Setor Barang1: </label>
                  <input type="text" class="form-control" id="datepicker-barang1" name="tanggal_setor_barang1" required>
                </div>
                <div class="form-group">
                    <label>Jumlah yang disetor1</label>
                    <input type="number" class="form-control" name="jumlah_setor1" required>
                </div>
                <div class="form-group update-section">
                  <label>Tanggal Setor Barang2: </label>
                  <input type="text" class="form-control" id="datepicker-barang2" name="tanggal_setor_barang2">
                </div>
                <div class="form-group update-section">
                    <label>Jumlah yang disetor2</label>
                    <input type="number" class="form-control" name="jumlah_setor2">
                </div>
                <button type="submit" class="btn btn-primary btn-block" id="delivery-button">Simpan</button>
            </form>
        </div>
    </div>
</section>
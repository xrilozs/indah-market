<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Terima Barang Jadi
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-archive"></i> Terima Barang Jadi</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-lg-12" id="save-delivery-warning"></div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="form-group">
          <label>Tanggal: </label>
          <input type="text" class="form-control datepicker" name="filter_tanggal" id="filter_tanggal">
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <button class="btn btn-primary btn-lg pull-right" id="save-delivery-button"><i class="fa fa-save"></i> Simpan</button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="databarang_table" class="table2 table-bordered table-hover">
            <thead>
              <tr>
                <th>No SPL</th>
                <th>Ukuran</th>
                <th>Kode Kustomer</th>
                <th>Tanggal Setor Barang #1</th>
                <th>Jumlah yang Disetor #1</th>
                <th>Tanggal Setor Barang #2</th>
                <th>Jumlah yang Disetor #2</th>
                <th>Diterima</th>
                <th>Dikirim</th>
                <th>Diretur</th>
                <th>Keterangan Retur</th>
                <th>Created At</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Lacak Data Barang
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-archive"></i> Lacak Data Barang</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="form-group">
          <label>Tanggal: </label>
          <input type="text" class="form-control monthly-datepicker" name="filter_tanggal" id="filter_tanggal">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="databarang_table" class="table2 table-bordered table-hover">
            <thead>
              <tr>
                <th>No SPL</th>
                <th>Ukuran</th>
                <th>Kode Kustomer</th>
                <th>Tanggal Setor Barang #1</th>
                <th>Jumlah yang Disetor #1</th>
                <th>Tanggal Setor Barang #2</th>
                <th>Jumlah yang Disetor #2</th>
                <th>Diterima</th>
                <th>Dikirim</th>
                <th>Diretur</th>
                <th>Keterangan Retur</th>
                <th>Created At</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div id="detail-spl-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Detail SPL</h4>
        </div>
        <div class="modal-body">
          <form id="detail-spl-form">
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer : </label>
                  <input type="text" name="customer_name" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" readonly>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Satuan: </label>
                  <input type="text" name="satuan" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" readonly></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Order: </label>
                  <input class="form-control" name="jumlah_order" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control" name="harga_satuan" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Komposisi: </label>
                  <input type="text" class="form-control" name="komposisi" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control" name="jumlah_roll" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input class="form-control" name="hasil_rumus" readonly>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div id="update-delivery-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Data Barang</h4>
        </div>
        <div class="modal-body">
          <div class="col-lg-12" id="update-delivery-warning"></div>

          <form id="update-delivery-form">
          <div class="form-group">
            <label>No SPL</label>
            <input type="text" class="form-control" name="no_spl" readonly>
          </div>
          <div class="form-group">
            <label>Customer</label>
            <input type="text" class="form-control" name="customer" readonly>
          </div>
          <div class="form-group">
            <label>Ukuran</label>
            <input type="text" class="form-control" name="ukuran" readonly>
          </div>
          <div class="form-group">
            <label>Hasil Rumus</label>
            <input type="text" class="form-control" name="hasil_rumus" readonly>
          </div>
          <div class="form-group">
            <label>Tanggal Setor Barang1: </label>
            <input type="text" class="form-control" id="datepicker-barang1" name="tanggal_setor_barang1" readonly>
          </div>
          <div class="form-group">
            <label>Jumlah yang disetor1</label>
            <input type="number" class="form-control" name="jumlah_setor1" required>
          </div>
          <div class="form-group update-section">
            <label>Tanggal Setor Barang2: </label>
            <input type="text" class="form-control" id="datepicker-barang2" name="tanggal_setor_barang2" readonly>
          </div>
          <div class="form-group update-section">
            <label>Jumlah yang disetor2</label>
            <input type="number" class="form-control" name="jumlah_setor2">
          </div>
          <div class="form-group update-section delivery-status" style="display:none;">
            <label>Diterima</label>
            <div class="radio">
              <label><input type="radio" name="received" id="received-yes" value="1">IYA</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="received" id="received-no" value="0" checked>TIDAK</label>
            </div>
          </div>
          <div class="form-group update-section delivery-status" style="display:none;">
            <label>Dikirim</label>
            <div class="radio">
              <label><input type="radio" name="delivered" id="delivered-yes" value="1">IYA</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="delivered" id="delivered-no" value="0" checked>TIDAK</label>
            </div>
          </div>
          <div class="form-group update-section delivery-status" style="display:none;">
            <label>Diretur</label>
            <div class="radio">
              <label><input type="radio" name="returned" id="returned-yes" value="1">IYA</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="returned" id="returned-no" value="0" checked>TIDAK</label>
            </div>
          </div>
          <div class="form-group update-section">
            <label>Deskripsi Retur</label>
            <input type="text" class="form-control" maxlength="30" name="ret_desc">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="update-delivery-button">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="delete-delivery-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Data Barang</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-delivery-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menghapus Data Barang ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-danger" id="delete-delivery-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

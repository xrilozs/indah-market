<table class="table table-bordered table-hover">
        <thead>
        <tr>
          <th>No</th>
          <th>User</th>
          <th>Kode Site</th>
          <th>No. Mesin</th>
          <th>Ukuran</th>
          <th>Jumlah (Kg)</th>
          <th>Jumlah (Lbr)</th>
          <th>Deskripsi</th>
          <th>Tanggal Produksi</th>
          <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $produksi = get_produksi_by_date($date, $member->site_code, $no_mesin);
        if($produksi!=false){
          $no = 0;
          foreach($produksi->result() as $item){
            $no +=1;
              $edit = '<a href="'.base_url('home/edit_produksi/'.$item->id).'" class="btn btn-primary btn-block">Edit</a>';
            echo '<tr>
            <td>'.$no.'</td>
            <td>'.$item->username.'</td>
            <td>'.strtoupper($item->site_code).'</td>
            <td>'.$item->no_machine.'</td>
            <td>'.$item->size.'</td>
            <td>'.$item->number_of_production_kg.'</td>
            <td>'.$item->number_of_production_sheet.'</td>
            <td>'.$item->description.'</td>
            <td>'.$item->tanggal_produksi.'</td>
            <td>'.$edit.'</td>
            </tr>
            ';
          }
        }
        ?>
        </tbody>
      </table>
<script>
$(document).ready(function(){
  $('.table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
});
</script>
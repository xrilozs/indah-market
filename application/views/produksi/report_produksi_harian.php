<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Lap. Produksi Harian
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-list"></i> Lap. Produksi Harian</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
          <?php if(!empty($this->session->flashdata('message'))) echo $this->session->flashdata('message');?>
          <form  method="post" action="<?php echo base_url('backend/download_produksi_harian');?>"  target="_blank">
        <div class="form-group">
          <label>Date :</label>
          <input type="text" class="datepicker" name="date" id="date_select" url="<?php echo base_url('backend/report_produksi_daily');?>">
        </div>
          <div class="form-group">
              <label>No.Mesin :</label>
              <select name="no_mesin" id="no_mesin_select"  url="<?php echo base_url('backend/report_produksi_daily');?>">
          <?php 
          $mesin = get_no_mesin($member->site_code);
          if($mesin!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($mesin->result() as $no_mesin){
              echo '<option value="'.$no_mesin->no_mesin.'">
            '.$no_mesin->no_mesin.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan No. Mesin
          </option>';
          }
          ?>
          
        </select>
        </div>
       <div class="form-group">
          <button type="submit" class="btn btn-primary"><span class="fa fa-download"></span> Download</button>
          <button type="button" class="btn btn-primary email_laporan" data-toggle="modal" data-target="#emailmodal"><span class="fa fa-envelope"></span> Email</button>
      </div>
      </form>
      </div>
      <div class="col-md-12">
      <div class="table-responsive load_table">
        <table class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th>User</th>
            <th>Kode Site</th>
            <th>No. Mesin</th>
            <th>Ukuran</th>
            <th>Jumlah (Kg)</th>
            <th>Jumlah (Lbr)</th>
            <th>Deskripsi</th>
            <th>Tanggal Produksi</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          <?php
            $date=date('Y-m-d');
            $produksi = get_produksi_by_date($date, $member->site_code);
          if($produksi!=false){
            $no = 0;
            foreach($produksi->result() as $item){
              $no +=1;
              $edit = '<a href="'.base_url('home/edit_produksi/'.$item->id).'" class="btn btn-primary btn-block">Edit</a>';
              echo '<tr>
              <td>'.$no.'</td>
              <td>'.$item->username.'</td>
              <td>'.strtoupper($item->site_code).'</td>
              <td>'.$item->no_machine.'</td>
              <td>'.$item->size.'</td>
              <td>'.$item->number_of_production_kg.'</td>
              <td>'.$item->number_of_production_sheet.'</td>
              <td>'.$item->description.'</td>
              <td>'.$item->tanggal_produksi.'</td>
              <td>'.$edit.'</td>
              </tr>
              ';
            }
          }
          ?>
          </tbody>
        </table>
        </div>
    </div>
    </div>
  </div>
  <div id="delete_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete User</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus user ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_user_footer" href="">Ya</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->
<div class="modal fade" id="emailmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form method="post" action="<?php echo base_url('backend/send_email_produksi_harian');?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kirim Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="no_mesin" id="no_mesin">
          <input type="hidden" name="date" id="date">
        <div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" id="email" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
      </form>
  </div>
</div>

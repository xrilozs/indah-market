<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Produksi
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-edit"></i> Edit Produksi</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div id="list" class="tab-pane fade in active">
        <div class="row">
            <div class="col-md-12">
                <?php if($this->session->flashdata('message')=='succes') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Perubahan Data Berhasil!.
    </div>
    ';
  }
  if($this->session->flashdata('message')=='failed') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error!</strong> Perubahan Data Gagal!.
    </div>
    ';
  }
        ?>
            </div>
            <div class="col-md-12">
                <a class="btn btn-primary" href="<?php echo base_url('report/produksi_harian');?>"><span class="fa fa-arrow-left"></span> Kembali</a>
                <form method="post" id="produksi_harian_form" action="<?php echo base_url('backend/produksi_edit_process');?>">
                    <div id="produksi_hariam_form_warning">
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="id" id="id" value="<?php echo $produksi->id;?>">
                      <label>Kode Site : </label><br>
                      <b><?php echo strtoupper($produksi->site_code);?></b>
                    </div>
                    <div class="form-group">
                      <label>Tanggal Produksi : </label>
                      <input type="text" class="form-control datepicker" name="tanggal_produksi" value="<?php echo $produksi->tanggal_produksi;?>">
                    </div>
                   <div class="form-group">
                        <label>No. Mesin : </label>
                        <select name="no_mesin" id="no_mesin" class="form-control">
                          <?php 
                          $mesin = get_no_mesin($member->site_code);
                          if($mesin!=false){
                            echo '<option value="">
                            Choose..
                          </option>';
                            foreach($mesin->result() as $no_mesin){
                              echo '<option value="'.$no_mesin->no_mesin.'">
                            '.$no_mesin->no_mesin.'
                          </option>';
                            }
                          } else  {
                            echo '<option value="">
                            Tidak ada pilihan No. Mesin
                          </option>';
                          }
                          ?>

                        </select>
                       <script>
                           $(document).ready(function(){
                              $("#no_mesin").val('<?php echo $produksi->no_machine;?>').change(); 
                           });
                       </script>
                      </div>
                    <div class="form-group">
                      <label>Ukuran : </label>
                      <input type="text" name="ukuran" class="form-control" value="<?php echo $produksi->size;?>">
                    </div>
                    <div class="form-group">
                      <label>Jumlah Produksi (Kg) : </label>
                      <input type="number" name="jml_prod_kg" step="0.01" class="form-control" value="<?php echo $produksi->number_of_production_kg;?>">
                    </div>
                    <div class="form-group">
                      <label>Jumlah Produksi (Lembar) : </label>
                      <input type="number" name="jml_prod_lbr" class="form-control" value="<?php echo $produksi->number_of_production_sheet;?>">
                    </div>
                    <div class="form-group">
                      <label>Keterangan : </label>
                      <textarea name="keterangan" class="form-control" rows="5" value="<?php echo $produksi->description;?>"><?php echo $produksi->description;?></textarea>
                    </div>
                    <div class="form-group">
                    <button type="submit" class="btn btn-success edit-ajax btn-block" form-id="produksi_harian_form"  warning_message="produksi_hariam_form_warning">Simpan</button>
                    </div>
    </form>
            </div>
        </div>
    </div>
    <div id="delete_returan_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Produksi</h4>
                </div>
                <div class="modal-body">
                    Aoakah anda yakin untuk menghapus produksi ini
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <a class="btn btn-danger" id="delete_footer" href="">Ya</a>
                </div>
            </div>
        </div>
    </div>
    <div id="edit_returan_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Returan</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success edit-ajax" form-id="edit_modal_form" warning_message="edit_modal_form_warning">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
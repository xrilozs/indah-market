<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Produksi harian
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-pencil"></i> Produksi Harian</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <form method="post" id="produksi_harian_form" action="<?php echo base_url('backend/produksi_harian_process');?>">
    <div id="produksi_hariam_form_warning">
    </div>
    <div class="form-group">
      <label>Kode Site : </label><br>
      <b><?php echo strtoupper($member->site_code);?></b>
    </div>
    <div class="form-group">
      <label>Tanggal Produksi : </label>
      <input type="text" class="form-control datepicker" name="tanggal_produksi">
    </div>
   <div class="form-group">
        <label>No. Mesin : </label>
        <select name="no_mesin" class="form-control">
          <?php 
          $mesin = get_no_mesin($member->site_code);
          if($mesin!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($mesin->result() as $no_mesin){
              echo '<option value="'.$no_mesin->no_mesin.'">
            '.$no_mesin->no_mesin.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan No. Mesin
          </option>';
          }
          ?>
          
        </select>
      </div>
    <div class="form-group">
      <label>Ukuran : </label>
      <input type="text" name="ukuran" class="form-control">
    </div>
    <div class="form-group">
      <label>Jumlah Produksi (Kg) : </label>
      <input type="number" name="jml_prod_kg" step="0.01" class="form-control">
    </div>
    <div class="form-group">
      <label>Jumlah Produksi (Lembar) : </label>
      <input type="number" name="jml_prod_lbr" class="form-control">
    </div>
    <div class="form-group">
      <label>Keterangan : </label>
      <textarea name="keterangan" class="form-control" rows="5"></textarea>
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success submit-ajax btn-block" form-id="produksi_harian_form"  warning_message="produksi_hariam_form_warning">Simpan</button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->

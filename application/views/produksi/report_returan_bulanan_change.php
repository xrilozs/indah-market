<table class="table table-bordered table-hover">
          <thead>
          <tr>
              <th>No</th>
              <th>User</th>
              <th>Kode site</th>
              <th>Customer</th>
              <th>Tanggal Diterima</th>
              <th>Ukuran</th>
              <th>Jumlah(Kg)</th>
              <th>Jumlah(Lbr)</th>
              <th>Tanda Terima</th>
              <th>Tindakan</th>
              <th>Tanggal dibuat</th>
          </tr>
          </thead>
          <tbody>
          <?php
        $returan = get_returan_by_month($date, $member->site_code, $flag);
        if($returan!=false){
          $no = 0;
          foreach($returan->result() as $item){
            $no +=1;
            echo '<tr>
              <td>'.$no.'</td>
              <td>'.$item->username.'</td>
              <td>'.strtoupper($item->site_code).'</td>
              <td>'.$item->retur_name.'</td>
              <td>'.$item->tanggal_diterima.'</td>
              <td>'.$item->ukuran.'</td>
              <td>'.$item->jumlah_retur_kg.'</td>
              <td>'.$item->jumlah_retur_lembar.'</td>
              <td>'.strtoupper($item->tanda_terima).'</td>
              <td>'.$item->tindak_lanjut.'</td>
              <td>'.$item->datecreated.'</td>
            </tr>
            ';
          }
        }
        ?>
          </tbody>
        </table>
<script>
$(document).ready(function(){
  $(".table").DataTable();
});
</script>
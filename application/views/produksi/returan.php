<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Returan
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-pencil"></i> Returan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <form method="post" id="produksi_harian_form" action="<?php echo base_url('backend/returan_process');?>">
    <div id="produksi_hariam_form_warning">
    </div>
    <div class="form-group">
      <label>Tanggal Retur Diterima : </label>
      <input type="text" name="tanggal_diterima" class="form-control datepicker" maxlength="6">
    </div>
    <div class="form-group">
        <label>Customer : </label>
        <select name="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer
          </option>';
          }
          ?>
          
        </select>
      </div>
    <div class="form-group">
      <label>Ukuran : </label>
      <input type="text" name="ukuran" class="form-control">
    </div>
    <div class="form-group">
      <label>Jumlah Retur (Kg) : </label>
      <input type="number" name="jml_retur_kg" class="form-control">
    </div>
    <div class="form-group">
      <label>Jumlah Retur (Lembar) : </label>
      <input type="number" name="jml_retur_lbr" class="form-control">
    </div>
    <div class="form-group">
      <label>Alasan Retur : </label>
      <textarea name="alasan_retur" class="form-control" rows="5"></textarea>
    </div>
    <div class="form-group">
        <label>Flag : </label>
        <select name="tanda_terima" class="form-control">
          <option value="">
            Choose..
          </option>
          <option value="ya">
            Ya
          </option>
          <option value="tidak">
            Tidak
          </option>
      </select>
    </div>
    <div class="form-group">
      <label>Tindak Lanjut : </label>
      <textarea name="tindak_lanjut" class="form-control" rows="5"></textarea>
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success submit-ajax btn-block" form-id="produksi_harian_form"  warning_message="produksi_hariam_form_warning">Simpan</button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Lap. Returan Bulanan
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-list"></i> Lap. Returan Bulanan</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div id="list" class="tab-pane fade in active">
        <div class="row">
            <div class="col-md-12">
                <?php if(!empty($this->session->flashdata('message'))) echo $this->session->flashdata('message');?>
                <form method="post" action="<?php echo base_url('backend/download_returan_bulanan');?>" target="_blank">
                    <div class="form-group">
                        <label>Date :</label>
                        <select class="" name="year" id="year_select" url="<?php echo base_url('backend/report_returan_bulanan_monthly');?>">
                            <option value="">Pilih Tahun</option>
                            <?php for($i=date('Y');$i>(date('Y')-5);$i--){
              echo '<option value="'.$i.'">'.$i.'</option>';
            } ?>
                        </select>
                        <select name="month" id="month_select" url="<?php echo base_url('backend/report_returan_bulanan_monthly');?>">
                            <option value="">Pilih Bulan</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Flag :</label>
                        <select class="" name="flag" id="flag_select" url="<?php echo base_url('backend/report_returan_bulanan_monthly');?>">
                            <option value="">Pilih Flag</option>
                            <option value="ya">
                                Ya
                            </option>
                            <option value="tidak">
                                Tidak
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
              <button type="submit" class="btn btn-primary"><span class="fa fa-download"></span> Download</button>
              <button type="button" class="btn btn-primary email_laporan" data-toggle="modal" data-target="#emailmodal"><span class="fa fa-envelope"></span> Email</button>
          </div>
                </form>
                    <div class="col-md-12">
                        <div class="table-responsive load_table">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>User</th>
                                        <th>Kode site</th>
                                        <th>Customer</th>
                                        <th>Tanggal Diterima</th>
                                        <th>Ukuran</th>
                                        <th>Jumlah(Kg)</th>
                                        <th>Jumlah(Lbr)</th>
                                        <th>Tanda Terima</th>
                                        <th>Tindakan</th>
                                        <th>Tanggal dibuat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
            $date = date('Y-m');
        $returan = get_returan_by_month($date, $member->site_code);
        if($returan!=false){
          $no = 0;
          foreach($returan->result() as $item){
            $no +=1;
            echo '<tr>
              <td>'.$no.'</td>
              <td>'.$item->username.'</td>
              <td>'.strtoupper($item->site_code).'</td>
              <td>'.$item->retur_name.'</td>
              <td>'.$item->tanggal_diterima.'</td>
              <td>'.$item->ukuran.'</td>
              <td>'.$item->jumlah_retur_kg.'</td>
              <td>'.$item->jumlah_retur_lembar.'</td>
              <td>'.strtoupper($item->tanda_terima).'</td>
              <td>'.$item->tindak_lanjut.'</td>
              <td>'.$item->datecreated.'</td>
            </tr>
            ';
          }
        }
        ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
            </div>
        </div>
        <div id="delete_user_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete User</h4>
                    </div>
                    <div class="modal-body">
                        Aoakah anda yakin untuk menghapus user ini
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <a class="btn btn-danger" id="delete_user_footer" href="">Ya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="emailmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form method="post" action="<?php echo base_url('backend/send_email_returan_bulanan');?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kirim Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="year" id="year">
          <input type="hidden" name="month" id="month">
          <input type="hidden" name="flag" id="flag">
        <div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" id="email" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
      </form>
  </div>
</div>
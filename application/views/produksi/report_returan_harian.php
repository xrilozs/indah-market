<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Lap. Returan Harian
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fa fa-list"></i> Lap. Returan Harian</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div id="list" class="tab-pane fade in active">
        <div class="row">
            <form method="post" action="<?php echo base_url('backend/download_returan_harian');?>" target="_blank">
                <div class="col-md-12">
                    <?php if(!empty($this->session->flashdata('message'))) echo $this->session->flashdata('message');?>
                    <div class="form-group">
                        <label>Date :</label>
                        <input type="text" class="datepicker" name="date" id="date_select" url="<?php echo base_url('backend/report_returan_daily');?>">
                    </div>
                <div class="form-group">
                        <label>Flag :</label>
                        <select class="" name="flag" id="flag_select" url="<?php echo base_url('backend/report_returan_daily');?>">
                            <option value="">Pilih Flag</option>
                            <option value="ya">
                                Ya
                            </option>
                            <option value="tidak">
                                Tidak
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
              <button type="submit" class="btn btn-primary"><span class="fa fa-download"></span> Download</button>
              <button type="button" class="btn btn-primary email_laporan" data-toggle="modal" data-target="#emailmodal"><span class="fa fa-envelope"></span> Email</button>
                </div>
                </div>
            </form>
            <div class="col-md-12">
                <div class="table-responsive load_table">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User</th>
                                <th>Kode site</th>
                                <th>Customer</th>
                                <th>Tanggal Diterima</th>
                                <th>Ukuran</th>
                                <th>Jumlah(Kg)</th>
                                <th>Jumlah(Lbr)</th>
                                <th>Tanda Terima</th>
                                <th>Tindakan</th>
                                <th>Tanggal dibuat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
            $date = date('Y-m-d');
        $returan = get_returan_by_date($date, $member->site_code);
        if($returan!=false){
          $no = 0;
          foreach($returan->result() as $item){
            $no +=1;
            echo '<tr>
              <td>'.$no.'</td>
              <td>'.$item->username.'</td>
              <td>'.strtoupper($item->site_code).'</td>
              <td>'.$item->retur_name.'</td>
              <td>'.$item->tanggal_diterima.'</td>
              <td>'.$item->ukuran.'</td>
              <td>'.$item->jumlah_retur_kg.'</td>
              <td>'.$item->jumlah_retur_lembar.'</td>
              <td>'.strtoupper($item->tanda_terima).'</td>
              <td>'.$item->tindak_lanjut.'</td>
              <td>'.$item->datecreated.'</td>
            </tr>
            ';
          }
        }
        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="delete_user_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete User</h4>
                </div>
                <div class="modal-body">
                    Aoakah anda yakin untuk menghapus user ini
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                    <a class="btn btn-danger" id="delete_user_footer" href="">Ya</a>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="emailmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form method="post" action="<?php echo base_url('backend/send_email_returan_harian');?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kirim Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="date" id="date">
          <input type="hidden" name="flag" id="flag">
        <div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" id="email" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
      </form>
  </div>
</div>
<!-- /.content -->
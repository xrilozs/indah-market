<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Update Returan
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-edit"></i> Update Returan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
         <?php if($this->session->flashdata('message')=='success_delete') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Penghapusan Data Berhasil!.
    </div>
    ';
  }
  if($this->session->flashdata('message')=='error_delete') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error!</strong> Penghapusan Data Gagal!.
    </div>
    ';
  }
        ?>
      </div>
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="returan_manager_table" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th width="5%">Customer</th>
              <th>Ukuran</th>
              <th>Jumlah(KG)</th>
              <th>Jumlah(Lembar)</th>
              <th>Tanggal Diterima</th>
              <th>Flag</th>
              <th>Tindakan</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
  <div id="delete_returan_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Returan</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus returan ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
        </div>
      </div>
    </div>
  </div>
  <div id="edit_returan_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Returan</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="edit_modal_form" action="<?php echo base_url('backend/edit_returan_process');?>">
            <input id="id" type="hidden" name="id" class="form-control">
            <div id="edit_modal_form_warning">
            </div>
    <div class="form-group">
      <label>Tanggal Retur Diterima : </label>
      <input id="edit_tanggal_diterima" type="text" name="tanggal_diterima" class="form-control datepicker" maxlength="6">
    </div>
    <div class="form-group">
        <label>Customer : </label>
        <select id="edit_customer_id" name="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer
          </option>';
          }
          ?>
          
        </select>
      </div>
    <div class="form-group">
      <label>Ukuran : </label>
      <input  id="edit_ukuran" type="text" name="ukuran" class="form-control">
    </div>
    <div class="form-group">
      <label>Jumlah Retur (Kg) : </label>
      <input id="edit_jml_prod_kg" type="number" name="jml_retur_kg" class="form-control">
    </div>
    <div class="form-group">
      <label>Jumlah Retur (Lembar) : </label>
      <input id="edit_jml_prod_lbr" type="number" name="jml_retur_lbr" class="form-control">
    </div>
    <div class="form-group">
      <label>Alasan Retur : </label>
      <textarea id="edit_alasan" name="alasan_retur" class="form-control" rows="5"></textarea>
    </div>
    <div class="form-group">
        <label>Ada Tanda Terima : </label>
        <select id="edit_tanda_terima" name="tanda_terima" class="form-control">
          <option value="">
            Choose..
          </option>
          <option value="ya">
            Ya
          </option>
          <option value="tidak">
            Tidak
          </option>
      </select>
    </div>
    <div class="form-group">
      <label>Tindak Lanjut : </label>
      <textarea id="edit_tindak_lanjut" name="tindak_lanjut" class="form-control" rows="5"></textarea>
    </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success edit-ajax" form-id="edit_modal_form"  warning_message="edit_modal_form_warning">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->
<script>
var table = $("#returan_manager_table").DataTable({
	ordering: false,
	processing: true,
	serverSide: true,
	ajax: {
	  url: "<?php echo base_url('backend/update_returan_list') ?>",
	  type:'POST',
	}
});
</script>
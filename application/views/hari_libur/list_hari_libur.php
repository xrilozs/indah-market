<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Hari Libur
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text"></i> Hari Libur Management</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row" style="margin-bottom:10px;">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
          <label>Tahun: </label>
          <input type="text" class="form-control yearly-datepicker" name="filter_tanggal" id="filter_tanggal">
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create-hari-libur-modal">
          <i class="fa fa-plus" aria-hidden="true"></i> Create Hari Libur
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="hari_libur_table" class="table2 table-bordered table-hover">
            <thead>
              <tr>
                <th>Date</th>
                <th>Description</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="create-hari-libur-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Hari Libur</h4>
        </div>
        <div class="modal-body">
          <form id="create-hari-libur-form">
            <div class="row">
              <div id="create-hari-libur-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Date:</label>
                  <input type="text" class="form-control datepicker" name="date" id="date_create" required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Description:</label>
                  <input type="text" class="form-control" name="description" id="description" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="create-hari-libur-button">Create Hari Libur</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="update-hari-libur-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Hari Libur</h4>
        </div>
        <div class="modal-body">
          <form id="update-hari-libur-form">
            <div class="row">
              <div id="update-hari-libur-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Date:</label>
                  <input type="text" class="form-control datepicker" name="date" id="date_update" required>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label>Description:</label>
                  <input type="text" class="form-control" name="description" id="description" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="update-hari-libur-button">Update Hari Libur</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="delete-hari-libur-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Hari Libur</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-hari-libur-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menghapus Hari Libur ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-danger" id="delete-hari-libur-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
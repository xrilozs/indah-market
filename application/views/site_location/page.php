<section class="content-header">
  <h1>
    Setting - Site Location
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-cog"></i> Setting - Site Location</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="limit" class="tab-pane fade in active">
    <div class="row">
        <div class="col-lg-12">
            <form class="form" id="site-location-form">
                <div id="site-location-warning">
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                        <label>Latitude</label>
                        <input type="text" class="form-control" name="lat" placeholder="Latitude.." required>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                        <label>Longitude</label>
                        <input type="text" class="form-control" name="long" placeholder="Longitude.." required>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                        <label>Address (Optional)</label>
                        <textarea class="form-control" name="address" placeholder="Address.."></textarea>
                    </div>
                  </div>
                </div>
                <button class="btn btn-primary" id="site-location-button">Submit</button>
            </form>
        </div>
    </div>
  </div>
</section>
<!-- /.content -->

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <?php
  if($this->session->flashdata('message')=='success_checkout') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Pemesanan produk berhasil!.
    </div>
    ';
  }
  ?>
</div>
  <h1>Selamat Datang Di <?php echo COMPANY_NAME;?></h1>
  <h4>
    Hari Ini :  <?php echo hari(date('D')).', '.date('d').' '.bulan(date('M')).' '.date('Y');?>
  </h4>
  <h4>
    Kode Site Anda : <?php echo strtoupper($member->site_code);?>
  </h4>
</section>
<!-- /.content -->

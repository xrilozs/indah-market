
<!DOCTYPE HTML>
<html>
  <head>
    <title>Data Jurnal Absensi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        @media print {
            table { page-break-after:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            td    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
        }
        .table-condensed{
            font-size: 12px;
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom:30px; ">
                <span class="company_name" style="font-weight:bold;font-size:20px;">Laporan Jurnal Absensi</span><br>
                <span class="company_name" style="font-weight:bold;">Periode: <?=date_format(date_create($start_date),"d/M/Y");?> - <?=date_format(date_create($end_date),"d/M/Y");?></span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tanggal Absen</th>
                            <th>Shift</th>
                            <th>Jam Masuk</th>
                            <th>Jam Pulang</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sum_total = 0;
                        foreach($absensi_harian as $item){
                            echo "<tr>
                                <td style ='word-break:break-all;'>$item->no</td>
                                <td style ='word-break:break-all;'>$item->pegawai_name</td>
                                <td style ='word-break:break-all;'>$item->shift</td>
                                <td style ='word-break:break-all;'>".date_format(date_create($item->absensi_date),"d M Y")."</td>
                                <td style ='word-break:break-all;'>$item->start_hour</td>
                                <td style ='word-break:break-all;'>$item->end_hour</td>
                            </tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: -15px; left: 0;">
                <p class="company_name" style="font-size:9px; padding-top:10px;">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
      </div>
  </body>
<script>
  window.print();
</script>
</html>
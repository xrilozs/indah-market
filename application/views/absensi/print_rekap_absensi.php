
<!DOCTYPE HTML>
<html>
  <head>
    <title>Data Rekap Absensi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        @media print {
            table { page-break-after:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            td    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
        }
        .table-condensed{
            font-size: 12px;
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom:30px; ">
                <span class="company_name" style="font-weight:bold;font-size:20px;">Laporan Rekap Absensi</span><br>
                <span class="company_name" style="font-weight:bold;">Periode: <?=date_format(date_create($date),"d/m/Y");?></span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Jlh Hadir Biasa</th>
                            <th>Jlh Hadir Libur</th>
                            <th>Jlh Jam Lembur</th>
                            <th>Jlh Uang Telat</th>
                            <th>Pot. Kasbon</th>
                            <th>Uang Tunjangan</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($rekap_absensi as $item){
                            echo "<tr>
                                <td style ='word-break:break-all;'>$item->pegawai_nip</td>
                                <td style ='word-break:break-all;'>$item->pegawai_name</td>
                                <td style ='word-break:break-all;'>$item->jumlah_hadir_biasa</td>
                                <td style ='word-break:break-all;'>$item->jumlah_hari_libur</td>
                                <td style ='word-break:break-all;'>$item->jumlah_jam_lembur</td>
                                <td style ='word-break:break-all;'>".number_format($item->jumlah_telat)."</td>
                                <td style ='word-break:break-all;'>".number_format($item->potongan_kasbon)."</td>
                                <td style ='word-break:break-all;'>".number_format($item->uang_tunjangan)."</td>
                            </tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: -15px; left: 0;">
                <p class="company_name" style="font-size:9px; padding-top:10px;">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
      </div>
  </body>
<script>
  window.print();
</script>
</html>
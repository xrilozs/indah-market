
<!DOCTYPE HTML>
<html>
  <head>
    <title>Data Histori Absensi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        @media print {
            table { page-break-after:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            td    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
        }
        .table-condensed{
            font-size: 10px;
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <!-- <div class="row mb-4">
        </div> -->
        <div class="row">
            <div class="col-lg-12 mb-4">
                <p align="center" style="font-weight:bold; font-size:10px;">
                    DATA INI HASIL KOMPUTER ADALAH SAH, MUTLAK TIDAK BISA DIKOMPLAIN
                </p>
            </div>
            <div class="col-lg-12" style="margin-bottom:30px; ">
                <span class="company_name" style="font-weight:bold;font-size:20px;">Data Histori Absensi</span><br>
                <span class="company_name" style="font-weight:bold;">Periode: <?=$date;?></span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>NIP</th>
                            <th>Nama Karyawan</th>
                            <th>Jumlah Akumulasi Hadir</th>
                            <th>Jumlah Akumulasi Libur</th>
                            <th>Jumlah Akumulasi Jam Lembur</th>
                            <th>Jumlah Akumulasi Telat</th>
                            <th>Jumlah Akumulasi Absen</th>
                            <th>Tanggal Periode</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($histori_absensis as $item){
                            echo "<tr>
                                <td style ='word-break:break-all;'>$item->pegawai_nip</td>
                                <td style ='word-break:break-all;'>$item->pegawai_name</td>
                                <td style ='word-break:break-all;'>$item->jumlah_akumulasi_hadir_biasa</td>
                                <td style ='word-break:break-all;'>$item->jumlah_akumulasi_hari_libur</td>
                                <td style ='word-break:break-all;'>$item->jumlah_akumulasi_jam_lembur</td>
                                <td style ='word-break:break-all;'>$item->jumlah_akumulasi_telat</td>
                                <td style ='word-break:break-all;'>$item->jumlah_akumulasi_absen</td>
                                <td style ='word-break:break-all;'>$date</td>
                            </tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: -15px; left: 0;">
                <p class="company_name" style="font-size:9px; padding-top:10px;">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
      </div>
  </body>
<script>
  window.print();
</script>
</html>
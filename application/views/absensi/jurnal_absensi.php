<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Jurnal Absensi
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-files-o"></i> Jurnal Absensi</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal Awal absensi: </label>
              <input type="text" class="form-control datepicker" name="filter_tanggal_awal" id="filter_tanggal_awal">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal Akhir absensi: </label>
              <input type="text" class="form-control datepicker" name="filter_tanggal_akhir" id="filter_tanggal_akhir">
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="row">
          <div class="col-lg-12">
            <button id="sync-jurnal-absensi-toggle" class="btn btn-primary pull-right" data-toggle="modal" data-target="#sync-jurnal-absensi-modal">
              <i class="fa fa-archive" aria-hidden="true"></i> Buat Rekap
            </button>
            <a href="#" id="print-jurnal-absensi" class="btn btn-default pull-right"  target="_blank" style="margin: 0px 10px;">
              <i class="fa fa-print" aria-hidden="true"></i> Print
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="jurnal_absensi_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>Pegawai</th>
              <th>Shift</th>
              <th>Jam Masuk</th>
              <th>Jam Pulang</th>
              <th>Tanggal Absensi</th>
              <th>Record ID</th>
              <th>Supervisor</th>
              <th>Rekap</th>
              <th>Lembur</th>
              <th>Terlambat</th>
              <th>Weekend</th>
              <th>Jam Terlambat</th>
              <th>Jam Lembur</th>
              <th>Tanggal</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div id="sync-jurnal-absensi-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Rekap Absensi</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="sync-jurnal-absensi-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk <b>merekap data absensi</b> pada jangka waktu yang Anda pilih?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-primary" id="sync-jurnal-absensi-button">IYA</button>
        </div>
      </div>
    </div>
  </div>

  <div id="jurnal-edit-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Jurnal Absensi</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="jurnal-edit-warning">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <form class="form" id="jurnal-edit-form">
                <div class="form-group">
                  <label>Pegawai</label>
                  <input type="text" name="pegawai_name" class="form-control" readonly>
                </div>
                <div class="form-group">
                  <label>Supervisor</label>
                  <input type="text" name="supervisor_name" class="form-control" readonly>
                </div>
                <div class="form-group">
                  <label>Shift</label>
                  <input type="text" name="shift" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>Jam Masuk</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" id="start_hour" name="start_hour">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Jam Keluar</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" id="end_hour" name="end_hour">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Tanggal Absensi</label>
                  <input type="text" name="absensi_date" class="form-control" readonly>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutp</button>
          <button type="submit" class="btn btn-primary" id="jurnal-edit-button">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->


<!DOCTYPE HTML>
<html>
  <head>
    <title>Data Absensi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        @media print {
            table { page-break-after:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            td    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
        }
        .table-condensed{
            font-size: 12px;
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom:30px; ">
                <span class="company_name" style="font-weight:bold;font-size:20px;">Laporan Upah Gaji</span><br>
                <span class="company_name" style="font-weight:bold;">Periode: <?=date_format(date_create($date),"d/m/Y");?></span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>NIP</th>
                            <th>Nama Karyawan</th>
                            <th>Masuk</th>
                            <th>Absen</th>
                            <th>Telat</th>
                            <th>Lembur</th>
                            <th>Libur</th>
                            <th>Uang Tunjangan</th>
                            <th>Potongan</th>
                            <th>Sisa Kasbon</th>
                            <th>Total Gaji</th>
                            <th>Total Gaji Lembur</th>
                            <th>Total Gaji Libur</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sum_total = 0;
                        foreach($absensis as $item){
                            $total_gaji = $item->jumlah_hadir_biasa*$item->pegawai_upah_harian;
                            $total_gaji_lembur = $item->jumlah_jam_lembur*$item->pegawai_upah_lembur;
                            $total_gaji_libur = $item->jumlah_hari_libur*$item->pegawai_upah_libur;
                            $total = ($total_gaji + $total_gaji_lembur + $total_gaji_libur + $item->uang_tunjangan) - $item->potongan_kasbon - $item->jumlah_telat;
                            $sum_total += $total;
                            echo "<tr>
                                <td style ='word-break:break-all;'>$item->pegawai_nip</td>
                                <td style ='word-break:break-all;'>$item->pegawai_name</td>
                                <td style ='word-break:break-all;'>$item->jumlah_hadir_biasa</td>
                                <td style ='word-break:break-all;'>$item->jumlah_absen</td>
                                <td style ='word-break:break-all;'>".number_format($item->jumlah_telat)."</td>
                                <td style ='word-break:break-all;'>$item->jumlah_jam_lembur</td>
                                <td style ='word-break:break-all;'>$item->jumlah_hari_libur</td>
                                <td style ='word-break:break-all;'>".number_format($item->uang_tunjangan)."</td>
                                <td style ='word-break:break-all;'>".number_format($item->potongan_kasbon)."</td>
                                <td style ='word-break:break-all;'>".number_format($item->pegawai_jumlah_kasbon)."</td>
                                <td style ='word-break:break-all;'>".number_format($total_gaji)."</td>
                                <td style ='word-break:break-all;'>".number_format($total_gaji_lembur)."</td>
                                <td style ='word-break:break-all;'>".number_format($total_gaji_libur)."</td>
                                <td style ='word-break:break-all;'>".number_format($total)."</td>
                            </tr>";
                        }
                        echo "<tr>
                            <td colspan='12' style='text-align:center; font-weight:bold;'>TOTAL JUMLAH</td>
                            <td>".number_format($sum_total)."</td>
                        </tr>";
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: -15px; left: 0;">
                <p class="company_name" style="font-size:9px; padding-top:10px;">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
      </div>
  </body>
<script>
  window.print();
</script>
</html>
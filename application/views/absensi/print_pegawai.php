
<!DOCTYPE HTML>
<html>
  <head>
    <title>Data Pegawai</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        @media print {
            table { page-break-after:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            td    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
        }
        .table-condensed{
            font-size: 10px;
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom:30px; text-align:center">
                <span class="company_name" style="font-weight:bold;">Data Pegawai</span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Nomor Telepon</th>
                            <th>Jabatan</th>
                            <th>Tanggal Masuk</th>
                            <th>Jumlah Kasbon</th>
                            <th>Upah Harian</th>
                            <th>Upah Bulanan</th>
                            <th>Upah Lembur</th>
                            <th>Upah Libur</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($pegawais as $item){
                            echo "<tr>
                                <td style ='word-break:break-all;'>$item->nip</td>
                                <td style ='word-break:break-all;'>$item->nama</td>
                                <td style ='word-break:break-all;'>$item->alamat</td>
                                <td style ='word-break:break-all;'>$item->telepon</td>
                                <td style ='word-break:break-all;'>$item->jabatan</td>
                                <td style ='word-break:break-all;'>".date_format(date_create($item->tanggal_masuk), "d-m-Y")."</td>
                                <td style ='word-break:break-all;'>$item->jumlah_kasbon_formatted</td>
                                <td style ='word-break:break-all;'>$item->upah_harian_formatted</td>
                                <td style ='word-break:break-all;'>$item->upah_bulanan_formatted</td>
                                <td style ='word-break:break-all;'>$item->upah_lembur_formatted</td>
                                <td style ='word-break:break-all;'>$item->upah_libur_formatted</td>
                            </tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: -15px; left: 0;">
                <p class="company_name" style="font-size:9px; padding-top:10px;">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
        
        
      </div>
  </body>
<script>
  window.print();
</script>
</html>
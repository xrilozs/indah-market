<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Histori Absensi
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-history"></i> Histori Absensi</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row" style="margin-bottom:10px;">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <a href="#" id="histori_absensi_print" class="btn btn-default"  target="_blank">
          <i class="fa fa-save" aria-hidden="true"></i> Print
        </a>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="histori_absensi_update_section">
        <button id="histori_absensi_update" class="btn btn-primary  pull-right" data-toggle="modal" data-target="#update-absensi-modal">
          <i class="fa fa-history" aria-hidden="true"></i> Update
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="form-group">
          <label>Tanggal Periode absensi: </label>
          <input type="text" class="form-control monthly-datepicker" name="filter_tanggal" id="filter_tanggal">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="histori_absensi_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>NIP</th>
              <th>Nama Pegawai</th>
              <th>Jumlah Akumulasi Hadir</th>
              <th>Jumlah Akumulasi Libur</th>
              <th>Jumlah Akumulasi Jam Lembur</th>
              <th>Jumlah Akumulasi Telat</th>
              <th>Jumlah Akumulasi Absen</th>
              <th>Tanggal Periode Absensi</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div id="update-absensi-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Histori Absensi</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="update-absensi-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk mengupdate histori absensi?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-primary" id="update-absensi-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

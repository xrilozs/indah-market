<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kasbon Pegawai
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Kasbon Pegawai</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row" style="margin-bottom:10px;">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <a href="#" id="kasbon_print" class="btn btn-default"  target="_blank">
          <i class="fa fa-save" aria-hidden="true"></i> Print
        </a>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create-kasbon-modal">
          <i class="fa fa-plus" aria-hidden="true"></i> Tambah Kasbon
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-12">
        <div class="form-group">
          <label>Tanggal Kasbon awal: </label>
          <input type="text" class="form-control datepicker" name="filter_tanggal_awal" id="filter_tanggal_awal">
        </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="form-group">
          <label>Tanggal Kasbon akhir: </label>
          <input type="text" class="form-control datepicker" name="filter_tanggal_akhir" id="filter_tanggal_akhir">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="kasbon_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>NIP</th>
              <th>Nama Pegawai</th>
              <th>Jumlah Kasbon</th>
              <th>Alasan Kasbon</th>
              <th>Potong Mingguan</th>
              <th>Potong Bulanan</th>
              <th>Tanggal Kasbon</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="create-kasbon-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Buat Kasbon</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="create-kasbon-form" action="<?php echo base_url('backend/create_kasbon_pegawai_process');?>">
            <div class="row">
              <div id="create-kasbon-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label for="pegawai_id_create">Pegawai:</label>
                  <select class="form-control list-pegawai" name="pegawai_id" id="pegawai_id_create"></select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Kasbon: </label>
                  <input type="text" name="jumlah_kasbon" class="form-control input_currency" value="0" placeholder="Jumlah Kasbon.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Alasan Kasbon: </label>
                  <textarea name="alasan_kasbon" class="form-control" placeholder="Alasan Kasbon.." required></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Potong Mingguan: </label>
                  <input type="text" name="potong_mingguan" class="form-control input_currency" placeholder="Potongan Mingguan.." value="0" required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Potong Bulanan: </label>
                  <input type="text" class="form-control input_currency" name="potong_bulanan" placeholder="Potong Bulanan.." value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tanggal Kasbon: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kasbon" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <hr>
              </div>
              <div class="col-lg-12 col-md-12" id="keyword-create-field">
                <div class="form-group">
                  <label>Keyword (keyword dibutuhkan untuk verifikasi): </label>
                  <input type="password" class="form-control" id="keyword-create" name="keyword" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="create-kasbon-button">Tambah Kasbon</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="update-kasbon-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Absensi</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="update-kasbon-form" action="<?php echo base_url('backend/update_kasbon_pegawai_process');?>">
            <div class="row">
              <div id="update-kasbon-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <input type="hidden" name="id" class="form-control" required>
                  <label for="pegawai_id_update">Pegawai:</label>
                  <select class="form-control list-pegawai" name="pegawai_id" id="pegawai_id_update" required></select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Kasbon: </label>
                  <input type="text" name="jumlah_kasbon" class="form-control input_currency" value="0" placeholder="Jumlah Kasbon.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Alasan Kasbon: </label>
                  <textarea name="alasan_kasbon" class="form-control" placeholder="Alasan Kasbon.." required></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Potong Mingguan: </label>
                  <input type="text" name="potong_mingguan" class="form-control input_currency" placeholder="Potongan Mingguan.." value="0" required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Potong Bulanan: </label>
                  <input type="text" class="form-control input_currency" name="potong_bulanan" placeholder="Potong Bulanan.." value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tanggal Kasbon: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kasbon" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <hr>
              </div>
              <div class="col-lg-12 col-md-12" id="keyword-update-field">
                <div class="form-group">
                  <label>Keyword (keyword dibutuhkan untuk verifikasi): </label>
                  <input type="password" class="form-control" id="keyword-update" name="keyword" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="update-absensi-button">Update Absensi</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="delete-kasbon-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Kasbon</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-kasbon-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk <b>menghapus data kasbon</b> yang Anda pilih?</p>
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <hr>
            </div>
            <form id="delete-kasbon-form">
            <div class="col-lg-12 col-md-12" id="keyword-delete-field">
              <div class="form-group">
                <label>Keyword (keyword dibutuhkan untuk verifikasi): </label>
                <input type="password" class="form-control" name="keyword" id="keyword-delete" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-danger" id="delete-kasbon-button">IYA</button>
          </form>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

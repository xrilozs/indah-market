<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Absensi
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text-o"></i> Absensi</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row" style="margin-bottom:10px;">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <a href="#" id="absensi_print" class="btn btn-default"  target="_blank">
          <i class="fa fa-save" aria-hidden="true"></i> Print Upah
        </a>
        <a href="#" id="rekap_absensi_print" class="btn btn-default"  target="_blank">
          <i class="fa fa-print" aria-hidden="true"></i> Print Rekap
        </a>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create-absensi-modal">
          <i class="fa fa-plus" aria-hidden="true"></i> Tambah Absensi
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="form-group">
          <label>Tanggal Periode absensi: </label>
          <input type="text" class="form-control datepicker" name="filter_tanggal" id="filter_tanggal">
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="form-group">
          <label>Jabatan: </label>
          <select name="filter_jabatan" id="filter_jabatan" class="form-control" >
            <option value="">Semua Jabatan</option>
            <option value="Ka.Bag">Ka.Bag</option>
            <option value="Produksi">Produksi</option>
            <option value="Kantor">Kantor</option>
            <option value="Sekuriti">Sekuriti</option>
            <option value="Supir Kenek">Supir Kenek</option>
            <option value="Lain Lain">Lain Lain</option>
          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="absensi_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>NIP</th>
              <th>Nama Pegawai</th>
              <th>Jumlah Hadir Biasa</th>
              <th>Jumlah Hari Libur</th>
              <th>Jumlah Jam Lembur</th>
              <th>Jumlah Uang Telat</th>
              <th>Jumlah Hari Telat</th>
              <th>Jumlah Absen</th>
              <th>Potongan Kasbon</th>
              <th>Uang Tunjangan</th>
              <th>Tanggal Periode Absensi</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="create-absensi-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Buat Absensi</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="create-absensi-form" action="<?php echo base_url('backend/create_absensi_process');?>">
            <div class="row">
              <div id="create-absensi-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label for="pegawai_id_create">Pegawai:</label>
                  <select class="form-control list-pegawai" name="pegawai_id" id="pegawai_id_create"></select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Hadir Biasa: </label>
                  <input type="text" name="jumlah_hadir_biasa" class="form-control input_decimal input_hadir" placeholder="Jumlah hadir biasa.. (mis: 3.5)" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Hari Libur: </label>
                  <input type="text" name="jumlah_hari_libur" class="form-control input_decimal" placeholder="Jumlah hari libur.. (mis: 3.5)" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Jam Lembur: </label>
                  <input type="number" name="jumlah_jam_lembur" class="form-control" placeholder="Jumlah jam lembur.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Uang Telat: </label>
                  <input type="text" class="form-control input_currency" name="jumlah_telat" placeholder="Jumlah telat.." value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Hari Telat: </label>
                  <input type="number" class="form-control " name="jumlah_telat_hari" placeholder="Jumlah telat.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Absen: </label>
                  <input type="text" class="form-control input_decimal input_absen" name="jumlah_absen" placeholder="Jumlah absen.." value="0" min="0" pattern="^\d*(\.\d{0,2})?$" required readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Potongan Kasbon: </label>
                  <input type="text" class="form-control input_currency" id="potongan_kasbon_create" name="potongan_kasbon" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Uang Tunjangan: </label>
                  <input type="text" class="form-control input_currency" name="uang_tunjangan" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tanggal Periode Absensi: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_periode_absensi" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="create-absensi-button">Tambah Absensi</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="update-absensi-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Absensi</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="update-absensi-form" action="<?php echo base_url('backend/update_absensi_process');?>">
            <div class="row">
              <div id="update-absensi-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <input type="hidden" name="id" class="form-control" required>
                  <label for="pegawai_id_update">Pegawai:</label>
                  <select class="form-control list-pegawai" name="pegawai_id" id="pegawai_id_update" required></select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Hadir Biasa: </label>
                  <input type="text" name="jumlah_hadir_biasa" class="form-control input_decimal input_hadir" placeholder="Jumlah hadir biasa.. (mis: 3.5)" pattern="^\d*(\.\d{0,2})?$" disabled>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Hari Libur: </label>
                  <input type="text" name="jumlah_hari_libur" class="form-control input_decimal" placeholder="Jumlah hari libur.. (mis: 3.5)" pattern="^\d*(\.\d{0,2})?$" disabled>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Jam Lembur: </label>
                  <input type="number" name="jumlah_jam_lembur" class="form-control" placeholder="Jumlah jam lembur.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Uang Telat: </label>
                  <input type="text" class="form-control input_currency" name="jumlah_telat" placeholder="Jumlah uang telat.." value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Hari Telat: </label>
                  <input type="number" class="form-control" name="jumlah_telat_hari" placeholder="Jumlah hari telat.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Absen: </label>
                  <input type="text" class="form-control input_decimal input_absen" name="jumlah_absen" placeholder="Jumlah absen.." pattern="^\d*(\.\d{0,2})?$" required readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Potongan Kasbon: </label>
                  <input type="text" class="form-control input_currency" id="potongan_kasbon_update" name="potongan_kasbon" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Uang Tunjangan: </label>
                  <input type="text" class="form-control input_currency" name="uang_tunjangan" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tanggal Periode Absensi: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_periode_absensi" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="update-absensi-button">Update Absensi</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="delete-absensi-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Absensi</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-absensi-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk <b>menghapus data absensi</b> yang Anda pilih?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-danger" id="delete-absensi-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

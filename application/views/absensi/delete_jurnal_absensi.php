<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Hapus Jurnal Absensi
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-files-o"></i> Hapus Jurnal Absensi</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal Awal absensi: </label>
              <input type="text" class="form-control datepicker" name="filter_tanggal_awal" id="filter_tanggal_awal">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal Akhir absensi: </label>
              <input type="text" class="form-control datepicker" name="filter_tanggal_akhir" id="filter_tanggal_akhir">
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="row">
          <div class="col-lg-12">
            <button id="delete-jurnal-absensi-toggle" class="btn btn-danger pull-right" data-toggle="modal" data-target="#delete-jurnal-absensi-modal">
              <i class="fa fa-archive" aria-hidden="true"></i> Hapus
            </button>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="jurnal_absensi_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>Pegawai</th>
              <th>Shift</th>
              <th>Jam Masuk</th>
              <th>Jam Pulang</th>
              <th>Tanggal Absensi</th>
              <th>Record ID</th>
              <th>Supervisor</th>
              <th>Rekap</th>
              <th>Lembur</th>
              <th>Terlambat</th>
              <th>Weekend</th>
              <th>Jam Terlambat</th>
              <th>Jam Lembur</th>
              <th>Tanggal</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div id="delete-jurnal-absensi-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Rekap Absensi</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-jurnal-absensi-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk <b>menghapus data absensi</b> pada jangka waktu yang Anda pilih?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-primary" id="delete-jurnal-absensi-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pegawai
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-user"></i> Pegawai</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row" style="margin-bottom:10px;">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <a href="#" id="pegawait_print" class="btn btn-default"  target="_blank">
          <i class="fa fa-save" aria-hidden="true"></i> Print
        </a>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <button class="btn btn-primary pull-right" id="create-pegawai-toggle" data-toggle="modal" data-target="#create-pegawai-modal">
          <i class="fa fa-plus" aria-hidden="true"></i> Tambah Pegawai
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="table-responsive">
          <table id="pegawai_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>NIP</th>
              <th>Nama</th>
              <th>Jabatan</th>
              <th>Tipe Absen</th>
              <th>Tanggal Masuk</th>
              <th>Jumlah Kasbon</th>
              <th>Upah Harian</th>
              <th>Upah Bulanan</th>
              <th>Upah Lembur</th>
              <th>Upah Libur</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="create-pegawai-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Buat Pegawai</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="create-pegawai-form" action="<?php echo base_url('backend/create_pegawai_process');?>">
            <div class="row">
              <div id="create-pegawai-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Nama: </label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Alamat: </label>
                  <textarea class="form-control" name="alamat" placeholder="Alamat.." required></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Telepon: </label>
                  <input type="text" name="telepon" class="form-control" placeholder="Telepon.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jabatan: </label>
                  <!-- <input type="text" name="jabatan" class="form-control" placeholder="Jabatan.." required> -->
                  <select name="jabatan" id="jabatan-create" class="form-control" requred>
                    <option value="Ka.Bag">Ka.Bag</option>
                    <option value="Produksi">Produksi</option>
                    <option value="Kantor">Kantor</option>
                    <option value="Sekuriti">Sekuriti</option>
                    <option value="Supir Kenek">Supir Kenek</option>
                    <option value="Lain Lain">Lain Lain</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tipe Absen: </label>
                  <select name="role" id="role-create" class="form-control" requred>
                    <option value="PEGAWAI">PEGAWAI</option>
                    <option value="SUPERVISOR">SUPERVISOR</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tanggal Masuk: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_masuk" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Kasbon: </label>
                  <input type="text" class="form-control input_currency" name="jumlah_kasbon" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label>Upah Harian: </label>
                  <input type="text" class="form-control input_currency" name="upah_harian" value="0" required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label>Upah Bulanan: </label>
                  <input type="text" class="form-control input_currency" name="upah_bulanan" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label>Upah Lembur: </label>
                  <input type="text" class="form-control input_currency" name="upah_lembur" value="0" required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label>Upah Libur: </label>
                  <input type="text" class="form-control input_currency" name="upah_libur" value="0" required>
                </div>
              </div>
            </div>
            <div class="row" id="password-create-section" style="display:none;">
              <div class="col-lg-12">
                <div class="form-group">
                  <label>Password: </label>
                  <input type="text" class="form-control" name="password" id="password-create" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="create-pegawai-button">Tambah Pegawai</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="update-pegawai-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Pegawai</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="update-pegawai-form">
            <div class="row">
              <div id="update-pegawai-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Nama: </label>
                  <input type="hidden" name="id" class="form-control" required>
                  <input type="text" name="nama" class="form-control" placeholder="Nama.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Alamat: </label>
                  <textarea class="form-control" name="alamat" placeholder="Alamat.." required></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Telepon: </label>
                  <input type="text" name="telepon" class="form-control" placeholder="Telepon.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jabatan: </label>
                  <!-- <input type="text" name="jabatan" class="form-control" placeholder="Jabatan.." required> -->
                  <select name="jabatan" id="jabatan-update" class="form-control" requred>
                    <option value="Ka.Bag">Ka.Bag</option>
                    <option value="Produksi">Produksi</option>
                    <option value="Kantor">Kantor</option>
                    <option value="Sekuriti">Sekuriti</option>
                    <option value="Supir Kenek">Supir Kenek</option>
                    <option value="Lain Lain">Lain Lain</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tipe Absen: </label>
                  <select name="role" id="role-update" class="form-control" requred>
                    <option value="PEGAWAI">PEGAWAI</option>
                    <option value="SUPERVISOR">SUPERVISOR</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tanggal Masuk: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_masuk" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Kasbon: </label>
                  <input type="text" class="form-control input_currency" name="jumlah_kasbon" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label>Upah Harian: </label>
                  <input type="text" class="form-control input_currency" name="upah_harian" value="0" required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="form-group">
                  <label>Upah Bulanan: </label>
                  <input type="text" class="form-control input_currency" name="upah_bulanan" value="0" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Upah Lembur: </label>
                  <input type="text" class="form-control input_currency" name="upah_lembur" value="0" required>
                </div>
              </div>
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Upah Libur: </label>
                  <input type="text" class="form-control input_currency" name="upah_libur" value="0" required>
                </div>
              </div>
            </div>
            <div class="row" id="password-update-section" style="display:none;">
              <div class="col-lg-12">
                <div class="form-group">
                  <label>Password: </label>
                  <input type="text" class="form-control" name="password" id="password-update" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <hr>
              </div>
              <div class="col-lg-12 col-md-12" id="keyword-update-field">
                <div class="form-group">
                  <label>Keyword (keyword dibutuhkan untuk verifikasi): </label>
                  <input type="password" class="form-control" name="keyword" id="keyword-update" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="update-pegawai-button">Update Pegawai</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="delete-pegawai-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hapus Pegawai</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-pegawai-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk <b>menghapus data pegawai</b> yang Anda pilih?</p>
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <hr>
            </div>
            <form method="post" id="delete-pegawai-form">
            <div class="col-lg-12 col-md-12" id="keyword-delete-field">
              <div class="form-group">
                <label>Keyword (keyword dibutuhkan untuk verifikasi): </label>
                <input type="password" class="form-control" name="keyword" id="keyword-delete" required>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-danger" id="delete-pegawai-button">IYA</button>
          </form>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

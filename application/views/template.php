<?php
$cart_count = 0;
if(!empty($this->session->userdata('cart'))) $cart_count = count($this->session->userdata('cart'));
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OLIN | <?php echo $title;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo ASSETS;?>css/custom.css?v=<?php echo date('s').rand();?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>bower_components/morris.js/morris.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Time Picker -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>plugins/timepicker/bootstrap-timepicker.min.css">
  
  <!-- Datatables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo ASSETS;?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
  <!-- jQuery 3 -->
  <script src="<?php echo ASSETS;?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <!-- jQuery Data Tables -->
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>
  <!--Selectize-->
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <!-- <link href="https://raw.githubusercontent.com/t0m/select2-bootstrap-css/bootstrap3/select2-bootstrap.css" rel="stylesheet" /> -->
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('dashboard');?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><?php echo COMPANY_NAME_SHORT;?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?php echo COMPANY_NAME;?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo ASSETS;?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $member->name;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo ASSETS;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                <?php echo $member->name;?>
                  <small>Member since <?php
$date = new DateTime($member->datecreated);
echo $date->format('M-Y');
?>
</small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('myprofile');?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <button id="logout-button" class="btn btn-default btn-flat">Sign out</button>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo ASSETS;?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $member->name;?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $this->load->view('sidebar');?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <?php $this->load->view($content); ?>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="<?php echo base_url();?>"><?php echo COMPANY_NAME;?></a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

  <!-- DatePicker -->
<script src="<?php echo ASSETS;?>js/bootstrap-datepicker.min.js"></script>

<!-- jQuery UI 1.11.4 -->
<script src="<?php echo ASSETS;?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo ASSETS;?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo ASSETS;?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo ASSETS;?>bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo ASSETS;?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo ASSETS;?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo ASSETS;?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo ASSETS;?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo ASSETS;?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- timepicker -->
<script src="<?php echo ASSETS;?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo ASSETS;?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo ASSETS;?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo ASSETS;?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo ASSETS;?>dist/js/adminlte.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<!-- Custom -->
<script>
  const CURRENT_PAGE = `<?= $content ? $content : null; ?>`;
  const BASE_URL = `<?=BASE_URL;?>`;
</script>
<script src="<?php echo ASSETS;?>js/custom.js?<?php echo date('s').rand();?>"></script>
<script src="<?php echo ASSETS;?>js/menu.js?<?php echo date('s').rand();?>"></script>
<?= $title == "Surat Pesanan Langganan" ? '<script src="'.ASSETS.'js/spl.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "SPL Titipan" ? '<script src="'.ASSETS.'js/spl_titipan.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "SPL - Burn" ? '<script src="'.ASSETS.'js/spl_burn.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "SPL - Laporan Bulanan" ? '<script src="'.ASSETS.'js/spl_monthly_report.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "SPL - Hapus Data" ? '<script src="'.ASSETS.'js/spl_delete.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "SPL - Limit" ? '<script src="'.ASSETS.'js/spl_limit.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Pegawai" ? '<script src="'.ASSETS.'js/pegawai.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Absensi" ? '<script src="'.ASSETS.'js/absensi.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Histori Absensi" ? '<script src="'.ASSETS.'js/histori_absensi.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Kasbon Pegawai" ? '<script src="'.ASSETS.'js/kasbon_pegawai.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Setor Barang Jadi" ? '<script src="'.ASSETS.'js/setor_barang_jadi.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Terima Barang Jadi" ? '<script src="'.ASSETS.'js/terima_barang_jadi.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Data Barang" ? '<script src="'.ASSETS.'js/data_barang.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Keyword" ? '<script src="'.ASSETS.'js/keyword.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Office Hour" ? '<script src="'.ASSETS.'js/office_hour.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Jurnal Absensi" ? '<script src="'.ASSETS.'js/jurnal_absensi.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Delete Jurnal Absensi" ? '<script src="'.ASSETS.'js/delete_jurnal_absensi.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Site Location" ? '<script src="'.ASSETS.'js/site_location.js?v='.date('s').rand().'"></script>' : ''; ?>
<?= $title == "Hari Libur" ? '<script src="'.ASSETS.'js/hari_libur.js?v='.date('s').rand().'"></script>' : ''; ?>

</body>
</html>

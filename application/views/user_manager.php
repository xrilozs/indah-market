<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    User
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-user"></i> User Manager</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
         <?php if($this->session->flashdata('message')=='success_delete') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Penghapusan User Berhasil!.
    </div>
    ';
  }
  if($this->session->flashdata('message')=='error_delete') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error!</strong> Penghapusan User Gagal!.
    </div>
    ';
  }
        ?>
      </div>
      <div class="col-md-12">
        <button class="btn btn-success pull-right" data-toggle="modal" data-target="#create_modal">
          Create User
        </button>
        <br>
        <br>
      </div>
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="user_manager_table" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Phone</th>
              <th>Type</th>
              <th>Site Code</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $member = get_all_member_list();
            if($member!=false){
              $no = 0;
              foreach($member->result() as $item){
                $no +=1;
                $type = "None";
                if($item->type=="super_admin") $type = "Super Admin";
                if($item->type=="proret") $type = "Produksi Return";
                if($item->type=="hut_put") $type = "Hutang Piutang";
                if($item->type=="delivery") $type = "Delivery";
                if($item->type=="adm_prod") $type = "Admin Produk";
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$item->name.'</td>
                <td>'.$item->username.'</td>
                <td>'.$item->phone.'</td>

                <td>'.$type.'</td>
                <td>'.strtoupper($item->site_code).'</td>
                <td><button url="'.base_url('home/edit_user/'.$item->id).'" class="btn btn-success edit-user" data-toggle="modal" data-target="#edit_user_modal">Edit</button>
                <button url="'.base_url('backend/delete_user/'.$item->id).'" class="btn btn-danger delete-ajax" " data-toggle="modal" data-target="#delete_user_modal">Delete</button></td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
  <div id="delete_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete User</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus user ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
        </div>
      </div>
    </div>
  </div>
  <div id="edit_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit User</h4>
        </div>
        <div class="modal-body">
          <form id="submit-form-edit" data-url="<?php echo base_url('backend/edit_user_process');?>">
            <div class="edit_modal_form_warning">
            </div>
            <div class="form-group">
              <label>Name : </label>
               <input id="edit_id" type="hidden" name="id" class="form-control">
              <input id="edit_name" type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
              <label>Username : </label>
              <input id="edit_username" type="text" name="username" class="form-control">
            </div>
            <div class="form-group">
              <label>Phone : </label>
              <input id="edit_phone" type="text" name="phone" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
            </div>
            <div class="form-group">
              <label>Type : </label>
              <select id="edit_type" name="type" class="form-control">
                <option value="">
                  Choose..
                </option>
                <option value="proret">
                  Produksi & Return
                </option>
                <option value="hut_put">
                  Hutang & Piutang
                </option>
                <option value="delivery">
                 Delivery
                </option>
                <option value="adm_prod">
                 Admin Produk
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Site Code : </label>
              <select name="site_code" id="edit_site_code" class="form-control">
                <option value="">
                  Choose..
                </option>
                <option value="none">
                  None
                </option>
                <option value="a">
                  A
                </option>
                <option value="c">
                  C
                </option>
                <option value="r">
                  R
                </option>
                <option value="s">
                  S
                </option>
                <option value="t">
                  T
                </option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" >Save Changes</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="create_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create User</h4>
        </div>
        <div class="modal-body">
          <form id="submit-form" data-url="<?php echo base_url('backend/create_user_process');?>">
            <div class="create_modal_form_warning">
            </div>
            <div class="form-group">
              <label>Name : </label>
              <input type="text" name="name" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Username : </label>
              <input type="text" name="username" class="form-control" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Password : </label>
              <input type="password" name="password" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Phone : </label>
              <input type="text" name="phone" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required>
            </div>
            <div class="form-group">
              <label>Type : </label>
              <select name="type" class="form-control" required>
                <option value="">
                  Choose..
                </option>
                <option value="proret">
                  Produksi & Return
                </option>
                <option value="hut_put">
                  Hutang & Piutang
                </option>
                <option value="delivery">
                  Delivery
                </option>
                <option value="super_admin">
                  Super Admin
                </option>
                <option value="adm_prod">
                 Admin Produk
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Site Code : </label>
              <select name="site_code" class="form-control">
                <option value="">
                  Choose..
                </option>
                <option value="none">
                  None
                </option>
                <option value="a">
                  A
                </option>
                <option value="c">
                  C
                </option>
                <option value="r">
                  R
                </option>
                <option value="s">
                  S
                </option>
                <option value="t">
                  T
                </option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Create User</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- /.content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    My Profile
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <?php
  if($this->session->flashdata('message')=='success') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Perubahan berhasil disimpan!.
    </div>
    ';
  }
  if($this->session->flashdata('message')=='error') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Gagal!</strong> Perubahan gagal disimpan!.
    </div>
    ';
  }
  ?>
</div>
<div class="col-md-12">
  <form method="post" action="<?php echo base_url('backend/profile_process');?>">
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="name" id="name" class="form-control" value="<?php echo $member->name;?>">
  </div>
  <div class="form-group">
    <label>No.HP</label>
    <input type="text" name="phone" id="phone" class="form-control" value="<?php echo $member->phone;?>">
  </div>
    <div class="form-group">
    <label>Password</label><br>
    <input type="button"  class="btn btn-primary" value="Ganti Password"  data-toggle="modal" data-target="#edit_password_modal">
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-block">Simpan Perubahan</button>
  </div>
</form>
</div>
</section>
<div id="edit_password_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ganti Password</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="edit_password_form" action="<?php echo base_url('backend/edit_password_process');?>">
            <div id="edit_password_form_warning">
            </div>
            <div class="form-group">
              <label>Password Lama : </label>
              <input id="old_password" type="password" name="old_password" class="form-control">
            </div>
            <div class="form-group">
              <label>Password Baru : </label>
              <input id="new_password" type="password" name="new_password" class="form-control">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success submit-ajax" form-id="edit_password_form"  warning_message="edit_password_form_warning">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
<!-- /.content -->

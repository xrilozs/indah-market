<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    SPL - Monthly Report
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text"></i> SPL - Monthly Report</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
              <label>Tanggal: </label>
              <input type="text" class="form-control monthly-datepicker" name="filter_tanggal" id="filter_tanggal">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group">
              <label>Status SPL: </label>
              <select name="status_spl" id="status_spl" class="form-control" required>
                <option value="ALL">
                  Semua
                </option>
                <option value="SELESAI">
                  Selesai
                </option>
                <option value="BELUM">
                  Belum Selesai
                </option>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;">
            <a href="#" class="btn btn-lg btn-default pull-right" id="print-monthly-spl-button" target="_blank">
              <i class="fa fa-save" aria-hidden="true"></i> Print
            </a>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px;">
            <a href="#" class="btn btn-lg btn-primary pull-right" id="export-monthly-spl-button" target="_blank">
              <i class="fa fa-arrow-down" aria-hidden="true"></i> Export
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table id="spl_manager_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>No. SPL</th>
              <th>Customer</th>
              <th>Customer Rating</th>
              <th>Marketing</th>
              <th>Hasil Rumus</th>
              <th>Jumlah Order</th>
              <th>Harga Satuan</th>
              <th>Bahan</th>
              <th>Ukuran</th>
              <th>Komposisi</th>
              <th>Jumlah Roll</th>
              <th>Keterangan</th>
              <th>Status Persetujuan</th>
              <th>Status SPL</th>
              <th>Tanggal SPL</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="delivery-spl-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delivery SPL</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="delivery-spl-form">
            <div class="row">
              <div id="delivery-spl-warning">
              </div>
            </div>
            <div class="row">
              <input type="hidden" name="id" class="form-control" readonly>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer : </label>
                  <select name="customer_id"  id="customer_id_spl_delivery" class="form-control" style="pointer-events:none">
                    <?php
                      $resp = get_all_customer();
                      if($resp){
                        $customers = $resp->result();
                        $no = 0;
                        foreach($customers as $customer){
                          $no +=1;
                          echo '<option value="'.$customer->id.'">
                            '.$customer->name.'
                          </option>';
                        }
                      }else{
                        echo '<option value="">
                          Tidak ada kustomer
                        </option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" placeholder="Lebar Bahan.." readonly>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" placeholder="Tebal Bahan.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" placeholder="Ukuran Potong.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Satuan: </label>
                  <select name="satuan" id="satuan_spl_delivery" class="form-control" style="pointer-events:none">
                    <option value="KG">
                      Kilogram
                    </option>
                    <option value="LB">
                      Lembar
                    </option>
                    <option value="RL">
                      Roll
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan.." readonly></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Order: </label>
                  <input class="form-control input_decimal" name="jumlah_order" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control input_decimal" name="harga_satuan" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Komposisi: </label>
                  <input type="text" class="form-control" name="komposisi" placeholder="Komposisi.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control input_decimal" name="jumlah_roll" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input class="form-control input_decimal" name="hasil_rumus" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-6">
                <div class="form-group">
                  <label>Tanggal Kirim 1: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kirim1" required>
                </div>
                <div class="form-group">
                  <label>Jumlah Kirim 1: </label>
                  <input class="form-control input_decimal" name="jumlah_kirim1" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
                <div class="form-group">
                  <label>Surat Jalan 1: </label>
                  <input type="text" class="form-control" name="sjkirim1" placeholder="Surat Jalan 1.." required>
                </div>
              </div>
              <div class="col-lg-4 col-md-6">
                <div class="form-group">
                  <label>Tanggal Kirim 2: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kirim2">
                </div>
                <div class="form-group">
                  <label>Jumlah Kirim 2: </label>
                  <input class="form-control input_decimal" name="jumlah_kirim2" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$">
                </div>
                <div class="form-group">
                  <label>Surat Jalan 2: </label>
                  <input type="text" class="form-control" name="sjkirim2" placeholder="Surat Jalan 2..">
                </div>
              </div>
              <div class="col-lg-4 col-md-6">
                <div class="form-group">
                  <label>Tanggal Kirim 3: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kirim3">
                </div>
                <div class="form-group">
                  <label>Jumlah Kirim 3: </label>
                  <input class="form-control input_decimal" name="jumlah_kirim3" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$">
                </div>
                <div class="form-group">
                  <label>Surat Jalan 3: </label>
                  <input type="text" class="form-control" name="sjkirim3" placeholder="Surat Jalan 3..">
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="delivery-spl-button">Delivery</button>
          </form>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

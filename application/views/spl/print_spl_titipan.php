<html>
  <head>
    <title>SPL <?=$spl->no_spl?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  </head>
    <style>
        .half-page{
            height: 50%;
        }
        @media print {
            html, body {
                height:100%; 
                margin: 0 !important; 
                padding: 0 !important;
                overflow: hidden;
            }
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <!-- Atas -->
        <div class="row half-page">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:30px;">
                <span class="company_name" style="font-weight:bold;">PT. Indah Karunia Sukses</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:30px;">
                <div class="row pull-right">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <span style="font-weight:bold; font-size:20px;"><?=$spl->spl_status;?></span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <span style="font-weight:bold; font-size:20px;" class="pull-right">ASLI</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        Site Awal: <?=$spl_titipan->site_awal;?><br>
                        Tanggal: <?=date_format(date_create($spl_titipan->tanggal_spl_titipan), "d-m-Y");?><br>
                        SPL No: <?=$spl_titipan->no_spl_titipan?><br>
                        Kode: <?=strtolower(substr(str_replace(" ", "", $spl->customer_name), 0, 4));?><br>
                        H. Satuan: <?=$spl->harga_satuan?><br>
                        H. Rumus: <?=$spl->hasil_rumus?><br>
                        Keterangan: <?=$spl->keterangan?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        Jml Order: <?=$spl->jumlah_order?><br>
                        Bahan: <?=$spl->lebar_bahan?>/<?=$spl->tebal_bahan?><br>
                        Unit: <?=$spl->satuan?><br>
                        Ukuran: <?=$spl->ukuran_potong?><br>
                        Jumlah Rol: <?=$spl->jumlah_roll?><br>
                        Komposisi: <?=$spl->komposisi?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row" style="margin-right:20px; margin-left:20px;">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <span style="font-weight:bold;">MKT</span><br>
                        <span style="font-weight:bold;"><?=$spl->marketing_name?></span><br><br><br><br>
                        <span style="font-weight:bold;">TK LIPAT</span><br>
                        <span style="font-weight:bold;">SHIFT 1</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <span style="font-weight:bold;">DIR</span><br>
                        <span style="font-weight:bold;">(Approved)</span><br><br><br><br>
                        <span style="font-weight:bold;">SHIFT 2</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">Gudang</span><br><br><br><br>
                        <span style="font-weight:bold;">TK SSN</span><br>
                        <span style="font-weight:bold;">SHIFT 1</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">ADM PROD</span><br><br><br><br>
                        <span style="font-weight:bold;">SHIFT 2</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">PENGAWAS</span><br><br><br><br>
                        <span style="font-weight:bold;">TK PAK</span><br>
                        <span style="font-weight:bold;">SHIFT 1</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">PENGIRIM</span><br><br><br><br>
                        <span style="font-weight:bold;">SHIFT 2</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bawah -->
        <div class="row half-page">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:30px;">
                <span class="company_name" style="font-weight:bold;">PT. Indah Karunia Sukses</span>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-bottom:30px;">
                <span style="font-weight:bold; font-size:20px;" class="pull-right">COPY</span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        Site Awal: <?=$spl_titipan->site_awal;?><br>
                        Tanggal: <?=date_format(date_create($spl_titipan->tanggal_spl_titipan), "d-m-Y");?><br>
                        SPL No: <?=$spl_titipan->no_spl_titipan?><br>
                        Kode: <?=strtolower(substr(str_replace(" ", "", $spl->customer_name), 0, 4));?><br>
                        H. Satuan: <?=$spl->harga_satuan?><br>
                        H. Rumus: <?=$spl->hasil_rumus?><br>
                        Keterangan: <?=$spl->keterangan?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        Jml Order: <?=$spl->jumlah_order?><br>
                        Bahan: <?=$spl->lebar_bahan?>/<?=$spl->tebal_bahan?><br>
                        Unit: <?=$spl->satuan?><br>
                        Ukuran: <?=$spl->ukuran_potong?><br>
                        Jumlah Rol: <?=$spl->jumlah_roll?><br>
                        Komposisi: <?=$spl->komposisi?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row" style="margin-right:20px; margin-left:20px;">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <span style="font-weight:bold;">MKT</span><br>
                        <span style="font-weight:bold;"><?=$spl->marketing_name?></span><br><br><br><br>
                        <span style="font-weight:bold;">TK LIPAT</span><br>
                        <span style="font-weight:bold;">SHIFT 1</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <span style="font-weight:bold;">DIR</span><br>
                        <span style="font-weight:bold;">(Approved)</span><br><br><br><br>
                        <span style="font-weight:bold;">SHIFT 2</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">Gudang</span><br><br><br><br>
                        <span style="font-weight:bold;">TK SSN</span><br>
                        <span style="font-weight:bold;">SHIFT 1</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">ADM PROD</span><br><br><br><br>
                        <span style="font-weight:bold;">SHIFT 2</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">PENGAWAS</span><br><br><br><br>
                        <span style="font-weight:bold;">TK PAK</span><br>
                        <span style="font-weight:bold;">SHIFT 1</span>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <br>
                        <span style="font-weight:bold;">PENGIRIM</span><br><br><br><br>
                        <span style="font-weight:bold;">SHIFT 2</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: 0; left: 0;">
                <p class="company_name" style="font-size:9px; padding-top:10px;">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
      </div>
  </body>
<script>
  window.print();
</script>
</html>
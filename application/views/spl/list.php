<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Surat Pesanan Langganan
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text"></i> SPL</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="multiple_approve_section">
            <form id="multiple_approve">
            <div class="form-group">
              <label>Multiple Approve: </label>
              <select class="selectpicker form-control" name="list_approve" id="list_approve" multiple required>
              </select>
            </div>
            <div class="form-group">
              <button type="submit" id="multiple_approve_button" class="btn btn-info">Approve</button>
              </form>
            </div>
            <div class="form-group" id="multiple_approve_warning">
            </div>
          </div>
          <?= $member->type=='super_admin' ? '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">' : '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'; ?> 
            <div class="row" style="margin-bottom:10px;">
              <div class="col-lg-12">
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create-spl-modal">
                  <i class="fa fa-plus" aria-hidden="true"></i> Create SPL
                </button>
              </div>
            </div>
            <div class="row" id="autodone_section">
              <div class="col-lg-12">
                <button class="btn btn-info pull-right" data-toggle="modal" data-target="#autodone-spl-modal">
                  <i class="fa fa-check" aria-hidden="true"></i> Auto Done
                </button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal: </label>
              <input type="text" class="form-control datepicker" name="filter_tanggal" id="filter_tanggal">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Status SPL: </label>
              <select name="status_spl" id="status_spl" class="form-control" required>
                <option value="ALL">
                  Semua
                </option>
                <option value="SELESAI">
                  Selesai
                </option>
                <option value="BELUM">
                  Belum Selesai
                </option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="spl_manager_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>No. SPL</th>
              <th>Customer</th>
              <th>Customer Rating</th>
              <th>Marketing</th>
              <th>Hasil Rumus</th>
              <th>Jumlah Order</th>
              <th>Harga Satuan</th>
              <th>Bahan</th>
              <th>Ukuran</th>
              <th>Komposisi</th>
              <th>Jumlah Roll</th>
              <th>Keterangan</th>
              <th>Status Persetujuan</th>
              <th>Status SPL</th>
              <th>Tanggal SPL</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="delivery-spl-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delivery SPL</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="delivery-spl-form">
            <div class="row">
              <div id="delivery-spl-warning">
              </div>
            </div>
            <div class="row">
              <input type="hidden" name="id" class="form-control" readonly>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer : </label>
                  <select name="customer_id"  id="customer_id_spl_delivery" class="form-control" style="pointer-events:none">
                    <option value="">--Pilih Kustomer--</option>
                    <?php
                      $resp = get_all_customer();
                      if($resp){
                        $customers = $resp->result();
                        $no = 0;
                        foreach($customers as $customer){
                          $no +=1;
                          echo '<option value="'.$customer->id.'">
                            '.$customer->name.'
                          </option>';
                        }
                      }else{
                        echo '<option value="">
                          Tidak ada kustomer
                        </option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" placeholder="Lebar Bahan.." readonly>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" placeholder="Tebal Bahan.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" placeholder="Ukuran Potong.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Satuan: </label>
                  <select name="satuan" id="satuan_spl_delivery" class="form-control" style="pointer-events:none">
                    <option value="KG">
                      Kilogram
                    </option>
                    <option value="LB">
                      Lembar
                    </option>
                    <option value="RL">
                      Roll
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan.." readonly></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Order: </label>
                  <input class="form-control input_decimal" name="jumlah_order" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control input_decimal" name="harga_satuan" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Komposisi: </label>
                  <input type="text" class="form-control" name="komposisi" placeholder="Komposisi.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control input_decimal" name="jumlah_roll" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input class="form-control input_decimal" name="hasil_rumus" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-6">
                <div class="form-group">
                  <label>Tanggal Kirim 1: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kirim1" required>
                </div>
                <div class="form-group">
                  <label>Jumlah Kirim 1: </label>
                  <input class="form-control input_decimal" name="jumlah_kirim1" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
                <div class="form-group">
                  <label>Surat Jalan 1: </label>
                  <input type="text" class="form-control" name="sjkirim1" placeholder="Surat Jalan 1.." required>
                </div>
              </div>
              <div class="col-lg-4 col-md-6">
                <div class="form-group">
                  <label>Tanggal Kirim 2: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kirim2">
                </div>
                <div class="form-group">
                  <label>Jumlah Kirim 2: </label>
                  <input class="form-control input_decimal" name="jumlah_kirim2" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$">
                </div>
                <div class="form-group">
                  <label>Surat Jalan 2: </label>
                  <input type="text" class="form-control" name="sjkirim2" placeholder="Surat Jalan 2..">
                </div>
              </div>
              <div class="col-lg-4 col-md-6">
                <div class="form-group">
                  <label>Tanggal Kirim 3: </label>
                  <input type="text" class="form-control datepicker" name="tanggal_kirim3">
                </div>
                <div class="form-group">
                  <label>Jumlah Kirim 3: </label>
                  <input class="form-control input_decimal" name="jumlah_kirim3" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$">
                </div>
                <div class="form-group">
                  <label>Surat Jalan 3: </label>
                  <input type="text" class="form-control" name="sjkirim3" placeholder="Surat Jalan 3..">
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="delivery-spl-button">Delivery</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  
  <div id="recreate-spl-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Recreate SPL</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="recreate-spl-form">
            <div class="row">
              <div id="recreate-spl-warning">
              </div>
            </div>
            <div class="row">
              <input type="hidden" name="id" class="form-control" required>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer : </label>
                  <select name="customer_id"  id="customer_id_spl_recreate" class="form-control" required>
                    <option value="">--Pilih Kustomer--</option>
                    <?php
                      $resp = get_all_customer();
                      if($resp){
                        $customers = $resp->result();
                        $no = 0;
                        foreach($customers as $customer){
                          $no +=1;
                          echo '<option value="'.$customer->id.'">
                            '.$customer->name.'
                          </option>';
                        }
                      }else{
                        echo '<option value="">
                          Tidak ada kustomer
                        </option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" placeholder="Lebar Bahan.." required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" placeholder="Tebal Bahan.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" placeholder="Ukuran Potong.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Satuan: </label>
                  <select name="satuan" id="satuan_spl_recreate" class="form-control" required>
                    <option value="KG">
                      Kilogram
                    </option>
                    <option value="LB">
                      Lembar
                    </option>
                    <option value="RL">
                      Roll
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan.."></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Order: </label>
                  <input class="form-control input_decimal" name="jumlah_order" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control input_decimal" name="harga_satuan" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Komposisi: </label>
                  <input type="text" class="form-control" name="komposisi" placeholder="Komposisi.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control input_decimal" name="jumlah_roll" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input class="form-control input_decimal" name="hasil_rumus" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="recreate-spl-button">Recreate SPL</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="update-spl-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update SPL</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="update-spl-form">
            <div class="row">
              <div id="update-spl-warning">
              </div>
            </div>
            <div class="row">
              <input type="hidden" name="id" class="form-control" required>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer : </label>
                  <select name="customer_id"  id="customer_id_spl_update" class="form-control" required>
                    <option value="">--Pilih Kustomer--</option>
                    <?php
                      $resp = get_all_customer();
                      if($resp){
                        $customers = $resp->result();
                        $no = 0;
                        foreach($customers as $customer){
                          $no +=1;
                          echo '<option value="'.$customer->id.'">
                            '.$customer->name.'
                          </option>';
                        }
                      }else{
                        echo '<option value="">
                          Tidak ada kustomer
                        </option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" placeholder="Lebar Bahan.." required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" placeholder="Tebal Bahan.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" placeholder="Ukuran Potong.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Satuan: </label>
                  <select name="satuan" id="satuan_spl_update" class="form-control" required>
                    <option value="KG">
                      Kilogram
                    </option>
                    <option value="LB">
                      Lembar
                    </option>
                    <option value="RL">
                      Roll
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan.."></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Order: </label>
                  <input class="form-control input_decimal" name="jumlah_order" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control input_decimal" name="harga_satuan" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Komposisi: </label>
                  <input type="text" class="form-control" name="komposisi" placeholder="Komposisi.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control input_decimal" name="jumlah_roll" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input class="form-control input_decimal" name="hasil_rumus" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="update-spl-button">Update SPL</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="create-spl-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create SPL</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="create-spl-form" action="<?php echo base_url('backend/create_spl_process');?>">
            <div class="row">
              <div id="create-spl-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer : <span id="customer-rating-create"></span></label>
                  <select name="customer_id" id="spl-customer-create" class="form-control" required>
                    <option value="">--Pilih Kustomer--</option>
                    <?php
                      $resp = get_all_customer();
                      if($resp){
                        $customers = $resp->result();
                        $no = 0;
                        foreach($customers as $customer){
                          $no +=1;
                          echo '<option value="'.$customer->id.'" data-rating="'.$customer->rating.'">
                            '.$customer->name.'
                          </option>';
                        }
                      }else{
                        echo '<option value="">
                          Tidak ada kustomer
                        </option>';
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" placeholder="Lebar Bahan.." required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" placeholder="Tebal Bahan.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" placeholder="Ukuran Potong.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Satuan: </label>
                  <select name="satuan" class="form-control" required>
                    <option value="KG">
                      Kilogram
                    </option>
                    <option value="LB">
                      Lembar
                    </option>
                    <option value="RL">
                      Roll
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan.."></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Order: </label>
                  <input class="form-control input_decimal" name="jumlah_order" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control input_decimal" name="harga_satuan" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Komposisi: </label>
                  <input type="text" class="form-control" name="komposisi" placeholder="Komposisi.." required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control input_decimal" name="jumlah_roll" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input class="form-control input_decimal" name="hasil_rumus" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="create-spl-button">Create SPL</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="approve-spl-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Approve SPL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="approve-spl-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menyetujui SPL ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-info" id="approve-spl-button">IYA</button>
        </div>
      </div>
    </div>
  </div>

  <div id="noapprove-spl-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reject SPL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="noapprove-spl-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk tidak menyetujui SPL ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-danger" id="noapprove-spl-button">IYA</button>
        </div>
      </div>
    </div>
  </div>

  <div id="done-spl-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Done SPL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="done-spl-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menyelesaikan SPL ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-primary" id="done-spl-button">IYA</button>
        </div>
      </div>
    </div>
  </div>

  <div id="delete-spl-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete SPL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-spl-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menghapus SPL ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-danger" id="delete-spl-button">IYA</button>
        </div>
      </div>
    </div>
  </div>

  <div id="autodone-spl-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Auto Done SPL</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="autodone-spl-form">
          <div class="form-group">
            <div id="autodone-spl-warning">
            </div>
          </div>
          <div class="form-group">
            <label>Bulan: </label>
            <input type="text" class="form-control monthly-datepicker" name="date">
          </div>
          <div class="form-group">
            <label>Marketing: </label>
            <select name="marketing_id" class="form-control">
              <option value="<?=$member->id;?>"><?=$member->name;?></option>
              <?php
                $resp = get_all_member_list_site();
                if($resp){
                  $marketings = $resp->result();
                  $no = 0;
                  foreach($marketings as $item){
                    $no +=1;
                    echo '<option value="'.$item->id.'">
                      '.$item->name.'
                    </option>';
                  }
                }else{
                  echo '<option value="">
                    Tidak ada marketing
                  </option>';
                }
              ?>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-info" id="autodone-spl-button">Submit</button>
        </div>
      </div>
    </div>
  </div>

  <div id="burn-spl-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Burn SPL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="burn-spl-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menghanguskan SPL ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger" id="burn-spl-button">Burn</button>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

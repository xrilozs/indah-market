<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    SPL - Hapus Data
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-trash"></i> SPL - Hapus Data</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a href="#" class="btn btn-lg btn-danger pull-right" id="print-monthly-spl-button" data-toggle="modal" data-target="#delete-spl-modal">
          <i class="fa fa-trash" aria-hidden="true"></i> Hapus SPL
        </a>
      </div>
      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal: </label>
              <input type="text" class="form-control monthly-datepicker" name="filter_tanggal" id="filter_tanggal">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Status SPL: </label>
              <select name="status_spl" id="status_spl" class="form-control" required>
                <option value="ALL">
                  Semua
                </option>
                <option value="SELESAI">
                  Selesai
                </option>
                <option value="BELUM">
                  Belum Selesai
                </option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">
          <table id="spl_manager_table" class="table2 table-bordered table-hover">
            <thead>
            <tr>
              <th>No. SPL</th>
              <th>Customer</th>
              <th>Marketing</th>
              <th>Hasil Rumus</th>
              <th>Jumlah Order</th>
              <th>Harga Satuan</th>
              <th>Bahan</th>
              <th>Ukuran</th>
              <th>Komposisi</th>
              <th>Jumlah Roll</th>
              <th>Keterangan</th>
              <th>Status Persetujuan</th>
              <th>Status SPL</th>
              <th>Tanggal SPL</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="delete-spl-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete SPL</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="delete-spl-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk <b>menghapus Semua SPL</b> berdasarkan filter yang Anda pilih?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="submit" class="btn btn-danger" id="delete-spl-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

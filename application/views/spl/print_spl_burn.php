
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        #table-list-item {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
		}
		.column-list-item{
			border: 1px solid #dddddd;
			text-align: left;
			padding: 5px;
            font-size:11px;
		}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom:30px; text-align:center">
                <span class="company_name" style="font-weight:bold;">SPL Hangus - <?=$tanggal;?></span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table style="width:100%" id="table-list-item">
                    <thead>
                        <tr>
                            <th class="column-list-item">No. SPL</th>
                            <th class="column-list-item">Customer</th>
                            <th class="column-list-item">Harga Satuan</th>
                            <th class="column-list-item">Bahan</th>
                            <th class="column-list-item">Ukuran</th>
                            <th class="column-list-item">Komposisi</th>
                            <th class="column-list-item">Jumlah Roll</th>
                            <th class="column-list-item">Keterangan</th>
                            <th class="column-list-item">Status SPL</th>
                            <th class="column-list-item">Tanggal SPL</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($items as $item){
                            echo "<tr>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->no_spl</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->customer_name</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->harga_satuan</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->lebar_bahan/$item->tebal_bahan</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->ukuran_potong</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->komposisi</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->jumlah_roll</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->keterangan</td>
                                <td class='column-list-item' style ='word-break:break-all;'>$item->spl_status</td>
                                <td class='column-list-item' style ='word-break:break-all;'>".date_format(date_create($item->tanggal_spl), "d-m-Y")."</td>
                            </tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
  </body>
</html>
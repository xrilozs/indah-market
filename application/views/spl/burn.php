<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    SPL - Hangus
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text"></i> SPL</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal: </label>
              <input type="text" class="form-control monthly-datepicker" name="filter_tanggal" id="filter_tanggal">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="row" style="margin-bottom:10px;">
              <div class="col-lg-12">
                <button class="btn btn-danger pull-right" id="burn-spl-toggle" data-toggle="modal" data-target="#burn-spl-modal">
                  <i class="fa fa-fire" aria-hidden="true"></i> Burn SPL
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="burn-spl-table" class="table2 table-bordered table-hover">
            <thead>
              <tr>
                <th>No. SPL</th>
                <th>Customer</th>
                <th>Customer Rating</th>
                <th>Marketing</th>
                <th>Hasil Rumus</th>
                <th>Jumlah Order</th>
                <th>Harga Satuan</th>
                <th>Bahan</th>
                <th>Ukuran</th>
                <th>Komposisi</th>
                <th>Jumlah Roll</th>
                <th>Keterangan</th>
                <th>Status Persetujuan</th>
                <th>Status SPL</th>
                <th>Tanggal SPL</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="burn-spl-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Burn SPL</h4>
        </div>
        <div class="modal-body">
          <form id="burn-spl-form">
            <div class="row">
              <div id="burn-spl-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Tanggal : </label>
                  <input type="hidden" class="form-control" name="tanggal" id="tanggal" readonly required>
                  <input type="text" class="form-control" name="tanggal_text" id="tanggal_text" readonly required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div class="alert alert-info">
                  <strong>Info!</strong> Link Hasil Burn akan dikirim ke no. hp dibawah ini.
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="form-group">
                  <label>Nomor Handphone 1: </label>
                  <select class="form-control phone-number-option" name="phone_number_1" id="phone_number_1"></select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="form-group">
                  <label>Nomor Handphone 2: </label>
                  <select class="form-control phone-number-option" name="phone_number_2" id="phone_number_2"></select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="form-group">
                  <label>Nomor Handphone 3: </label>
                  <select class="form-control phone-number-option" name="phone_number_3" id="phone_number_3"></select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="form-group">
                  <label>Nomor Handphone 4: </label>
                  <select class="form-control phone-number-option" name="phone_number_4" id="phone_number_4"></select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="form-group">
                  <label>Nomor Handphone 5: </label>
                  <select class="form-control phone-number-option" name="phone_number_5" id="phone_number_5"></select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="form-group">
                  <label>Nomor Handphone 6: </label>
                  <select class="form-control phone-number-option" name="phone_number_6" id="phone_number_6"></select>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-danger" id="burn-spl-button">Burn</button>
          </form>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

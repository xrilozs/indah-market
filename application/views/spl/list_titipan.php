<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    SPL Titipan
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text"></i> SPL</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="form-group">
              <label>Tanggal: </label>
              <input type="text" class="form-control monthly-datepicker" name="filter_tanggal" id="filter_tanggal">
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="row" style="margin-bottom:10px;">
              <div class="col-lg-12">
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create-spltitipan-modal">
                  <i class="fa fa-plus" aria-hidden="true"></i> Create SPL Titipan
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="spltitipan_manager_table" class="table2 table-bordered table-hover">
            <thead>
              <tr>
                <th>No. SPL Titipan</th>
                <th>No. SPL Awal</th>
                <th>Site Tujuan</th>
                <th>Site Awal</th>
                <th>Marketing Penerima</th>
                <th>Customer</th>
                <th>Ket. Kirim 1</th>
                <th>Ket. Kirim 2</th>
                <th>Ket. Retur</th>
                <th>Status</th>
                <th>Tanggal SPL Titipan</th>
                <th>Tanggal SPL Asli</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="update-spltitipan-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update SPL Titipan</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="update-spltitipan-form">
            <div class="row">
              <div id="update-spltitipan-warning">
              </div>
            </div>
            <div class="row">
              <input type="hidden" name="id" class="form-control" required>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>No SPL Titipan:</label>
                  <input type="text" name="no_spl_titipan" id="no_spl_titipan" class="form-control" readonly>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>No SPL Awal:</label>
                  <input type="text" name="no_spl" id="no_spl" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Marketing Penerima:</label>
                  <input type="text" name="marketing_penerima" id="marketing_penerima" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Site Tujuan:</label>
                  <input type="text" name="site_titipan" id="site_titipan" class="form-control" readonly>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Site Awal:</label>
                  <input type="text" name="site_awal" id="site_awal" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Keterangan kirim 1: </label>
                  <textarea class="form-control" name="ket_kirim1" placeholder="Keterangan Kirim 1.."></textarea>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Keterangan kirim 2: </label>
                  <textarea class="form-control" name="ket_kirim2" placeholder="Keterangan Kirim 2.."></textarea>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Keterangan kirim 3: </label>
                  <textarea class="form-control" name="ket_kirim3" placeholder="Keterangan Kirim 3.."></textarea>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Keterangan kirim 4: </label>
                  <textarea class="form-control" name="ket_kirim4" placeholder="Keterangan Kirim 4.."></textarea>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Keterangan kirim 5: </label>
                  <textarea class="form-control" name="ket_kirim5" placeholder="Keterangan Kirim 5.."></textarea>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Keterangan Retur: </label>
                  <textarea class="form-control" name="ket_retur" placeholder="Keterangan Retur.."></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer: <span id="customer-rating-create"></span></label>
                  <input type="text" name="customer" id="customer" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" placeholder="Lebar Bahan.." readonly>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" placeholder="Tebal Bahan.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" placeholder="Ukuran Potong.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan.." readonly></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control input_decimal" name="harga_satuan" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control input_decimal" name="jumlah_roll" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input class="form-control" name="hasil_rumus" placeholder="contoh: 3.14" readonly>
                </div>
              </div>
            </div>
            <div class="row" id="status-section">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Status:</label>
                  <select class="form-control" name="status" id="status">
                    <option value="BELUM SELESAI">BELUM SELESAI</option>
                    <option value="SELESAI">SELESAI</option>
                  </select>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="update-spltitipan-button">Update SPL Titipan</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="create-spltitipan-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create SPL Titipan</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="create-spltitipan-form" action="<?php echo base_url('backend/create_spltitipan_process');?>">
            <div class="row">
              <div id="create-spltitipan-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label for="select_spl">No SPL Awal:</label>
                  <select class="form-control" name="no_spl" id="select_spl" required></select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Site Tujuan:</label>
                  <select class="form-control" name="site_titipan" id="site_titipan_create" required>
                    <option value="A">A</option>
                    <option value="C">C</option>
                    <option value="R">R</option>
                    <option value="S">S</option>
                    <option value="T">T</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Marketing Penerima:</label>
                  <input type="text" name="marketing_penerima" id="marketing_penerima" class="form-control" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Nomor HP Marketing Penerima:</label>
                  <input type="text" name="no_hp_marketing_penerima" id="no_hp_marketing_penerima" class="form-control" required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Site Awal:</label>
                  <input type="text" name="site_awal" id="site_awal" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Kustomer: <span id="customer-rating-create"></span></label>
                  <input type="text" name="customer" id="customer" class="form-control" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Lebar Bahan: </label>
                  <input type="text" name="lebar_bahan" class="form-control" maxlength="4" placeholder="Lebar Bahan.." readonly>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Tebal Bahan: </label>
                  <input type="text" name="tebal_bahan" class="form-control" maxlength="3" placeholder="Tebal Bahan.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Ukuran Potong: </label>
                  <input type="text" name="ukuran_potong" class="form-control" placeholder="Ukuran Potong.." readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keterangan: </label>
                  <textarea class="form-control" name="keterangan" placeholder="Keterangan.." readonly></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Harga Satuan: </label>
                  <input class="form-control input_decimal" name="harga_satuan" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Jumlah Roll: </label>
                  <input class="form-control input_decimal" name="jumlah_roll" placeholder="contoh: 3.14" pattern="^\d*(\.\d{0,2})?$" readonly>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Hasil Rumus: </label>
                  <input type="text" class="form-control" name="hasil_rumus" placeholder="contoh: 3.14" readonly>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="create-spltitipan-button">Create SPL Titipan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
        
</section>
<!-- /.content -->

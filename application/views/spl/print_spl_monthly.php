
<!DOCTYPE HTML>
<html>
  <head>
    <title>SPL - Monthly Report</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        @media print {
            table { page-break-after:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            td    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
        }
        .table-condensed{
            font-size: 10px;
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom:30px; text-align:center">
                <span class="company_name" style="font-weight:bold;">SPL - Monthly Report</span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>No. SPL</th>
                            <th>Customer</th>
                            <th>Marketing</th>
                            <th>Harga Satuan</th>
                            <th>Bahan</th>
                            <th>Ukuran</th>
                            <th>Jumlah Order</th>
                            <th>Jumlah Kirim #1</th>
                            <th>Jumlah Kirim #2</th>
                            <th>Jumlah Kirim #3</th>
                            <th>Total Kiriman</th>
                            <th>Surat Jalan #1</th>
                            <th>Surat Jalan #2</th>
                            <th>Surat Jalan #3</th>
                            <th>Keterangan</th>
                            <th>Status Persetujuan</th>
                            <th>Status SPL</th>
                            <th>Tanggal SPL</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($spls as $item){
                            $total_kiriman = $item->jumlah_kirim1 + $item->jumlah_kirim2 + $item->jumlah_kirim3;
                            echo "<tr>
                                <td style ='word-break:break-all;'>$item->no_spl</td>
                                <td style ='word-break:break-all;'>$item->customer_name</td>
                                <td style ='word-break:break-all;'>$item->marketing_name</td>
                                <td style ='word-break:break-all;'>$item->harga_satuan</td>
                                <td style ='word-break:break-all;'>$item->lebar_bahan/$item->tebal_bahan</td>
                                <td style ='word-break:break-all;'>$item->ukuran_potong</td>
                                <td style ='word-break:break-all;'>$item->jumlah_order</td>
                                <td style ='word-break:break-all;'>$item->jumlah_kirim1</td>
                                <td style ='word-break:break-all;'>$item->jumlah_kirim2</td>
                                <td style ='word-break:break-all;'>$item->jumlah_kirim3</td>
                                <td style ='word-break:break-all;'>$total_kiriman</td>
                                <td style ='word-break:break-all;'>$item->sjkirim1</td>
                                <td style ='word-break:break-all;'>$item->sjkirim2</td>
                                <td style ='word-break:break-all;'>$item->sjkirim3</td>
                                <td style ='word-break:break-all;'>$item->keterangan</td>
                                <td style ='word-break:break-all;'>".str_replace("_", " ", $item->approval_status)."</td>
                                <td style ='word-break:break-all;'>$item->spl_status</td>
                                <td style ='word-break:break-all;'>".date_format(date_create($item->tanggal_spl), "d-m-Y")."</td>
                            </tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: -15px; left: 0;">
                <p class="company_name" style="font-size:9px; ">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
      </div>
  </body>
<script>
  window.print();
</script>
</html>
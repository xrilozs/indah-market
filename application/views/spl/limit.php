<section class="content-header">
  <h1>
    SPL - Limit
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-cog"></i> SPL - Limit</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="limit" class="tab-pane fade in active">
    <div class="row">
        <div class="col-lg-12">
            <form class="form" id="spl-limit-form">
                <div id="spl-limit-warning">
                </div>
                <div class="form-group">
                    <label>Auto Done Threshold</label>
                    <input type="number" class="form-control" name="nilai" min="0" max="100" placeholder="min 0 | max 100" required>
                </div>
                <button class="btn btn-primary" id="spl-limit-button">Submit</button>
            </form>
        </div>
    </div>
  </div>
</section>
<!-- /.content -->

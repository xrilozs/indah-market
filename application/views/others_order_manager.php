<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Others Order Manager
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-list"></i> Others Order Manager</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <?php
  if($this->session->flashdata('message')=='success') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Konfirmasi Order Berhasil!.
    </div>
    ';
  }
  ?>
  <?php
  if($this->session->flashdata('message')=='error') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error!</strong> Konfirmasi Order Gagal!.
    </div>
    ';
  }
  ?>
  <?php
  if($this->session->flashdata('message')=='success_delete') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Menghapus Order Berhasil!.
    </div>
    ';
  }
  ?>
  <?php
  if($this->session->flashdata('message')=='error_delete') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error!</strong> Menghapus Order Gagal!.
    </div>
    ';
  }
  ?>
</div>
<ul class="nav nav-tabs">
<li class="active"><a data-toggle="tab" href="#list">List</a></li>
</ul>
<div class="tab-content">
  <div id="list" class="tab-pane fade in active">
    <h3>List</h3>
    <div class="row">
      <div class="col-md-12">
      <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>No Hp</th>
          <th>Deskripsi Pemesanan</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $order = get_order_others_list();
        if($order->num_rows()!=0){
          $no = 0;
          foreach($order->result() as $item){
            $no +=1;
            $konfirmasi = ($item->status=='unpaid'?'<a href="'.base_url('backend/konfirmasi_prewed/'.$item->id).'" class="btn btn-success">Konfirmasi</a>':'');
            echo '<tr>
            <td>'.$no.'</td>
            <td>'.$item->name.'</td>
            <td>'.$item->alamat.'</td>
            <td>'.$item->phone.'</td>
            <td>'.$item->description.'</td>
            </tr>
            ';
          }
        }
        ?>
        </tbody>
      </table>
    </div>
    </div>
  </div>
</div>
</section>
<!-- /.content -->

<li>
  <a href="<?php echo base_url('dashboard');?>">
    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    <span class="pull-right-container"></span>
  </a>
</li>
<li class="hidden-menu" id="user-menu">
  <a href="<?php echo base_url('user_manager');?>">
    <i class="fa fa-user"></i>
    <span>User</span>
  </a>
</li>
<li class="treeview hidden-menu" id="proret-menu">
  <a href="#">
    <i class="fa fa-files-o"></i>
    <span>Produksi & Returan</span>
    <span class="pull-right-container">
      <span class="label label-primary pull-right"></span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li class="proret-menu-item proret-item"><a href="<?php echo base_url('produksi_harian');?>"><i class="fa fa-pencil"></i>Input Produksi Harian</a></li>
    <li class="proret-menu-item proret-item"><a href="<?php echo base_url('returan');?>"><i class="fa fa-pencil"></i>Input Returan</a></li>
    <li class="proret-menu-item proret-item"><a href="<?php echo base_url('customer');?>"><i class="fa fa-pencil"></i>Input Data Customer</a></li>
    <li class="proret-menu-item proret-item"><a href="<?php echo base_url('report/produksi_bulanan');?>"><i class="fa fa-list"></i>Lap. Produksi Bulanan</a></li>
    <li class="proret-menu-item proret-item"><a href="<?php echo base_url('report/produksi_harian');?>"><i class="fa fa-list"></i>Lap. Produksi Harian</a></li>
    <li class="proret-menu-item proret-item"><a href="<?php echo base_url('report/returan_bulanan');?>"><i class="fa fa-list"></i>Lap. Returan Bulanan</a></li>
    <li class="proret-menu-item proret-item"><a href="<?php echo base_url('report/returan_harian');?>"><i class="fa fa-list"></i>Lap. Returan Harian</a></li>
    <li class="proret-menu-item"><a href="<?php echo base_url('update_returan');?>"><i class="fa fa-edit"></i> Update Data Returan</a></li>
  </ul>
</li>
<li class="treeview hidden-menu" id="delivery-menu">
  <a href="#">
    <i class="fa fa-truck"></i>
    <span>Delivery</span>
    <span class="pull-right-container">
      <span class="label label-primary pull-right"></span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li class="delivery-menu-item admprod-item"><a href="<?php echo base_url('setor-barang-jadi');?>"><i class="fa fa-archive"></i>Setor Barang Jadi</a></li>
    <li class="delivery-menu-item"><a href="<?php echo base_url('terima-barang-jadi');?>"><i class="fa fa-archive"></i>Terima Barang Jadi</a></li>
    <li class="delivery-menu-item proret-item admprod-item"><a href="<?php echo base_url('data-barang');?>"><i class="fa fa-archive"></i>Lacak Data Barang</a></li>
  </ul>
</li>
<li class="treeview hidden-menu" id="hutang-menu">
  <a href="#">
    <i class="fa fa-usd"></i>
    <span>Hutang</span>
    <span class="pull-right-container">
      <span class="label label-primary pull-right"></span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="<?php echo base_url('hutang');?>"><i class="fa fa-pencil"></i>Input Data Hutang</a></li>
    <li><a href="<?php echo base_url('pelunasan_hutang');?>"><i class="fa fa-edit"></i>Update Pelunasan Hutang</a></li>
    <li><a href="<?php echo base_url('report/pembelian_hutang_bulanan');?>"><i class="fa fa-list"></i>Lap. Pembelian PerBulan</a></li>
    <li><a href="<?php echo base_url('report/pembelian_hutang_harian');?>"><i class="fa fa-list"></i>Lap. Pembelian PerHarian</a></li>
  </ul>
</li>
<li class="treeview hidden-menu" id="piutang-menu">
  <a href="#">
    <i class="fa fa-usd"></i>
    <span>Piutang</span>
    <span class="pull-right-container">
      <span class="label label-primary pull-right"></span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="<?php echo base_url('customer');?>"><i class="fa fa-pencil"></i>Input Data Customer</a></li>
    <li><a href="<?php echo base_url('faktur');?>"><i class="fa fa-pencil"></i>Input Data Faktur</a></li>
    <li><a href="<?php echo base_url('nota_penjualan');?>"><i class="fa fa-pencil"></i>Input Nota Penjualan</a></li>
    <li><a href="<?php echo base_url('kwitansi');?>"><i class="fa fa-pencil"></i>Kwitansi</a></li>
    <li><a href="<?php echo base_url('update_pelunasan_faktur');?>"><i class="fa fa-edit"></i>Update Pelunasan Faktur</a></li>
    <li><a href="<?php echo base_url('report/penjualan_piutang_bulanan');?>"><i class="fa fa-list"></i>Lap. Penjualan Perbulan</a></li>
    <li><a href="<?php echo base_url('report/nota_penjualan');?>"><i class="fa fa-list"></i>Lap. Nota Penjualan</a></li> 
    <li><a href="<?php echo base_url('report/faktur_harian');?>"><i class="fa fa-list"></i>Lap. Faktur Harian</a></li>
  </ul>
</li>
<li class="treeview hidden-menu" id="spl-menu">
  <a href="#">
    <i class="fa fa-file-text"></i>
    <span>SPL</span>
    <span class="pull-right-container">
      <span class="label label-primary pull-right"></span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li class="spl-menu-item proret-item"><a href="<?php echo base_url('spl'); ?>"><i class="fa fa-pencil"></i> Daily SPL</a></li>
    <li class="spl-menu-item proret-item"><a href="<?php echo base_url('spl-monthly-report'); ?>"><i class="fa fa-file-text-o"></i> Monthly Report</a></li>
    <li class="spl-menu-item"><a href="<?php echo base_url('spl-delete'); ?>"><i class="fa fa-trash"></i> Delete SPL</a></li>
    <li class="spl-menu-item"><a href="<?php echo base_url('spl-limit'); ?>"><i class="fa fa-cog"></i> Limit SPL</a></li>
    <li class="spl-menu-item proret-item"><a href="<?php echo base_url('spl-titipan'); ?>"><i class="fa fa-exchange"></i> SPL Titipan</a></li>
    <li class="spl-menu-item proret-item"><a href="<?php echo base_url('spl-burn'); ?>"><i class="fa fa-fire"></i> Burn SPL</a></li>
  </ul>
</li>
<li class="treeview hidden-menu" id="absensi-menu">
  <a href="#">
    <i class="fa fa-user"></i>
    <span>Absensi Pegawai</span>
    <span class="pull-right-container">
      <span class="label label-primary pull-right"></span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="<?php echo base_url('pegawai'); ?>"><i class="fa fa-user"></i> Pegawai</a></li>
    <li><a href="<?php echo base_url('kasbon-pegawai'); ?>"><i class="fa fa-file-text-o"></i> Kasbon Pegawai</a></li>
    <li><a href="<?php echo base_url('absensi'); ?>"><i class="fa fa-file-text-o"></i> Absensi</a></li>
    <li><a href="<?php echo base_url('histori-absensi'); ?>"><i class="fa fa-history"></i> Histori Absensi</a></li>
    <li><a href="<?php echo base_url('jurnal-absensi'); ?>"><i class="fa fa-files-o"></i> Jurnal Absensi</a></li>
    <?php
      $user = get_current_member();
      if($user && $user->type == "super_admin"):
    ?>
      <li><a href="<?php echo base_url('delete-jurnal-absensi'); ?>"><i class="fa fa-files-o"></i> Hapus Jurnal Absensi</a></li>
    <?php
      endif;
    ?>
    <li><a href="<?php echo base_url('office_hour'); ?>"><i class="fa fa-clock-o"></i> Office Hour</a></li>
    <li><a href="<?php echo base_url('hari-libur'); ?>"><i class="fa fa-calendar"></i> Hari Libur</a></li>
  </ul>
</li>
<li class="treeview hidden-menu" id="setting-menu">
  <a href="#">
    <i class="fa fa-cog"></i>
    <span>Setting</span>
    <span class="pull-right-container">
      <span class="label label-primary pull-right"></span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="<?php echo base_url('keyword'); ?>"><i class="fa fa-key"></i> Keyword</a></li>
    <li><a href="<?php echo base_url('site_location'); ?>"><i class="fa fa-map"></i> Site Location</a></li>
  </ul>
</li>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Edit Lainnya
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-book"></i> Edit Lainnya</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <?php
  if($this->session->flashdata('message')=='success') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Perubahan berhasil disimpan!.
    </div>
    ';
  }
  if($this->session->flashdata('message')=='error') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Gagal!</strong> Perubahan gagal disimpan!.
    </div>
    ';
  }
  ?>
</div>
<div class="col-md-12">
  <a href="<?php echo base_url('others_manager');?>" class="btn btn-primary">Kembali</a>
  <form method="post" action="<?php echo base_url('backend/edit_others_process');?>">
    <div class="col-md-12">
      <br>
      <div class="form-group">
        <label>Nama</label>
        <input type="hidden" name="id" id="id" value="<?php echo $others->id;?>">
        <input type="text" name="name" id="name" class="form-control" value="<?php echo $others->name;?>">
      </div>
      <div class="form-group">
        <label>Harga</label>
        <input type="number" name="price" id="price" class="form-control"  value="<?php echo $others->price;?>">
      </div>
      <div class="form-group">
        <label>Gambar</label>
        <input type="file" name="cover" id="cover" class="form-control">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
      </div>
    </div>
</form>
</div>
</section>
<!-- /.content -->

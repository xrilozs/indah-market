<table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>No.Nota</th>
              <th>Tanggal Nota</th>
              <th>Customer</th>
              <th>P/NP</th>
              <th>ID Faktur</th>
              <th>Grand Total</th>
              <th>Keterangan Pembayaran</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
              <th>Edit</th>
              <th>Print</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $nota_list = get_nota_penjualan_by_date($date);
            if($nota_list!=false){
              $no = 0;
              foreach($nota_list->result() as $nota){
                $no +=1;
                $edit= "<a href='".base_url('backend/edit_nota_penjualan/'.$nota->id)."' class='btn btn-success'>Edit</a>";
                $print = "<a href='".base_url('backend/print_nota_penjualan/'.$nota->id)."' class='btn btn-warning' target='_blank' >Print</a>";
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$nota->no_nota.'</td>
                <td>'.$nota->tanggal_nota.'</td>
                <td>'.$nota->customer_name.'</td>
                <td>'.$nota->pajak_non_pajak.'</td>
                <td>'.$nota->faktur_id.'</td>
                <td>'.money($nota->grand_total).'</td>
                <td>'.$nota->keterangan_pembayaran.'</td>
                <td>'.$nota->tanggal_bayar.'</td>
                <td>'.money($nota->jumlah_bayar).'</td>
                <td>'.$edit.'</td>
                <td>'.$print.'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
<script>
$(document).ready(function(){
  $(".table").DataTable();
});
</script>
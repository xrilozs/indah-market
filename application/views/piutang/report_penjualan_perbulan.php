<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Penjualan Perbulan
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Laporan Penjualan Perbulan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
      <?php if(!empty($this->session->flashdata('message'))) echo $this->session->flashdata('message');?>
        <form method="post" action="<?php echo base_url('backend/download_penjualan_bulanan');?>" target="_blank">
        <div class="form-group">
          <label>Date :</label>
          <select class="" name="year" id="year_select" url="<?php echo base_url('backend/report_faktur_monthly');?>">
            <option value="">Pilih Tahun</option>
            <?php for($i=date('Y');$i>(date('Y')-5);$i--){
              echo '<option value="'.$i.'">'.$i.'</option>';
            } ?>
          </select>
          <select name="month" id="month_select" url="<?php echo base_url('backend/report_faktur_monthly');?>">
          <option value="">Pilih Bulan</option>
          <option value="1">Januari</option>
          <option value="2">Februari</option>
          <option value="3">Maret</option>
          <option value="4">April</option>
          <option value="5">Mei</option>
          <option value="6">Juni</option>
          <option value="7">Juli</option>
          <option value="8">Agustus</option>
          <option value="9">September</option>
          <option value="10">Oktober</option>
          <option value="11">November</option>
          <option value="12">Desember</option>
        </select>
        </div>
        <div class="form-group">
          <label>Customer :</label>
          <select class="" name="customer_id" id="customer_select" url="<?php echo base_url('backend/report_faktur_monthly');?>">
              <?php 
            $customer = get_all_customer();
            if($customer!=false){
              echo '<option value="">
              Pilih Customer..
            </option>';
              foreach($customer->result() as $cust){
                echo '<option value="'.$cust->id.'">
              '.$cust->name.'
            </option>';
              }
            } else  {
              echo '<option value="">
              Tidak ada pilihan Customer/Customer
            </option>';
            }
            ?>
          </select>
        </div>
             <button type="submit" class="btn btn-primary"><span class="fa fa-download"></span> Download</button>
              <button type="button" class="btn btn-primary email_laporan" data-toggle="modal" data-target="#emailmodal"><span class="fa fa-envelope"></span> Email</button>
            <br><br>
          </form>
      </div>
      <div class="col-md-12">
        <div class="table-responsive load_table">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Customer</th>
              <th>No.Faktur</th>
              <th>Tanggal Faktur</th>
              <th>Jumlah Tagihan</th>
              <th>No. Faktur Pajak</th>
              <th>PPN</th>
              <th>Status</th>
              <th>Keterangan Bayar</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
            </tr>
            </thead>
            <tbody>
            <?php
              $date = date('Y-m');
            $piutang = get_faktur_list_by_month($date);
            if($piutang!=false){
              $no = 0;
              foreach($piutang->result() as $item){
                $no +=1;
                echo '
                <tr>
                <td>'.$no.'</td>
                <td>'.$item->customer_name.'</td>
                <td>'.$item->no_faktur.'</td>
                <td>'.$item->tanggal_faktur.'</td>
                <td>'.money($item->total).'</td>
                <td>'.$item->no_faktur_pajak.'</td>
                <td>'.money($item->ppn).'</td>
                <td>'.decode_enum($item->status).'</td>
                <td>'.($item->keterangan_pembayaran==''?'Belum ada':decode_enum($item->keterangan_pembayaran)).'</td>
                <td>'.($item->tanggal_bayar=='0000:00:00'?'Belum ada':$item->tanggal_bayar).'</td>
                <td>'.($item->jumlah_bayar=='00,0'?'Belum ada':money($item->jumlah_bayar)).'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
</section>
<!-- /.content -->
<div class="modal fade" id="emailmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form method="post" action="<?php echo base_url('backend/send_email_penjualan_bulanan');?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kirim Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="year" id="year">
          <input type="hidden" name="month" id="month">
          <input type="hidden" name="customer_id" id="customer_id">
        <div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" id="email" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
      </form>
  </div>
</div>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Customer
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-user"></i> Customer</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
         <?php if($this->session->flashdata('message')=='success_delete') {
    echo '
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Sukses!</strong> Penghapusan Data Berhasil!.
    </div>
    ';
  }
  if($this->session->flashdata('message')=='error_delete') {
    echo '
    <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Error!</strong> Penghapusan Data Gagal!.
    </div>
    ';
  }
        ?>
      </div>

      <?php
        if($member->type == 'super_admin'):
      ?>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <a href="backend/get_customer_and_print" id="customer_print" class="btn btn-default"  target="_blank">
          <i class="fa fa-save" aria-hidden="true"></i> Print
        </a>
        <a href="backend/get_customer_and_export" id="customer_export" class="btn btn-primary"  target="_blank">
          <i class="fa fa-arrow-down" aria-hidden="true"></i> Export
        </a>
      </div>
      <?php
        endif;
      ?>

      <?php
        if($member->type == 'super_admin'):
      ?>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <?php
        else:
      ?>
        <div class="col-lg-12">
      <?php
        endif;
      ?>
        <button class="btn btn-success pull-right" data-toggle="modal" data-target="#create_modal">
          Create Customer
        </button>
        <br>
        <br>
      </div>
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="user_manager_table" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Contact Person</th>
              <th>Telepon Kerja</th>
              <th>Alamat 1</th>
              <th>Alamat 2</th>
              <th>Pajak-NonPajak</th>
              <th>Limit Plafon</th>
              <th>Tanggal Update</th>
              <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $customer = get_customer_list();
            if($customer!=false){
              $no = 0;
              foreach($customer->result() as $item){
                $no +=1;
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$item->name.'</td>
                <td>'.$item->contact_person.'</td>
                <td>'.$item->work_phone.'</td>
                <td>'.$item->address_1.'</td>
                <td>'.$item->address_2.'</td>
                <td>'.strtoupper($item->tax_non_tax).'</td>
                <td>'.$item->limit_plafon.'</td>
                <td>'.$item->datemodified.'</td>
                <td><button url="'.base_url('home/edit_customer/'.$item->id).'" class="btn btn-success edit-customer" data-toggle="modal" data-target="#edit_user_modal">Edit</button>
                <button url="'.base_url('backend/delete_customer/'.$item->id).'" class="btn btn-danger delete-ajax" " data-toggle="modal" data-target="#delete_user_modal">Delete</button></td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
  <div id="delete_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus customer ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
        </div>
      </div>
    </div>
  </div>
  <div id="edit_user_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Customer</h4>
        </div>
        <div class="modal-body">
          <form id="submit-form-edit" data-url="<?php echo base_url('backend/edit_customer_process');?>">
            <div class="edit_modal_form_warning">
            </div>
            <input id="edit_id" type="hidden" name="id" class="form-control">
            <div class="form-group">
              <label>Nama : </label>
              <input id="edit_name" type="text" name="name" class="form-control" >
            </div>
            <div class="form-group">
              <label>No. Telepon : </label>
             <input id="edit_work_phone" type="text" name="work_phone" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" >
            </div>
            <div class="form-group">
              <label>Contact Person : </label>
             <input id="edit_contact_person" type="text" name="contact_person" class="form-control" >
            </div>
            <div class="form-group">
              <label>Alamat : </label>
             <input id="edit_address_1" type="text" name="address_1" class="form-control" ><br>
              <input id="edit_address_2" type="text" name="address_2" class="form-control">
            </div>
            <div class="form-group">
              <label>Pajak/Non Pajak : </label>
              <select id="edit_tax_non_tax" name="tax_non_tax" class="form-control" >
                <option value="">
                  Choose..
                </option>
                <option value="p">
                  Pajak
                </option>
                <option value="n">
                  Non Pajak
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Limit Plafon : </label>
              <input id="edit_limit_plafon" type="number" name="limit_plafon" class="form-control">
            </div>
            <div class="form-group">
              <label>Bidang Usahan : </label>
              <select id="edit_business_type" name="business_type" class="form-control">
                <option value="">
                  Choose..
                </option>
                <option value="plastik">
                  Plastik
                </option>
                <option value="garment">
                  Garment
                </option>
                <option value="perorangan">
                  Perorangan
                </option>
                <option value="kaos_kaki">
                  Kaos Kaki
                </option>
                <option value="korporasi">
                  Korporasi
                </option>
                <option value="lain-lain">
                 Lain-lain
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>NIK KTP : </label>
              <input type="text" name="nik_ktp" id="nik_ktp" class="form-control" required>
            </div>
            <div class="form-group">
              <label>No NPWP : </label>
              <input type="text" name="no_npwp" id="no_npwp" class="form-control" required>
            </div>
            <div class="form-group">
              <label>No Handphone : </label>
              <input type="text" name="no_hp" id="no_hp" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Sumber Info : </label>
              <select name="sumber_info" id="sumber_info" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="website">
                  Website
                </option>
                <option value="teman">
                  Teman
                </option>
                <option value="lain-lain">
                  Lain-lain
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Tipe Lampiran : </label>
              <select name="tipe_lampiran" id="tipe_lampiran" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="ktp">
                  KTP
                </option>
                <option value="npwp">
                  NPWP
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Cara Bayar : </label>
              <select name="cara_bayar" id="cara_bayar" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="cod">
                  COD
                </option>
                <option value="dp">
                  DP
                </option>
                <option value="kredit">
                  Kredit
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Marketing : </label>
              <input type="text" name="mkt" id="mkt" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Pembayaran Ekspedisi : </label>
              <select name="pembayaran_ekspedisi" id="pembayaran_ekspedisi" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="free ongkir">
                  Free Ongkir
                </option>
                <option value="dibayar cust">
                  Dibayar Customer
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Di Acc Oleh : </label>
              <input type="text" name="acc_oleh" id="acc_oleh" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Rating : </label>
              <input type="number" name="rating" id="rating" min="0" max="5" class="form-control" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Save Changes</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="create_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Customer</h4>
        </div>
        <div class="modal-body">
          <form id="submit-form" data-url="<?php echo base_url('backend/create_customer_process');?>">
            <div class="create_modal_form_warning">
            </div>
            <div class="form-group">
              <label>Nama : </label>
              <input type="text" name="name" class="form-control" required>
            </div>
            <div class="form-group">
              <label>No. Telepon : </label>
             <input type="text" name="work_phone" class="form-control" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required>
            </div>
            <div class="form-group">
              <label>Contact Person : </label>
             <input type="text" name="contact_person" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Alamat : </label>
             <input type="text" name="address_1" class="form-control" required><br>
              <input type="text" name="address_2" class="form-control">
            </div>
            <div class="form-group">
              <label>Pajak/Non Pajak : </label>
              <select name="tax_non_tax" class="form-control" required>
                <option value="">
                  Choose..
                </option>
                <option value="p">
                  Pajak
                </option>
                <option value="n">
                  Non Pajak
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Limit Plafon : </label>
              <input type="number" name="limit_plafon" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Bidang Usahan : </label>
              <select name="business_type" class="form-control" required>
                <option value="">
                  Choose..
                </option>
                <option value="plastik">
                  Plastik
                </option>
                <option value="garment">
                  Garment
                </option>
                <option value="perorangan">
                  Perorangan
                </option>
                <option value="kaos_kaki">
                  Kaos Kaki
                </option>
                <option value="korporasi">
                  Korporasi
                </option>
                <option value="lain-lain">
                 Lain-lain
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>NIK KTP : </label>
              <input type="text" name="nik_ktp" class="form-control" required>
            </div>
            <div class="form-group">
              <label>No NPWP : </label>
              <input type="text" name="no_npwp" class="form-control" required>
            </div>
            <div class="form-group">
              <label>No Handphone : </label>
              <input type="text" name="no_hp" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Sumber Info : </label>
              <select name="sumber_info" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="website">
                  Website
                </option>
                <option value="teman">
                  Teman
                </option>
                <option value="lain-lain">
                  Lain-lain
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Tipe Lampiran : </label>
              <select name="tipe_lampiran" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="ktp">
                  KTP
                </option>
                <option value="npwp">
                  NPWP
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Cara Bayar : </label>
              <select name="cara_bayar" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="cod">
                  COD
                </option>
                <option value="dp">
                  DP
                </option>
                <option value="kredit">
                  Kredit
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Marketing : </label>
              <input type="text" name="mkt" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Pembayaran Ekspedisi : </label>
              <select name="pembayaran_ekspedisi" class="form-control" required>
                <option value="">
                  Pilih..
                </option>
                <option value="free ongkir">
                  Free Ongkir
                </option>
                <option value="dibayar cust">
                  Dibayar Customer
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Di Acc Oleh : </label>
              <input type="text" name="acc_oleh" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Rating : </label>
              <input type="number" name="rating" min="0" max="5" class="form-control" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Create Customer</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- /.content -->

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Faktur Piutang
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-pencil"></i> Faktur Piutang</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
    <?php if(!empty($this->session->flashdata('msg'))) echo $this->session->flashdata('msg');?>
  <form method="post" id="faktur_piutang_form" action="<?php echo base_url('backend/faktur_piutang_process');?>">
    <div id="faktur_piutang_form_warning">
    </div>
    <div class="form-group">
      <label>Kepada YTH : </label><br>
        <select name="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer/Customer
          </option>';
          }
          ?>
          
        </select>
    </div>
    <div class="form-group">
      <label>Tanggal : </label>
      <input type="text" class="form-control datepicker" name="tanggal_faktur">
    </div>
      <div class="form-group">
      <label>Kode Site</label>
          <select name="site_code" id="site_code" class="form-control">
          <option value="">Pilih Site</option>
          <option value="a">A</option>
          <option value="c">C</option>
          <option value="r">R</option>
          <option value="s">S</option>
          <option value="t">T</option>
        </select>
      </div>
      <script>
          $(document).ready(function(){
             $("#site_code").val('<?php echo $member->site_code;?>').change(); 
          });
      </script>
    <div class="form-group">
      <label>No. SJ</label>
      <input type="text" id="sj_no_search" name="sj_no_search" class="form-control" data-url="<?php echo base_url('backend/sj_search');?>" placeholder="No.SJ" aria-label="No.SJ" aria-describedby="basic-addon2">
      <label id="info_sj"></label>
      <br>
      <div class="input-group-append">
        <button class="btn btn-outline-secondary btn-primary" type="button" id="sj_no_search_btn">Tambah</button>
      </div>
      <input type="hidden" name="jumlah_no_sj" id="jumlah_no_sj">
      <input type="hidden" name="sj" id="sj">
      <br>
      <div id="sj_list">
      </div>
    </div>
    <div class="form-group">
      <label>No. Faktur Pajak: </label>
      <input type="text" class="form-control" name="no_faktur_pajak">
    </div>
      <div class="form-group">
      <label>Cetak : </label>
      <select name="cetak" id="cetak" class="form-control">
          <option value="ya">Ya</option>
          <option value="tidak">Tidak</option>
          </select>
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success btn-block" form-id="faktur_piutang_form"  warning_message="faktur_piutang_form_warning">Simpan</button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->

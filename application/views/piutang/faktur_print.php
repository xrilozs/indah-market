<html>
  <head>
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <p><label class="pull-right"><?php echo $new_date = date('d M Y', strtotime($tanggal_faktur));?></label><br></p>
    <label>&nbsp;&nbsp;FAKTUR NO : <?php echo $faktur_no;?></label>
    <label class="pull-right">Kepada YTH : <?php echo $customer_name;?></label><br>
    <br><br>
    <?php echo $barang_det;?>
    <br>
    <div class="row" style="width:100%">
        <div class="col-sm-4" style="float:left;width:20%">
            <label>&nbsp;&nbsp;HORMAT KAMI</label>
        </div>
        <div class="col-sm-4" style="float:left;width:50%">
            <p style="text-align:center">
              *Cek/bg a/n: <?php echo ($ppn!=0?'PT. INDAH KARUNIA SUKSES':'SUHARJANTO G');?> <br>
              <?php echo date('d/m/Y H:i:s');?>
            </p>
        </div>
        <div class="col-sm-4" style="text-align:right;float:right;width:30%">
              <label>Sub-Total : Rp.<?php echo money($subtotal);?></label><br>
              <label>PPn (10%) : Rp.<?php echo money($ppn);?></label><br>
              <label>TOTAL : Rp.<?php echo money($total);?></label><br>
        </div>
    </div>
  </body>
<script>
  window.print();
</script>
</html>
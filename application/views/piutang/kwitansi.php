<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kwitansi
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-pencil"></i> Faktur Piutang</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
      <form method="post" action="<?php echo base_url('backend/cetak_kwitansi');?>"  target="_blank">
          <div class="form-group">
              <label>Sudah Terima Dari</label>
              <input type="text" class="form-control" name="diterima_dari">
          </div>
          <div class="form-group">
              <label>Untuk Pembayaran</label>
              <input type="text" class="form-control" name="untuk_pembayaran_1">
              <input type="text" class="form-control" name="untuk_pembayaran_2">
          </div>
          <div class="form-group">
              <label>Jumlah Rp.</label>
               <input type="number" class="form-control" step="0.01"  name="jumlah_rp" >
          </div>
          <div class="form-group">
              <label>Tempat &amp; Tanggal </label>
               <input type="text" class="form-control" step="0.01"  name="tempat_dan_tanggal" >
          </div>
          <button type="submit" class="btn btn-primary">Cetak</button>
      </form>
    </div>
</section>
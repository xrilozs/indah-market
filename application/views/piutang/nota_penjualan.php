<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nota Penjualan
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-pencil"></i> Nota Penjualan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <form method="post" id="nota_penjualan_form" action="<?php echo base_url('backend/nota_process');?>">
    <div id="nota_penjualan_form_warning">
    </div>
    <div class="form-group">
      <label>No. Nota : </label>
      <input type="text" class="form-control" name="no_nota">
    </div>
        <div class="form-group">
      <label>Tanggal Nota : </label>
      <input type="text" class="form-control datepicker" name="tanggal_nota">
    </div>
    <div class="form-group">
      <label>Customer: </label><br>
              <select name="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer
          </option>';
          }
          ?>
          
        </select>
    </div>
   <div class="form-group">
        <label>Pajak Non Pajak : </label>
        <select name="pajak_non_pajak" class="form-control">
          <option value="">
            Choose..
          </option>
          <option value="p">
            Pajak
          </option>
          <option value="n">
            Non Pajak
          </option>
        </select>
    </div>
    <div class="form-group">
      <label>No. Faktur</label>
      <input type="text" id="faktur_no_search" name="faktur_no_search" class="form-control" data-url="<?php echo base_url('backend/faktur_search');?>" placeholder="No.Faktur" aria-label="No.Faktur" aria-describedby="basic-addon2">
      <label id="info_faktur"></label>
      <br>
      <div class="input-group-append">
        <button class="btn btn-outline-secondary btn-primary" type="button" id="faktur_no_search_btn">Tambah</button>
      </div>
      <input type="hidden" name="jumlah_no_faktur" id="jumlah_no_faktur">
      <input type="hidden" name="faktur" id="faktur">
      <br>
      <div id="faktur_list">
      </div>
    </div>
    <div class="form-group">
      <label>Keterangan Pembayaran : </label>
      <textarea rows="4" class="form-control" name="keterangan_pembayaran"></textarea>
    </div>
    <div class="form-group">
      <label>Tanggal Bayar : </label>
      <input type="text" class="form-control datepicker" name="tanggal_bayar">
    </div>
    <div class="form-group">
      <label>Jumlah Bayar: </label>
      <input type="number" class="form-control" name="jumlah_bayar" step="0.01">
    </div>
      <div class="form-group">
      <label>Cetak : </label>
      <select name="cetak" id="cetak" class="form-control">
          <option value="ya">Ya</option>
          <option value="tidak">Tidak</option>
          </select>
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success  btn-block" form-id="nota_penjualan_form"  warning_message="nota_penjualan_form_warning">Simpan </button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->
          

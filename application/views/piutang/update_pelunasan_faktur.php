<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Update Pelunasan Faktur
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-edit"></i> Update Pelunasan Faktur</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="pelunasan_hutang" class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Customer</th>
              <th>No.Faktur</th>
              <th>Tanggal Faktur</th>
              <th>Jumlah Tagihan</th>
              <th>No. Faktur Pajak</th>
              <th>PPN</th>
              <th>Status</th>
              <th>Keterangan Bayar</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $piutang = get_faktur_list('belum');
            if($piutang!=false){
              $no = 0;
              foreach($piutang->result() as $item){
                $no +=1;
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$item->customer_name.'</td>
                <td>'.$item->no_faktur.'</td>
                <td>'.$item->tanggal_faktur.'</td>
                <td>'.money($item->total).'</td>
                <td>'.$item->no_faktur_pajak.'</td>
                <td>'.money($item->ppn).'</td>
                <td>'.decode_enum($item->status).'</td>
                <td>'.($item->keterangan_pembayaran==''?'Belum ada':decode_enum($item->keterangan_pembayaran)).'</td>
                <td>'.($item->tanggal_bayar=='0000-00-00'?'Belum ada':$item->tanggal_bayar).'</td>
                <td>'.($item->jumlah_bayar=='0,00'?'Belum ada':money($item->jumlah_bayar)).'</td>
                <td><button url="'.base_url('home/edit_faktur/'.$item->id).'" class="btn btn-success edit-faktur" data-toggle="modal" data-target="#edit_fakturin_modal">Edit</button></td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
  <div id="edit_fakturin_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Faktur</h4>
        </div>
        <div class="modal-body">
          <form method="post" id="edit_fakturin_form" action="<?php echo base_url('backend/edit_faktur_pelunasan_process');?>">
            <div id="edit_fakturin_form_warning">
            </div>
           <input id="edit_id" type="hidden" name="id" class="form-control">
            <div class="form-group">
              <label>Status : </label>
              <select id="edit_status_lunas" name="status_lunas" class="form-control">
                <option value="">
                  Choose..
                </option>
                <option value="lunas">
                  LUNAS
                </option>
                <option value="belum">
                  BELUM LUNAS
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Jumlah Bayar : </label>
              <input id="edit_jumlah_bayar" type="number" name="jumlah_bayar" class="form-control">
            </div>
            <div class="form-group">
              <label>Tanggal Bayar : </label>
              <input id="edit_tanggal_bayar" type="text" name="tanggal_bayar" class="form-control datepicker">
            </div>
            <div class="form-group">
              <label>Keterangan Pembayaran : </label>
              <select id="edit_keterangan_pembayaran" name="keterangan_pembayaran" class="form-control">
                <option value="">
                  Choose..
                </option>
                <option value="belum">
                  BELUM
                </option>
                <option value="cek">
                  CEK
                </option>
                <option value="tunai">
                  TUNAI
                </option>
              </select>
            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success edit-ajax" form-id="edit_fakturin_form"  warning_message="edit_fakturin_form_warning">Save Changes</button>
        </div>
            </form>
      </div>
    </div>
  </div>
    </div>
</section>
<!-- /.content -->

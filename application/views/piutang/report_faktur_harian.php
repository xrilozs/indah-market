<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Faktur Perhari
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-list"></i> Laporan Faktur Perhari</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Date :</label>
          <input type="text" class="datepicker" name="date" id="date_select" url="<?php echo base_url('backend/report_faktur_daily');?>">
        </div>
      </div>
      <div class="col-md-12">
        <div class="table-responsive load_table">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Kepada YTH</th>
              <th>No.Faktur</th>
              <th>Tanggal Faktur</th>
              <th>Jumlah Tagihan</th>
              <th>No. Faktur Pajak</th>
              <th>PPN</th>
              <th>Status</th>
              <th>Keterangan Bayar</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php
              $date = date('Y-m-d');
            $piutang = get_faktur_list_by_date($date);
            if($piutang!=false){
              $no = 0;
              foreach($piutang->result() as $item){
                $no +=1;
                $edit= "<a href='".base_url('backend/edit_faktur/'.$item->id)."' class='btn btn-success'>Edit</a>";
                $print = "<a href='".base_url('backend/print_faktur/'.$item->id)."' class='btn btn-warning' target='_blank' >Print</a>";
                echo '
                <tr>
                <td>'.$no.'</td>
                <td>'.$item->customer_name.'</td>
                <td>'.$item->no_faktur.'</td>
                <td>'.$item->tanggal_faktur.'</td>
                <td>'.money($item->total).'</td>
                <td>'.$item->no_faktur_pajak.'</td>
                <td>'.money($item->ppn).'</td>
                <td>'.decode_enum($item->status).'</td>
                <td>'.($item->keterangan_pembayaran==''?'Belum ada':decode_enum($item->keterangan_pembayaran)).'</td>
                <td>'.($item->tanggal_bayar=='0000:00:00'?'Belum ada':$item->tanggal_bayar).'</td>
                <td>'.($item->jumlah_bayar=='00,0'?'Belum ada':money($item->jumlah_bayar)).'</td>
                <td>'.$edit.'</td>
                <td>'.$print.'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
</section>
<!-- /.content -->
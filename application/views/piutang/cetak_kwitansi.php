<html>
  <head>
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
    <style>
        td{
            letter-spacing: 1px;
        }
    </style>
  <body>
      <h1 style="text-align:center;text-decoration:underline">KWITANSI</h1><br>
      <table>
          <tr>
              <td style="padding-bottom:10px">Sudah Terima Dari<br></td>
              <td style="padding-bottom:10px">: </td>
              <td style="padding-bottom:10px;padding-left:20px;"> <?php echo $diterima_dari;?></td>
          </tr>
          <tr>
              <td style="padding-bottom:10px">Terbilang</td>
              <td style="padding-bottom:10px">: </td>
              <td style="padding-bottom:10px;padding-left:20px;"> <?php echo $banyaknya_uang;?></td>
          </tr>
          <tr>
              <td>Untuk Pembayaran</td>
              <td>:</td>
              <td style="padding-left:20px;"><?php echo $untuk_pembayaran_1;?></td>
          </tr>
          <tr>
              <td></td>
              <td></td>
              <td><?php echo $untuk_pembayaran_2;?></td>
          </tr>
      </table>
      <hr>
      <label>Jumlah : </label> Rp<?php echo money($jumlah_rp);?>
      <hr>
      <p><?php echo $tempat_dan_tanggal;?>
      </p>
  </body>
    <script>
  window.print();
</script>
</html>
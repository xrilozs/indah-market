<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Edit Nota Penjualan
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-edit"></i>Edit Nota Penjualan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
  <form method="post" id="nota_penjualan_form" action="<?php echo base_url('backend/edit_nota_process');?>">
    <div id="nota_penjualan_form_warning">
    </div>
    <div class="form-group">
        <input type="hidden" name="id_nota" id="id_nota" value="<?php echo $nota->id;?>">
      <label>No. Nota : </label>
      <input type="text" class="form-control" name="no_nota" value="<?php echo $nota->no_nota;?>">
    </div>
        <div class="form-group">
      <label>Tanggal Nota : </label>
      <input type="text" class="form-control datepicker" name="tanggal_nota"  value="<?php echo $nota->tanggal_nota;?>">
    </div>
    <div class="form-group">
      <label>Customer: </label><br>
              <select name="customer_id" id="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer
          </option>';
          }
          ?>
          
        </select>
        <script>
            $(document).ready(function(){
               $("#customer_id").val('<?php echo $nota->customer_id;?>').change(); 
            });
        </script>
    </div>
   <div class="form-group">
        <label>Pajak Non Pajak : </label>
        <select name="pajak_non_pajak" id="pajak_non_pajak" class="form-control">
          <option value="">
            Choose..
          </option>
          <option value="p">
            Pajak
          </option>
          <option value="n">
            Non Pajak
          </option>
        </select>
       <script>
            $(document).ready(function(){
               $("#pajak_non_pajak").val('<?php echo $nota->pajak_non_pajak;?>').change(); 
            });
        </script>
    </div>
    <div class="form-group">
      <label>No. Faktur</label>
      <input type="text" id="faktur_no_search" name="faktur_no_search" class="form-control" data-url="<?php echo base_url('backend/faktur_search');?>" placeholder="No.Faktur" aria-label="No.Faktur" aria-describedby="basic-addon2">
      <label id="info_faktur"></label>
      <br>
      <div class="input-group-append">
        <button class="btn btn-outline-secondary btn-primary" type="button" id="faktur_no_search_btn">Tambah</button>
      </div>
      <input type="hidden" name="faktur" id="faktur" value="<?php echo $nota->faktur_id;?>">
      <br>
      <div id="faktur_list">
          <?php 
          echo $faktur;
          ?>
      </div>
    </div>
    <div class="form-group">
      <label>Keterangan Pembayaran : </label>
      <textarea rows="4" class="form-control" name="keterangan_pembayaran" value="<?php echo $nota->keterangan_pembayaran;?>"> <?php echo $nota->keterangan_pembayaran;?></textarea>
    </div>
    <div class="form-group">
      <label>Tanggal Bayar : </label>
      <input type="text" class="form-control datepicker" name="tanggal_bayar" value="<?php echo $nota->tanggal_bayar;?>">
    </div>
    <div class="form-group">
      <label>Jumlah Bayar: </label>
      <input type="number" class="form-control" name="jumlah_bayar" step="0.01" value="<?php echo $nota->jumlah_bayar;?>">
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success  btn-block" form-id="nota_penjualan_form"  warning_message="nota_penjualan_form_warning">Simpan </button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->
          

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Pembelian Perbulan
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Laporan Pembelian Perbulan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Date :</label>
          <select class="" name="year" id="year_select" url="<?php echo base_url('backend/report_faktur_monthly');?>">
            <option value="">Pilih Tahun</option>
            <?php for($i=date('Y');$i>(date('Y')-5);$i--){
              echo '<option value="'.$i.'">'.$i.'</option>';
            } ?>
          </select>
          <select name="month" id="month_select" url="<?php echo base_url('backend/report_faktur_monthly');?>">
          <option value="">Pilih Bulan</option>
          <option value="1">Januari</option>
          <option value="2">Februari</option>
          <option value="3">Maret</option>
          <option value="4">April</option>
          <option value="5">Mei</option>
          <option value="6">Juni</option>
          <option value="7">Juli</option>
          <option value="8">Agustus</option>
          <option value="9">September</option>
          <option value="10">Oktober</option>
          <option value="11">November</option>
          <option value="12">Desember</option>
        </select>
        </div>
        <div class="form-group">
          <label>Customer :</label>
          <select class="" name="customer_id" id="customer_select" url="<?php echo base_url('backend/report_faktur_monthly');?>">
              <?php 
            $customer = get_all_customer();
            if($customer!=false){
              echo '<option value="">
              Pilih Customer..
            </option>';
              foreach($customer->result() as $cust){
                echo '<option value="'.$cust->id.'">
              '.$cust->name.'
            </option>';
              }
            } else  {
              echo '<option value="">
              Tidak ada pilihan Customer/Customer
            </option>';
            }
            ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <div class="table-responsive load_table">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>No. Faktur Pajak</th>
              <th>Customer</th>
              <th>PPN</th>
              <th>Status</th>
              <th>Keterangan Bayar</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
            </tr>
            </thead>
            <tbody>
            <?php
              $date = date('Y-m');
            $piutang = get_faktur_list_by_month($date);
            if($piutang!=false){
              $no = 0;
              foreach($piutang->result() as $item){
                $no +=1;
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$item->customer_name.'</td>
                <td>'.$item->no_faktur.'</td>
                <td>'.$item->tanggal_faktur.'</td>
                <td>'.money($item->jumlah_tagihan).'</td>
                <td>'.$item->no_faktur_pajak.'</td>
                <td>'.money($item->ppn).'</td>
                <td>'.decode_enum($item->status_lunas).'</td>
                <td>'.($item->status_lunas=='belum_lunas'?'Belum ada':decode_enum($item->keterangan_pembayaran)).'</td>
                <td>'.($item->status_lunas=='belum_lunas'?'Belum ada':$item->tanggal_bayar).'</td>
                <td>'.($item->status_lunas=='belum_lunas'?'Belum ada':money($item->jumlah_bayar)).'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
</section>
<!-- /.content -->
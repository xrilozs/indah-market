
<!DOCTYPE HTML>
<html>
  <head>
    <title>Data Customer</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        @media print {
            table { page-break-after:auto }
            tr    { page-break-inside:avoid; page-break-after:auto }
            td    { page-break-inside:avoid; page-break-after:auto }
            thead { display:table-header-group }
        }
        .table-condensed{
            font-size: 10px;
        }
        @page{size: auto;}
    </style>
  <body>
      <div class="container-fluid" style="margin-top:50px; margin-right:20px; margin-left:20px;">
        <div class="row">
            <div class="col-lg-12" style="margin-bottom:30px; text-align:center">
                <span class="company_name" style="font-weight:bold;">Data Customer</span>
            </div>
            <div class="col-lg-12" style="margin-bottom: 50px;">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Nomor Telepon</th>
                            <th>Contact Person</th>
                            <th>Alamat 1</th>
                            <th>Alamat 2</th>
                            <th>NIK</th>
                            <th>NPWP</th>
                            <th>Marketing</th>
                            <th>Rating</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($customers as $item){
                            echo "<tr>
                                <td style ='word-break:break-all;'>$item->name</td>
                                <td style ='word-break:break-all;'>$item->no_hp</td>
                                <td style ='word-break:break-all;'>$item->contact_person</td>
                                <td style ='word-break:break-all;'>$item->address_1</td>
                                <td style ='word-break:break-all;'>$item->address_2</td>
                                <td style ='word-break:break-all;'>$item->nik_ktp</td>
                                <td style ='word-break:break-all;'>$item->no_npwp</td>
                                <td style ='word-break:break-all;'>$item->mkt</td>
                                <td style ='word-break:break-all;'>$item->rating</td>
                            </tr>";
                        }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12" style="position: fixed; bottom: -15px; left: 0;">
                <p class="company_name" style="font-size:9px; padding-top:10px;">
                    <?=date('d-m-Y H:i');?>
                </p>
            </div>
        </div>
        
        
      </div>
  </body>
<script>
  window.print();
</script>
</html>

<!DOCTYPE HTML>
<html>
  <head>
    <title>Customer Export</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  </head>
    <style>
        .table-condensed{
            font-size: 10px;
        }
    </style>
  <body>
    <?php
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Customer Export.xls");
	?>
    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Nomor Telepon</th>
                <th>Contact Person</th>
                <th>Alamat 1</th>
                <th>Alamat 2</th>
                <th>NIK</th>
                <th>NPWP</th>
                <th>Marketing</th>
                <th>Rating</th>
            </tr>
        </thead>
        <tbody>
        <?php
            foreach($customers as $item){
                echo "<tr>
                    <td style ='word-break:break-all;'>$item->name</td>
                    <td style ='word-break:break-all;'>$item->no_hp</td>
                    <td style ='word-break:break-all;'>$item->contact_person</td>
                    <td style ='word-break:break-all;'>$item->address_1</td>
                    <td style ='word-break:break-all;'>$item->address_2</td>
                    <td style ='word-break:break-all;'>$item->nik_ktp</td>
                    <td style ='word-break:break-all;'>$item->no_npwp</td>
                    <td style ='word-break:break-all;'>$item->mkt</td>
                    <td style ='word-break:break-all;'>$item->rating</td>
                </tr>";
            }
        ?>
        </tbody>
    </table>
  </body>
</html>
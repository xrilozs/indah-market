          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Customer</th>
              <th>No.Faktur</th>
              <th>Tanggal Faktur</th>
              <th>Jumlah Tagihan</th>
              <th>No. Faktur Pajak</th>
              <th>PPN</th>
              <th>Status</th>
              <th>Keterangan Bayar</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $piutang = get_faktur_list_by_month($date, $customer_id);
            if($piutang!=false){
              $no = 0;
              foreach($piutang->result() as $item){
                $no +=1;
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$item->customer_name.'</td>
                <td>'.$item->no_faktur.'</td>
                <td>'.$item->tanggal_faktur.'</td>
                <td>'.money($item->total).'</td>
                <td>'.$item->no_faktur_pajak.'</td>
                <td>'.money($item->ppn).'</td>
                <td>'.decode_enum($item->status).'</td>
                <td>'.($item->keterangan_pembayaran==''?'Belum ada':decode_enum($item->keterangan_pembayaran)).'</td>
                <td>'.($item->tanggal_bayar=='0000:00:00'?'Belum ada':$item->tanggal_bayar).'</td>
                <td>'.($item->jumlah_bayar=='00,0'?'Belum ada':money($item->jumlah_bayar)).'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
<script>
$(document).ready(function(){
  $(".table").DataTable();
});
</script>
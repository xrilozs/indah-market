<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Nota Penjualan
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-list"></i> Nota Penjualan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
          <?php if(!empty($this->session->flashdata('msg'))) echo $this->session->flashdata('msg');?>
        <div class="form-group">
          <label>Date :</label>
          <input type="text" class="datepicker" name="date" id="date_select" url="<?php echo base_url('backend/report_nota_penjualan_daily');?>">
        </div>
      </div>
      <div class="col-md-12">
        <div class="table-responsive load_table">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>No.Nota</th>
              <th>Tanggal Nota</th>
              <th>Customer</th>
              <th>P/NP</th>
              <th>ID Faktur</th>
              <th>Grand Total</th>
              <th>Keterangan Pembayaran</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
              <th>Edit</th>
              <th>Print</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $date = date('Y-m-d');
            $nota_list = get_nota_penjualan_by_date($date);
            if($nota_list!=false){
              $no = 0;
              foreach($nota_list->result() as $nota){
                $no +=1;
                $edit= "<a href='".base_url('backend/edit_nota_penjualan/'.$nota->id)."' class='btn btn-success'>Edit</a>";
                $print = "<a href='".base_url('backend/print_nota_penjualan/'.$nota->id)."' class='btn btn-warning' target='_blank' >Print</a>";
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$nota->no_nota.'</td>
                <td>'.$nota->tanggal_nota.'</td>
                <td>'.$nota->customer_name.'</td>
                <td>'.$nota->pajak_non_pajak.'</td>
                <td>'.$nota->faktur_id.'</td>
                <td>'.money($nota->grand_total).'</td>
                <td>'.$nota->keterangan_pembayaran.'</td>
                <td>'.$nota->tanggal_bayar.'</td>
                <td>'.money($nota->jumlah_bayar).'</td>
                <td>'.$edit.'</td>
                <td>'.$print.'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
</section>
<!-- /.content -->
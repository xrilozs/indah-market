<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Detail Order
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-list"></i> Detail Order</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<ul class="nav nav-tabs">
<li class="active"><a data-toggle="tab" href="#list">List</a></li>
</ul>
<div class="tab-content">
  <div id="list" class="tab-pane fade in active">
    <h3>List</h3>
    <a href="<?php echo base_url('order_manager');?>" class="btn btn-primary">Kembali</a>
    <div class="row">
      <div class="col-md-12">
      <br>
      <table id="example2" class="table table-bordered table-hover">
        <thead>
        <tr>
          <th>No</th>
          <th>Nama Buku</th>
          <th>Harga Buku</th>
          <th>Qty</th>
          <th>Subtotal</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $order = get_order_det_list($order_id);
        if($order->num_rows()!=0){
          $no = 0;
          foreach($order->result() as $item){
            $no +=1;
            echo '<tr>
            <td>'.$no.'</td>
            <td>'.$item->name.'</td>
            <td>'.money($item->price).'</td>
            <td>'.$item->qty.'</td>
            <td>'.money($item->subtotal).'</td>
            </tr>
            ';
          }
        }
        ?>
        </tbody>
      </table>
    </div>
    </div>
  </div>
</div>
</section>
<!-- /.content -->

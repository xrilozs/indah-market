<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Hutang
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-pencil"></i> Hutang</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message'); ?>
  <form method="post" id="hutang_form" action="<?php echo base_url('backend/hutang_process');?>" >
    <div id="hutang_form_warning">
    </div>
    <div class="form-group">
        <label>Customer/Supplier : </label>
        <select name="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer/Customer
          </option>';
          }
          ?>
          
        </select>
      </div>
    <div class="form-group">
      <label>No. Tanda Terima : </label>
      <input type="text" name="tanda_terima" class="form-control">
    </div>
    <div class="form-group">
      <label>Tanggal Faktur : </label><br>
      <input type="text" name="tanggal_faktur" class="form-control datepicker">
    </div>
    <div class="form-group">
      <label>Tanggal Kembali : </label>
      <input type="text" name="tanggal_kembali" class="form-control datepicker">
    </div>
    <div class="form-group">
      <label>Barang : </label><br>
      <input type="button" class="btn btn-primary produk_add" value='Tambah'>
      <input type="hidden" id="jumlah_produk" name="jumlah_produk" value="1">
    <table class=" table-condensed">
    <thead>
      <tr>
        <th>Banyaknya</th>
        <th>No. Faktur</th>
        <th>Tipe Berat</th>
        <th>Nama Barang</th>
        <th>Harga</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody id="produk_add_table">
      <tr>
        <td><input type="text"  class="form-control" name="banyaknya_barang_1"></td>
        <td><input type="text" class="form-control" name="no_faktur_1"></td>
        <td><select name="tipe_berat_barang_1" class="form-control">
          <option value="">
            Choose..
          </option>
          <option value="kg">
            Kg
          </option>
          <option value="lb">
            Lb
          </option>
          <option value="rl">
            Rl
          </option>
        </select></td>
        <td><input type="text" class="form-control" name="nama_barang_1"></td>
        <td><input type="text" class="form-control" name="harga_barang_1" ></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
    </div>
    <div class="form-group">
      <label>No. Faktur Pajak : </label>
      <input type="text" name="no_faktur_pajak" class="form-control">
    </div>
    <div class="form-group">
      <label>PPN(%) : </label>
      <select name="ppn_type" class="form-control">
        <option value="">Choose..</option>
        <option value="0">0%</option>
        <option value="0.10">10%</option>
      </select>
    </div>
    <div class="form-group">
      <label>No. PO. : </label>
      <input type="text" name="no_po" class="form-control">
    </div>
      <div class="form-group">
      <label>Cetak : </label>
      <select name="cetak" id="cetak" class="form-control">
          <option value="ya">Ya</option>
          <option value="tidak">Tidak</option>
          </select>
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success btn-block" form-id="hutang_form"  warning_message="hutang_form_warning">Simpan</button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->

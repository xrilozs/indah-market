<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
   Edit Hutang
  </h1>
  <ol class="breadcrumb">
    <li><a href="active"><i class="fa fa-edit"></i>Edit Hutang</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
      <a class="btn btn-success" href="<?php echo base_url('report/pembelian_hutang_harian');?>"><span class="fa fa-arrow-left"></span> Kembali</a><br><br>
  <form method="post" id="hutang_form" action="<?php echo base_url('backend/edit_hutang_process');?>">
    <div id="hutang_form_warning">
    </div>
    <div class="form-group">
        <input type="hidden" name="fakturin_id" id="fakturin_id" value="<?php echo $fakturin->id;?>">
        <label>Customer/Supplier : </label>
        <select name="customer_id" id="customer_id" class="form-control">
          <?php 
          $customer = get_all_customer();
          if($customer!=false){
            echo '<option value="">
            Choose..
          </option>';
            foreach($customer->result() as $cust){
              echo '<option value="'.$cust->id.'">
            '.$cust->name.'
          </option>';
            }
          } else  {
            echo '<option value="">
            Tidak ada pilihan Customer/Customer
          </option>';
          }
          ?>
        </select>
        <script>
            $(document).ready(function(){
              $("#customer_id").val('<?php echo $fakturin->customer_id;?>').change();  
            });
        </script>
      </div>
    <div class="form-group">
      <label>No. Faktur : </label>
      <input type="text" name="no_faktur" class="form-control" value="<?php echo $fakturin->no_faktur;?>">
    </div>
    <div class="form-group">
      <label>Tanggal Faktur : </label><br>
      <input type="text" name="tanggal_faktur" class="form-control datepicker"  value="<?php echo $fakturin->tanggal_faktur;?>">
    </div>
    <div class="form-group">
      <label>Tanggal Kembali : </label>
      <input type="text" name="tanggal_kembali" class="form-control datepicker"  value="<?php echo $fakturin->tanggal_kembali;?>">
    </div>
    <div class="form-group">
      <label>Barang : </label><br>
      <input type="button" class="btn btn-primary produk_add_piutang" value='Tambah'>
        <?php 
        $nama_barang = explode(';',$fakturin->nama_barang);
        $jumlah_satuan = explode(';', $fakturin->jumlah_satuan);
        $harga_barang = explode(';', $fakturin->harga_barang);
        $tipe_berat = explode(';', $fakturin->tipe_berat);
        $no_spl = explode(';', $fakturin->no_spl);
        $jumlah_total = explode(';', $fakturin->jumlah_total);
        $jumlah_barang = count($nama_barang);
        ?>
      <input type="hidden" id="jumlah_produk" name="jumlah_produk" value="<?php echo $jumlah_barang;?>">
    <table class=" table-condensed">
    <thead>
      <tr>
        <th>Banyaknya</th>
        <th>Tipe Berat</th>
        <th>Nama Barang</th>
        <th>SPL</th>
        <th>Harga</th>
        <th>Jumlah</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody id="produk_add_table">
        <?php
        for($i=0;$i<$jumlah_barang;$i++){ ?>
        <tr id="row_<?php echo $i+1;?>">
        <td><input type="number" class="form-control" name="banyaknya_barang_<?php echo $i+1;?>" value="<?php  if(!empty($jumlah_satuan[$i])) echo $jumlah_satuan[$i] ;?>"></td>
        <td><select name="tipe_berat_barang_<?php echo $i+1;?>" class="form-control">
          <option value="">
            Choose..
          </option>
          <option value="kg" <?php echo ($tipe_berat[$i]=='kg'?'selected':''); ?>>
            Kg
          </option>
          <option value="lb" <?php echo ($tipe_berat[$i]=='lb'?'selected':''); ?>>
            Lb
          </option>
          <option value="rl" <?php echo ($tipe_berat[$i]=='rl'?'selected':''); ?>>
            Rl
          </option>
            <script>
            $("input[name='tipe_berat_barang_<?php echo $i+1;?>']").val('<?php  if(!empty($tipe_berat[$i])) echo $tipe_berat[$i] ;?>').change();
            </script>
        </select></td>
        <td><input type="text" class="form-control" name="nama_barang_<?php echo $i+1;?>"  value="<?php  if(!empty($nama_barang[$i])) echo $nama_barang[$i] ;?>"></td>
        <td><input type="text" class="form-control" name="no_spl_barang_<?php echo $i+1;?>" value="<?php  if(!empty($no_spl[$i])) echo $no_spl[$i] ;?>"></td>
        <td><input type="number" class="form-control" step="0.01"  name="harga_barang_<?php echo $i+1;?>" value="<?php  if(!empty($harga_barang[$i])) echo $harga_barang[$i] ;?>"></td>
        <td></td>
        <td><?php if($i!=0) {?>
            <input type="button" class="delete_produk btn-danger btn" value="Hapus" onclick="$('#row_<?php echo $i+1;?>').remove();$('#jumlah_produk').val('<?php echo $i-1;?>')" id_row="row_<?php echo $i+1;?>">
            <?php } ?> </td>
      </tr>
        <?php
        }
        ?>
    </tbody>
  </table>
    </div>
    <div class="form-group">
      <label>No. Faktur Pajak : </label>
      <input type="text" name="no_faktur_pajak" class="form-control"  value="<?php echo $fakturin->no_faktur_pajak;?>">
    </div>
    <div class="form-group">
      <label>PPN(%) : </label>
      <select name="ppn_type" id="ppn_type" class="form-control">
        <option value="">Choose..</option>
        <option value="0">0%</option>
        <option value="0.10">10%</option>
      </select>
        <script>
            $(document).ready(function(){
              $("#ppn_type").val('<?php echo ($fakturin->dpp_pajak?'0':'0.10');?>').change();  
            });
        </script>
    </div>
    <div class="form-group">
      <label>No. PO. : </label>
      <input type="text" name="no_po" class="form-control"  value="<?php echo $fakturin->no_po;?>">
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-success edit-ajax btn-block" form-id="hutang_form"  warning_message="hutang_form_warning">Simpan</button>
    </div>
    </form>
</div>
</section>
<!-- /.content -->

          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Username</th>
              <th>Customer</th>
              <th>No. Tanda Terima</th>
              <th>Tanggal Faktur</th>
              <th>Tanggal Kembali</th>
              <th>Nama Barang</th>
              <th>No. Faktur</th>
              <th>Harga Barang</th>
              <th>Jumlah Tagihan</th>
              <th>No. Faktur Pajak</th>
              <th>Dpp Pajak</th>
              <th>No. PO</th>
              <th>Status</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $hutang = get_fakturin_list_by_month($date, $customer_id);
            if($hutang!=false){
              $no = 0;
              foreach($hutang->result() as $item){
                $no +=1;
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$item->username.'</td>
                <td>'.$item->customer_name.'</td>
                <td>'.$item->tanda_terima.'</td>
                <td>'.$item->tanggal_faktur.'</td>
                <td>'.$item->tanggal_kembali.'</td>
                <td>'.$item->nama_barang.'</td>
                <td>'.$item->no_faktur.'</td>
                <td>'.$item->harga_barang.'</td>
                <td>'.money($item->jumlah_tagihan).'</td>
                <td>'.$item->no_faktur_pajak.'</td>
                <td>'.money($item->dpp_pajak).'</td>
                <td>'.$item->no_po.'</td>
                <td>'.$item->status.'</td>
                <td>'.($item->status=='BELUM'?'Belum ada':$item->tanggal_bayar).'</td>
                <td>'.($item->status=='BELUM'?'Belum ada':money($item->jumlah_bayar)).'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
<script>
$(document).ready(function(){
  $(".table").DataTable();
});
</script>
<html>
  <head>
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <label class="pull-right"><?php echo date('d M Y');?></label><br>
      <label>TANDA TERIMA </label> <label style="padding-left:16%;">  <?php echo $tanda_terima;?></label>
    <br>
      <p><label>TANGGAL : <?php echo $new_date = date('d-m-Y', strtotime($tanggal_faktur));?></label> <label style="padding-left:10%"> DARI IP</label> <label class="pull-right">KEPADA : <?php echo $customer_name;?></label></p>
    <table class="table">
        <thead>
        <tr>
            <td>
                No
            </td>
            <td>
                No.Faktur
            </td>
            <td>
                Nama Barang
            </td>
            <td>
                Qty
            </td>
            <td>
                Harga Satuan
            </td>
            <td>
                Total
            </td>
            </tr>
        </thead>
        <tbody>
        <?php 
            for($i=0;$i<$jumlah_produk;$i++){ ?>
             <tr>
                 <td><?php echo $i+1;?></td>
                 <td><?php echo $no_faktur[$i];?></td>
                 <td><?php echo $nama_barang[$i];?></td>
                 <td><?php echo money($jumlah_satuan[$i]);?></td>
                 <td><?php echo money($harga_barang[$i]);?></td>
                 <td><?php echo money($jumlah_harga[$i]);?></td>
            </tr>   
        <?php    }
            ?>
        </tbody>
      </table>
    <label>HORMAT KAMI</label><label style="padding-left:16%;">YANG TERIMA PEMBAYARAN</label>
    <label class="pull-right">Sub Total:<?php echo money($subtotal);?></label><br>
      <p><label class="pull-right">PPN (<?php echo $persen_pajak;?>%) Rp <?php echo money($ppn);?></label></p><br>
  <p><label class="pull-right">TOTAL Rp<?php echo money($total);?></label></p>
<!--
  <p style="text-align:center">
    *Cek/bg a/n: <?php //echo ($ppn=='0'?'PT. INDAH KARUNIA SUKSES':'SUHARJANTO G');?> <br>
    <?php //echo date('d/m/Y H:i:s');?>
  </p>
-->
  </body>
<script>
  window.print();
</script>
</html>
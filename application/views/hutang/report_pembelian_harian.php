<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Pembelian Perhari
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-dollar"></i> Laporan Pembelian Perhari</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row">
      <div class="col-md-12">
          <?php if(!empty($this->session->flashdata('message'))) echo $this->session->flashdata('message');?>
            <form method="post" action="<?php echo base_url('backend/download_pembelian_harian');?>" target="_blank">
        <div class="form-group">
          <label>Date :</label>
          <input type="text" class="datepicker" name="date" id="date_select" url="<?php echo base_url('backend/report_fakturin_daily');?>">
        </div>
          <div class="form-group">
          <label>Customer :</label>
          <select class="" name="customer_id" id="customer_select" url="<?php echo base_url('backend/report_fakturin_daily');?>" name="customer_id">
              <?php 
            $customer = get_all_customer();
            if($customer!=false){
              echo '<option value="">
              Pilih Customer..
            </option>';
              foreach($customer->result() as $cust){
                echo '<option value="'.$cust->id.'">
              '.$cust->name.'
            </option>';
              }
            } else  {
              echo '<option value="">
              Tidak ada pilihan Customer/Customer
            </option>';
            }
            ?>
          </select>
        </div>
                <button type="submit" class="btn btn-primary"><span class="fa fa-download"></span> Download</button>
              <button type="button" class="btn btn-primary email_laporan" data-toggle="modal" data-target="#emailmodal"><span class="fa fa-envelope"></span> Email</button>
          </form>
          <br>
      </div>
      <div class="col-md-12">
        <div class="table-responsive load_table">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <th>No</th>
              <th>Username</th>
              <th>Customer</th>
              <th>No. Tanda Terima</th>
              <th>Tanggal Faktur</th>
              <th>Tanggal Kembali</th>
              <th>Jumlah Tagihan</th>
              <th>No. Faktur Pajak</th>
              <th>Dpp Pajak</th>
              <th>No. PO</th>
              <th>Status</th>
              <th>Tanggal Bayar</th>
              <th>Jumlah Bayar</th>
              <th>Edit</th>
              <th>Print</th>
            </tr>
            </thead>
            <tbody>
            <?php
              $date = date('Y-m-d');
            $hutang = get_fakturin_by_date($date);
            if($hutang!=false){
              $no = 0;
              foreach($hutang->result() as $item){
                $no +=1;
                $edit= "<a href='".base_url('backend/edit_fakturin/'.$item->id)."' class='btn btn-success'>Edit</a>";
                $print = "<a href='".base_url('backend/print_fakturin/'.$item->id)."' class='btn btn-warning' target='_blank' >Print</a>";
                echo '<tr>
                <td>'.$no.'</td>
                <td>'.$item->username.'</td>
                <td>'.$item->customer_name.'</td>
                <td>'.$item->tanda_terima.'</td>
                <td>'.$item->tanggal_faktur.'</td>
                <td>'.$item->tanggal_kembali.'</td>
                <td>'.money($item->jumlah_tagihan).'</td>
                <td>'.$item->no_faktur_pajak.'</td>
                <td>'.money($item->dpp_pajak).'</td>
                <td>'.$item->no_po.'</td>
                <td>'.$item->status.'</td>
                <td>'.($item->status=='BELUM'?'Belum ada':$item->tanggal_bayar).'</td>
                <td>'.($item->status=='BELUM'?'Belum ada':money($item->jumlah_bayar)).'</td>
                <td>'.$edit.'</td>
                <td>'.$print.'</td>
                </tr>
                ';
              }
            }
            ?>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>
</section>
<!-- /.content -->
<div class="modal fade" id="emailmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <form method="post" action="<?php echo base_url('backend/send_email_pembelian_harian');?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Kirim Email</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <input type="hidden" name="date" id="date">
          <input type="hidden" name="customer_id" id="customer_id">
        <div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" id="email" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
      </form>
  </div>
</div>
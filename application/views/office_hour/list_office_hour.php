<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Jam Kerja
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text"></i> Manajemen Jam Kerja</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <div class="row" style="margin-bottom:10px;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create-office-hour-modal">
          <i class="fa fa-plus" aria-hidden="true"></i> Buat Jam Kerja
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="office_hour_table" class="table2 table-bordered table-hover">
            <thead>
              <tr>
                <th>Shift</th>
                <th>Jam Masuk</th>
                <th>Jam Pulang</th>
                <th>Jam Masuk Hari Sabtu</th>
                <th>Jam Pulang Hari Sabtu</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="create-office-hour-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Buat Jam Kerja</h4>
        </div>
        <div class="modal-body">
          <form id="create-office-hour-form">
            <div class="row">
              <div id="create-office-hour-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Shift:</label>
                  <select name="shift" class="form-control" required>
                    <option value="SIANG">Siang</option>
                    <option value="MALAM">Malam</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Masuk:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" name="start_hour">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Pulang:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" name="end_hour">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Masuk Hari Sabtu:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" name="start_hour_saturday">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Pulang Hari Sabtu:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" name="end_hour_saturday">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success" id="create-office-hour-button">OK</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="update-office-hour-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ubah Jam Kerja</h4>
        </div>
        <div class="modal-body">
          <form id="update-office-hour-form">
            <div class="row">
              <div id="update-office-hour-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Shift:</label>
                  <select name="shift" id="shift" class="form-control" required>
                    <option value="SIANG">Siang</option>
                    <option value="MALAM">Malam</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Masuk:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" id="start_hour" name="start_hour">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Pulang:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" id="end_hour" name="end_hour">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Masuk Hari Sabtu:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" id="start_hour_saturday" name="start_hour_saturday">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-6 col-md-6">
                <div class="form-group">
                  <label>Jam Pulang Hari Sabtu:</label>
                  <div class="input-group">
                    <input type="text" class="form-control timepicker" id="end_hour_saturday" name="end_hour_saturday">
                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-success" id="update-office-hour-button">OK</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="active-office-hour-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Aktikan Jam Kerja</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="active-office-hour-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk mengaktifkan Office Hour ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-info" id="active-office-hour-button">IYA</button>
        </div>
      </div>
    </div>
  </div>

  <div id="inactive-office-hour-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nonaktifkan Jam Kerja</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="inactive-office-hour-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menonaktifkan Office Hour ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-danger" id="inactive-office-hour-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
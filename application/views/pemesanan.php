<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pemesanan
    <small>Pemesanan Buku, Kitab dan Prewed</small>
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-book"></i> Pemesanan</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12">
    <?php
    if($this->session->flashdata('message')=='success') {
      echo '
      <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Sukses!</strong> Pemesanan Berhasil!.
      </div>
      ';
    }
    ?>
  </div>
  <div class="col-md-12">
    <?php
    if($this->session->flashdata('message')=='error') {
      echo '
      <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> Terjadi kesalahan saat memesan.
      </div>
      ';
    }
    ?>
  </div>
  <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#buku">Buku</a></li>
  <li><a data-toggle="tab" href="#kitab">Kitab</a></li>
  <li><a data-toggle="tab" href="#prewed">Prewed</a></li>
  <li><a data-toggle="tab" href="#others">Lainnya</a></li>
</ul>

<div class="tab-content">
  <div id="buku" class="tab-pane fade in active">
    <h3>Buku</h3>
    <div class="row">
      <?php
      $buku = get_book_list();
      if($buku->num_rows()!=0){
        foreach($buku->result() as $item){
          echo '<div class="col-md-3 col-sm-12">
          <div class="panel panel-default">
          <div class="panel-heading">          <b>'.$item->name.'</b></div>
          <div class="panel-body centered-content">
          <p><img src="'.ASSETS.$item->pict.'" class="book-pict"></p>
          <label>Harga '.money($item->price).'</label>
          <hr>
          <button class="btn btn-primary btn-block add-cart" book-id="'.$item->id.'" url="'.base_url('backend/addcart').'"><span class="fa fa-shopping-cart"></span> Beli</button>
          </div>
          </div>
          </div>';
        }
      } else echo 'Tidak ada pilihan saat ini';
      ?>
    </div>
  </div>
  <div id="kitab" class="tab-pane fade">
    <h3>Kitab</h3>
    <div class="row">
      <?php
      $buku = get_kitab_list();
      if($buku->num_rows()!=0){
        foreach($buku->result() as $item){
          echo '<div class="col-md-3 col-sm-12">
          <div class="panel panel-default">
          <div class="panel-heading">          <b>'.$item->name.'</b></div>
          <div class="panel-body centered-content">
          <p><img src="'.ASSETS.$item->pict.'" class="book-pict"></p>
          <label>Harga '.money($item->price).'</label>
          <hr>
          <button class="btn btn-primary btn-block add-cart" book-id="'.$item->id.'" url="'.base_url('backend/addcart').'"><span class="fa fa-shopping-cart"></span> Beli</button>
          </div>
          </div>
          </div>';
        }
      } else echo 'Tidak ada pilihan saat ini';
      ?>
    </div>
  </div>
  <div id="prewed" class="tab-pane fade">
    <h3>Prewed</h3>
    <?php
    $prewed = get_prewed_list();
    if($prewed->num_rows()!=0){
      foreach($prewed->result() as $item){
        echo '<div class="col-md-3 col-sm-12">
        <div class="panel panel-default">
        <div class="panel-heading">          <b>'.$item->name.'</b></div>
        <div class="panel-body centered-content">
        <p><img src="'.ASSETS.$item->pict.'" class="book-pict"></p>
        <label>Harga '.money($item->price).'</label>
        </div>
        </div>
        </div>';
      }
    } else echo 'Tidak ada pilihan saat ini';
    ?>
    <div class="col-md-12 col-sm-12">
      <form method="post" action="<?php echo base_url('backend/prewed_process');?>">
      <div class="form-group">
        <label>Form Pemesanan</label>
        <textarea class="form-control" name="description" id="description" rows="10" placeholder="Tulis Gaya mana yang diinginkan, Contoh : Gaya 1, Gaya 2" required="required"></textarea><br>
        <button class="btn btn-primary btn-block" type="submit">Pesan</button>
      </div>
    </form>
    </div>
  </div>
  <div id="others" class="tab-pane fade">
    <h3>Lainnya</h3>
    <?php
    $others = get_others_list();
    if($others->num_rows()!=0){
      foreach($others->result() as $item){
        echo '<div class="col-md-3 col-sm-12">
        <div class="panel panel-default">
        <div class="panel-heading">          <b>'.$item->name.'</b></div>
        <div class="panel-body centered-content">
        <p><img src="'.ASSETS.$item->pict.'" class="book-pict"></p>
        <label>Harga '.money($item->price).'</label>
        </div>
        </div>
        </div>';
      }
    } else echo 'Tidak ada pilihan saat ini';
    ?>
    <div class="col-md-12 col-sm-12">
      <form method="post" action="<?php echo base_url('backend/others_process');?>">
      <div class="form-group">
        <label>Form Pemesanan</label>
        <textarea class="form-control" name="description" id="description" rows="10" placeholder="Tulis Produk mana yang diinginkan, Contoh : Baju Hitam (2), Topi Merah (1)" required="required"></textarea><br>
        <button class="btn btn-primary btn-block" type="submit">Pesan</button>
      </div>
    </form>
    </div>
  </div>
</div>
</section>
<!-- /.content -->

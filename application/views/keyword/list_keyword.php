<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Keyword Management
  </h1>
  <ol class="breadcrumb">
    <li class="active"><a href="#"><i class="fa fa-file-text"></i> Keyword Management</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div id="list" class="tab-pane fade in active">
    <!-- <div class="row" style="margin-bottom:10px;">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create-keyword-modal">
          <i class="fa fa-plus" aria-hidden="true"></i> Create Keyword
        </button>
      </div>
    </div> -->
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="keyword_table" class="table2 table-bordered table-hover">
            <thead>
              <tr>
                <th>Module</th>
                <th>Deskripsi</th>
                <th>Status</th>
                <th>Tanggal</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
    </div>
    </div>
  </div>

  <div id="create-keyword-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create Keyword</h4>
        </div>
        <div class="modal-body">
          <form id="create-keyword-form">
            <div class="row">
              <div id="create-keyword-info" class="col-lg-12">
                <div class="alert alert-warning">
                  <strong>Perhatian!</strong> Silahkan hubungi developer terlebih dahulu untuk pembuatan keyword baru.
                </div>
              </div>
              <div class="col-lg-12" id="create-keyword-warning"></div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Module:</label>
                  <select name="module" id="module" class="form-control" required>
                    <option value="PEGAWAI">Pegawai</option>
                    <option value="KASBON">Kasbon</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Deskripsi: </label>
                  <textarea class="form-control" name="description" placeholder="Deskripsi.." required></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keyword: </label>
                  <input type="text" name="keyword" class="form-control" placeholder="Keyword.." required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="create-keyword-button">Create Keyword</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="update-keyword-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-md">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Keyword</h4>
        </div>
        <div class="modal-body">
          <form id="update-keyword-form">
            <div class="row">
              <div id="update-keyword-warning">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Module:</label>
                  <select name="module" class="form-control" readonly>
                    <option value="PEGAWAI">Pegawai</option>
                    <option value="KASBON">Kasbon</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Deskripsi: </label>
                  <textarea class="form-control" name="description" placeholder="Deskripsi.." required></textarea>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="form-group">
                  <label>Keyword baru: </label>
                  <input type="text" name="keyword" class="form-control" placeholder="Keyword.." required>
                </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="update-keyword-button">Update Keyword</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="active-keyword-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Active Keyword</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="active-keyword-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk mengaktifkan Keyword ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-info" id="active-keyword-button">IYA</button>
        </div>
      </div>
    </div>
  </div>

  <div id="inactive-keyword-modal" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Inactive Keyword</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="inactive-keyword-warning">
            </div>
          </div>
          <p>Apakah Anda yakin untuk menonaktifkan Keyword ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">TIDAK</button>
          <button type="button" class="btn btn-danger" id="inactive-keyword-button">IYA</button>
        </div>
      </div>
    </div>
  </div>
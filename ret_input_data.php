<?php
    $tgl = date('Y-m-d');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Input Returan</title>

    <link rel="shortcut icon" href="favicon.png"/>
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
</head>

<body>

<div class="container">
    <div class="navbar navbar-inner">
         <div class="brand">Input Returan</div>
    </div>

    <h3><a href="index.php" class="btn btn-warning"><i class="icon-arrow-left"></i></a> Input Data Returan</h3>

    <form action="proses/ret_proses_input.php" method="post">
        <table class="table table-hover" width="100%">

            <tr>
                <td>Tanggal</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <input type="text" name="tgl" class="input-xlarge" value="<?php echo $tgl?>"/>
                    </div>

                </td>
            </tr>

            <tr>
                <td>ID Cust</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <select name="idcust">
                            <?php
                            include "koneksi.php";
                            $data_customer = mysql_query("select * from customer ");
                            while($row = mysql_fetch_array($data_customer)){
                                echo "
                                    <option value='$row[cust_id]'>$row[cust_nama]</option>
                                     ";
                                }
                            ?>
                        </select>
                    </div></td>
            </tr>

            <tr>
                <td>Jenis </td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <textarea name="jenis" placeholder="PP OPP" rows="5" class="input-xlarge"></textarea>
                    </div>

                </td>
            </tr>

            <tr>
                <td>Ukuran</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="ukuran" placeholder="ukuran" class="input-xlarge">
                    </div>
                </td>
            </tr>

            <tr>
                <td>Kg</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="kg" placeholder="kg" class="input-xlarge">
                    </div>
                </td>
            </tr>

            <tr>
                <td>Lbr</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="lbr" placeholder="lbr" class="input-xlarge">
                    </div>
                </td>
            </tr>

            <tr>
                <td>Ket.Retur</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="ketretur" placeholder="ket.retur" class="input-xlarge">
                    </div>
                </td>
            </tr>

            <tr>
                <td>Ket.Tindakan</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="kettindakan" placeholder="ket.tindakan" class="input-xlarge">
                    </div>
                </td>
            </tr>




            <tr>
                <td></td>
                <td>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" name="submit" class="btn btn-danger">Reset</button>
                </td>
            </tr>

        </table>
    </form>
</div>
</body>
</html>
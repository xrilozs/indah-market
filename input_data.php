<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Laporan Produksi CRUD</title>

    <link rel="shortcut icon" href="favicon.png"/>
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
</head>

<body>

<div class="container">
    <div class="navbar navbar-inner">
         <div class="brand">Lap. Produksi</div>
    </div>


    <h3><a href="index.php" class="btn btn-warning"><i class="icon-arrow-left"></i></a> Input Data</h3>

    <form action="proses/proses_input.php" method="post">
        <table class="table table-hover" width="100%">

            <tr>
                <td>TGL</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <input type="text" name="tgl" placeholder="YYYY-MM-DD" class="input-xlarge" />
                    </div>

                </td>
            </tr>

            <tr>
                <td>NoMesin</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <input type="text" name="nom" placeholder="NO.MESIN" class="input-xlarge" />
                    </div></td>
            </tr>

            <tr>
                <td>Ukuran</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pencil"></i></span>
                        <textarea name="uk" placeholder="UKURAN" rows="5" class="input-xlarge"></textarea>
                    </div>

                </td>
            </tr>

            <tr>
                <td>KG</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="kg" placeholder="KG" class="input-xlarge">
                    </div>
                </td>
            </tr>
            <tr>
                <td>Lembar</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="lb" placeholder="LEMBAR" class="input-xlarge">
                    </div>
                </td>
            </tr>

            <tr>
                <td>Keterangan</td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input type="text" name="ket" placeholder="KET" class="input-xlarge">
                    </div>
                </td>
            </tr>




            <tr>
                <td></td>
                <td>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" name="submit" class="btn btn-danger">Reset</button>
                </td>
            </tr>

        </table>
    </form>
</div>
</body>
</html>
<?php

// Panggil Library FPDF
include 'plugins/fpdf17/fpdf.php';
include '../koneksi.php';

// Buat class PDF
class PDF extends FPDF
{
    // Buat fungsi untuk mengatur header halaman
    function Header()
    {
        // Setting Font ('string family', 'string style', 'font size')
        $this->SetFont('Helvetica','B',14);

        /* Cell : untuk menuliskan text kedalam cell
                  19  : menunjukan panjang Cell
                  0.7 : menunjukan tinggi cell
                  0   : yang pertama menunjukan Cell tanpa border, Isi 1 jika menginginkan Cell diberi border
                  0   : yang kedua menunjukan posisi text berikutnya disebelah kanan
                  C   : menunjukan text berada ditengah-tengah / Center atau Left atau Right
        */
        $this->Cell(19,0.7,'Laporan Produksi',0,0,'L');

        /*$this->Ln();
         $this->SetFont('courier','',10);
         $this->Cell(19,0.7,'Tanggal : '.date('d-m-Y'),0,0,'L');
         // Line : untuk membuat garis
         $this->Line(1,2.9,20,2.9);
         */
    }

    // Buat fungsi untuk mengatur Footer halaman
    function Footer()
    {
        // Posisi 1 cm dari bawah
        $this->SetY(-1);
        $this->SetFont('Arial','',9);
        $this->Cell(19,  0.3 ,date('d-m-Y'),0,'LR','R');
    }
}

// Fungsi Konstruktor PDF
$pdf = new PDF("P","cm", array(29.7, 21));

// Fungsi untuk membuat halaman
$pdf->AddPage();

// Fungsi untuk setting margin halaman (kiri, atas, kanan)
$pdf->SetMargins(1,0.5,1);

$pdf->Ln(1);  //line pertama
$pdf->SetFont('courier','',10);
$pdf->Line(1,2,20,2); // buat garis header

$pdf->Cell(1  ,0.7, 'NO'     ,0, 0, 'C');
$pdf->Cell(3 ,0.7, 'TGL' ,0, 0, 'C');
$pdf->Cell(3 ,0.7, 'NoMesin'  ,0, 0, 'C');
$pdf->Cell(3  ,0.7, 'UKURAN'  ,0, 0, 'C');
$pdf->Cell(1  ,0.7, 'KG'  ,0, 0, 'C');
$pdf->Cell(1  ,0.7, 'LB'  ,0, 0, 'C');
$pdf->Cell(2  ,0.7, 'KET.'  ,0, 0, 'C');

// Line : untuk membuat garis
$pdf->Line(1,2.7,20,2.7); // buat garis header
//$pdf->Line(1,2.95,20,2.95);

$no    = 1;
$total = 0;

$query= "select * from produksi";
$hasil = mysql_query($query);

while($data = mysql_fetch_array($hasil)){

    $pdf->Ln();
    $pdf->SetFont('courier','',10);
    $pdf->Cell(1 ,0.7, $no,0, 'LR', 'C');
    $pdf->Cell(3 ,0.7, $data['prod_tgl'] ,0, 'LR', 'C');
    $pdf->Cell(3 ,0.7, $data['prod_nom'] ,0, 'LR', 'C');
    $pdf->Cell(3 ,0.7, $data['prod_ukuran'] ,0, 'LR', 'C');
    $pdf->Cell(1 ,0.7, $data['prod_kg'] ,0, 'LR', 'C');
    $pdf->Cell(1 ,0.7, $data['prod_lb'] ,0, 'LR', 'C');
    $pdf->Cell(2 ,0.7, $data['prod_ket'] ,0, 'LR', 'C');

    $no++;
    $total +=$data['prod_id'];
}

$pdf->Output("Laporan Produksi.pdf", "I");
?>
<?php
include "../koneksi.php";
include "plugins/excel/PHPExcel.php";

$objPHPExcel = new PHPExcel();
$query = "select * from produksi ";
$hasil = mysql_query($query);

// Set properties
$objPHPExcel->getProperties()->setCreator("har")
    ->setLastModifiedBy("har")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Laporan Prod.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Laporan Prod.");

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'No')
    ->setCellValue('B1', 'TGL')
    ->setCellValue('C1', 'NoMesin')
    ->setCellValue('D1', 'UKURAN')
    ->setCellValue('E1', 'KG')
    ->setCellValue('F1', 'LB')
    ->setCellValue('G1', 'KET.');

$baris = 2;
$no = 0;
while($row=mysql_fetch_array($hasil)){

    $no = $no +1;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A$baris", $no)
        ->setCellValue("B$baris", $row['prod_tgl'])
        ->setCellValue("C$baris", $row['prod_nom'])
        ->setCellValue("D$baris", $row['prod_ukuran'])
        ->setCellValue("E$baris", $row['prod_kg'])
        ->setCellValue("f$baris", $row['prod_lb'])
        ->setCellValue("g$baris", $row['prod_ket']);
    $baris = $baris + 1;
}

// Nama sheet excel
$objPHPExcel->getActiveSheet()->setTitle('Data1');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');

// Untuk nama file
header('Content-Disposition: attachment;filename="Laporan Data.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>

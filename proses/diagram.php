<html>
<?php
include "koneksi.php";

$data_pria = mysql_query("select * from produksi where prod_kg <> '0'");
$pria = mysql_num_rows($data_pria);

$data_wanita = mysql_query("select * from produksi where prod_lb <> '0'");
$wanita = mysql_num_rows($data_wanita);

?>
<head>
    <title>Statistik Data</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>
    <script src="js/highcharts.js"></script>

    <script type="text/javascript">
        $(function () {
            var chart;
            $(document).ready(function() {
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'container',
                        type: 'bar',
                        marginRight: 130, // lebar ke kanan
                        marginBottom: 25 // tinggi ke bawah
                    },
                    title: {
                        text: 'Statistik Data'
                    },
                    subtitle: {
                        text: 'matawebsite.com'
                    },
                    xAxis: {
                        categories: ['KG', 'LBR']
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>Statistik Data</b><br/>'+
                            this.x +': '+ this.y ;
                        }
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        borderWidth: 0
                    },
                    series: [{
                        name: '/ Hari Produksi',
                        data: [<?php echo $pria ?>, <?php echo $wanita?>]
                    }]
                });
            });
        });
    </script>
</head>
<body>

<div class="container">
    <div class="navbar navbar-inner">
        <div class="brand">Mataweb Media Teknologi</div>
    </div>

    <div id="container" ></div>
</div>
</html>
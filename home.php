<a href="input_data.php" class="btn btn-success">
    <i class="icon-plus"></i> Tambah Data
</a>

<div class="input-append pull-right">
    <form action="index.php" method="get">
        <input type="text" name="search" placeholder="search" class="input-large" >

        <input type="hidden" name="get" value="home" >

        <button type="submit" class="btn">
            <i class="icon-search"></i>
        </button>
    </form>
</div>



<table class="table table-hover" cellpadding="0" cellspacing="0">
    <thead>
    <tr align="center">
        <th>no</th>
        <th>tgl</th>
        <th>nom</th>
        <th>ukuran</th>
        <th>kg</th>
        <th>lb</th>
        <th>ket</th>
    </tr>
    </thead>

    <tbody>
    <?php
    include "koneksi.php";

    if(isset($_GET["search"])) {
        $search = $_GET['search'];
        $no=0;
        $limit = 3;
        if (!isset($_GET['page'])){
            $pages=1;
        }else{
            $pages=$_GET['page'];
        }
        $start = ($pages-1)*$limit;

        $query	= "select * from produksi where prod_tgl LIKE '%$search%' or prod_nom LIKE '%$search%' or prod_ukuran LIKE '%$search%' order by prod_id DESC limit $start, $limit";
        $sql	= mysql_query($query);
    }
    else
    {
        $no=0;
        $limit = 3;
        if (!isset($_GET['page'])){
            $pages=1;
        }else{
            $pages=$_GET['page'];
        }
        $start = ($pages-1)*$limit;

        $query	= "select * from produksi order by prod_id DESC limit $start, $limit";
        $sql	= mysql_query($query);
    }


    while($r = mysql_fetch_array($sql)) {
        $no++;
        ?>

        <tr>
            <td><?php echo $no?></td>
            <td><?php echo $r['prod_tgl']?></td>
            <td><?php echo $r['prod_nom']?></td>
            <td><?php echo $r['prod_ukuran']?></td>
            <td><?php echo $r['prod_kg']?></td>
            <td><?php echo $r['prod_lb']?></td>
            <td><?php echo $r['prod_ket']?></td>

            <td>
                <a href="index.php?get=edit_data&id=<?php echo $r['prod_id']?>" class="btn btn-info"><i class="icon-edit"></i> Edit</a>
                <a href="proses/proses_delete.php?id=<?php echo $r['prod_id']?>" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin ?')"><i class="icon-trash"></i> Delete</a>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>

<div class="pull-left">
    <a href="cetak/cetak_data.php" target="_blank" class="btn btn-warning"><i class="icon-download-alt"></i> Excell</a>
    <a href="cetak/cetak_data_pdf.php" target="_blank" class="btn btn-primary"><i class="icon-download-alt"></i> PDF</a>

</div>
<!--pagination-->
<?php

if (!isset($_GET['$search'])) {

    $query_page = "select * from produksi";
    $sql_page = mysql_query($query_page);
    $count = mysql_num_rows($sql_page);
    $jml_halaman = ceil($count / $limit);
    ?>

    <div class="pagination pagination-right">
        <ul>
            <?php
            if($pages<=1){
                echo "<li class='disabled'><a> << </a></li>";
            }else
            {
                echo "<li><a href='index.php?get=home&page=1'> << </a></li>";
            }

            for($i=1;$i<=$jml_halaman;$i++){
                if ($pages != $i) {
                    echo "<li><a href='index.php?get=home&page=$i'>$i</a></li>";
                }
                else{
                    echo "<li class='disabled'><a>$i</a></li>";
                }
            }

            if($pages >= $jml_halaman){
                echo "<li class='disabled'><a> >> </a></li>";
            }else
            {
                echo "<li><a href='index.php?get=home&page=$jml_halaman'> >> </a></li>";
            }
            ?>
        </ul>
    </div>

<?php
}else{

    $search = $_GET['search'];

    $query_page = "select * from produksi where prod_tgl LIKE '%$search%' or prod_nom LIKE '%$search%' or prod_ukuran LIKE '%$search%' ";
    $sql_page = mysql_query($query_page);
    $count = mysql_num_rows($sql_page);
    $jml_halaman = ceil($count / $limit);
    ?>

    <div class="pagination pagination-right">
        <ul>
            <?php
            if($pages<=1){
                echo "
            <li class='disabled'><a> << </a></li>
            ";
            }else
            {
                echo "
            <li><a href='index.php?get=home&page=1&search=$search'> << </a></li>
            ";
            }

            for($i=1;$i<=$jml_halaman;$i++){
                if ($pages != $i) {
                    echo "
            <li><a href='index.php?get=home&page=$i&search=$search'>$i</a></li>
            ";
                }
                else{
                    echo "
            <li class='disabled'><a>$i</a></li>
            ";
                }
            }

            if($pages >= $jml_halaman){
                echo "
            <li class='disabled'><a> >> </a></li>
            ";
            }else
            {
                echo "
            <li><a href='index.php?get=home&page=$jlh_halaman&search=&search'> >> </a></li>
            ";
            }
            ?>
        </ul>
    </div>
<?php
}
?>
